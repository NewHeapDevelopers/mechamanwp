<?php
/**
 * Template Name: Nieuwsbrieftotaalform
 *
 * A custom page template with sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>


<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <meta name="format-detection" content="telephone=no,address=no,email=no,date=no,url=no">
    <title></title>

    <link href="https://fonts.googleapis.com/css?family=PT+Sans:700&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Vollkorn:400,700&amp;display=swap" rel="stylesheet">

    
    <style>
    body {margin:0; background:#fafafa;}
    body, input, select {font-family:'Vollkorn', arial, sans-serif; font-size:16px;}

    
    h2 {margin:10px 0 0;}
    .containerposities, .containeraantallen {border-bottom: 1px solid #ddd;}
    .containerposities > div, .containeraantallen > div { margin:10px 0; padding:10px 0;}
    
    .header {position:relative;}
    

    .formulierveldencontainer {padding:20px;}

    label {font-weight:500; margin-right:5px; width:150px; display:inline-block;}
    input, select {border:1px solid #ddd; padding:2px;}
    input[type=submit] {margin-top:30px; padding:10px 15px; font-weight:500; border:1px solid #aaa; border-radius:5px; box-shadow:0 2px 4px #ccc; transition:0.3s; background:#000; color:#fff;}

    input[type=submit]:hover {box-shadow:0 2px 12px #888; background:#444; cursor:pointer}
    
    </style>

</head>

<body>

<div style="background:#fff; width: 100%;max-width: 700px;margin: 20px auto 20px; padding: 0; border: 1px solid #ddd; border-radius: 10px;">

<div class="header" style="display:flex; justify-content: space-between; background-color:#000; padding: 10px;text-align: left;">
    <img src="https://www.mechaman.nl/tuin-en-park-techniek/wp-content/themes/tuinenparktechniek/assets/images/TPT_logo_2017_RGB.png" style="display:block; filter: drop-shadow(0 0px 2px rgba(0,0,0,0.8));width: 350px; height:30px;">
    <h1 style="z-index:20; margin:20px 0 0; margin-bottom: 10px;  float: right;color: #fff;text-shadow: 0 2px 4px rgba(0,0,0,0.5);width: 300px;line-height: 25px;background: ; font-size:40px; line-height:36px; font-family:'PT Sans', arial, sans-serif;">Nieuwsbrief Generator</h1>
</div>


<div class="formulierveldencontainer">

<form action="https://www.mechaman.nl/tuin-en-park-techniek/nieuwsbrief-totaal/" method="GET">

<div class="containerposities">
    <h2>Berichten per positie</h2>
    <div>
        <label>Uitgelicht: </label>
        <select id="berichtuitgelicht" name="berichtuitgelicht">
            <?php
            global $post;
            $args = array( 'numberposts' => 10, 'offset'=> 0);
            $myposts = get_posts( $args );
            foreach( $myposts as $post ) :	setup_postdata($post); 

            $ids[] = get_the_ID();

            ?>
            
            <option value="<?php echo get_the_ID(); ?>"><?php get_the_ID(); the_title(); ?></option>

            <?php endforeach; 
                wp_reset_postdata(); 
            ?>
        </select>
    </div>
    <div>
        <label>Bericht 1: </label>
        <select id="bericht1" name="bericht1">
            <?php
            global $post;
            $args = array( 'numberposts' => 10, 'offset'=> 0);
            $myposts = get_posts( $args );
            foreach( $myposts as $post ) :	setup_postdata($post); 

            $ids[] = get_the_ID();

            ?>
            
            <option value="<?php echo get_the_ID(); ?>"><?php get_the_ID(); the_title(); ?></option>

            <?php endforeach; 
                wp_reset_postdata(); 
            ?>
        </select>
    </div>
    <div>
        <label>Bericht 2: </label>
        <select id="bericht2" name="bericht2">
            <?php
            global $post;
            $args = array( 'numberposts' => 10, 'offset'=> 0);
            $myposts = get_posts( $args );
            foreach( $myposts as $post ) :	setup_postdata($post); 

            $ids[] = get_the_ID();

            ?>
            
            <option value="<?php echo get_the_ID(); ?>"><?php get_the_ID(); the_title(); ?></option>

            <?php endforeach; 
                wp_reset_postdata(); 
            ?>
        </select>
    </div>


    <div>
        <label>Uitgesloten 1: </label>
        <select id="uitgeslotenbericht1" name="uitgeslotenbericht1">
            <option value="0">Geen</option>
            <?php
            global $post;
            $args = array( 'numberposts' => 15, 'offset'=> 0);
            $myposts = get_posts( $args );
            foreach( $myposts as $post ) :	setup_postdata($post); 

            $ids[] = get_the_ID();

            ?>
            
            <option value="<?php echo get_the_ID(); ?>"><?php get_the_ID(); the_title(); ?></option>

            <?php endforeach; 
                wp_reset_postdata(); 
            ?>
        </select>
    </div>

    

    <div>
    <label>Partnerbericht 1: </label>
        <select id="partnerbericht1" name="partnerbericht1">
            <option value="0">Geen</option>
            <?php
            global $post;
            $args = array( 'numberposts' => 10, 'offset'=> 0, 'post_type' => 'partner');
            $myposts = get_posts( $args );
            foreach( $myposts as $post ) :	setup_postdata($post); 

            $ids[] = get_the_ID();

            ?>
            
            <option value="<?php echo get_the_ID(); ?>"><?php get_the_ID(); the_title(); ?></option>

            <?php endforeach; 
                wp_reset_postdata(); 
            ?>
        </select>
    </div>

    <div>
    <label>Partnerbericht 2: </label>
        <select id="partnerbericht2" name="partnerbericht2">
            <option value="0">Geen</option>
            <?php
            global $post;
            $args = array( 'numberposts' => 10, 'offset'=> 0, 'post_type' => 'partner');
            $myposts = get_posts( $args );
            foreach( $myposts as $post ) :	setup_postdata($post); 

            $ids[] = get_the_ID();

            ?>
            
            <option value="<?php echo get_the_ID(); ?>"><?php get_the_ID(); the_title(); ?></option>

            <?php endforeach; 
                wp_reset_postdata(); 
            ?>
        </select>
    </div>


</div>

<div class="containeraantallen">
    <h2>Aantallen</h2>
    <div>
    <label>Aantal rijen: </label>
    <select id="aantalrijen" name="aantalrijen">
        
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
    
    </select>
    </div>

    <div>
    <label>Advertorialblok: </label>
 
    <select id="advertorials" name="advertorials">
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
    </select>
    </div>

</div>
</div>

<div style="max-width: 700px;text-align: center; margin-bottom:50px;"><input type="submit" value="Nieuwsbrief genereren"></div>



        </form>
        </div>


</div>
    </body>
</html>