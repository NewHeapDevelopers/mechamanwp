<section class="postloop">

    <div class="container">
        <h3 class="section-title">
            <?php _e('Nu in het vakblad', 'newheap'); ?>
        </h3>
        <div class="nh-tile-holder row">
            <?php
            wp_reset_query();

            $vakblad = get_field('in_het_vakblad');

            if (!empty( $vakblad )) {
                foreach($vakblad as $blad) {
                    $item = $blad['vakblad_item'];
                    $itemID = url_to_postid($item);

                    $input = get_the_excerpt($itemID);

                    $str = $input;

                    if( strlen( $input) > 100 ) {
                        $str = explode( "\n", wordwrap( $input, 100));
                        $str = $str[0] . '...';
                    }
                    ?>
                    <div class="col-sm-12 col-md-3">
                        <a href="<?php echo get_the_permalink($itemID) ?>" class="post-item">
                            <div class="bg-image-holder">
                                <div class="bg-image-div" style="padding-top:60%;background-image: url(<?php echo get_the_post_thumbnail_url($itemID, 'large') ?>);"></div>
                                <img src="<?php echo get_the_post_thumbnail_url($itemID, 'medium') ?>" width="100%" height="auto" style="display: none;">
                                <?php if (!empty(get_field('premium_article', $itemID))) { ?>
                                    <div class="premium-label">
                                        <i class="far fa-gem"></i> Premium
                                    </div>
                                <?php } ?>
                            </div>

                            <div class="grey-border-bottom-right text-black p-3">
                                <h2 class=""><?php echo get_the_title($itemID); ?></h2>
                                <p class="mb-0">
	                                <?php echo $str; ?>
                                </p>
                            </div>
                        </a>
                    </div>
                <?php }
            } ?>
        </div>
    </div>
</section>
