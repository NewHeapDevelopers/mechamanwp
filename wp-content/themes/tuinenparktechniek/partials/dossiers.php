<?php
$dossiers = get_terms([
    'taxonomy' => 'dossier',
    'hide_empty' => true,
    'number' => 5,
    'meta_query' => array(
        'relation' => 'OR',
        array(
            'key'     => 'do_not_show',
            'value'   => '0',
            'compare' => '=',
        ),
        array(
            'key' => 'do_not_show',
            'compare' => 'NOT EXISTS'
        ),
    )
]);

if (!empty($dossiers)) : ?>
    <section class="dossiers my-4">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-sm-12 col-md-8">
                            <h3 class="section-title">
                                <?php _e('Dossiers', 'newheap'); ?>
                            </h3>
                        </div>
                    </div>
                    <div class="row">

	                    <?php
	                    $counter = 0;

	                    foreach ($dossiers as $key => $dossier) : $counter++; ?>
		                    <?php if ($counter === 1) : ?>
                                <?php
                                $dossier_image = get_field('term_image', $dossier);

                                if (!empty($dossier_image)) {
                                    $dossier_image = get_field('term_image', $dossier)['sizes']['medium_large'];
                                } else {
                                    $dossier_image = get_field('placeholder_afbeelding', 'option')['sizes']['medium_large'];
                                }
                                ?>
                                <div class="col-sm-12 col-md-4">
                                    <a href="<?php echo get_term_link($dossier); ?>">
                                        <div class="image-media" style="background-image: url('<?php echo $dossier_image; ?>');"></div>
                                        <div class="content-title text-white absolute text-black w-75 d-flex bg-white border-bottom-right-cut"
                                             style="bottom: 20%;">
                                            <div class="dossier-info">
                                                <h2 class="bold px-2 pt-2  mb-0 ">
                                                    <?php _e('Dossier', 'newheap') ?>: <?php echo $dossier->name ?>
                                                </h2>
                                                <p class="px-2 pb-2 mb-0"><?php _e('Nieuwste bericht op:', 'newheap'); ?>
                                                    <?php echo \NewHeap\Theme\Helpers::get_taxonomy_latest_date($dossier); ?></p>
                                            </div>

                                            <div class="primary-background p-2 text-center d-flex border-bottom-right-cut w-100">
                                                <h5 class="mb-0 w-100">
                                            <span class="primary-background">
                                                <i class="fas fa-folder-open"></i>
                                            </span>
                                                </h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php else : ?>
			                    <?php if ($counter === 2) : ?>
                                    <div class="col-sm-12 col-md-4">
                                <?php endif; ?>

                                <div class="bg-gray p-3 mb-1">
                                    <a class="block-link" href="<?php echo get_term_link($dossier); ?>">
                                        <h5 class="bold text-black">
                                            <i class="fas fa-folder-open"></i>
                                            <?php echo $dossier->name ?>
                                        </h5>
                                        <p class="sub-title bold">
                                            <?php _e('Nieuwste bericht op:', 'newheap'); ?>
                                            <?php echo \NewHeap\Theme\Helpers::get_taxonomy_latest_date($dossier); ?>
                                        </p>
                                    </a>
                                </div>

			                    <?php if ($counter === count($dossiers)) : ?>
                                    <a href="<?=site_url()?>/dossiers/" class="see-more my-2 w-100 text-black bold py-2 text-right pr-2"><?php _e('Alle dossiers', 'newheap'); ?>
                                        <div class="arrow-right pr-2 ml-20"></div>
                                    </a>

                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>

	                    <?php if(!empty(get_field('advertisement_archive', 'option'))) : ?>
                            <div class="col-sm-12 col-md-4 google-ad-holder ad-between-posts-holder text-center">
                                <div class="pt-5 pb-5 mb-3">
                                    <div class="ad-title"><?php _e('Advertentie', 'newheap'); ?></div>
                                    <div class="ad-holder">
                                        <?=get_field('advertisement_archive', 'option')?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif;
wp_reset_query();
