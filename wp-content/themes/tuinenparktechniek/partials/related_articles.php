<?php
$counter = 0;

$args = array(
    'post_type' => 'post',
    'posts_per_page' => 4,
    'tax_query' => [
        'relation' => 'OR'
    ],
);

$categories = get_the_terms(get_the_ID(), 'category');
$dossiers = get_the_terms(get_the_ID(), 'dossier');

if (!empty($categories)) {
    foreach ($categories as $category) {
	    $args['tax_query'][] = [
		    'taxonomy' => 'category',
		    'field' => 'term_id',
		    'terms' => $category->term_id,
	    ];
    }
}

if (!empty($dossiers)) {
    foreach ($dossiers as $dossier) {
	    $args['tax_query'][] = [
		    'taxonomy' => 'dossier',
		    'field' => 'term_id',
		    'terms' => $dossier->term_id,
	    ];
    }
}
$query = new \WP_Query($args);
?>

<section class="recent-items mt-4">

    <div class="container-fluid">
        <h3 class="section-title">
            <?php _e('Gerelateerde artikelen','newheap') ?>
        </h3>
        <?php if ($query->have_posts()) : ?>
            <div class="nh-tile-holder">
                <?php while ($query->have_posts()) :
                    $counter++;
                    $query->the_post();

                    $input = get_the_excerpt();

                    $str = $input;

                    if( strlen( $input) > 150) {
                        $str = explode( "\n", wordwrap( $input, 150));
                        $str = $str[0] . '...';
                    }

                    ?>
                    <div class="nh-tile <?php echo ($counter > 4) ? 'd-none' : ''; ?>">
                        <a href="<?php echo get_the_permalink() ?>">
                            <div class="tile-image-holder relative" class="post-item">
                                <div class="tile-image" style="background-image: url('<?php echo get_the_post_thumbnail_url(null, 'large') ?>');"></div>

                                <?php if (!empty(get_field('premium_article'))) { ?>
                                    <div class="content-type text-white absolute">
                                        <span class="primary-background px-4 pl-3 py-2"><i
                                                    class="far fa-gem"></i> <?php _e('Premium','newheap') ?></span>
                                    </div>
                                <?php } ?>
                            </div>


                            <div class="tile-content-holder">
                                <div class="grey-border-bottom-right text-black p-3">
                                    <h2>
                                        <?php echo get_the_title() ?>
                                    </h2>

                                    <span class="bold">
                                        <?php echo get_the_date('d-m-Y'); ?>
                                        |
                                        <i class="far fa-clock"></i>
                                        <?php echo \NewHeap\Theme\Helpers::get_read_time(get_the_ID()); ?>
                                    </span>

                                    <p class="mt-2 mb-0">
                                        <?php echo $str; ?>
                                        <strong>
                                            <?php _e('Lees verder','newheap')?>
                                        </strong>
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</section>
