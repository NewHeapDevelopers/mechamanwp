<section class="recent-items my-4">
    <div class="container">
        <h3 class="section-title">
            <?php _e('Recent','newheap') ?>
        </h3>
        <?php
        $counter = 0;
        $sticky_post = get_option( 'sticky_posts' );

        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 8,
            'ignore_sticky_posts' => 1,
            'post__not_in' => [$sticky_post, $header->ID],
            'tax_query' => [
	            [
		            'taxonomy' => 'artikel_type',
		            'field' => 'slug',
		            'operator' => 'NOT IN',
		            'terms' => 'columns',
	            ]
            ]
        );

        $query = new \WP_Query($args);

        if ($query->have_posts()) : ?>

            <div class="nh-tile-holder">
                <?php while ($query->have_posts()) :
                    $counter++;
                    $query->the_post();

                    $input = get_the_excerpt();

                    $str = $input;

                    if( strlen( $input) > 150) {
                        $str = explode( "\n", wordwrap( $input, 150));
                        $str = $str[0] . '...';
                    }

                    ?>
                    <div class="nh-tile <?php echo ($counter > 4) ? 'd-none' : ''; ?>">
                        <a class="block-link" href="<?php echo get_the_permalink() ?>">
                            <div class="tile-image-holder">
                                <div class="tile-image" style="background-image: url('<?php echo get_the_post_thumbnail_url(null, 'medium') ?>');"></div>

                                <?php if (!empty(get_field('premium_article'))) { ?>
                                    <div class="content-type text-white absolute">
                                        <span class="primary-background px-4 pl-3 py-2"><i
                                                    class="far fa-gem"></i> <?php _e('Premium','newheap') ?></span>
                                    </div>
                                <?php } ?>
                            </div>


                            <div class="tile-content-holder">
                                <h2>
                                    <?php echo get_the_title() ?>
                                </h2>

                                <span>
                                    <?php echo get_the_date('d-m-Y'); ?>
                                    |
                                    <i class="far fa-clock"></i>
                                    <?php echo \NewHeap\Theme\Helpers::get_read_time(get_the_ID()); ?>
                                </span>

                                <p class="mt-2">
                                    <?php echo $str; ?>
                                    <strong>
                                        <?php _e('Lees verder','newheap')?>
                                    </strong>
                                </p>
                            </div>
                        </a>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="load-more load-more-btn text-center text-black bold w-100 mt-4 loadmore">
                    <?php _e('Meer recent','newheap'); ?>
                    <br/>
                    <div class="arrow-bottom"></div>
                </div>

                <div class="load-more go-to-archive-btn text-center text-black bold w-100 mt-4 loadmore">
                    <a href="<?php echo get_field('archive_page', 'option'); ?>">
                        <?php _e('Alle berichten','newheap'); ?>
                        <div class="arrow-right"></div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <style>
        .eq-row-fix {
            flex: 1;
            height: 100%;
        }
        .text-tend-bottom {
            display: flex;
            flex-direction: column;
            justify-content: flex-end;
        }
    </style>
</section>
