<section class="tests my-4">

    <div class="container">
        <h3 class="section-title">
            <?php _e('Tests', 'newheap'); ?>
        </h3>
        <div class="row">
	        <?php
	        $args = [
		        'post_type' => 'post',
		        'posts_per_page' => 2,
		        'tax_query' => [
			        [
				        'taxonomy' => 'artikel_type',
				        'field'    => 'slug',
				        'terms'    => 'tests',
			        ],
		        ],
	        ];

	        $query = new WP_Query($args);

	        if ($query->have_posts()) : ?>
	            <?php while ($query->have_posts()) : $query->the_post(); ?>
                    <div class="col-sm-12 col-md-6 mb-3">
                        <a href="<?php echo get_the_permalink(); ?>" class="post-item">
                            <div class="relative">
                                <div class="image-holder" style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large') ?>');"></div>

                                <?php if (!empty(get_field('premium_article'))) : ?>
                                    <div class="content-type text-black absolute">
                                        <span class="primary-background px-4 py-2">
                                            <i class="far fa-gem"></i>
                                            <?php _e('Premium', 'newheap'); ?>
                                        </span>
                                    </div>
                                <?php endif; ?>

                                <div class="content-title bg-black-trans text-black grey-border-bottom-right border-bottom-right-cut-large p-3">
                                    <h5><?php echo get_the_title() ?></h5>
                                    <p><?php echo get_the_excerpt() ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
	        <?php wp_reset_query(); ?>

            <div class="container">
                <div class="row">
                    <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
                        <a href="<?php echo get_field('tests_archive_page', 'option') ?>">
                            <?php _e('Alle tests', 'newheap') ?>
                            <div class="arrow-right pr-2 ml-20"></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
