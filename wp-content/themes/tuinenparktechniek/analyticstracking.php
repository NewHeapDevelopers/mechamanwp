<?php 
if(is_single()) {
		
    global $post;		
    
    $site_twitter_username = "@redactievt";
    $creator_twitter_username = "@redactievt";
    $twitter_facebook_url = get_permalink();
    $twitter_facebook_title = get_the_title();
      
    $twitter_facebook_description = substr(strip_tags($post->post_content), 0, 150);
    
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
  
    $twitter_facebook_thumbs = $image[0];
  
    echo '<meta name="twitter:card" content="summary_large_image" />'; // summary  of  summary_large_image
    echo '<meta name="twitter:site" content="@redactievt" />';
    echo '<meta name="twitter:url" content="'.$twitter_facebook_url.'" />';
    echo '<meta name="twitter:title" content="'.$twitter_facebook_title.'" />';
    echo '<meta name="twitter:description" content="'.$twitter_facebook_description.'..." />';
    echo '<meta name="twitter:image" content="' . $twitter_facebook_thumbs . '" />';
    
    echo '<meta property="og:url" content="'.$twitter_facebook_url.'" />';
    echo '<meta property="og:type" content="article" />';
    echo '<meta property="og:title" content="'.$twitter_facebook_title.'" />';
    echo '<meta property="og:description" content="'.$twitter_facebook_description.'..." />';
    echo '<meta property="og:image" content="'.$twitter_facebook_thumbs.'" />';
  }
?>

<script type="text/javascript">
    var _ss = _ss || [];
    _ss.push(['_setDomain', 'https://koi-3QNJXQNLAC.marketingautomation.services/net']);
    _ss.push(['_setAccount', 'KOI-45AAEDEXXK']);
    _ss.push(['_trackPageView']);
(function() {
    var ss = document.createElement('script');
    ss.type = 'text/javascript'; ss.async = true;
    ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-3QNJXQNLAC.marketingautomation.services/client/ss.js?ver=2.2.1';
    var scr = document.getElementsByTagName('script')[0];
    scr.parentNode.insertBefore(ss, scr);
})();
</script>
<?php
if ( is_user_logged_in() ) { // Gebruiker is ingelogd
	$gaIngelogd = "1"; 
  $user_id = get_current_user_id();
} else { // Gebruiker is NIET ingelogd
	$gaIngelogd = "0";
}


$premium_article = get_field('premium_article'); // i.v.m. GA dimension6 

?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-843861-24', 'www.mechaman.nl');
<?php if ($gaIngelogd == "1") { ?>
ga('set', 'dimension1', '<?php echo $gaIngelogd; // per Hit ?>');
<?php	}
  if ($user_id) {	?>
ga('set', 'dimension4', '<?php echo $user_id; // per Gebruiker ?>');
<?php } 
if ($premium_article == 1) { ?>
ga('set', 'dimension6', '1');
<?php } // per Hit ?>
ga('send', 'pageview');
</script>