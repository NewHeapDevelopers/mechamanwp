<?php
/*
Template Name: Front page
*/

get_header();

?>
<!--TODO: temp-->
<?php include 'partials/_main-slider.php'; ?>
<!--TODO: end temp-->

<?php include 'partials/header.php'; ?>

<?php include 'partials/recent_items.php'; ?>

<?php include 'partials/partners.php'; ?>

<?=get_field('advertisement_leaderboard', 'option')?>

<?php include 'partials/multimedia.php'; ?>

<?php include 'partials/dossiers.php'; ?>

<?php include 'partials/tests.php'; ?>

<?php include 'partials/categories.php'; ?>

<?php include 'partials/read_most.php'; ?>

<?php include 'partials/loop.php'; ?>

<?php //echo get_the_content(); ?>

<?php

get_footer();

?>


