<?php
namespace NewHeap\Child;

// Prevent direct access
if (!defined('ABSPATH')) {
    exit;
}

class NewHeapChild
{
    public function __construct()
    {
        // Init hooks
        $this->init_hooks();
    }

    public function init_hooks()
    {
        add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts_styles'], 20);
    }

    public function enqueue_scripts_styles()
    {
        // Enqueue style & script
	    wp_enqueue_style( 'vt-child-style', get_stylesheet_directory_uri() . '/assets/css/styles.css', ['newheap'], 1);
//        wp_enqueue_script('lm-child-script', get_stylesheet_directory_uri() . '/assets/js/scripts.js', ['jquery'], false, true);
    }
}

new NewHeapChild();

