<?php
/**
 * Template Name: Nieuwsbrieftotaaltool
 *
 * A custom page template with sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */





/*

- HTML-selectbox voor 'uitgelicht', 'bericht1', 'bericht2', 'bericht3', 'partnerbericht', 'uitgeslotenbericht', 'advertorial', 'promogeel'. 

- HTML-selectbox vullen met meest recente berichten en value = bericht-id

- url-parameters bevatten nummers (bericht-ID's en true of false)

- get-waarde uit url ophalen en in een variabele plaatsen

- Per contentblok get-waarde gebruiken in query

Voorbeeld:

$bericht1 = htmlspecialchars($_POST["bericht1"]);


Rij met voorkeurberichten:

$args = array( 'numberposts' => 2, 'offset'=> 0, 'post__in' => array( 2, 5 ));

$gebruikteposts = Gebruikte post-id's opslaan in variabele.


Volgende rij:

$args = array( 'numberposts' => 2, 'post__not_in' => array( $gebruikteposts ));


*/


$berichtuitgelicht = htmlspecialchars($_GET["berichtuitgelicht"]);

$bericht1 = htmlspecialchars($_GET["bericht1"]);

$bericht2 = htmlspecialchars($_GET["bericht2"]);

$aantalrijen = htmlspecialchars($_GET["aantalrijen"]);

$partnerbericht1 = htmlspecialchars($_GET["partnerbericht1"]);

$partnerbericht2 = htmlspecialchars($_GET["partnerbericht2"]);

$advertorials = htmlspecialchars($_GET["advertorials"]);

$uitgeslotenbericht1 = htmlspecialchars($_GET["uitgeslotenbericht1"]);

$uitgeslotenberichten = $uitgeslotenbericht1;

$gebruikteberichten = array($berichtuitgelicht,$bericht1,$bericht2, $uitgeslotenberichten);

// $gebruikteberichten = array($berichtuitgelicht,$bericht1,$bericht2);

/*
echo $voorkeurberichten[0];
echo "<br>";
echo $voorkeurberichten[1];
echo "<br>";
echo $voorkeurberichten[2];
*/


function autoblank($text) {
	$return = str_replace('<a', '<a target="_blank"', $text);
	return $return;
}
add_filter('the_excerpt', 'autoblank');


remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );


function content($limit) {
$content = explode(' ', get_the_excerpt(), $limit);
if (count($content)>=$limit) {
   array_pop($content);
    $content = implode(" ",$content).'...';
  } else {
    $content = implode(" ",$content);
  }	
  $content = preg_replace('/\[.+\]/','', $content);
  $content = apply_filters('the_content', $content); 
  $content = str_replace(']]>', ']]&gt;', $content);
 return $content;
}

// Replaces the excerpt "more" text by a link
function new_excerpt_more($more) {
       global $post;
	return '<a class="moretag" href="'. get_permalink($post->ID) . '"> Read the full article...</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');



?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <meta name="format-detection" content="telephone=no,address=no,email=no,date=no,url=no">
    <title></title>

    <!-- Web Font / @font-face : BEGIN -->
    
    <!--[if mso]>
        <style>
            * {
                font-family: 'Roboto', sans-serif !important;
            }
        </style>
    <![endif]-->
   
    <!--[if !mso]><!-->
    <!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
    <!--<![endif]-->

    <!-- Web Font / @font-face : END -->

    <style>

        /* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
            margin: 0 !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }

		h1 {margin: 0 0 10px; font-size: 25px; line-height: 30px; color: #222222; font-weight: bold;}
		h2 {margin-top:0; margin-bottom:10px; font-size:23px; line-height:27px; color: #222222; font-weight: bold;}
		
		a {color:#000; font-weight:bold;}
		
		.plattetekst {color:#222222; font-weight:normal;}
		
        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What it does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        /* What it does: Replaces default bold style. */
        th {
        	font-weight: normal;
        }

        /* What it does: Fixes webkit padding issue. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }

        /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
        a {
            text-decoration: none;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }

        /* What it does: A work-around for email clients meddling in triggered links. */
        a[x-apple-data-detectors],  /* iOS */
        .unstyle-auto-detected-links a,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* What it does: Prevents Gmail from changing the text color in conversation threads. */
        .im {
            color: inherit !important;
        }

        /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
        .a6S {
           display: none !important;
           opacity: 0.01 !important;
		}
		/* If the above doesn't work, add a .g-img class to any image in question. */
		img.g-img + div {
		   display: none !important;
		}

        /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
        /* Create one of these media queries for each additional viewport size you'd like to fix */

        /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */
        @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
            u ~ div .email-container {
                min-width: 320px !important;
            }
        }
        /* iPhone 6, 6S, 7, 8, and X */
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
            u ~ div .email-container {
                min-width: 375px !important;
            }
        }
        /* iPhone 6+, 7+, and 8+ */
        @media only screen and (min-device-width: 414px) {
            u ~ div .email-container {
                min-width: 414px !important;
            }
        }

    </style>

    <!-- What it does: Makes background images in 72ppi Outlook render at correct size. -->
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->

    <style>

        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
	    .button-td-primary:hover,
	    .button-a-primary:hover {
	        background: #555555 !important;
	        border-color: #555555 !important;
	    }

        /* Media Queries */
        @media screen and (max-width: 600px) {

            .email-container {
                width: 100% !important;
                margin: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }

            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: left !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }

            /* What it does: Adjust typography on small screens to improve readability */
            .email-container p {
                font-size: 17px !important;
            }
        }

    </style>
</head>

<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fafafa;">





	<center style="width: 100%; background-color: #f5f5f5;">
    <!--[if mso | IE]>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #fafafa;">
    <tr>
    <td>
    <![endif]-->


        <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: 'Roboto', sans-serif; ">
            Nieuws van Veehouderij Techniek.
        </div>

        <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: 'Roboto', sans-serif; ">
            ‌............... ‌............... ‌............... ‌............... ‌............... ‌............... ‌............... ‌............... ‌............... ‌............... ‌............... ‌............... ‌............... ‌............... ‌............... ‌............... ‌............... ‌............... 
        </div>

        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" style="margin: auto;" class="email-container">
	        
		
            <tr>
                <td style="padding:0; text-align: center">
                    <img src="https://img.ymlp.com/vtupdate_VeehouderijTechnieklogogrijs.gif" width="600" height="125" alt="afbeelding" border="0" style="width: 100%; max-width: 600px; height: auto; background: #dddddd; font-family: Georgia, serif; font-size: 17px; line-height: 17px; color: #222222; margin: auto; display: block;">
                </td>
            </tr>
			<?php
	        // <tr>
	        //     <td style="background-color: #ffffff;">
	        //         <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
	        //             <tr>
	        //                 <td style="padding: 20px; padding-bottom:10px; font-family:'Roboto', sans-serif; font-size: 17px; line-height: 20px; color: #1bb7e9;">
	        //                     <h2 style="margin-top:0; margin-bottom:10px; font-size:23px; line-height:27px; color: #1bb7e9; font-weight: bold;">UITGELICHT BERICHT</h2>
	        //                 </td>
	        //             </tr>
	        //         </table>
	        //     </td>
	        // </tr>

			?>

			<?php
			global $post;
			$args = array( 'numberposts' => 1, 'offset'=> 0, 'include' => $berichtuitgelicht);
			$myposts = get_posts( $args );
			foreach( $myposts as $post ) :	setup_postdata($post); ?>
			

		<tr>
		<td style="padding:0px; background-color: #ffffff;">
		<table>

            <tr>
                <td>
					<a href="<?php the_permalink(); ?>" target="_blank">
					<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' ); ?>
						<img src="<?php echo $thumb[0];?>" width="600" height="" alt="afbeelding" border="0" style="width: 100%; max-width: 600px; height: auto; background: #dddddd; margin: auto; display: block;" class="g-img"></a>
                </td>
            </tr>

            <tr>
                <td style="background-color: #4f4f4f; color:#ffffff;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                            <td style="padding: 20px; font-family: georgia, serif; font-size: 17px; line-height: 20px; color: #ffffff;">
                                <a href="<?php the_permalink(); ?>">
								<h1 style="margin: 0 0 10px; font-family: 'Roboto', sans-serif; font-size: 25px; line-height: 30px; color: #ffffff; font-weight: bold;"><?php the_title(); ?></h1>
								<p class="plattetekst" style="margin: 0 0 10px; color:#ffffff; font-weight:normal;">
								<?php
															
								$premiumartikel = get_post_meta( $post->ID, 'premium_article', true );

								if($premiumartikel == 1) { 
									echo '<strong><span style="color: #fff;font-family: \'Roboto\', sans-serif;background: #1bb7e9; padding-top:0; padding-right:3px; padding-bottom:0; padding-left:3px;">Premium</span> | ';
								if(in_category('magazine')) { echo 'Magazine  ';}
									echo '</strong> ';
								}?>
								 <?php echo content(15); ?></p></p>
								<p style="text-align:right; color:#ffffff; font-family: 'Roboto', sans-serif; font-weight:bold;">Lees verder</p>
								</a>
                            </td>
                        </tr>
                    </table>
                </td>
			</tr>

		</table>
			

	</td>
</tr>

<tr><td aria-hidden="true" height="40" style="font-size: 0px; line-height: 0px; background:#ffffff"></td></tr>


			<?php endforeach; 
				wp_reset_postdata();?>



<?php 
	if ($advertorials >= 1)  {
			
?>
			<tr>
	            <td style="background-color: #eeeeee;">
	                <table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0">
	                    <tbody><tr>
	                        <td style="padding: 20px; padding-top:10px; font-family: georgia; font-size: 17px; line-height: 20px; color: #222222;">
								<div style="text-align: center;margin-bottom: 5px;">- Advertorial -</div>
	                            <h2 style="margin-top:0; margin-bottom:10px; font-size:20px; line-height:27px; color: #222222; font-weight: bold;">Advertorial titel</h2>
								<table>
                                    <tbody><tr>
                                        <td valign="top"><img src="advertorial-afbeelding.jpg" alt="afbeelding" style="width: 100px; margin-right:5px; max-width: 100px; height: auto;display: block;" width="100px" height="" border="0"></td><td valign="top"><p class="plattetekst" style="font-family: georgia, serif; font-size: 17px; line-height: 20px; color: #222222; margin:0; padding: 0 10px 10px; text-align: left;">Advertorial hoofdtekst</p>
                                            <a href="https://www.de_link_komt_hier" target="_blank" style="font-family: georgia, serif; font-size: 16px; line-height: 20px; color: #222222; padding: 0 10px 10px; text-align: left;"><span>Advertorial linktekst <img src="https://img.emailnewsletter-software.net/tahq_leesmeerZWART.png" style="margin-left:5px; height:10px; width:6px;" width="6" height="10" border="0"><span></span></span></a>
                                        </td>
                                    </tr>
                                </tbody></table>
								
	                        </td>
	                    </tr>
	                </tbody></table>
	            </td>
			</tr>
			<tr><td aria-hidden="true" height="40" style="font-size: 0px; line-height: 0px; background:#ffffff"></td></tr>
<?php
}
?>


<?php

$afbeeldingenlijst = array();

?>


	        <tr>
	            <td style="background-color: #4f4f4f;">
	                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
	                    <tr>
	                        <td style="padding: 20px; padding-top:0; padding-bottom:0; font-family: 'Roboto', sans-serif; font-size: 17px; line-height: 20px; background-color:#4f4f4f; color: #fff;">
	                            <h2 style="margin-top:0; margin-bottom:0px; font-size:23px; line-height:36px; color: #fff; font-weight: bold;">NIEUWSTE ARTIKELEN</h2>
	                        </td>
	                    </tr>
	                </table>
	            </td>
			</tr>


<tr>

<td style="padding: 10px; background-color: #ffffff;">
	<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>

			<?php




			global $post;
			$args = array( 'numberposts' => 1, 'offset'=> 0, 'include' => $bericht1); // 'post__in' => array( $bericht1 ); // 'post__in' => array($bericht1,$bericht2) );
			$myposts = get_posts( $args );
			foreach( $myposts as $post ) :	setup_postdata($post);?>

			<th valign="top" width="50%" class="stack-column-center">

			<?php 
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' ); 
				$afbeeldingthumb = $thumb[0];
				array_push($afbeeldingenlijst, $afbeeldingthumb);
			?>


				<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td style="padding: 10px; text-align: center">
							<a href="<?php the_permalink(); ?>" target="_blank"><img src="<?php echo $thumb[0];?>" width="270" height="" alt="afbeelding" border="0" style="width: 100%; max-width: 375px; height: auto; background: #dddddd;"></a>
						</td>
					</tr>
					<tr>
						<td style="font-family: Georgia, serif; font-size: 17px; line-height: 20px; color: #222222; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
							<a href="<?php the_permalink(); ?>" target="_blank">
							<h2 style="margin-top:0; margin-bottom:10px; font-family: 'Roboto', sans-serif; font-size:23px; line-height:27px; color: #4f4f4f; font-weight: bold;"><?php the_title(); ?></h2>
							<p class="plattetekst" style="margin: 0 0 10px; color:#222222; font-weight:normal;"><?php 
								$premiumartikel = get_post_meta( $post->ID, 'premium_article', true );
								if($premiumartikel == 1) { 
								echo '<strong><span style="color: #fff;font-family: \'Roboto\', sans-serif;background: #1bb7e9; padding-top:0; padding-right:3px; padding-bottom:0; padding-left:3px;">Premium</span> | ';
								if(in_category('magazine')) { echo 'Magazine  ';}
								echo '</strong> ';
							}
							?>
							
							<?php echo content(12); ?> <span style="text-align:right; color:#4f4f4f; font-family: 'Roboto', sans-serif; font-weight:bold;">Lees verder</span></p>
							</a>
						</td>
					</tr>
				</table>

			</th>

			<?php endforeach; 
				wp_reset_postdata(); 



			

			
			global $post;
			$args = array( 'numberposts' => 1, 'offset'=> 0, 'include' => $bericht2); // 'post__in' => array('' $bericht2 ); // 'post__in' => array($bericht1,$bericht2) ); 'post__in' => array($bericht1,$bericht2) );
			$myposts = get_posts( $args );
			foreach( $myposts as $post ) :	setup_postdata($post);?>

			<th valign="top" width="50%" class="stack-column-center">

			<?php 
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'small' );
				$afbeeldingthumb = $thumb[0];
				array_push($afbeeldingenlijst, $afbeeldingthumb);

			?>


			<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td style="padding: 10px; text-align: center">
							<a href="<?php the_permalink(); ?>" target="_blank"><img src="<?php echo $thumb[0];?>" width="270" height="" alt="afbeelding" border="0" style="width: 100%; max-width: 375px; height: auto; background: #dddddd;"></a>
						</td>
					</tr>
					<tr>
						<td style="font-family: Georgia, serif; font-size: 17px; line-height: 20px; color: #222222; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
							<a href="<?php the_permalink(); ?>" target="_blank">
							<h2 style="margin-top:0; margin-bottom:10px; font-family: 'Roboto', sans-serif; font-size:23px; line-height:27px; color: #4f4f4f; font-weight: bold;"><?php the_title(); ?></h2>
							<p class="plattetekst" style="margin: 0 0 10px; color:#222222; font-weight:normal;"><?php 
							$premiumartikel = get_post_meta( $post->ID, 'premium_article', true );
							if($premiumartikel == 1) { 
								echo '<strong><span style="color: #fff;font-family: \'Roboto\', sans-serif;background: #1bb7e9; padding-top:0; padding-right:3px; padding-bottom:0; padding-left:3px;">Premium</span> | ';
								if(in_category('magazine')) { echo 'Magazine  ';}
								echo '</strong> ';
							}
							?>
							
							<?php echo content(12); ?> <span style="text-align:right; color:#4f4f4f; font-family: 'Roboto', sans-serif; font-weight:bold;">Lees verder</span></p>
							</a>
						</td>
					</tr>
				</table>

			</th>

			<?php endforeach; 
				wp_reset_postdata(); ?>

		</tr>
	</table>
</td>
</tr>
	



<?php 
if ($advertorials >= 2)  {
?>
			<tr>
	            <td style="background-color: #eeeeee;">
	                <table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0">
	                    <tbody><tr>
	                        <td style="padding: 20px; padding-top:10px; font-family: georgia; font-size: 17px; line-height: 20px; color: #222222;">
								<div style="text-align: center;margin-bottom: 5px;">- Advertorial -</div>
	                            <h2 style="margin-top:0; margin-bottom:10px; font-size:20px; line-height:27px; color: #222222; font-weight: bold;">Advertorial titel met 50 tekens in de titel</h2>
								<table>
                                    <tbody><tr>
                                        <td valign="top"><img src="advertorial-afbeelding.jpg" alt="afbeelding" style="width: 100px; margin-right:5px; max-width: 100px; height: auto;display: block;" width="100px" height="" border="0"></td><td valign="top"><p class="plattetekst" style="font-family: georgia, serif; font-size: 17px; line-height: 20px; color: #222222; margin:0; padding: 0 10px 10px; text-align: left;">Advertorial hoofdtekst met een lengte van 235 tekens. Dit wordt aangevuld met een tekstlink</p>
                                            <a href="https://www.de_link_komt_hier" target="_blank" style="font-family: georgia, serif; font-size: 16px; line-height: 20px; color: #222222; padding: 0 10px 10px; text-align: left;"><span>Advertorial linktekst <img src="https://img.emailnewsletter-software.net/tahq_leesmeerZWART.png" style="margin-left:5px; height:10px; width:6px;" width="6" height="10" border="0"><span></span></span></a>
                                        </td>
                                    </tr>
                                </tbody></table>
								
	                        </td>
	                    </tr>
	                </tbody></table>
	            </td>
			</tr>
			<tr><td aria-hidden="true" height="40" style="font-size: 0px; line-height: 0px; background:#ffffff"></td></tr>
			
			<?php
			}
			?>





<?php 
if ($aantalrijen >= 2) {
?>



<tr>

<td style="padding: 10px; background-color: #ffffff;">
	<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>

			<?php

			global $post;
			$args = array( 'numberposts' => 1, 'offset'=> 0, 'post__not_in' => $gebruikteberichten );
			$myposts = get_posts( $args );
			foreach( $myposts as $post ) :	setup_postdata($post);

			$berichtnummer = $post->ID;

			array_push($gebruikteberichten, $berichtnummer );
			?>

			<th valign="top" width="50%" class="stack-column-center">

			<?php 
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'small' );
				$afbeeldingthumb = $thumb[0];
				array_push($afbeeldingenlijst, $afbeeldingthumb);
			?>


				<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td style="padding: 10px; text-align: center">
							<a href="<?php the_permalink(); ?>" target="_blank"><img src="<?php echo $thumb[0];?>" width="270" height="" alt="afbeelding" border="0" style="width: 100%; max-width: 375px; height: auto; background: #dddddd;"></a>
						</td>
					</tr>
					<tr>
						<td style="font-family: Georgia, serif; font-size: 17px; line-height: 20px; color: #222222; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
							<a href="<?php the_permalink(); ?>" target="_blank">
							<h2 style="margin-top:0; margin-bottom:10px; font-family: 'Roboto', sans-serif; font-size:23px; line-height:27px; color: #4f4f4f; font-weight: bold;"><?php the_title(); ?></h2>
							<p class="plattetekst" style="margin: 0 0 10px; color:#222222; font-weight:normal;"><?php 
							$premiumartikel = get_post_meta( $post->ID, 'premium_article', true );
							if($premiumartikel == 1) { 
								echo '<strong><span style="color: #fff;font-family: \'Roboto\', sans-serif;background: #1bb7e9; padding-top:0; padding-right:3px; padding-bottom:0; padding-left:3px;">Premium</span> | ';
								if(in_category('magazine')) { echo 'Magazine  ';}
								echo '</strong> ';
							}
							?>
							
							<?php echo content(12); ?> <span style="text-align:right; color:#4f4f4f; font-family: 'Roboto', sans-serif; font-weight:bold;">Lees verder</span></p>
							</a>
						</td>
					</tr>
				</table>

			</th>

			<?php endforeach; 
				wp_reset_postdata(); 




				
			if ($partnerbericht1 > 0) { // Als Partnerbericht is "Geen" (= 0 ), dan normaal bericht laten zien.
			//	echo "Partnerberichtnummer is ".$partnerbericht1;

				global $post;
				$args = array( 'numberposts' => 1, 'offset'=> 0, 'include' => $partnerbericht1, 'post_type' => 'partners'); // 'post__in' => array('' $bericht2 ); // 'post__in' => array($bericht1,$bericht2) ); 'post__in' => array($bericht1,$bericht2) );
				$myposts = get_posts( $args );
				foreach( $myposts as $post ) :	setup_postdata($post);

				$berichtnummer = $post->ID;

				array_push($gebruikteberichten, $berichtnummer );
				?>

				<th valign="top" width="50%" class="stack-column-center">

				<?php 
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'small' );
					$afbeeldingthumb = $thumb[0];
					array_push($afbeeldingenlijst, $afbeeldingthumb);
				?>

					<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
							<td style="padding: 10px; text-align: center; background-color:#eeeeee;">
								<a href=<?php the_permalink(); ?>" target="_blank"><img src="<?php echo $thumb[0];?>" width="270" height="" alt="afbeelding" border="0" style="width: 100%; max-width: 375px; height: auto; background: #dddddd; font-family: Georgia, serif; font-size: 17px; line-height: 17px; color: #222222;"></a>
							</td>
						</tr>
						<tr>
							<td style="font-family: Georgia, serif; font-size: 17px; line-height: 20px; color: #222222; background-color:#eeeeee; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
								<a href="<?php the_permalink(); ?>" target="_blank">
								<h2 style="margin-top:0; margin-bottom:10px; font-size:20px; line-height:27px; color: #222222; font-family: \'Roboto\', sans-serif; font-weight: bold;"><?php the_title(); ?></h2>
								<p class="plattetekst" style="margin: 0 0 10px; color:#222222; \'Roboto\', sans-serif; font-weight:normal;"><strong><span>Kennispartner</span> |</strong> <?php echo content(15); ?></p>
								<p style="text-align:right; color:#4f4f4f; font-family: \'Roboto\', sans-serif; font-weight:bold;">Lees verder</p>
								</a>
							</td>
						</tr>
					</table>

				</th>

			<?php
				endforeach; 
				wp_reset_postdata(); 

			} else { // Als Partnerbericht is "ID van Gekozen bericht", dan partnerbericht opmaak laten zien.
				
				global $post;
				$args = array( 'numberposts' => 1, 'offset'=> 0, 'post__not_in' => $gebruikteberichten ); // 'post__in' => array('' $bericht2 ); // 'post__in' => array($bericht1,$bericht2) ); 'post__in' => array($bericht1,$bericht2) );
				$myposts = get_posts( $args );
				foreach( $myposts as $post ) :	setup_postdata($post);

				$berichtnummer = $post->ID;

				array_push($gebruikteberichten, $berichtnummer );
				?>

				<th valign="top" width="50%" class="stack-column-center">

				<?php 
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'small' );
					$afbeeldingthumb = $thumb[0];
					array_push($afbeeldingenlijst, $afbeeldingthumb);
				?>


					<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
							<td style="padding: 10px; text-align: center">
								<a href="<?php the_permalink(); ?>" target="_blank"><img src="<?php echo $thumb[0];?>" width="270" height="" alt="afbeelding" border="0" style="width: 100%; max-width: 375px; height: auto; background: #dddddd;"></a>
							</td>
						</tr>
						<tr>
							<td style="font-family: Georgia, serif; font-size: 17px; line-height: 20px; color: #222222; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
								<a href="<?php the_permalink(); ?>" target="_blank">
								<h2 style="margin-top:0; margin-bottom:10px; font-family: 'Roboto', sans-serif; font-size:23px; line-height:27px; color: #4f4f4f; font-weight: bold;"><?php the_title(); ?></h2>
								<p class="plattetekst" style="margin: 0 0 10px; color:#222222; font-weight:normal;"><?php 
								$premiumartikel = get_post_meta( $post->ID, 'premium_article', true );
								if($premiumartikel == 1) { 
									echo '<strong><span style="color: #fff;font-family: \'Roboto\', sans-serif;background: #1bb7e9; padding-top:0; padding-right:3px; padding-bottom:0; padding-left:3px;">Premium</span> | ';
									if(in_category('magazine')) { echo 'Magazine  ';}
									echo '</strong> ';
								}
								?>
								
								<?php echo content(12); ?> <span style="text-align:right; color:#4f4f4f; font-family: 'Roboto', sans-serif; font-weight:bold;">Lees verder</span></p>
								</a>
							</td>
						</tr>
					</table>

				</th>
			<?php
				endforeach; 
				wp_reset_postdata(); 

			}

			?>







		</tr>
	</table>
</td>
</tr>


<?php
} // Einde aantal rijen
?>


		


<?php 
if ($aantalrijen >= 3) {
?>



<tr>

<td style="padding: 10px; background-color: #ffffff;">
	<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>

			<?php

			global $post;
			$args = array( 'numberposts' => 1, 'offset'=> 0, 'post__not_in' => $gebruikteberichten );
			$myposts = get_posts( $args );
			foreach( $myposts as $post ) :	setup_postdata($post);

			$berichtnummer = $post->ID;

			array_push($gebruikteberichten, $berichtnummer );
			?>

			<th valign="top" width="50%" class="stack-column-center">

			<?php 
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'small' );
				$afbeeldingthumb = $thumb[0];
				array_push($afbeeldingenlijst, $afbeeldingthumb);
			?>


				<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td style="padding: 10px; text-align: center">
							<a href="<?php the_permalink(); ?>" target="_blank"><img src="<?php echo $thumb[0];?>" width="270" height="" alt="afbeelding" border="0" style="width: 100%; max-width: 375px; height: auto; background: #dddddd;"></a>
						</td>
					</tr>
					<tr>
						<td style="font-family: Georgia, serif; font-size: 17px; line-height: 20px; color: #222222; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
							<a href="<?php the_permalink(); ?>" target="_blank">
							<h2 style="margin-top:0; margin-bottom:10px; font-family: 'Roboto', sans-serif; font-size:23px; line-height:27px; color: #4f4f4f; font-weight: bold;"><?php the_title(); ?></h2>
							<p class="plattetekst" style="margin: 0 0 10px; color:#222222; font-weight:normal;"><?php 
							$premiumartikel = get_post_meta( $post->ID, 'premium_article', true );
							if($premiumartikel == 1) { 
								echo '<strong><span style="color: #fff;font-family: \'Roboto\', sans-serif;background: #1bb7e9; padding-top:0; padding-right:3px; padding-bottom:0; padding-left:3px;">Premium</span> | ';
								if(in_category('magazine')) { echo 'Magazine  ';}
								echo '</strong> ';
							}
							?>
							
							<?php echo content(12); ?> <span style="text-align:right; color:#4f4f4f; font-family: 'Roboto', sans-serif; font-weight:bold;">Lees verder</span></p>
							</a>
						</td>
					</tr>
				</table>

			</th>

			<?php endforeach; 
				wp_reset_postdata(); 




				
			if ($partnerbericht2 > 0) { // Als Partnerbericht is "Geen" (= 0 ), dan normaal bericht laten zien.
			//	echo "Partnerberichtnummer is ".$partnerbericht1;

				global $post;
				$args = array( 'numberposts' => 1, 'offset'=> 0, 'include' => $partnerbericht2, 'post_type' => 'partners'); // 'post__in' => array('' $bericht2 ); // 'post__in' => array($bericht1,$bericht2) ); 'post__in' => array($bericht1,$bericht2) );
				$myposts = get_posts( $args );
				foreach( $myposts as $post ) :	setup_postdata($post);

				$berichtnummer = $post->ID;

				array_push($gebruikteberichten, $berichtnummer );
				?>

				<th valign="top" width="50%" class="stack-column-center">

				<?php 
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'small' );
					$afbeeldingthumb = $thumb[0];
					array_push($afbeeldingenlijst, $afbeeldingthumb);
				?>

					<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
							<td style="padding: 10px; text-align: center; background-color:#eeeeee;">
								<a href=<?php the_permalink(); ?>" target="_blank"><img src="<?php echo $thumb[0];?>" width="270" height="" alt="afbeelding" border="0" style="width: 100%; max-width: 375px; height: auto; background: #dddddd; font-family: Georgia, serif; font-size: 17px; line-height: 17px; color: #222222;"></a>
							</td>
						</tr>
						<tr>
							<td style="font-family: Georgia, serif; font-size: 17px; line-height: 20px; color: #222222; background-color:#eeeeee; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
								<a href="<?php the_permalink(); ?>" target="_blank">
								<h2 style="margin-top:0; margin-bottom:10px; font-size:20px; line-height:27px; color: #222222; font-family: \'Roboto\', sans-serif; font-weight: bold;"><?php the_title(); ?></h2>
								<p class="plattetekst" style="margin: 0 0 10px; color:#222222; \'Roboto\', sans-serif; font-weight:normal;"><strong><span>Kennispartner</span> |</strong> <?php echo content(15); ?></p>
								<p style="text-align:right; color:#4f4f4f; font-family: \'Roboto\', sans-serif; font-weight:bold;">Lees verder</p>
								</a>
							</td>
						</tr>
					</table>

				</th>

			<?php
				endforeach; 
				wp_reset_postdata(); 

			} else { // Als Partnerbericht is "ID van Gekozen bericht", dan partnerbericht opmaak laten zien.
				
				global $post;
				$args = array( 'numberposts' => 1, 'offset'=> 0, 'post__not_in' => $gebruikteberichten ); // 'post__in' => array('' $bericht2 ); // 'post__in' => array($bericht1,$bericht2) ); 'post__in' => array($bericht1,$bericht2) );
				$myposts = get_posts( $args );
				foreach( $myposts as $post ) :	setup_postdata($post);

				$berichtnummer = $post->ID;

				array_push($gebruikteberichten, $berichtnummer );
				?>

				<th valign="top" width="50%" class="stack-column-center">

				<?php 
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'small' );
					$afbeeldingthumb = $thumb[0];
					array_push($afbeeldingenlijst, $afbeeldingthumb);
				?>


					<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
							<td style="padding: 10px; text-align: center">
								<a href="<?php the_permalink(); ?>" target="_blank"><img src="<?php echo $thumb[0];?>" width="270" height="" alt="afbeelding" border="0" style="width: 100%; max-width: 375px; height: auto; background: #dddddd;"></a>
							</td>
						</tr>
						<tr>
							<td style="font-family: Georgia, serif; font-size: 17px; line-height: 20px; color: #222222; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
								<a href="<?php the_permalink(); ?>" target="_blank">
								<h2 style="margin-top:0; margin-bottom:10px; font-family: 'Roboto', sans-serif; font-size:23px; line-height:27px; color: #4f4f4f; font-weight: bold;"><?php the_title(); ?></h2>
								<p class="plattetekst" style="margin: 0 0 10px; color:#222222; font-weight:normal;"><?php 
								$premiumartikel = get_post_meta( $post->ID, 'premium_article', true );
								if($premiumartikel == 1) { 
									echo '<strong><span style="color: #fff;font-family: \'Roboto\', sans-serif;background: #1bb7e9; padding-top:0; padding-right:3px; padding-bottom:0; padding-left:3px;">Premium</span> | ';
									if(in_category('magazine')) { echo 'Magazine  ';}
									echo '</strong> ';
								}
								?>
								
								<?php echo content(12); ?> <span style="text-align:right; color:#4f4f4f; font-family: 'Roboto', sans-serif; font-weight:bold;">Lees verder</span></p>
								</a>
							</td>
						</tr>
					</table>

				</th>
			<?php
				endforeach; 
				wp_reset_postdata(); 

			}

			?>







		</tr>
	</table>
</td>
</tr>


<?php
} // Einde aantal rijen
?>



<?php 
if ($advertorials >= 3)  {
?>
			<tr>
	            <td style="background-color: #eeeeee;">
	                <table role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0">
	                    <tbody><tr>
	                        <td style="padding: 20px; padding-top:10px; font-family: georgia; font-size: 17px; line-height: 20px; color: #222222;">
								<div style="text-align: center;margin-bottom: 5px;">- Advertorial -</div>
	                            <h2 style="margin-top:0; margin-bottom:10px; font-size:20px; line-height:27px; color: #222222; font-weight: bold;">Advertorial titel</h2>
								<table>
                                    <tbody><tr>
                                        <td valign="top"><img src="advertorial-afbeelding.jpg" alt="afbeelding" style="width: 100px; margin-right:5px; max-width: 100px; height: auto;display: block;" width="100px" height="" border="0"></td><td valign="top"><p class="plattetekst" style="font-family: georgia, serif; font-size: 17px; line-height: 20px; color: #222222; margin:0; padding: 0 10px 10px; text-align: left;">Advertorial hoofdtekst</p>
                                            <a href="https://www.de_link_komt_hier" target="_blank" style="font-family: georgia, serif; font-size: 16px; line-height: 20px; color: #222222; padding: 0 10px 10px; text-align: left;"><span>Advertorial linktekst <img src="https://img.emailnewsletter-software.net/tahq_leesmeerZWART.png" style="margin-left:5px; height:10px; width:6px;" width="6" height="10" border="0"><span></span></span></a>
                                        </td>
                                    </tr>
                                </tbody></table>
								
	                        </td>
	                    </tr>
	                </tbody></table>
	            </td>
			</tr>
			<tr><td aria-hidden="true" height="40" style="font-size: 0px; line-height: 0px; background:#ffffff"></td></tr>
			
			<?php
			}
			?>


<?php 
if ($aantalrijen >= 4) {
?>

<tr>

<td style="padding: 10px; background-color: #ffffff;">
	<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>

			<?php

			global $post;
			$args = array( 'numberposts' => 2, 'offset'=> 0, 'post__not_in' => $gebruikteberichten );
			$myposts = get_posts( $args );
			foreach( $myposts as $post ) :	setup_postdata($post); 

			$berichtnummer = $post->ID;

			array_push($gebruikteberichten, $berichtnummer );
			//print_r($gebruikteberichten);

			?>

			<th valign="top" width="50%" class="stack-column-center">

			<?php 
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'small' );
				$afbeeldingthumb = $thumb[0];
				array_push($afbeeldingenlijst, $afbeeldingthumb); 
			?>


				<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td style="padding: 10px; text-align: center">
							<a href="<?php the_permalink(); ?>" target="_blank"><img src="<?php echo $thumb[0];?>" width="270" height="" alt="afbeelding" border="0" style="width: 100%; max-width: 375px; height: auto; background: #dddddd;"></a>
						</td>
					</tr>
					<tr>
						<td style="font-family: Georgia, serif; font-size: 17px; line-height: 20px; color: #222222; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
							<a href="<?php the_permalink(); ?>" target="_blank">
							<h2 style="margin-top:0; margin-bottom:10px; font-family: 'Roboto', sans-serif; font-size:23px; line-height:27px; color: #4f4f4f; font-weight: bold;"><?php the_title(); ?></h2>
							<p class="plattetekst" style="margin: 0 0 10px; color:#222222; font-weight:normal;"><?php 
							$premiumartikel = get_post_meta( $post->ID, 'premium_article', true );
							if($premiumartikel == 1) { 
								echo '<strong><span style="color: #fff;font-family: \'Roboto\', sans-serif;background: #1bb7e9; padding-top:0; padding-right:3px; padding-bottom:0; padding-left:3px;">Premium</span> | ';
								if(in_category('magazine')) { echo 'Magazine  ';}
								echo '</strong> ';
							}
							?>
							
							
							<?php echo content(12); ?> <span style="text-align:right; color:#4f4f4f; font-family: 'Roboto', sans-serif; font-weight:bold;">Lees verder</span></p>
							
							</a>
						</td>
					</tr>
				</table>

			</th>

			<?php endforeach; 
				wp_reset_postdata(); ?>

		</tr>
	</table>
</td>
</tr>

<?php
} // Einde aantal rijen
?>




<?php 
if ($aantalrijen >= 5) {
?>

<tr>

<td style="padding: 10px; background-color: #ffffff;">
	<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>

			<?php

			global $post;
			$args = array( 'numberposts' => 2, 'offset'=> 0, 'post__not_in' => $gebruikteberichten );
			$myposts = get_posts( $args );
			foreach( $myposts as $post ) :	setup_postdata($post); 

			$berichtnummer = $post->ID;

			array_push($gebruikteberichten, $berichtnummer );
			//print_r($gebruikteberichten);

			?>

			<th valign="top" width="50%" class="stack-column-center">

			<?php 
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'small' );
				$afbeeldingthumb = $thumb[0];
				array_push($afbeeldingenlijst, $afbeeldingthumb); 
			?>


				<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td style="padding: 10px; text-align: center">
							<a href="<?php the_permalink(); ?>" target="_blank"><img src="<?php echo $thumb[0];?>" width="270" height="" alt="afbeelding" border="0" style="width: 100%; max-width: 375px; height: auto; background: #dddddd;"></a>
						</td>
					</tr>
					<tr>
						<td style="font-family: Georgia, serif; font-size: 17px; line-height: 20px; color: #222222; padding: 0 10px 10px; text-align: left;" class="center-on-narrow">
							<a href="<?php the_permalink(); ?>" target="_blank">
							<h2 style="margin-top:0; margin-bottom:10px; font-family: 'Roboto', sans-serif; font-size:23px; line-height:27px; color: #4f4f4f; font-weight: bold;"><?php the_title(); ?></h2>
							<p class="plattetekst" style="margin: 0 0 10px; color:#222222; font-weight:normal;"><?php 
							$premiumartikel = get_post_meta( $post->ID, 'premium_article', true );
							if($premiumartikel == 1) { 
								echo '<strong><span style="color: #fff;font-family: \'Roboto\', sans-serif;background: #1bb7e9; padding-top:0; padding-right:3px; padding-bottom:0; padding-left:3px;">Premium</span> | ';
								if(in_category('magazine')) { echo 'Magazine  ';}
								echo '</strong> ';
							}
							?>
							
							<?php echo content(12); ?> <span style="text-align:right; color:#4f4f4f; font-family: 'Roboto', sans-serif; font-weight:bold;">Lees verder</span></p>
							</a>
						</td>
					</tr>
				</table>

			</th>

			<?php endforeach; 
				wp_reset_postdata(); ?>

		</tr>
	</table>
</td>
</tr>

<?php
} // Einde aantal rijen
?>





<tr>
	<td>

	<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%;" style="background-color: #1bb7e9; color:#ffffff;">
		<tr>
			<td><p style="text-align:center; color:#ffffff; font-family: 'Roboto', sans-serif;">Toegang tot alle berichten op veehouderijtechniek.nl?</p><p style="text-align:center;"><a href="https://www.mechaman.nl/veehouderij-techniek/abonnementen/" target="_blank" style="color:#ffffff; font-family: 'Roboto', sans-serif; font-weight:bold;">Bekijk onze abonnementen</a></p></td>
		</tr>
		<tr>
			<td style="background: #4f4f4f; padding:10px; text-align:center;">
			<p style="color:#ffffff; font-family: 'Roboto', sans-serif;">Volg ons via: 
			<a href="https://www.facebook.com/veehouderijtechniek" target="_blank" style="color:#ffffff; font-family: 'Roboto', sans-serif; font-weight:bold;">Facebook</a> | <a href="https://www.youtube.com/user/mechamanportaal" target="_blank" style="color:#ffffff; font-family: 'Roboto', sans-serif; font-weight:bold;">YouTube</a></p>
			</td>
		</tr>
		<tr>
			<td style="padding: 10px; text-align: left; font-family: 'Roboto', sans-serif; font-size: 17px; line-height: 20px; background: #4f4f4f;  color: #ffffff;">
				<p style="margin: 0; padding:10px; text-align:center; font-family: 'Roboto', sans-serif;"><a href="https://www.veehouderijtechniek.nl" target="_blank" style="color:#ffffff; text-transform:uppercase;">veehouderijtechniek.nl</a> | <a href="https://www.agrimedia.nl" target="_blank" style="color:#ffffff; text-transform:uppercase;">AgriMedia</a></p>
			</td>
		</tr>
	</table>

	</td>
</tr>

</table>



    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <![endif]-->
    </center>
</body>
</html>


<div style="text-align:left">
<?php 

foreach($afbeeldingenlijst as $afbeelding) {
//	echo substr($afbeelding, 40) $afbeelding.'<br>';

	echo (substr(strrchr($afbeelding,"/"),1)).'<br>';
}

?>
</div>