<?php
$categories = get_terms([
    'taxonomy' => 'category',
    'hide_empty' => false,
    'number' => 6,
    'meta_query' => array(
        'relation' => 'OR',
        array(
            'key'     => 'do_not_show',
            'value'   => '0',
            'compare' => '=',
        ),
        array(
            'key' => 'do_not_show',
            'compare' => 'NOT EXISTS'
        ),
    )
]);

if (!empty($categories)) : ?>
    <section class="categories">

        <div class="container">
            <h3 class="section-title"><?php _e('Categorieën', 'newheap'); ?>
            </h3>
            <div class="row" style="margin-left:-.25rem;margin-right:-.25rem;">
                <?php foreach ($categories as $category) : ?>
                    <?php
                    $category_image = get_field('term_image', $category);

                    if (!empty($category_image)) {
                        $category_image = get_field('term_image', $category)['url'];
                    } else {
                        $category_image = get_field('placeholder_afbeelding', 'option')['url'];
                    }

                    ?>
                    <div class="col-6 col-xl-2 col-md-4 px-1">
                        <a href="<?php echo get_term_link($category); ?>" class="post-item">
                            <div class="image-media large relative">
                                <div class="image-holder" style="background-image: url('<?php echo $category_image; ?>');"></div>

                                <div class="content-title bg-black-trans text-white">
                                    <h5 class="p-2">
                                        <?php echo $category->name; ?>
                                    </h5>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
            <a href="<?php echo get_field('categories_archive_page', 'option'); ?>" class="more-button">
                <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2 mt-0">
                    <?php echo __("Alle categorieën","newheap"); ?>
                    <div class="arrow-right pr-2 ml-20"></div>
                </div>
            </a>
        </div>
    </section>
<?php endif;
