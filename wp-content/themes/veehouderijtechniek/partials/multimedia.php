<section class="multimedia my-4">
    <div class="container">
        <h3 class="section-title">
            <?php _e('Multimedia', 'newheap'); ?>
        </h3>
        <div class="row">
            <div class="col-md-12">

                <?php
                wp_reset_query();

                $media = get_field('multimedia');

                $mainItem = $media['main_item'];
                $mainID = url_to_postid($mainItem);
                ?>
                <a class="block-link" href="<?php echo get_the_permalink($mainID) ?>">
                    <div class="d-flex mb-5 row-eq-height">
                        <div class="image-media">
                            <img src="<?php echo get_the_post_thumbnail_url($mainID, 'large'); ?>" width="100%"
                                 height="auto"/>
	                        <?php if (get_field('premium_article', $mainID)) { ?>
                                <div class="content-type absolute">
                                    <span class="primary-background px-3 py-1"><i
                                                class="far fa-gem"></i>&nbsp;<?php _e('Premium', 'newheap'); ?></span>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="content-holder">
                            <div class="content-title bg-black text-white">
                                <h5 class="p-3">
                                    <?php echo \NewHeap\Theme\Helpers::get_type_icon($mainID); ?>
	                                <?php printf(__('Uitgelichte %s'), \NewHeap\Theme\Helpers::get_post_article_type($mainID)->name); ?>
                                </h5>

                            </div>
                            <div class="grey-border-bottom-right p-3">
                                <h3>
	                                <?php echo get_the_title($mainID); ?>
                                </h3>
                                <p><?php echo get_the_excerpt($mainID); ?></p>
                            </div>
                        </div>

                    </div>
                </a>

                <div class="row">
                    <?php
                    wp_reset_query();
                    $posts = new WP_Query([
	                    'post_type' => 'post',
	                    'posts_per_page' => 1,
	                    'tax_query' => [
		                    [
			                    'taxonomy' => 'artikel_type',
			                    'field'    => 'slug',
			                    'terms'    => 'videos',
		                    ]
	                    ],
                    ]);

                    $videoID = $posts->posts[0]->ID;
                    ?>
                    <div class="col-sm-12 col-md-4 relative pb-2">
                        <a href="<?php echo get_the_permalink($videoID) ?>">
                            <div class="">
                                <img src="<?php echo get_the_post_thumbnail_url($videoID, 'large'); ?>" width="100%"
                                     height="auto"/>

	                            <?php if (get_field('premium_article', $videoID)) { ?>
                                    <div class="content-type absolute">
                                        <span class="primary-background px-3 py-1"><i
                                                    class="far fa-gem"></i>&nbsp;<?php _e('Premium', 'newheap'); ?></span>
                                    </div>
                                <?php } ?>

                                <div class="content-title text-white absolute text-black w-100 d-flex">
                                    <h5 class="p-2 bg-white mb-0 w-50">
                                        <span><?php echo get_the_title($videoID); ?></span>
                                    </h5>
                                    <div class="primary-background p-2 w-25 text-center d-flex d-flex-10 border-bottom-right-cut">
                                        <h5 class="mb-0">
                                            <span class="primary-background">
                                                <?php echo \NewHeap\Theme\Helpers::get_type_icon($videoID); ?>
                                            </span>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php
                    wp_reset_query();
                    $posts = new WP_Query([
	                    'post_type' => 'post',
	                    'posts_per_page' => 1,
	                    'tax_query' => [
		                    [
			                    'taxonomy' => 'artikel_type',
			                    'field'    => 'slug',
			                    'terms'    => 'podcasts',
		                    ]
	                    ],
                    ]);

                    $podcastID = $posts->posts[0]->ID;
                    ?>
                    <div class="col-sm-12 col-md-4 relative pb-2">
                        <a href="<?php echo get_the_permalink($podcastID) ?>">
                            <div class="">
                                <img src="<?php echo get_the_post_thumbnail_url($podcastID, 'large'); ?>" width="100%"
                                     height="auto"/>

	                            <?php if (get_field('premium_article', $podcastID)) { ?>
                                    <div class="content-type absolute">
                                        <span class="primary-background px-3 py-1"><i
                                                    class="far fa-gem"></i>&nbsp;<?php _e('Premium', 'newheap'); ?></span>
                                    </div>
                                <?php } ?>

                                <div class="content-title text-white absolute text-black w-100 d-flex">
                                    <h5 class="p-2 bg-white mb-0 w-50">
                                        <span><?php echo get_the_title($podcastID); ?></span>
                                    </h5>
                                    <div class="primary-background p-2 w-25 text-center d-flex d-flex-10 border-bottom-right-cut">
                                        <h5 class="mb-0">
                                            <span class="primary-background">
                                                <?php echo \NewHeap\Theme\Helpers::get_type_icon($podcastID); ?>
                                            </span>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                    <?php
                    wp_reset_query();
                    $posts = new WP_Query([
	                    'post_type' => 'post',
	                    'posts_per_page' => 1,
	                    'tax_query' => [
		                    [
			                    'taxonomy' => 'artikel_type',
			                    'field'    => 'slug',
			                    'terms'    => 'infographics',
		                    ]
	                    ],
                    ]);

                    $infographicID = $posts->posts[0]->ID;
                    ?>
                    <div class="col-sm-12 col-md-4 relative pb-2">
                        <a href="<?php echo get_the_permalink($infographicID) ?>">
                            <div class="">
                                <img src="<?php echo get_the_post_thumbnail_url($infographicID, 'large'); ?>"
                                     width="100%" height="auto"/>

	                            <?php if (get_field('premium_article', $infographicID)) { ?>
                                    <div class="content-type absolute">
                                        <span class="primary-background px-3 py-1"><i
                                                    class="far fa-gem"></i>&nbsp;<?php _e('Premium', 'newheap'); ?></span>
                                    </div>
                                <?php } ?>

                                <div class="content-title text-white absolute text-black w-100 d-flex">
                                    <h5 class="p-2 bg-white mb-0 w-50">
                                        <span><?php echo get_the_title($infographicID); ?></span>
                                    </h5>
                                    <div class="primary-background p-2 w-25 text-center d-flex d-flex-10 border-bottom-right-cut">
                                        <h5 class="mb-0">
                                            <span class="primary-background">
                                                <?php echo \NewHeap\Theme\Helpers::get_type_icon($infographicID); ?>
                                            </span>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>

                <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
                    <a href="<?php echo get_field('multimedia_archive_page', 'option'); ?>">
                        <?php _e('Meer multimedia', 'newheap') ?>
                        <div class="arrow-right pr-2 ml-20"></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <h3 class="section-title"><?php _e('Opinie', 'newheap'); ?></h3>
        <div class="row">

            <div class="col-md-8">
                <?php
                wp_reset_query();
                $posts = new WP_Query([
	                'post_type' => 'post',
	                'posts_per_page' => 1,
	                'tax_query' => [
		                [
			                'taxonomy' => 'artikel_type',
			                'field'    => 'slug',
			                'terms'    => 'columns',
		                ]
	                ],
                ]);

                $opinieID = $posts->posts[0]->ID;

                $input = get_the_excerpt($opinieID);

                $str = $input;
                if (strlen($input) > 200) {
                    $str = explode("\n", wordwrap($input, 200));
                    $str = $str[0] . '...';
                }
                ?>
                <a class="block-link" href="<?php echo get_the_permalink($opinieID) ?>" class="post-item">

                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="column-image-holder object-fit-fix"
                                 style="background-image: url('<?php echo get_the_post_thumbnail_url($opinieID, 'large') ?>'); background-size: cover; background-repeat: no-repeat; background-position: center;">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="text-black p-3 yellow-thick-border">
                                <h2 class=""><?php echo get_the_title($opinieID); ?></h2>
                                <span class="bold">
                                        <?php echo get_the_date('d-m-Y', $opinieID); ?>
                                    |
                                        <i class="far fa-clock"></i>
                                    <?php echo \NewHeap\Theme\Helpers::get_read_time($opinieID); ?>
                                </span>

                                <p class="mt-2">
                                    <?php echo $str; ?>
                                    <br/>
                                    <strong> <?php _e('Lees verder', 'newheap'); ?></strong>
                                </p>
                            </div>
                        </div>
                    </div>
                </a>

                <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
                    <a href="<?php echo get_field('columns_archive_page', 'option'); ?>">
                        <?php _e('Meer columns', 'newheap') ?>
                        <div class="arrow-right pr-2 ml-20"></div>
                    </a>
                </div>
            </div>

            <div class="col-md-4">

                <?php include get_template_directory() . '/partials/latest-poll.php'; ?>

                <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
                    <!-- TODO: Fix URL -->
                    <a href="<?=site_url()?>/polls/">
                        <?php _e('Uitslagen vorige polls', 'newheap') ?>
                        <div class="arrow-right pr-2 ml-20"></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
