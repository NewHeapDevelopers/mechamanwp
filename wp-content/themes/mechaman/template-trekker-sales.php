<?php
/**
 * Template Name: Trekker verkoopcijfers
 */

get_header(); ?>

<div class="container py-4">
    <div class="row">
        <div class="col-md-12">
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>

            <?php include 'partials/trekker_sales.php'; ?>
        </div>
    </div>
</div>

<?php get_footer();
