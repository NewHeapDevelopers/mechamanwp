<?php
/**
 * Template Name: Bedrijfsnieuws
 */

if(isset($_GET['search']) and !empty($_GET['search'])) {
    include('template-category-archive.php');
} else {
    get_header();

    $counter = 0;
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	$args = array(
		'post_type' => 'post',
		'posts_per_page' => 15,
		'paged' => $paged,
		'category_name' => 'bedrijfsnieuws',
	);

	$query = new \WP_Query($args);
    ?>

        <section class="author-posts my-5">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-10 offset-md-1">
                        <h3 class="section-title">
                            <?php _e('Artikelen uit de categorie "Bedrijfsnieuws"', 'newheap'); ?>
                        </h3>
                    </div>

                    <?php if ($query->have_posts()) { ?>
                        <?php while ($query->have_posts()) {
                            $query->the_post(); $counter++;
                            if ($counter == 11) : ?>
                                <div class="col-sm-12 col-md-10 offset-md-1 google-ad-holder ad-between-posts-holder text-center">
                                    <div class="dotted-bottom-black pt-5 pb-5 mb-3">
                                        <div class="ad-between-posts">
                                            <?=get_field('advertisement_archive', 'option')?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="col-sm-12 col-md-10 offset-md-1">
                                <?php include get_theme_file_path('elements/article-listing-item.php'); ?>
                            </div>
                        <?php } ?>

                        <?php
                        global $posts;
                        if (count($posts) < 11) : ?>
                            <div class="col-sm-12 col-md-10 offset-md-1 google-ad-holder ad-between-posts-holder text-center">
                                <div class="pt-5 pb-5 mb-3">
                                    <div class="ad-between-posts">
                                        <?=get_field('advertisement_archive', 'option')?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="col-sm-12 col-md-10 offset-md-1 pb-3 mb-3">
                            <?php
                            $GLOBALS['wp_query']->max_num_pages = $query->max_num_pages;

                            echo the_posts_pagination([
                                'screen_reader_text' => ' ',
                                'mid_size' => 1,
                                'prev_text' => '<i class="fas fa-chevron-left"></i>',
                                'next_text' => '<i class="fas fa-chevron-right"></i>',
                            ]); ?>

                        </div>
                    <?php } else { ?>
                        <div class="col-sm-12 col-md-10 offset-md-1">
                            <?php _e('Geen resultaten gevonden.', 'newheap'); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>

    <?php get_footer();
}
