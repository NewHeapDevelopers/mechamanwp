<?php
namespace NewHeap\Child\Cron;

use NewHeap\Theme\Cache\Cache;
use NewHeap\Theme\Cache\CacheRss;

class Cron
{
    public function __construct()
    {
        add_action('init' , [$this, 'register_events'], 20);
	    add_action('cache_posts_from_all_sites', [$this, 'cache_posts_from_all_sites']);
	    add_action('cache_external_news', [$this, 'cache_external_news']);
    }

    /**
     * @author Ferhat Ileri <ferhat@codio.nl>
     */
    public function register_events()
    {
        if (!wp_next_scheduled ('cache_external_news')) {
            wp_schedule_event(time(), '15min', 'cache_external_news');
        }
        if (!wp_next_scheduled ('cache_posts_from_all_sites')) {
            wp_schedule_event(time(), 'hourly', 'cache_posts_from_all_sites');
        }
    }

    public function cache_posts_from_all_sites()
    {
	    Cache::cache_posts_from_all_sites();
    }

    public function cache_external_news()
    {
	    CacheRss::cache_external_news_mm();
    }
}
