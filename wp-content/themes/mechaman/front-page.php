<?php
/*
Template Name: Front page
*/

get_header();

?>

<?php include 'partials/header.php'; ?>

<?php if(!empty(get_field('advertisement_leaderboard', 'option'))) { ?>
    <div class="w-100 text-center mt-5 pt-5"><?=get_field('advertisement_leaderboard', 'option')?></div>
<?php } ?>

<?php include 'partials/recent_items2.php'; ?>

<?php include 'partials/multimedia.php'; ?>

<br />

<?php include get_theme_file_path('partials/newsletter.php'); ?>

<?php include 'partials/dossiers.php'; ?>

<?php include 'partials/opinie.php'; ?>

<?php include 'partials/news_lm.php'; ?>

<?php include 'partials/news_tpvt.php'; ?>

<?php include 'partials/videos.php'; ?>

<?php include 'partials/loop.php'; ?>

<?php //include 'partials/trekker_sales.php'; ?>

<?php //echo get_the_content(); ?>

<?php

get_footer();

?>


