<section class="tests my-4">

    <div class="container">
        <h3 class="section-title"><?php _e('Tests', 'newheap'); ?></h3>
        <div class="row">
            <?php
            wp_reset_query();
            $tests = get_field('tests');

            if (!empty( $tests )) {
                $i = 0;

                foreach($tests as $test) {
                    $item = $test['test_pagina'];
                    $itemID = url_to_postid($item);

                    $input = get_the_excerpt($itemID);

                    $str = $input;
                    if (strlen($input) > 125) {
                        $str = explode("\n", wordwrap($input, 125));
                        $str = $str[0] . '...';
                    }

                    if($i < 2) {
                        ?>
                        <div class="col-sm-12 col-md-6 mb-3">
                            <a href="<?php echo get_the_permalink($itemID); ?>" class="post-item">
                                <div class="relative">
                                    <div class="image-holder" style="background-image: url('<?php echo get_the_post_thumbnail_url($itemID, 'large') ?>');"></div>

                                    <?php if (!empty(get_field('premium_article', $itemID))) { ?>
                                        <div class="content-type text-black absolute">
                                            <span class="primary-background px-4 py-2"><i class="far fa-gem"></i> Premium</span>
                                        </div>
                                    <?php } ?>

                                    <div class="content-title bg-black-trans text-black grey-border-bottom-right border-bottom-right-cut-large p-3">
                                        <h5 class="font-tests"><?php echo get_the_title($itemID) ?></h5>
                                        <p><?php echo $str ?></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php                    }
                    $i++;
                } // end while
            } // end if
            ?>
            <div class="container">
                <div class="row">
                    <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
                        <a href="<?php echo get_field(\NewHeap\Theme\Helpers::get_post_article_type($itemID)->slug . '_archive_page', 'option') ?>">
                            <?php _e('Alle tests', 'newheap') ?>
                            <div class="arrow-right pr-2 ml-20"></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
