<?php
$counter = 0;

$args = array(
    'post_type' => 'post',
    'posts_per_page' => 7,
    'category_name' => 'bedrijfsnieuws',
    'post__not_in' => [$header->ID]
);

$query = new \WP_Query($args);
?>

<section class="postloop mt-4">

    <div class="container-fluid">
        <h3 class="section-title">
            <?php _e('Bedrijfsnieuws', 'newheap') ?>
        </h3>
        <?php if ($query->have_posts()) : ?>
            <div class="nh-tile-holder">
                <?php while ($query->have_posts()) :
                    $counter++;
                    $query->the_post();

                    if($counter >= 8) { continue; }
                    $broadcast_data = ThreeWP_Broadcast()->get_post_broadcast_data( get_current_blog_id(), get_the_ID());
                    $parent = $broadcast_data->get_linked_parent();

                    if ( ! empty( $parent ) ) {
                        $current_blog_id = $parent['blog_id'];
                        $blog_name       = \NewHeap\Theme\Multisite\Multisite::get_site_name( $current_blog_id );
                        switch_to_blog( $current_blog_id );

                        if ( get_the_permalink( $parent['post_id'] ) ) {
                            $url = get_the_permalink( $parent['post_id'] );
                        } else {
                            $url = get_the_permalink();
                        }

                        restore_current_blog();
                    } else {
                        $current_blog_id = get_current_blog_id();
                        $blog_name       = \NewHeap\Theme\Multisite\Multisite::get_site_name();
                        $url             = get_the_permalink();
                    }

                    $theme_color = '';
                    $theme_title = '';

                    switch ($current_blog_id) {
                        case 1:
                            $theme_color = '#bc1413'; // mechaman
                            break;
                        case 2:
                            $theme_color = '#52ae32';  // landbouw mechanisatie
                            break;
                        case 5:
                            $theme_color = '#ffec00'; // tuin en park techniek
                            break;
                        case 7:
                            $theme_color = '#1bbbe9'; // veehouderij techniek
                            break;
                    }

                    $current_blog_details = get_blog_details( array( 'blog_id' => $parent['blog_id'] ) );

                    $input = get_the_excerpt();

                    $str = $input;

                    if (strlen($input) > 150) {
                        $str = explode("\n", wordwrap($input, 150));
                        $str = $str[0] . '...';
                    }

                    ?>

                    <div class="nh-tile theme-<?php if(empty($parent['blog_id'])) { ?>1<?php } else { ?><?php echo $parent['blog_id']; ?><?php } ?> <?php if ($counter < 2) { echo 'big-one'; } ?>" style="border-bottom: 0;">
                        <a class="post-item" href="<?php echo $url ?>">
                            <div class="relative post-item">
                                <div class="postloop-image-holder tile-image"
                                     style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium') ?>');"></div>

                                <?php if (!empty(get_field('premium_article'))) { ?>
                                    <div class="content-type text-white absolute">
                                        <span class="primary-background px-4 pl-3 py-2"><i
                                                    class="far fa-gem"></i> <?php _e('Premium', 'newheap') ?></span>
                                    </div>
                                <?php } ?>
                            </div>


                            <div class="text-black p-4" <?php echo ($counter < 2) ? 'style="background-color: rgba(188, 20, 19, 0.143);"' : ''; ?>>
                                    <span class="bold relative">
                                        <?php if (!empty($parent)) { ?>
                                            <?php echo $current_blog_details->blogname; ?>
                                        <?php } else { ?>
                                            <?php echo get_bloginfo( 'name' ); ?>
                                        <?php } ?> |
                                        <?php echo get_the_date('d-m-Y'); ?>
                                    </span>
                                <h2>
                                    <?php echo get_the_title() ?>
                                </h2>

                                <!--                                    <p class="mt-2 mb-0">-->
                                <!--                                        --><?php //echo $str;
                                ?>
                                <!--                                    </p>-->
                            </div>
                        </a>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>

        <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
            <a href="<?php echo get_field('companynews_archive_page', 'option'); ?>">
                <?php _e('Meer bedrijfsnieuws', 'newheap') ?>
                <div class="arrow-right pr-2 ml-20"></div>
            </a>
        </div>
    </div>
</section>
