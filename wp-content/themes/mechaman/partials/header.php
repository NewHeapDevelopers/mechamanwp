<?php
$args = array(
	'posts_per_page'      => 1,
	'post__in'            => get_option( 'sticky_posts' ),
	'ignore_sticky_posts' => 1,
    'tax_query' => [
        [
            'taxonomy' => 'artikel_type',
            'field' => 'slug',
            'operator' => 'NOT IN',
            'terms' => 'columns',
        ]
    ]
);
$header = new WP_Query( $args );

?>
    <div class="header">
        <div class="container">
            <div class="row mt-5">
                <div class="col-sm-12 col-md-8 theme-<?php echo get_current_blog_id(); ?> large-block">
					<?php if ( $header->have_posts() ) {
						while ( $header->have_posts() ) {
							$header->the_post();

                            $broadcast_data = ThreeWP_Broadcast()->get_post_broadcast_data( get_current_blog_id(), get_the_ID());
                            $parent = $broadcast_data->get_linked_parent();

                            if ( ! empty( $parent ) ) {
                                $current_blog_id = $parent['blog_id'];
                                $blog_name       = \NewHeap\Theme\Multisite\Multisite::get_site_name( $current_blog_id );
                                switch_to_blog( $current_blog_id );

                                if ( get_the_permalink( $parent['post_id'] ) ) {

                                    $url = get_the_permalink( $parent['post_id'] );
                                } else {
                                    $url = get_the_permalink();
                                }

                                restore_current_blog();
                            } else {
                                $current_blog_id = get_current_blog_id();
                                $blog_name       = \NewHeap\Theme\Multisite\Multisite::get_site_name();
                                $url             = get_the_permalink();
                            }

                            $theme_color = '';
                            $theme_title = '';

                            switch ($current_blog_id) {
                                case 1:
                                    $theme_color = '#bc1413';
                                    $theme_title = 'Mechaman';
                                    break;
                                case 2:
                                    $theme_color = '#52ae32';
                                    $theme_title = 'Landbouw Mechanisatie';
                                    break;
                                case 5:
                                    $theme_color = '#ffec00';
                                    $theme_title = 'Tuin en Park Techniek';
                                    break;
                                case 7:
                                    $theme_color = '#1bbbe9';
                                    $theme_title = 'Veehouderij Techniek';
                                    break;
                            }

							$_SESSION["ignore_post"] = get_the_ID();

							if ( has_post_thumbnail( get_the_ID() ) ) {
								$header_image = get_the_post_thumbnail_url( get_the_ID(), 'large' );
							} else {
								$header_image = get_field( 'placeholder_afbeelding', 'option' )['url'];
							}
							$current_blog_details = get_blog_details( array( 'blog_id' => $parent['blog_id'] ) );
							?>
                            <style>
                                .header .large-block .theme-<?php echo $parent['blog_id']; ?>.header-content h5:before {
                                    color: <?php echo $theme_color ?>;
                                }
                            </style>
                            <a href="<?php echo $url ?>">
                                <div class="header-image"
                                     style="background-image: url('<?php echo $header_image ?>'); background-size: cover; background-repeat: no-repeat; background-position: center;">
	                                <?php if (get_field('premium_article')) { ?>
                                        <div class="content-type text-white">
                                            <span class="primary-background px-4 pl-3 py-2"><i class="far fa-gem"></i> Premium</span>
                                        </div>
	                                <?php } ?>
                                </div>

                                <div class="header-content theme-<?php echo $parent['blog_id']; ?> p-3">
                                    <h5 class="pl-5 bold text-black">
                                        <?php if (!empty($parent)) : ?>
	                                        <?php echo $current_blog_details->blogname; ?>
                                        <?php else : ?>
	                                        <?php echo get_bloginfo( 'name' ); ?>
                                        <?php endif; ?>
                                    </h5>
                                    <h2><?php echo get_the_title() ?></h2>

                                    <p class="mt-2 text-black"><?php echo get_the_excerpt(); ?> <span
                                                class="bold text-black ml-2"><?php echo __( 'Lees meer',
												'newheap' ) ?></span></p>
                                </div>
                            </a>
						<?php }
						wp_reset_query();
					} ?>

                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="py-2 mb-2">
                        <div class="container">
                            <div class="row">
                                <h2 class="primary-color underline"><?php _e( 'Recent', 'newheap' ); ?></h2>
                            </div>
                        </div>
                    </div>
                    <div class="nh-tile-holder">
						<?php
						$i = 0;

						$args2 = array(
							'post_type'      => 'post',
							'posts_per_page' => 5,
							'post__not_in'   => array(
								$_SESSION["ignore_post"],
							),
						);

						$query = new WP_Query( $args2 );

						if ( $query->have_posts() ) {
							while ( $query->have_posts() ) {
								$query->the_post();

								$broadcast_data = ThreeWP_Broadcast()->get_post_broadcast_data( get_current_blog_id(),
									$post->ID );
								$parent         = $broadcast_data->get_linked_parent();

								if ( ! empty( $parent ) ) {
									$current_blog_id = $parent['blog_id'];
									$blog_name       = \NewHeap\Theme\Multisite\Multisite::get_site_name( $current_blog_id );
									switch_to_blog( $current_blog_id );

									if ( get_the_permalink( $parent['post_id'] ) ) {

										$url = get_the_permalink( $parent['post_id'] );
									} else {
										$url = get_the_permalink();
									}

									restore_current_blog();
								} else {
									$current_blog_id = get_current_blog_id();
									$blog_name       = \NewHeap\Theme\Multisite\Multisite::get_site_name();
									$url             = get_the_permalink();
								}

								$theme_color = '';

                                switch ($current_blog_id) {
                                    case 1:
                                        $theme_color = '#bc1413';
                                        break;
                                    case 2:
                                        $theme_color = '#52ae32';
                                        break;
                                    case 5:
                                        $theme_color = '#ffec00';
                                        break;
                                    case 7:
                                        $theme_color = '#1bbbe9';
                                        break;
                                }

								$input = get_the_excerpt();

								$str = $input;

								if ( strlen( $input ) > 120 ) {
									$str = explode( "\n", wordwrap( $input, 120 ) );
									$str = $str[0] . '...';
								}

								?>
                                <style>
                                    .header .nh-tile-holder .tile-content-holder.theme-<?php echo $parent['blog_id']; ?>:before {
                                        color: <?php echo $theme_color ?>;
                                    }
                                </style>
								<?php
								?>
                                <div class="nh-tile">
                                    <a class="block-link" href="<?php echo $url ?>">
                                     

                                        <div class="tile-content-holder theme-<?php echo $parent['blog_id']; ?> pl-5">
                                            <h2>
												<?php echo get_the_title() ?>
                                            </h2>

                                            <p class="mt-2 text-black">
												<?php echo $str; ?>
                                                <strong>
													<?php _e( 'Lees verder', 'newheap' ) ?>
                                                </strong>
                                            </p>
                                        </div>
                                    </a>
                                </div>
								<?php
							}
							$i ++;
						}
						?>

                        <a href="<?php echo get_field( 'archive_page', 'option' ); ?>" class="text-right bold">
							<?php _e( 'Meer recent', 'newheap' ) ?>
                            >
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php wp_reset_query();
