<div class="container opinie">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <h2 class="primary-color mb-4"><?php _e('Opinie', 'newheap'); ?></h2>
        </div>
        <div class="col-md-8">
            <?php
            wp_reset_query();
            $posts = new WP_Query([
	            'post_type' => 'post',
	            'posts_per_page' => 1,
	            'tax_query' => [
		            [
			            'taxonomy' => 'artikel_type',
			            'field'    => 'slug',
			            'terms'    => 'columns',
		            ]
	            ],
            ]);

            $opinieID = $posts->posts[0]->ID;

            $input = get_the_excerpt($opinieID);

            $str = $input;
            if (strlen($input) > 200) {
                $str = explode("\n", wordwrap($input, 200));
                $str = $str[0] . '...';
            }
            ?>
            <a class="block-link" href="<?php echo get_the_permalink($opinieID) ?>" class="post-item">

                <div class="row bg-red">
                    <div class="col-sm-12 col-md-6">
                        <div class="column-image-holder object-fit-fix"
                             style="background-image: url('<?php echo get_the_post_thumbnail_url($opinieID, 'medium_large') ?>'); background-size: cover; background-repeat: no-repeat; background-position: center;">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="text-black p-3 yellow-thick-border">
                            <h5 class="p-2 mb-0 pt-3 pb-0">
                                <span class="from-site from-site-code-<?php echo get_current_blog_id(); ?>">
                                    <?php echo bloginfo('name'); ?> | <?php echo get_the_date('d-m-Y', $opinieID); ?><br/>
                                </span>
                            </h5>
                            <h2 class=""><?php echo get_the_title($opinieID); ?></h2>


                            <p class="mt-2">
                                <?php echo $str; ?>
                            </p>
                        </div>
                    </div>
                </div>
            </a>

            <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
                <a href="<?php echo get_field('columns_archive_page', 'option'); ?>">
                    <?php _e('Meer columns', 'newheap') ?>
                    <div class="arrow-right pr-2 ml-20"></div>
                </a>
            </div>
        </div>

        <div class="col-md-4">
            <?php include get_template_directory() . '/partials/latest-poll.php'; ?>

            <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
                <a href="<?=site_url()?>/polls/">
                    <?php _e('Uitslagen vorige polls', 'newheap') ?>
                    <div class="arrow-right pr-2 ml-20"></div>
                </a>
            </div>
        </div>
    </div>
</div>
