<section class="postloop">
    <div class="container">
        <div class="nh-tile-holder row">
            <?php
            wp_reset_query();

            $vakblad = get_field('in_het_vakblad');

            if (!empty( $vakblad )) {
                foreach($vakblad as $blad) {
                    $item = $blad['vakblad_item'];
                    $itemID = url_to_postid($item);

                    $input = get_the_excerpt($itemID);

                    $str = $input;

                    if( strlen( $input) > 100 ) {
                        $str = explode( "\n", wordwrap( $input, 100));
                        $str = $str[0] . '...';
                    }

                    ?>
                    <div class="col-sm-12 col-md-3">
                        <a href="<?php echo get_the_permalink($itemID) ?>" class="post-item">
                            <div class="postloop-image-holder relative"
                                 style="background-image: url('<?php echo get_the_post_thumbnail_url($itemID, 'large') ?>'); background-size: cover; background-repeat: no-repeat; background-position: center;">
                                <?php if (!empty(get_field($itemID, 'premium_article'))) { ?>
                                    <div class="content-type text-white absolute">
                                        <span class="primary-background px-4 pl-3 py-2"><i
                                                    class="far fa-gem"></i> <?php _e('Premium','newheap') ?></span>
                                    </div>
                                <?php } ?>
                            </div>

                            <div class="grey-border-bottom-right text-black p-3">
                                <span class="bold">
                                    <?php echo get_the_date('d-m-Y'); ?>
                                    |

                                </span>
                                <h2 class=""><?php echo get_the_title($itemID); ?></h2>

                                <p class="mb-0">
	                                <?php echo $str; ?>
                                </p>
                            </div>
                        </a>
                    </div>
                <?php }
            } ?>
        </div>
    </div>
</section>
