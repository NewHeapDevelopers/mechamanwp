<section class="multimedia my-4 mb-5">
    <div class="container">
        <h3 class="section-title">
            <?php _e('Multimedia', 'newheap'); ?>
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="row multi">
                    <?php
                    // Get the current blog id
                    $original_blog_id = get_current_blog_id();

                    // All the blog_id's to loop through
                    $bids = array(1, 2, 5, 7);

                    ?>
                    <?php


                    $i = 0;
                    foreach ($bids as $bid) :
                        // Switch to the blog with the blog_id $bid
                        switch_to_blog($bid);

                        $myposts = get_posts(
                            array(
                                'post_type' => 'post',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'artikel_type',
                                        'field'    => 'slug',
                                        'terms'    => array(
//                                            'artikelen',
                                            'fotoreportages',
                                            'infographics',
                                            'podcasts',
                                            'videos',
                                            ),
                                    ),
                                ),
                                'posts_per_page' => 1,
                            )
                        );

                        foreach ($myposts as $mypost) :
                            $theme_color = '';
                            $theme_title = '';

                            switch ($bid) {
                                case 1:
                                    $theme_color = '#bc1413';
                                    $theme_title = 'Mechaman';
                                    break;
                                case 2:
                                    $theme_color = '#52ae32';
                                    $theme_title = 'LandbouwMechanisatie';
                                    break;
                                case 5:
                                    $theme_color = '#ffec00';
                                    $theme_title = 'Tuin en Park Techniek';
                                    break;
                                case 7:
                                    $theme_color = '#1bbbe9';
                                    $theme_title = 'Veehouderij Techniek';
                                    break;
                            }

                            $input = get_the_excerpt($mypost->ID);

                            $str = $input;

                            if (strlen($input) > 150) {
                                $str = explode("\n", wordwrap($input, 150));
                                $str = $str[0] . '...';
                            }


                            ?>
                        <style>
                            .multi .theme-icon-<?php echo $bid; ?> .content-title  h5:before {
                                color: <?php echo $theme_color ?>;
                                content: "●";
                                top: -8px;
                                font-size: 1.5em;
                                left: -5px !important;
                                position: relative;
                            }
                        </style>

                            <div class="col-sm-12 col-md-4 relative pb-2 multi-item">
                                <a class="block-link" href="<?php echo get_the_permalink($mypost->ID) ?>">
                                    <div class="relative theme-icon-<?php echo $bid; ?>">
                                        <div class="image-holder multimedia">
                                            <div class="bg-image" style="background-image: url(<?php echo get_the_post_thumbnail_url($mypost->ID, 'medium'); ?>);"></div>
                                            <img src="<?php echo get_the_post_thumbnail_url($mypost->ID, 'medium'); ?>" width="100%"
                                                 style="visibility: hidden;" height="auto"/>

                                            <div class="absolute bg-black text-white px-4 py-3 image-icon">
                                                <?php echo \NewHeap\Theme\Helpers::get_type_icon($mypost->ID); ?>
                                            </div>
                                        </div>

                                        <?php if (get_field('premium_article', $mypost->ID)) { ?>
                                            <div class="content-type absolute">
                                                <span class="px-3 py-1"><i
                                                            class="far fa-gem"></i> <?php _e('Premium', 'newheap'); ?></span>
                                            </div>
                                        <?php } ?>

                                        <div class="content-title text-white text-black w-100">
                                            <h5 class="p-2 bg-white mb-0 pt-3 pb-0">
                                                <?php echo $theme_title; ?> | <?php echo get_the_date('d-m-Y', $mypost->ID); ?><br/>
                                            </h5>
                                            <h2><?php echo get_the_title($mypost->ID); ?></h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach;
                        restore_current_blog($original_blog_id);
                    endforeach;
                    ?>

                    <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
                        <a href="<?php echo get_field('multimedia_archive_page', 'option'); ?>">
                            <?php _e('Meer multimedia', 'newheap') ?>
                            <div class="arrow-right pr-2 ml-20"></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    </div>
</section>
