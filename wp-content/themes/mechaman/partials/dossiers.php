<?php
$dossiers = get_terms([
    'taxonomy' => 'dossier',
    'hide_empty' => true,
    'number' => 5,
    'meta_query' => array(
        'relation' => 'OR',
        array(
            'key'     => 'do_not_show',
            'value'   => '0',
            'compare' => '=',
        ),
        array(
            'key' => 'do_not_show',
            'compare' => 'NOT EXISTS'
        ),
    )
]);

if (!empty($dossiers)) : ?>
    <section class="dossiers my-4">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-8">
                    <h3 class="section-title">
                        <?php _e('Dossiers', 'newheap'); ?>
                    </h3>
                    <div class="row">

                        <?php
                        $counter = 0;

                        foreach ($dossiers as $key => $dossier) : $counter++; ?>
                            <?php if ($counter === 1) : ?>
                                <?php
                                $dossier_image = get_field('term_image', $dossier);

                                if (!empty($dossier_image)) {
                                    $dossier_image = get_field('term_image', $dossier)['sizes']['medium_large'];
                                } else {
                                    $dossier_image = get_field('placeholder_afbeelding', 'option')['sizes']['medium_large'];
                                }
                                ?>
                                <div class="col-sm-12 col-md-6">
                                    <a href="<?php echo get_term_link($dossier); ?>" class="first-dossier-item">
                                        <div class="image-media"
                                             style="background-image: url('<?php echo $dossier_image; ?>');"></div>
                                        <div class="content-title text-white text-black d-flex bg-white"
                                             style="bottom: 20%;">
                                            <div class="bg-black-trans d-flex w-100">
                                                <h5 class="mb-0 w-100">
                                                    <span class="">
                                                        <i class="fas fa-folder-open"></i>
                                                    </span>
                                                </h5>
                                            </div>
                                            <div class="dossier-info">
                                                <h2 class="bold px-2 pt-2  mb-0 ">
                                                    <?php _e('Dossier', 'newheap') ?>: <?php echo $dossier->name ?>
                                                </h2>
                                            </div>
                                        </div>
                                    </a>

                                    <?php $args = array(
                                        'post_type' => 'post',
                                        'posts_per_page' => 2,
                                        'tax_query' => array(
                                            array(
                                                'taxonomy' => 'dossier',
                                                'field' => 'slug',
                                                'terms' => $dossier->slug,
                                            ),
                                        ),
                                    );
                                    $query = new WP_Query($args);

                                    if ($query->have_posts()) {
                                        while ($query->have_posts()) {
                                            $query->the_post();

	                                        $broadcast_data = ThreeWP_Broadcast()->get_post_broadcast_data( get_current_blog_id(), get_the_ID() );
	                                        $parent = $broadcast_data->get_linked_parent();

	                                        if (!empty($parent)) {
		                                        $current_blog_id = $parent['blog_id'];
		                                        $blog_name = \NewHeap\Theme\Multisite\Multisite::get_site_name($current_blog_id);

		                                        switch_to_blog( $current_blog_id );
		                                        $url = get_the_permalink($parent['post_id']);
		                                        restore_current_blog();
	                                        } else {
		                                        $current_blog_id = get_current_blog_id();
		                                        $blog_name = \NewHeap\Theme\Multisite\Multisite::get_site_name();
		                                        $url = get_the_permalink();
                                            }
	                                        ?>
                                            <a href="<?php echo $url; ?>">
                                                <div class="theme-content-item">
                                                    <div class="pb-3">
                                                        <h5 class="bold text-black date-holder">
                                                            <span class="from-site from-site-code-<?php echo $current_blog_id; ?>">
                                                                <?php echo $blog_name; ?>
                                                                |
                                                                <?php echo get_the_date('d-m-Y'); ?>
                                                            </span>
                                                        </h5>

                                                        <h3>
                                                            <?php echo get_the_title() ?>
                                                        </h3>
                                                    </div>
                                                </div>
                                            </a>
                                        <?php }
                                    }
                                    wp_reset_query();
                                    ?>

                                    <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
                                        <a href="<?php echo get_term_link($dossier); ?>">
			                                <?php _e('Meer uit dit dossier', 'newheap') ?>
                                            <div class="arrow-right pr-2 ml-20"></div>
                                        </a>
                                    </div>

                                </div>
                            <?php else : ?>
                                <?php if ($counter === 2) : ?>
                                    <div class="col-sm-12 col-md-6">
                                <?php endif; ?>

                                <div class="primary-background-trans mb-3">
                                    <a href="<?php echo get_term_link($dossier); ?>" class="dossier-item">
                                        <h5 class="bold text-black">
                                            <i class="fas fa-folder-open primary-color"></i>
                                            <?php echo $dossier->name ?>
                                        </h5>
                                        <p class="sub-title">
                                            <strong>
	                                            <?php _e('Nieuwste bericht op:', 'newheap'); ?><br/>
                                            </strong>
                                            <?php echo \NewHeap\Theme\Helpers::get_taxonomy_latest_date($dossier); ?>
                                        </p>
                                    </a>
                                </div>

                                <?php if ($counter === count($dossiers)) : ?>
                                    <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
                                        <a href="<?= site_url() ?>/dossiers/">
                                            <?php _e('Meer dossiers', 'newheap') ?>
                                            <div class="arrow-right pr-2 ml-20"></div>
                                        </a>
                                    </div>


                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="py-2 mb-2 mt-0">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="section-title"><?php _e('Trekkerverkoopcijfers', 'newheap'); ?></h3>
                                    <?php
                                    $args = [
                                        'post_type' => 'page',
                                        'fields' => 'ids',
                                        'nopaging' => true,
                                        'meta_key' => '_wp_page_template',
                                        'meta_value' => 'template-trekker-sales.php'
                                    ];
                                    $pages = get_posts($args);
                                    ?>

                                    <a href="<?php echo get_the_permalink($pages[0]); ?>" class="post-holder">
                                        <div class="post-image">
                                            <img src="<?php echo get_the_post_thumbnail_url($pages[0], 'medium_large'); ?>">
                                        </div>

                                        <h3><?php echo get_the_title($pages[0]); ?></h3>
                                        <p><?php echo get_the_excerpt($pages[0]); ?></p>

                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
                            <a href="<?php echo get_the_permalink($pages[0]); ?>">
                                <?php _e('Meer verkoopcijfers', 'newheap') ?>
                                <div class="arrow-right pr-2 ml-20"></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif;
wp_reset_query();
