<?php
// Get the current blog id
$original_blog_id = get_current_blog_id();

// All the blog_id's to loop through
$bids = array(2);
?>
<div class="container news-items mt-5 news-lm">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <h2 class="primary-color mb-4"><?php _e('Nieuws uit LandbouwMechanisatie', 'newheap'); ?></h2>
        </div>
    </div>
    <div class="row">



                <?php

                $i = 0;
                foreach ($bids

                as $bid) {
                // Switch to the blog with the blog_id $bid
                switch_to_blog($bid);

                $myposts = get_posts(
                    array(
                        'post_type' => 'post',
                        'posts_per_page' => 6,
                    )
                );

                foreach ($myposts

                as $mypost) {
                $theme_color = '';
                $theme_title = '';

                switch ($bid) {
                    case 1:
                        $theme_color = '#bc1413';
                        $theme_title = 'Mechaman';
                        break;
                    case 2:
                        $theme_color = '#52ae32';
                        $theme_title = 'Landbouw Mechanisatie';
                        break;
                    case 5:
                        $theme_color = '#ffec00';
                        $theme_title = 'Tuin en Park Techniek';
                        break;
                    case 7:
                        $theme_color = '#1bbbe9';
                        $theme_title = 'Veehouderij Techniek';
                        break;
                }

                $input = get_the_excerpt($mypost->ID);

                $str = $input;

                if (strlen($input) > 150) {
                    $str = explode("\n", wordwrap($input, 150));
                    $str = $str[0] . '...';
                }

                ?>

            <div class="col-sm-12 col-md-6">
                <div class="theme-content-item pt-3" <?php if($i < 2) { echo 'style="border-top: 1px solid black;"'; } ?>>
                <div class="row ">
                <div class="col-sm-12 col-md-2  py-2">
                    <div class="mt-3">

                        <a href="<?php the_permalink($mypost->ID); ?>">
                            <div class="image-holder">
                                <img src="<?php echo get_the_post_thumbnail_url($mypost->ID, 'thumbnail'); ?>" width="100%"
                                        height="auto"/>
                            </div>
                        </a>

                        <?php if (get_field($mypost->ID, 'premium_article')) { ?>
                            <div class="content-type text-black absolute">
                                        <span class="primary-background px-3 py-1"><i
                                                    class="far fa-gem"></i></span>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-md-10 col-sm-12">
                    <div class="p-3 mb-1">
                        <a class="theme-icon-<?php echo $bid; ?>" href="<?php the_permalink($mypost->ID); ?>">
                            <h5 class="pl-5 bold text-black">
                                <?php echo $theme_title; ?> | <?php echo get_the_date('d-m-Y', $mypost->ID); ?>
                            </h5>

                            <h2>
                                <?php echo get_the_title($mypost->ID) ?>
                            </h2>
                        </a>
                    </div>
                </div>
            </div>
            </div>

        </div>
            <?php $i++; }
            restore_current_blog($original_blog_id);
            } ?>



</div>
    <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
        <a href="<?php echo get_site_url(\NewHeap\Theme\Multisite\Multisite::SITE_LANDBOUW_MECHANISATIE); ?>/artikelen/">
            <?php _e('Meer uit LandbouwMechanisatie', 'newheap') ?>
            <div class="arrow-right pr-2 ml-20"></div>
        </a>
    </div>
</div>
