<?php
$sold_per_year = get_field('verkocht_per_jaar', 'option');
$sold_per_brand = get_field('verkocht_per_merk', 'option');
$sold_total = get_field('totaal_trekkers_verkocht_per_jaar', 'option');
?>
<section class="trekker-sales">
    <div class="container">
        <div role="main" id="content">
            <h2>Totaal aantal verkochte trekkers per maand 2011-2020</h2>

            <div style="position:relative; width:100%; display:inline-block; overflow:hidden">
                <canvas style="position:relative;" id="canvas1" height="400" width="600"></canvas>
            </div>

            <div style="margin:0 auto; width:95%; margin-bottom:30px; border-bottom:1px solid #ddd; padding-bottom:10px; text-align:center;">
                <div style="float:left; border-top:10px solid #f4d0d0; padding:3px 2%; width:6%">'11</div>
                <div style="float:left; border-top:10px solid #eabfbf; padding:3px 2%; width:6%">'12</div>
                <div style="float:left; border-top:10px solid #d47f7f; padding:3px 2%; width:6%">'13</div>
                <div style="float:left; border-top:10px solid #bf4040; padding:3px 2%; width:6%">'14</div>
                <div style="float:left; border-top:10px solid #aa0000; padding:3px 2%; width:6%">'15</div>
                <div style="float:left; border-top:10px solid #920000; padding:3px 2%; width:6%">'16</div>
                <div style="float:left; border-top:10px solid #780000; padding:3px 2%; width:6%">'17</div>
                <div style="float:left; border-top:10px solid #5e0000; padding:3px 2%; width:6%">'18</div>
                <div style="float:left; border-top:10px solid #450000; padding:3px 2%; width:6%">'19</div>
                <div style="float:left; border-top:10px solid #000000; margin-left:1px; padding:3px 2%; width:5%">2020
                </div>

                <div style="clear:left;"></div>
            </div>


            <h2>Totaal aantal verkochte trekkers per merk 2006-2018</h2>

            <div style="position:relative; width:100%; display:inline-block; overflow:hidden">
                <canvas style="position:relative;" id="canvas2" height="500" width="600"></canvas>
            </div>

            <div style="margin-bottom:20px; border-bottom:1px solid #ddd; padding-bottom:10px; padding-left:20px;">
                <div style="float:left;  width:50%;">
                    <div style="border-left:10px solid rgba(30, 30, 30, 1); padding:3px 2%;">Case-IH/Steyr</div>
                    <div style="border-left:10px solid rgba(180, 198, 24, 1); padding:3px 2%;">Claas</div>
                    <div style="border-left:10px solid rgba(111, 176, 46, 1); padding:3px 2%;">Deutz-Fahr</div>
                    <div style="border-left:10px solid rgba(99, 141, 55, 1); padding:3px 2%;">Fendt</div>
                    <div style="border-left:10px solid rgba(250, 213, 2, 1); padding:3px 2%;">John Deere</div>
                    <div style="border-left:10px solid rgba(255, 109, 27, 1); padding:3px 2%;">Kubota</div>
                    <div style="border-left:10px solid rgba(238, 238, 238, 1); padding:3px 2%;">Lamborghini</div>
                    <div style="border-left:10px solid rgba(0, 148, 210, 1); padding:3px 2%;">Landini</div>
                </div>
                <div style="float:left; width:50%;">
                    <div style="border-left:10px solid rgba(204, 0, 0, 1); padding:3px 2%;">Massey Ferguson</div>
                    <div style="border-left:10px solid rgba(204, 204, 204, 1); padding:3px 2%;">McCormick</div>
                    <div style="border-left:10px solid rgba(0, 37, 119, 1); padding:3px 2%;">New Holland</div>
                    <div style="border-left:10px solid rgba(170, 170, 170, 1); padding:3px 2%;">Same</div>
                    <div style="border-left:10px solid rgba(234, 191, 191, 1); padding:3px 2%;">Steyr</div>
                    <div style="border-left:10px solid rgba(255, 0, 19, 1); padding:3px 2%;">Valtra</div>
                    <div style="border-left:10px solid rgba(100, 100, 100, 1); padding:3px 2%;">Zetor</div>
                </div>
                <div style="clear:left;"></div>
            </div>


            <h2>Totaal aantal verkochte trekkers 2007-2019</h2>

            <div style="position:relative; width:100%; display:inline-block; overflow:hidden;">
                <canvas style="position:relative;" id="canvas3" height="400" width="600"></canvas>
            </div>

            <script src="https://www.mechaman.nl/images/Chart.min.js"></script>
            <script>

                var lineChartData = {
                    labels: ['jan', 'feb', 'mrt', 'apr', 'mei', 'jun', 'jul', 'aug', 'sep', 'okt', 'nov', 'dec'],
                    datasets: [
                        {
                            label: "2011",
                            fillColor: "rgba(244, 208, 208, 0.2)",
                            strokeColor: "#f4d0d0",
                            pointColor: "#f4d0d0",
                            pointStrokeColor: "#f4d0d0",
                            pointHighlightFill: "#f4d0d0",
                            pointHighlightStroke: "#f4d0d0",
                            data: [<?php echo $sold_per_year[4]['aantal_verkocht']; ?>]
                        },
                        {
                            label: "2012",
                            fillColor: "rgba(212, 127, 127, 0.2)",
                            strokeColor: "#eabfbf",
                            pointColor: "#eabfbf",
                            pointStrokeColor: "#eabfbf",
                            pointHighlightFill: "#eabfbf",
                            pointHighlightStroke: "#eabfbf",
                            data: [<?php echo $sold_per_year[5]['aantal_verkocht']; ?>]
                        },
                        {
                            label: "2013",
                            fillColor: "rgba(230, 64, 64, 0.2)",
                            strokeColor: "#d47f7f",
                            pointColor: "#d47f7f",
                            pointStrokeColor: "#d47f7f",
                            pointHighlightFill: "#d47f7f",
                            pointHighlightStroke: "#d47f7f",
                            data: [<?php echo $sold_per_year[6]['aantal_verkocht']; ?>]
                        },
                        {
                            label: "2014",
                            fillColor: "rgba(212, 0, 0, 0.2)",
                            strokeColor: "#bf4040",
                            pointColor: "#bf4040",
                            pointStrokeColor: "#bf4040",
                            pointHighlightFill: "#bf4040",
                            pointHighlightStroke: "#bf4040",
                            data: [<?php echo $sold_per_year[7]['aantal_verkocht']; ?>]
                        },
                        {
                            label: "2015",
                            fillColor: "rgba(191, 0, 0, 0.2)",
                            strokeColor: "#aa0000",
                            pointColor: "#aa0000",
                            pointStrokeColor: "#aa0000",
                            pointHighlightFill: "#aa0000",
                            pointHighlightStroke: "#aa0000",
                            data: [<?php echo $sold_per_year[8]['aantal_verkocht']; ?>]
                        },
                        {
                            label: "2016",
                            fillColor: "rgba(170, 0, 0, 0.2)",
                            strokeColor: "#920000",
                            pointColor: "#920000",
                            pointStrokeColor: "#920000",
                            pointHighlightFill: "#920000",
                            pointHighlightStroke: "#920000",
                            data: [<?php echo $sold_per_year[9]['aantal_verkocht']; ?>]
                        },
                        {
                            label: "2017",
                            fillColor: "rgba(120, 0, 0, 0.2)",
                            strokeColor: "#780000",
                            pointColor: "#780000",
                            pointStrokeColor: "#780000",
                            pointHighlightFill: "#780000",
                            pointHighlightStroke: "#780000",
                            data: [<?php echo $sold_per_year[10]['aantal_verkocht']; ?>]
                        },
                        {
                            label: "2018",
                            fillColor: "rgba(94, 0, 0, 0.2)",
                            strokeColor: "#5e0000",
                            pointColor: "#5e0000",
                            pointStrokeColor: "#5e0000",
                            pointHighlightFill: "#5e0000",
                            pointHighlightStroke: "#5e0000",
                            data: [<?php echo $sold_per_year[11]['aantal_verkocht']; ?>]
                        },
                        {
                            label: "2019",
                            fillColor: "rgba(69, 0, 0, 0.2)",
                            strokeColor: "#450000",
                            pointColor: "#450000",
                            pointStrokeColor: "#450000",
                            pointHighlightFill: "#450000",
                            pointHighlightStroke: "#450000",
                            data: [<?php echo $sold_per_year[12]['aantal_verkocht']; ?>]
                        },
                        {
                            label: "2020",
                            fillColor: "rgba(25, 0, 0, 0.2)",
                            strokeColor: "#000000",
                            pointColor: "#000000",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#000000",
                            pointHighlightStroke: "#000000",
                            data: [<?php echo $sold_per_year[13]['aantal_verkocht']; ?>]
                        }
                    ]

                }


                var lineChartData2 = {
                    labels: ['2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018'],
                    datasets: [
                        {
                            label: "Case-IH/Steyr",
                            fillColor: "rgba(30, 30, 30, 0)",
                            strokeColor: "rgba(30, 30, 30, 1)",
                            pointColor: "rgba(30, 30, 30, 1)",
                            pointStrokeColor: "rgba(30, 30, 30, 1)",
                            pointHighlightFill: "rgba(30, 30, 30, 1)",
                            pointHighlightStroke: "rgba(30, 30, 30, 1)",
                            data: [<?php echo $sold_per_brand[0]['aantal_verkocht'] ?>]
                        },
                        {
                            label: "Claas",
                            fillColor: "rgba(180, 198, 24, 0)",
                            strokeColor: "rgba(180, 198, 24, 1)",
                            pointColor: "rgba(180, 198, 24, 1)",
                            pointStrokeColor: "rgba(180, 198, 24, 1)",
                            pointHighlightFill: "rgba(180, 198, 24, 1)",
                            pointHighlightStroke: "rgba(180, 198, 24, 1)",
                            data: [<?php echo $sold_per_brand[1]['aantal_verkocht'] ?>]
                        },
                        {
                            label: "Deutz-Fahr",
                            fillColor: "rgba(111, 176, 46, 0)",
                            strokeColor: "rgba(111, 176, 46, 1)",
                            pointColor: "rgba(111, 176, 46, 1)",
                            pointStrokeColor: "rgba(111, 176, 46, 1)",
                            pointHighlightFill: "rgba(111, 176, 46, 1)",
                            pointHighlightStroke: "rgba(111, 176, 46, 1)",
                            data: [<?php echo $sold_per_brand[2]['aantal_verkocht'] ?>]
                        },
                        {
                            label: "Fendt",
                            fillColor: "rgba(99, 141, 55, 0)",
                            strokeColor: "rgba(99, 141, 55, 1)",
                            pointColor: "rgba(99, 141, 55, 1)",
                            pointStrokeColor: "rgba(99, 141, 55, 1)",
                            pointHighlightFill: "rgba(99, 141, 55, 1)",
                            pointHighlightStroke: "rgba(99, 141, 55, 1)",
                            data: [<?php echo $sold_per_brand[3]['aantal_verkocht'] ?>]
                        },
                        {
                            label: "John Deere",
                            fillColor: "rgba(250, 213, 2, 0)",
                            strokeColor: "rgba(250, 213, 2, 1)",
                            pointColor: "rgba(250, 213, 2, 1)",
                            pointStrokeColor: "rgba(250, 213, 2, 1)",
                            pointHighlightFill: "rgba(250, 213, 2, 1)",
                            pointHighlightStroke: "rgba(250, 213, 2, 1)",
                            data: [<?php echo $sold_per_brand[4]['aantal_verkocht'] ?>]
                        },
                        {
                            label: "Kubota",
                            fillColor: "rgba(255, 109, 27, 0)",
                            strokeColor: "rgba(255, 109, 27, 1)",
                            pointColor: "rgba(255, 109, 27, 1)",
                            pointStrokeColor: "rgba(255, 109, 27, 1)",
                            pointHighlightFill: "rgba(255, 109, 27, 1)",
                            pointHighlightStroke: "#eabfbf",
                            data: [<?php echo $sold_per_brand[5]['aantal_verkocht'] ?>]
                        },
                        {
                            label: "Lamborghini",
                            fillColor: "rgba(238, 238, 238, 0)",
                            strokeColor: "rgba(238, 238, 238, 1)",
                            pointColor: "#rgba(238, 238, 238, 1)",
                            pointStrokeColor: "rgba(238, 238, 238, 1)",
                            pointHighlightFill: "rgba(238, 238, 238, 1)",
                            pointHighlightStroke: "rgba(238, 238, 238, 1)",
                            data: [<?php echo $sold_per_brand[6]['aantal_verkocht'] ?>]
                        },
                        {
                            label: "Landini",
                            fillColor: "rgba(0, 148, 210, 0)",
                            strokeColor: "rgba(0, 148, 210, 1)",
                            pointColor: "rgba(0, 148, 210, 1)",
                            pointStrokeColor: "rgba(0, 148, 210, 1)",
                            pointHighlightFill: "rgba(0, 148, 210, 1)",
                            pointHighlightStroke: "rgba(0, 148, 210, 1)",
                            data: [<?php echo $sold_per_brand[7]['aantal_verkocht'] ?>]
                        },
                        {
                            label: "Massey Ferguson",
                            fillColor: "rgba(204, 0, 0, 0)",
                            strokeColor: "rgba(204, 0, 0, 1)",
                            pointColor: "rgba(204, 0, 0, 1)",
                            pointStrokeColor: "rgba(204, 0, 0, 1)",
                            pointHighlightFill: "rgba(204, 0, 0, 1)",
                            pointHighlightStroke: "rgba(204, 0, 0, 1)",
                            data: [<?php echo $sold_per_brand[8]['aantal_verkocht'] ?>]
                        },
                        {
                            label: "McCormick",
                            fillColor: "rgba(204, 204, 204, 0)",
                            strokeColor: "rgba(204, 204, 204, 1)",
                            pointColor: "rgba(204, 204, 204, 1)",
                            pointStrokeColor: "rgba(204, 204, 204, 1)",
                            pointHighlightFill: "rgba(204, 204, 204, 1)",
                            pointHighlightStroke: "rgba(204, 204, 204, 1)",
                            data: [<?php echo $sold_per_brand[9]['aantal_verkocht'] ?>]
                        },
                        {
                            label: "New Holland",
                            fillColor: "rgba(0, 37, 119, 0)",
                            strokeColor: "rgba(0, 37, 119, 1)",
                            pointColor: "rgba(0, 37, 119, 1)",
                            pointStrokeColor: "rgba(0, 37, 119, 1)",
                            pointHighlightFill: "rgba(0, 37, 119, 1)f",
                            pointHighlightStroke: "rgba(0, 37, 119, 1)",
                            data: [<?php echo $sold_per_brand[10]['aantal_verkocht'] ?>]
                        },
                        {
                            label: "Same",
                            fillColor: "rgba(170, 170, 170, 0)",
                            strokeColor: "rgba(170, 170, 170, 1)",
                            pointColor: "rgba(170, 170, 170, 1)",
                            pointStrokeColor: "rgba(170, 170, 170, 1)",
                            pointHighlightFill: "rgba(170, 170, 170, 1)",
                            pointHighlightStroke: "rgba(170, 170, 170, 1)",
                            data: [<?php echo $sold_per_brand[11]['aantal_verkocht'] ?>]
                        }
                        ,
                        {
                            label: "Steyr",
                            fillColor: "rgba(234, 191, 191, 0)",
                            strokeColor: "#eabfbf",
                            pointColor: "#eabfbf",
                            pointStrokeColor: "#444",
                            pointHighlightFill: "#eabfbf",
                            pointHighlightStroke: "#eabfbf",
                            data: [<?php echo $sold_per_brand[12]['aantal_verkocht'] ?>]
                        }
                        ,
                        {
                            label: "Valtra",
                            fillColor: "rgba(255, 0, 19, 0)",
                            strokeColor: "rgba(255, 0, 19, 1)",
                            pointColor: "rgba(255, 0, 19, 1)",
                            pointStrokeColor: "rgba(255, 0, 19, 1)",
                            pointHighlightFill: "rgba(255, 0, 19, 1)",
                            pointHighlightStroke: "rgba(255, 0, 19, 1)",
                            data: [<?php echo $sold_per_brand[12]['aantal_verkocht'] ?>]
                        }
                        ,
                        {
                            label: "Zetor",
                            fillColor: "rgba(100, 100, 100, 0)",
                            strokeColor: "rgba(100, 100, 100, 1)",
                            pointColor: "rgba(100, 100, 100, 1)",
                            pointStrokeColor: "rgba(100, 100, 100, 1)",
                            pointHighlightFill: "rgba(100, 100, 100, 1)",
                            pointHighlightStroke: "rgba(100, 100, 100, 1)",
                            data: [<?php echo $sold_per_brand[13]['aantal_verkocht'] ?>]
                        }
                    ]


                }


                var lineChartData3 = {
                    labels: ['2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019'],
                    datasets: [
                        {
                            label: "Jaartotaal",
                            fillColor: "rgba(30, 30, 30, 0)",
                            strokeColor: "rgba(30, 30, 30, 1)",
                            pointColor: "rgba(30, 30, 30, 1)",
                            pointStrokeColor: "rgba(30, 30, 30, 1)",
                            pointHighlightFill: "rgba(30, 30, 30, 1)",
                            pointHighlightStroke: "rgba(30, 30, 30, 1)",
                            data: [<?php echo $sold_total ?>]
                        }

                    ]


                }


                window.onload = function () {
                    var ctx = document.getElementById("canvas1").getContext("2d");
                    window.myLine = new Chart(ctx).Line(lineChartData, {
                        bezierCurve: false,
                        responsive: true,
                        datasetStrokeWidth: 2,
                        animation: false,
                        maintainAspectRatio: true,
                    });

                    var ctx2 = document.getElementById("canvas2").getContext("2d");
                    window.myLine = new Chart(ctx2).Line(lineChartData2, {
                        bezierCurve: false,
                        responsive: true,
                        datasetStrokeWidth: 2,
                        pointDotRadius: 4,
                        animation: false,
                        maintainAspectRatio: true,
                    });

                    var ctx3 = document.getElementById("canvas3").getContext("2d");
                    window.myLine = new Chart(ctx3).Line(lineChartData3, {
                        bezierCurve: false,
                        responsive: true,
                        datasetStrokeWidth: 2,
                        pointDotRadius: 4,
                        animation: false,
                        maintainAspectRatio: true,
                    });


                }
            </script>

        </div><!-- #content -->
    </div><!-- #container -->
</section>
