<?php
/**
 * Template Name: Abonnementen overzicht
 */

$subscriptions = get_field('abonnementen', 'options');

get_header(); ?>

<div class="subscription-overview">
    <?php if (has_post_thumbnail()) : $thumbnail_url = get_the_post_thumbnail_url(get_the_ID(), 'large'); ?>
        <div class="header">
            <div class="header-image">
                <img src="<?php echo $thumbnail_url; ?>" width="100%" height="100%" alt="<?php echo $image_alt; ?>"/>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-8 offset-md-2">
                        <div class="header-content">
                            <div class="article-content-bar">
                                <h1>
                                    <?php the_title(); ?>
                                </h1>

                                <?php if (has_excerpt()) : ?>
                                    <p class="mt-2">
                                        <?php the_excerpt(); ?>
                                    </p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if (!empty($subscriptions)) : ?>
        <div class="subscriptions-holder">
            <div class="container">
                <div class="row">
	                <?php foreach ($subscriptions as $subscription) : ?>
                        <div class="col-md-4">
                            <div class="subscription">
                                <h2><?php echo $subscription['naam']; ?></h2>
                                <p><?php echo $subscription['omschrijving']; ?></p>

                                <ul>
		                            <?php foreach($subscription['eigenschap'] as $property) : ?>
                                        <li><?php echo $property['beschrijving']; ?></li>
                                    <?php endforeach; ?>
                                </ul>

                                <a href="<?php echo $subscription['aanmeld_pagina']; ?>" class="cta-2">
                                    <?php _e('Account aanmaken', 'newheap'); ?>
                                </a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="container">
        <div class="mb-5">
            <div class="row">
                <div class="col-md-8">
                    <?php the_content(); ?>
                </div>

                <div class="col-md-4">
                    <div class="subscription-cta">
                        <h2>
                            <?php _e('Heeft u een vraag over een abonnement?', 'newheap'); ?>
                        </h2>

                        <p><?php _e('Neem dan contact op met de abonnementen-administratie van AgriMedia via:', 'newheap'); ?></p>

                        <div class="phone-holer">
                            <strong>
                                <?php _e('Telefoon', 'newheap'); ?>:
                                <a href="tel:<?php echo get_field('general_phone', 'option'); ?>">
                                    <?php echo get_field('general_phone', 'option'); ?>
                                </a>
                            </strong>
                        </div>

                        <div class="email-holder mb-4">
                            <strong>
                                <?php _e('Mail', 'newheap'); ?>:
                                <a href="<?php echo antispambot('mailto:' . get_field('general_mail', 'option')); ?>">
                                    <?php echo antispambot(get_field('general_mail', 'option')); ?>
                                </a>
                            </strong>
                        </div>

                        <?php _e('Of bekijk de veelgestelde vragen op', 'newheap'); ?>:
                        <a href="https://www.agrimedia.nl">www.agrimedia.nl</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php get_footer();
