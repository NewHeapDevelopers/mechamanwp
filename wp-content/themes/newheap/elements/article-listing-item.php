<?php
$broadcast_data = ThreeWP_Broadcast()->get_post_broadcast_data( get_current_blog_id(), get_the_ID());
$parent = $broadcast_data->get_linked_parent();
?>
<a class="article-item" href="<?php the_permalink(); ?>">
    <div class="row">
        <div class="col-xl-3 col-md-4 relative">
            <div class="bg-image-holder h-100">
                <div class="bg-image-div h-100" style="background-image: url(<?php echo get_the_post_thumbnail_url(null, 'medium'); ?>);"></div>
                <img src="<?php echo get_the_post_thumbnail_url(null, 'large'); ?>" width="100%" height="auto" style="display: none;">
                <?php if (get_field('premium_article')) { ?>
                    <div class="premium-label">
                        <i class="far fa-gem"></i> Premium
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-xl-9 col-md-8 my-2 theme-container theme-<?php if (!empty($parent)) : ?><?php echo $parent['blog_id'] ?><?php else : ?><?php echo get_current_blog_id(); ?><?php endif; ?>">
            <?php if(get_current_blog_id() == \NewHeap\Theme\Multisite\Multisite::SITE_MECHAMAN) { ?>
                <h5 class="p-2 mb-0 pt-3 pb-0 dot-before">
                    <?php if(!empty($parent)) { ?>
                        <?php echo get_blog_details(['blog_id' => $parent['blog_id']])->blogname; ?>
                    <?php } else { ?>
                        <?php echo get_bloginfo( 'name' ); ?>
                    <?php } ?>
                </h5>
            <?php } ?>
            <h3 class="sub-title"><?php the_title(); ?></h3>
            <div class="meta-holder">
                <div class="meta-date"><?php echo get_the_date('d-m-Y'); ?></div>
                <div class="meta-read-time">
                    <i class="far fa-clock"></i>
                    <span class="text"><?php echo \NewHeap\Theme\Helpers::get_read_time(get_the_ID()); ?></span>
                </div>
                <?php if (!empty($partners = get_the_terms( get_the_ID(), 'partner' ))) : ?>
                    <div class="meta-author">
                        <?php if (isset($wp_query->query['post_type']) and $wp_query->query['post_type'] == "partners") {
                            echo $partners[0]->name;
                        } else {
                            echo get_the_author_meta('display_name');
                        }
                        ?>
                    </div>
                <?php endif; ?>
            </div>
            <p class="excerpt"><?=get_the_excerpt(); ?></p>
        </div>
    </div>
</a>
