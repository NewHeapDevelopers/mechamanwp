<?php if (!empty(get_post_meta(get_the_ID(), 'dem_poll_id', true))) { ?>
    <div class="container">
        <div class="poll-holder bg-gray p-5">
            <h4 class="bold text-black"><?php _e('Poll', 'newheap') ?></h4>
            <?php
            echo do_shortcode('[democracy id="' . get_post_meta(get_the_ID(), 'dem_poll_id', true) . '"]');
            ?>
        </div>
    </div>
<?php }
?>
