<?php
$latest_magazine = get_posts([
    'post_type' => 'magazine',
    'posts_per_page' => 1,
    'meta_query' => [
        'key' => '_thumbnail_id'
    ]
]);

$button = get_field('subscription_button', 'option');
?>

    <section class="subscriptions-cta my-5">
        <div class="container">
            <div class="subscription-holder">
                <div class="subscription-image-holder">
                    <div class="image-holder">
	                    <?php echo get_the_post_thumbnail($latest_magazine[0]->ID); ?>
                    </div>
                </div>

                <div class="subscription-content-holder">
                    <div class="subscription-content">
                        <h2><?php echo get_field('subscription_title', 'option'); ?></h2>
	                    <?php echo get_field('subscription_content', 'option'); ?>
                    </div>

                    <div class="subscription-btn">
                        <a href="<?php echo $button['url']; ?>" class="btn btn-primary">
			                <?php echo $button['title']; ?>
                        </a>
                    </div>
                </div>


            </div>
            <div class="row">
                <div class="col-md-2"></div>
            </div>
        </div>
    </section>

<?php wp_reset_query();
