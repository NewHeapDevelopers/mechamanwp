<div class="poll-holder bg-gray p-3">
    <h4 class="bold text-black">Poll</h4>
    <?php
    if(get_current_blog_id() == 1) {
        $result = $wpdb->get_results( " SELECT * FROM `nhme_democracy_q` WHERE `active` = 1 ORDER BY `id` DESC LIMIT 1");
    } else {
        $result = $wpdb->get_results( " SELECT * FROM `nhme_".get_current_blog_id()."_democracy_q` WHERE `active` = 1 ORDER BY `id` DESC LIMIT 1");
    }
    if(isset($result[0]) and !empty($result[0])) {
        echo do_shortcode('[democracy id="'.$result[0]->id.'"]');
    } else {
        _e('Momenteel is er geen poll beschikbaar', 'newheap');
    }
    ?>
</div>
