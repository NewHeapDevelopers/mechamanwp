<ul>
    <!--twitter-->
	<li>
		<a title="Deel op twitter" href="http://twitter.com/share?url=<?php echo \NewHeap\Theme\Helpers::get_current_url(); ?>" target="_blank">
			<i class="fab fa-twitter-square"></i>
		</a>
	</li>
    <!--linkedin-->
    <li>
        <a title="Deel op LinkedIn" href="https://www.linkedin.com/sharing/share-offsite/?url=<?php echo \NewHeap\Theme\Helpers::get_current_url(); ?>" target="_blank">
            <i class="fab fa-linkedin"></i>
        </a>
    </li>
    <!--facebook-->
    <li>
        <a title="Deel op Facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo \NewHeap\Theme\Helpers::get_current_url(); ?>" target="_blank">
            <i class="fab fa-facebook-square"></i>
        </a>
    </li>
    <!--whatsapp-->
    <li>
        <a title="Deel op Whatsapp" href="https://web.whatsapp.com/send?text=<?php echo \NewHeap\Theme\Helpers::get_current_url(); ?>" target="_blank">
            <i class="fab fa-whatsapp-square"></i>
        </a>
    </li>
    <!--kopieer-->
    <li>
        <a title="Kopieer URL" class="urlToClipboard" href="#" target="_blank">
            <i style="font-size: 26px;padding-top: 2px;" class="fas fa-copy"></i> <span style="display: none;font-size:12px;display: block;position: absolute;"></span>
        </a>
    </li>
</ul>
