<?php
$args = array(
	'posts_per_page' => 1,
	'post__in' => get_option( 'sticky_posts' ),
	'ignore_sticky_posts' => 1,
	'tax_query' => [
		[
			'taxonomy' => 'artikel_type',
			'field' => 'slug',
			'operator' => 'NOT IN',
			'terms' => 'columns',
		]
	]
);
$header = (new WP_Query($args))->posts[0];

if (has_post_thumbnail($header->ID)) {
    $header_image = get_the_post_thumbnail_url($header->ID, 'large');
} else {
    $header_image = get_field('placeholder_afbeelding', 'option');
}
?>
<div class="header">
    <div class="header-image"
         style="background-image: url('<?php echo $header_image ?>'); background-size: cover; background-repeat: no-repeat; background-position: center;">

        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6 blackbox">
                    <a href="<?php echo get_the_permalink($header->ID); ?>">
                        <div class="header-content text-white bg-black-trans p-3">
                            <?php if(get_field('premium_article', $header->ID)) { ?>
                                <div class="content-type text-white">
                                    <span class="primary-background px-3 py-1"><i class="far fa-gem"></i>&nbsp;Premium</span>
                                </div>
                            <?php } ?>

                            <h2><?php echo $header->post_title ?></h2>
                            <span>
                            <?php echo get_the_date('d-m-Y', $header->ID); ?>
                            |
                            <i class="far fa-clock"></i>
                            <?php echo \NewHeap\Theme\Helpers::get_read_time($header->ID) ?>
                        </span>

                            <p class="mt-2"><?php echo $header->post_excerpt ?></p>
                        </div>
                    </a>
                </div>
                <div class="col-sm-12 col-md-6">

                </div>
            </div>
        </div>
    </div>
</div>
