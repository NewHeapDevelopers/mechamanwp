<?php
wp_reset_query();

$counter = 0;

$args = array(
	'post_type' => 'partners',
	'posts_per_page' => 6,
);

$query = new WP_Query($args);
?>

<?php if ($query->have_posts()) : ?>
    <section class="kennispartners my-4">


        <div class="container">
            <h3 class="section-title"><?php _e('Van onze kennispartners', 'newheap'); ?>
            </h3>
            <div class="row">
				<?php while ($query->have_posts()) : $query->the_post(); $counter++; ?>
					<?php
					$partners = get_the_terms(get_the_ID(), 'partner');

                    if (has_post_thumbnail()) {
						$post_image = get_the_post_thumbnail_url(null, 'large');
					} else {
						$post_image = get_field('placeholder_afbeelding', 'option');
					}

					?>
                    <div class="col-sm-12 col-md-4 relative pb-2 <?php echo ($counter > 3) ? 'd-none' : ''; ?>">
                        <a class="block-link" href="<?php the_permalink(); ?>">
                            <div class="image-partner">
                                <div class="image-holder" style="background-image: url('<?php echo $post_image; ?>');"></div>
                                <div class="content-type text-white absolute">
                                    <span class="primary-background px-3 py-1">
                                        <i class="fas fa-handshake"></i>
                                        <?php echo $partners[0]->name; ?>
                                    </span>
                                </div>

                                <div class="content-title bg-white-trans text-black">
                                    <h5 class="p-2 px-3">
										<?php the_title(); ?>
                                    </h5>
                                </div>
                            </div>
                        </a>
                    </div>
				<?php endwhile; ?>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div class="load-more go-to-archive-btn <?php if(is_single()) { ?>text-right<?php } else { ?>text-center<?php } ?> text-black bold w-100 mt-4 loadmore" style="display: block">
                        <a href="<?php echo get_post_type_archive_link('partners'); ?>">
						    <?php _e('Alle kennispartner berichten','newheap'); ?>
                            <div class="arrow-right"></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif;

wp_reset_query();
