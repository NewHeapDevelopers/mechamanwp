<?php if(!empty(get_field('advertisement_leaderboard', 'option'))) { ?>
    <div class="col-md-12 google-ad-holder ad-between-posts-holder text-center">
        <div class="pt-5 pb-5 mb-3">
            <!---<div class="ad-title">Advertentie</div>-->
            <div class="ad-holder">
                <?=get_field('advertisement_leaderboard', 'option')?>
            </div>
        </div>
    </div>
<?php } ?>
