<?php
$args = array(
	'post_type' => 'post',
	'posts_per_page' => 4,
	'tax_query' => [
		'relation' => 'OR'
	],
);

$categories = get_the_terms(get_the_ID(), 'category');
$dossiers = get_the_terms(get_the_ID(), 'dossier');

if (!empty($categories)) {
	foreach ($categories as $category) {
		$args['tax_query'][] = [
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $category->term_id,
		];
	}
}

if (!empty($dossiers)) {
	foreach ($dossiers as $dossier) {
		$args['tax_query'][] = [
			'taxonomy' => 'dossier',
			'field' => 'term_id',
			'terms' => $dossier->term_id,
		];
	}
}
$query = new \WP_Query($args);
?>
<section class="postloop">

    <div class="container-fluid">
        <h3 class="section-title">
            <?php _e('Nu in het vakblad', 'newheap'); ?>
        </h3>
        <div class="nh-tile-holder">
            <?php
            wp_reset_query();

            $vakblad = get_field('in_het_vakblad');

            if (!empty( $vakblad )) {
                foreach($vakblad as $blad) {
                    $item = $blad['vakblad_item'];
                    $itemID = url_to_postid($item);
                    ?>
                    <div class="nh-tile">
                        <a href="<?php echo get_the_permalink($itemID) ?>" class="post-item block-link">
                            <div class="bg-image-holder">
                                <div class="bg-image-div" style="padding-top:60%;background-image: url(<?php echo get_the_post_thumbnail_url($itemID, 'large') ?>);"></div>
                                <img src="<?php echo get_the_post_thumbnail_url($itemID, 'large') ?>" width="100%" height="auto" style="display: none;">
                                <?php if (!empty(get_field('premium_article', $itemID))) { ?>
                                    <div class="premium-label">
                                        <i class="far fa-gem"></i> Premium
                                    </div>
                                <?php } ?>
                            </div>

                            <div class="bg-black text-white dotted-top p-3">
                                <h2 class=""><?php echo get_the_title($itemID); ?></h2>

                                <p>
	                                <?php echo get_the_excerpt($itemID); ?>
                                </p>
                            </div>
                        </a>
                    </div>
                <?php }
            } ?>
        </div>
    </div>
</section>
