<section class="recent-items my-4">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="uppercase primary-color"><?php _e('Veel gelezen', 'newheap'); ?></h3>
            </div>
        </div>
    </div>
    <div class="container-fluid col-fluid-fix">
        <div class="row">
            <?php
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => 4,
                'meta_key' => 'post_view_count',
                'orderby' => 'meta_value_num',
                'order' => 'DESC'
            );
            $query = new \WP_Query($args);

            if ($query->have_posts()) {
                while ($query->have_posts()) {
                    $query->the_post();

                    $input = get_the_excerpt();

                    $str = $input;
                    if( strlen( $input) > 150) {
                        $str = explode( "\n", wordwrap( $input, 150));
                        $str = $str[0] . '...';
                    }

                    ?>
                        <div class="col-sm-12 col-md-6 mb-1 pr-fix">
                            <a href="<?php echo get_the_permalink() ?>">

                            <div class="mobile-up"
                                 style="background-image: url('<?php echo get_the_post_thumbnail_url(null, 'large') ?>'); background-size: 100%; background-repeat: no-repeat; background-position: center;height:100%;">
                                <div class="row eq-row-fix">
                                    <div class="col-md-7">
                                    </div>
                                    <div class="col-md-5 bg-black text-white dotted-left p-3 text-tend-bottom">
                                        <h2 class=""><?php echo get_the_title() ?></h2>
                                        <span>
                                            <?php echo get_the_date('d-m-Y'); ?>
                                            |
                                            <i class="far fa-clock"></i>
                                            <?php echo get_post_meta(get_the_ID(), 'post_read_time', true) ?>
                                        </span>

                                        <p class="mt-2"><?php echo $str; ?><strong>Lees verder</strong></p>
                                    </div>
                                </div>
                            </div>
                            </a>

                        </div>
                <?php }
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="load-more text-center text-black bold w-100 mt-4 loadmore">Meer van Kennispartners
            <br/>
            <div class="arrow-bottom"></div>
        </div>
    </div>
    <style>
        .eq-row-fix {
            flex: 1;
            height: 100%;
        }
        .text-tend-bottom {
            display: flex;
            flex-direction: column;
            justify-content: flex-end;
        }
    </style>
</section>
