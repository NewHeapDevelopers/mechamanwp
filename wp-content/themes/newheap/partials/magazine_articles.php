<?php
$args = array(
	'post_type' => 'post',
	'posts_per_page' => -1,
	'meta_query' => [
        'relation' => 'AND',
        [
            'key' => 'artikel_magazine',
            'compare' => 'EXISTS',
        ]
    ],
);

$query = new \WP_Query($args);
?>

<section class="postloop pt-5 mb-5">

    <div class="container-fluid">
        <h3 class="section-title">
            <?php _e('Nu in het vakblad', 'newheap'); ?>
        </h3>
        <div class="nh-tile-holder">
            <?php if ($query->have_posts()) : $counter = 0;?>
                <?php while ($query->have_posts()) : $query->the_post(); ?>
                    <?php
                    if (empty(get_field('artikel_magazine'))) {

                        continue;
                    }

		            $counter++;

                    if ($counter > 4) {
                        break;
                    }
                    ?>
                    <div class="nh-tile">
                        <a href="<?php echo get_the_permalink() ?>" class="post-item block-link">
                            <div class="bg-image-holder">
                                <div class="bg-image-div" style="padding-top:60%;background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large') ?>);"></div>
                                <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large') ?>" width="100%" height="auto" style="display: none;">
                                <?php if (!empty(get_field('premium_article', get_the_ID()))) { ?>
                                    <div class="premium-label">
                                        <i class="far fa-gem"></i> Premium
                                    </div>
                                <?php } ?>
                            </div>

                            <div class="bg-black text-white dotted-top p-3">
                                <h2 class=""><?php echo get_the_title(); ?></h2>
                                
                                <p>
	                                <?php echo get_the_excerpt(); ?>
                                </p>
                            </div>
                        </a>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>

<?php wp_reset_query();
