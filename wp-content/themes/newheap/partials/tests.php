<section class="tests my-4">

    <div class="container-fluid">
        <h3 class="section-title">
            <?php _e('Tests', 'newheap') ?>
        </h3>
        <div class="row">
            <?php
            $args = [
	            'post_type' => 'post',
	            'posts_per_page' => 4,
	            'tax_query' => [
		            [
			            'taxonomy' => 'artikel_type',
			            'field'    => 'slug',
			            'terms'    => 'tests',
		            ],
	            ],
            ];

            $query = new WP_Query($args);

            if ($query->have_posts()) : ?>
                <?php while ($query->have_posts()) : $query->the_post(); ?>
                    <div class="col-sm-12 col-md-6" style="padding:1px">
                        <a href="<?php echo get_the_permalink(); ?>" class="post-item">
                            <div class="col-sm-12 col-md-12 relative image-media large">
                                <div class="image-holder" style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large') ?>');"></div>
					            <?php if(!empty( get_field('premium_article') )) { ?>
                                    <div class="content-type text-white absolute">
                                        <span class="primary-background px-4 py-2">
                                            <i class="far fa-gem"></i>
                                            <?php _e('Premium', 'newheap'); ?>
                                        </span>
                                    </div>
					            <?php } ?>

                                <div class="content-title bg-black-trans text-white">
                                    <h5 class="p-4"><?php echo get_the_title() ?></h5>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>

            <div class="container">
                <div class="row">
                    <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
                        <a href="<?php echo get_field('tests_archive_page', 'option') ?>">
                            <?php _e('Meer tests', 'newheap') ?>
                            <div class="arrow-right pr-2 ml-20"></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
