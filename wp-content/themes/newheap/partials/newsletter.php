<?php $rand = rand(0, 100000); ?>
<section class="newsletter my-4">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1">
            </div>
            <div class="col-sm-10">
                <i class="fas fa-envelope-open-text"></i>
                <form class="newsletter-<?=$rand?>" name="newsletter-<?=$rand?>" action="#" method="post">
                    <div class="title">
                        <?=get_field('newsletter_cta_text', 'option')?>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-4 pl-0">
                            <input class="newsletter-input news-name" type="text" name="name" placeholder="Naam" />
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <input class="newsletter-input news-mail" type="text" name="mail" placeholder="E-mailadres" />
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 btn-col">
                            <div data-newsletter-message=".newsletter-<?=$rand?> .news-msg" data-newsletter-name=".newsletter-<?=$rand?> .news-name" data-newsletter-mail=".newsletter-<?=$rand?> .news-mail" class="newsletter-btn see-more my-2 text-black bold py-2 text-right pr-2 mt-0">
                                Aanmelden   <div class="arrow-right pr-2 ml-20"></div>
                            </div>
                        </div>
                        <div class="col-12 pl-0">
                            <div class="news-msg"></div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>


