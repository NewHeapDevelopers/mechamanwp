<section class="multimedia my-4">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h3 class="section-title"><?php _e('Multimedia', 'newheap'); ?></h3>
                <?php
                wp_reset_query();

                $media = get_field('multimedia');


                $mainItem = $media['main_item'];
                $mainID = url_to_postid($mainItem);
                ?>
                <div class="row mb-3">
                    <div class="col-sm-12 col-md-12 relative">
                        <a href="<?php echo get_the_permalink($mainID) ?>">
                            <div class="image-media large"
                                 style="background-image: url('<?php echo get_the_post_thumbnail_url($mainID, 'large'); ?>'); background-size: 100%; background-repeat: no-repeat; background-position: center;">
                                <?php if(get_field('premium_article', $mainID)) { ?>
                                    <div class="content-type text-white absolute">
                                        <span class="primary-background px-3 py-1"><i class="far fa-gem"></i>&nbsp;<?php _e('Premium','newheap');?></span>
                                    </div>
                                <?php } ?>

                                <div class="content-title bg-black-trans text-white">
                                    <h5 class="p-2">
                                        <?php echo \NewHeap\Theme\Helpers::get_type_icon($mainID); ?>
                                        <span><?php echo get_the_title($mainID); ?></span>
                                    </h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <?php
                    wp_reset_query();
                    $posts = new WP_Query([
                        'post_type' => 'post',
                        'posts_per_page' => 1,
                        'tax_query' => [
                                [
	                                'taxonomy' => 'artikel_type',
	                                'field'    => 'slug',
	                                'terms'    => 'videos',
                                ]
                        ],
                    ]);

                    $videoID = $posts->posts[0]->ID;
                    ?>
                    <div class="col-sm-12 col-md-6 relative pb-2">
                        <a href="<?php echo get_the_permalink($videoID) ?>">
                            <div class="image-media"
                                 style="background-image: url('<?php echo get_the_post_thumbnail_url($videoID, 'large'); ?>'); background-size: 100%; background-repeat: no-repeat; background-position: center;">
                                <?php if(get_field('premium_article', $videoID)) { ?>
                                    <div class="content-type text-white absolute">
                                        <span class="primary-background px-3 py-1"><i class="far fa-gem"></i>&nbsp;<?php _e('Premium','newheap');?></span>
                                    </div>
                                <?php } ?>

                                <div class="content-title bg-black-trans text-white">
                                    <h5 class="p-2">
	                                    <?php echo \NewHeap\Theme\Helpers::get_type_icon($videoID); ?>
                                        <span><?php echo get_the_title($videoID); ?></span>
                                    </h5>
                                </div>
                            </div>
                        </a>

                        <a href="<?php echo get_field(\NewHeap\Theme\Helpers::get_post_article_type($videoID)->slug . '_archive_page', 'option') ?>" class="more-button">
                            <div class="see-more my-2 w-100 text-black bold bg-gray py-2 text-right pr-2 mt-0">
			                    <?php echo __("Meer","newheap") . ' ' . \NewHeap\Theme\Helpers::get_post_article_type($videoID)->name; ?>
                                <div class="arrow-right pr-2 ml-20"></div>
                            </div>
                        </a>
                    </div>
                    <?php
                    wp_reset_query();
                    $posts = new WP_Query([
	                    'post_type' => 'post',
	                    'posts_per_page' => 1,
	                    'tax_query' => [
		                    [
			                    'taxonomy' => 'artikel_type',
			                    'field'    => 'slug',
			                    'terms'    => 'podcasts',
		                    ]
	                    ],
                    ]);

                    $podcastID = $posts->posts[0]->ID;
                    ?>
                    <div class="col-sm-12 col-md-6 relative pb-2">
                        <a href="<?php echo get_the_permalink($podcastID) ?>">
                            <div class="image-media"
                                 style="background-image: url('<?php echo get_the_post_thumbnail_url($podcastID, 'large'); ?>'); background-size: 100%; background-repeat: no-repeat; background-position: center;">
                                <?php if(get_field('premium_article', $podcastID)) { ?>
                                    <div class="content-type text-white absolute">
                                        <span class="primary-background px-3 py-1"><i class="far fa-gem"></i>&nbsp;<?php _e('Premium','newheap');?></span>
                                    </div>
                                <?php } ?>

                                <div class="content-title bg-black-trans text-white">
                                    <h5 class="p-2">
	                                    <?php echo \NewHeap\Theme\Helpers::get_type_icon($podcastID); ?>
                                        <span><?php echo get_the_title($podcastID); ?></span>
                                    </h5>
                                </div>
                            </div>
                        </a>

                        <a href="<?php echo get_field(\NewHeap\Theme\Helpers::get_post_article_type($podcastID)->slug . '_archive_page', 'option') ?>" class="more-button">
                            <div class="see-more my-2 w-100 text-black bold bg-gray py-2 text-right pr-2 mt-0">
			                    <?php echo __("Meer","newheap") . ' ' . \NewHeap\Theme\Helpers::get_post_article_type($podcastID)->name; ?>
                                <div class="arrow-right pr-2 ml-20"></div>
                            </div>
                        </a>
                    </div>
                    <?php
                    wp_reset_query();
                    $posts = new WP_Query([
	                    'post_type' => 'post',
	                    'posts_per_page' => 1,
	                    'tax_query' => [
		                    [
			                    'taxonomy' => 'artikel_type',
			                    'field'    => 'slug',
			                    'terms'    => 'infographics',
		                    ]
	                    ],
                    ]);

                    $infographicID = $posts->posts[0]->ID;
                    ?>
                    <div class="col-sm-12 col-md-6 relative pb-2">
                        <a href="<?php echo get_the_permalink($infographicID) ?>">
                            <div class="image-media"
                                 style="background-image: url('<?php echo get_the_post_thumbnail_url($infographicID, 'large') ?>'); background-size: 100%; background-repeat: no-repeat; background-position: center;">
                                <?php if(get_field('premium_article', $infographicID)) { ?>
                                    <div class="content-type text-white absolute">
                                        <span class="primary-background px-3 py-1"><i class="far fa-gem"></i>&nbsp;<?php _e('Premium','newheap');?></span>
                                    </div>
                                <?php } ?>

                                <div class="content-title bg-black-trans text-white">
                                    <h5 class="p-2">
	                                    <?php echo \NewHeap\Theme\Helpers::get_type_icon($infographicID); ?>
                                        <span><?php echo get_the_title($infographicID); ?></span>
                                    </h5>
                                </div>
                            </div>
                        </a>

                        <a href="<?php echo get_field(\NewHeap\Theme\Helpers::get_post_article_type($infographicID)->slug . '_archive_page', 'option') ?>" class="more-button">
                            <div class="see-more my-2 w-100 text-black bold bg-gray py-2 text-right pr-2 mt-0">
			                    <?php echo __("Meer","newheap") . ' ' . \NewHeap\Theme\Helpers::get_post_article_type($infographicID)->name; ?>
                                <div class="arrow-right pr-2 ml-20"></div>
                            </div>
                        </a>
                    </div>
                    <?php
                    wp_reset_query();
                    $posts = new WP_Query([
	                    'post_type' => 'post',
	                    'posts_per_page' => 1,
	                    'tax_query' => [
		                    [
			                    'taxonomy' => 'artikel_type',
			                    'field'    => 'slug',
			                    'terms'    => 'fotoreportages',
		                    ]
	                    ],
                    ]);

                    $fotoID = $posts->posts[0]->ID;
                    ?>
                    <div class="col-sm-12 col-md-6 relative pb-2">
                        <a href="<?php echo get_the_permalink($fotoID) ?>">
                            <div class="image-media"
                                 style="background-image: url('<?php echo get_the_post_thumbnail_url($fotoID, 'large') ?>'); background-size: 100%; background-repeat: no-repeat; background-position: center;">
                                <?php if(get_field('premium_article', $fotoID)) { ?>
                                    <div class="content-type text-white absolute">
                                        <span class="primary-background px-3 py-1"><i class="far fa-gem"></i>&nbsp;<?php _e('Premium','newheap');?></span>
                                    </div>
                                <?php } ?>

                                <div class="content-title bg-black-trans text-white">
                                    <h5 class="p-2">
	                                    <?php echo \NewHeap\Theme\Helpers::get_type_icon($fotoID); ?>
                                        <span><?php echo get_the_title($fotoID); ?></span>
                                    </h5>
                                </div>
                            </div>
                        </a>

                        <a href="<?php echo get_field(\NewHeap\Theme\Helpers::get_post_article_type($fotoID)->slug . '_archive_page', 'option') ?>" class="more-button">
                            <div class="see-more my-2 w-100 text-black bold bg-gray py-2 text-right pr-2 mt-0">
			                    <?php echo __("Meer","newheap") . ' ' . \NewHeap\Theme\Helpers::get_post_article_type($fotoID)->name; ?>
                                <div class="arrow-right pr-2 ml-20"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-4 column">
                <h3 class="section-title"><?php _e('Opinie', 'newheap'); ?></h3>
                <?php
                wp_reset_query();
                $posts = new WP_Query([
	                'post_type' => 'post',
	                'posts_per_page' => 1,
	                'tax_query' => [
		                [
			                'taxonomy' => 'artikel_type',
			                'field'    => 'slug',
			                'terms'    => 'columns',
		                ]
	                ],
                ]);

                $opinieID = $posts->posts[0]->ID;

                $input = get_the_excerpt($opinieID);

                $str = $input;
                if( strlen( $input) > 300) {
                    $str = explode( "\n", wordwrap( $input, 300));
                    $str = $str[0] . '...';
                }
                ?>
                <a href="<?php echo get_the_permalink($opinieID) ?>">
                    <div class="column-image-holder"
                         style="background-image: url('<?php echo get_the_post_thumbnail_url($opinieID, 'large') ?>'); background-size: cover; background-repeat: no-repeat; background-position: center;">
                    </div>

                    <div class="bg-black text-white dotted-top p-3">
                        <h2 class=""><?php echo get_the_title($opinieID); ?></h2>
                        <span>
                            <?php echo get_the_date('d-m-Y', $opinieID); ?>
                            |
                            <i class="far fa-clock"></i>
                            <?php echo \NewHeap\Theme\Helpers::get_read_time($opinieID); ?>
                        </span>

                        <p class="mt-2">
                            <?php echo $str; ?>
                            <strong> <?php _e('Lees verder', 'newheap'); ?></strong>
                        </p>
                    </div>
                </a>

                <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
                    <a href="<?php echo get_field('columns_archive_page', 'option'); ?>">
	                    <?php _e('Meer columns', 'newheap') ?>
                        <div class="arrow-right pr-2 ml-20"></div>
                    </a>
                </div>

                <?php include get_template_directory() . '/partials/latest-poll.php'; ?>

                <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
                    <a href="<?=site_url()?>/polls/">
                        <?php _e('Uitslagen vorige polls', 'newheap') ?>
                        <div class="arrow-right pr-2 ml-20"></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
