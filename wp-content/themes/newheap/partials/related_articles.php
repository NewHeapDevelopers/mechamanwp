<?php
$counter = 0;

$args = array(
    'post_type' => 'post',
    'posts_per_page' => 4,
    'tax_query' => [
        'relation' => 'OR'
    ],
);

$categories = get_the_terms(get_the_ID(), 'category');
$dossiers = get_the_terms(get_the_ID(), 'dossier');

if (!empty($categories)) {
    foreach ($categories as $category) {
	    $args['tax_query'][] = [
		    'taxonomy' => 'category',
		    'field' => 'term_id',
		    'terms' => $category->term_id,
	    ];
    }
}

if (!empty($dossiers)) {
    foreach ($dossiers as $dossier) {
	    $args['tax_query'][] = [
		    'taxonomy' => 'dossier',
		    'field' => 'term_id',
		    'terms' => $dossier->term_id,
	    ];
    }
}
$query = new \WP_Query($args);
?>

<section class="recent-items mt-4">

    <div class="container-fluid">
        <h3 class="section-title">
            <?php _e('Gerelateerde artikelen','newheap') ?>
        </h3>
        <?php if ($query->have_posts()) : ?>
            <div class="nh-tile-holder">
                <?php while ($query->have_posts()) :
                    $counter++;
                    $query->the_post();

                    $input = get_the_excerpt();

                    $str = $input;

                    if( strlen( $input) > 150) {
                        $str = explode( "\n", wordwrap( $input, 150));
                        $str = $str[0] . '...';
                    }

                    ?>
                    <div class="nh-tile <?php echo ($counter > 4) ? 'd-none' : ''; ?>">
                        <a href="<?php echo get_the_permalink() ?>">
                            <div class="tile-image-holder">
                                <div class="tile-image" style="background-image: url('<?php echo get_the_post_thumbnail_url(null, 'large') ?>');"></div>
                                <?php if (!empty(get_field('premium_article'))) { ?>
                                    <div class="premium-label">
                                        <i class="far fa-gem"></i> <?php _e('Premium','newheap') ?>
                                    </div>
                                <?php } ?>
                            </div>


                            <div class="tile-content-holder">
                                <h2>
                                    <?php echo get_the_title() ?>
                                </h2>

                                <span>
                                    <?php echo get_the_date('d-m-Y'); ?>
                                    |
                                    <i class="far fa-clock"></i>
                                    <?php echo \NewHeap\Theme\Helpers::get_read_time(get_the_ID()); ?>
                                </span>

                                <p class="mt-2">
		                            <?php echo $str; ?>
                                    <strong>
			                            <?php _e('Lees verder','newheap')?>
                                    </strong>
                                </p>
                            </div>
                        </a>
                    </div>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-sm-12">

                <div class="load-more go-to-archive-btn text-right text-black bold w-100 mt-4 loadmore" style="display: block">
                    <a href="<?=site_url()?>/artikelen/?category=<?=$categories[0]->term_id?>">
                        <?php _e('Meer gerelateerd','newheap'); ?>
                        <div class="arrow-right"></div>
                    </a>
                </div>
            </div>
        </div>
    </div>

</section>
