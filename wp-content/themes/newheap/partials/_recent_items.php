<section class="recent-items my-4">
    <div class="container-fluid">
        <h3 class="section-title"><?php _e('Recent','newheap') ?></h3>
        <div class="row">
            <?php
            $sticky_post = get_option( 'sticky_posts' );

            $args = array(
                'post_type' => 'post',
                'posts_per_page' => 4,
                'ignore_sticky_posts' => 1,
                'post__not_in' => $sticky_post
            );
            $query = new \WP_Query($args);

            if ($query->have_posts()) {
                while ($query->have_posts()) {
                    $query->the_post();

                    $input = get_the_excerpt();

                    $str = $input;
                    if( strlen( $input) > 150) {
                        $str = explode( "\n", wordwrap( $input, 150));
                        $str = $str[0] . '...';
                    }

                    ?>
                        <div class="col-sm-12 col-md-6 mb-1">
                            <a href="<?php echo get_the_permalink() ?>">
                                <div class="mobile-up"
                                     style="background-image: url('<?php echo get_the_post_thumbnail_url(null, 'large') ?>'); background-size: 100%; background-repeat: no-repeat; background-position: center;height:100%;">
                                    <div class="row eq-row-fix">
                                        <div class="col-md-7">
                                        </div>
                                        <div class="col-md-5">
                                            <div class="bg-black text-white dotted-left p-3 text-tend-bottom" style="height: 100%">
                                                <h2 class=""><?php echo get_the_title() ?></h2>
                                                <span>
                                                    <?php echo get_the_date('d-m-Y'); ?>
                                                    |
                                                    <i class="far fa-clock"></i>
                                                    <?php echo get_post_meta(get_the_ID(), 'post_read_time', true) ?>
                                                </span>

                                                <p class="mt-2">
                                                    <?php echo $str; ?>
                                                    <strong>
                                                        <?php _e('Lees verder','newheap')?>
                                                    </strong>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                <?php }
            }
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="load-more text-center text-black bold w-100 mt-4 loadmore"><?php _e('Meer van Kennispartners','newheap'); ?>
                <br/>
                <div class="arrow-bottom"></div>
            </div>
        </div>
    </div>

    <style>
        .eq-row-fix {
            flex: 1;
            height: 100%;
        }
        .text-tend-bottom {
            display: flex;
            flex-direction: column;
            justify-content: flex-end;
        }
    </style>
</section>
