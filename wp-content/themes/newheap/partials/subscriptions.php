<?php
$subscriptions = get_field('abonnementen', 'options');

if (!empty($subscriptions)) : ?>
    <section class="subscriptions my-5">

        <div class="container">
            <h3 class="section-title">
                <?php _e('Abonnementen','newheap') ?>
            </h3>
            <div class="row">
                <?php foreach ($subscriptions as $subscription) : ?>
                    <div class="col">
                        <div class="subscription-holder">
                            <h3><?php echo $subscription['naam']; ?></h3>
                            <p><?php echo $subscription['omschrijving']; ?></p>

                            <?php foreach($subscription['eigenschap'] as $property) : ?>
                                <div class="subscription-meta-holder">
                                    <div class="subscription-meta-icon">
                                        <?php echo $property['icoon']; ?>
                                    </div>

                                    <div class="subscription-meta-detail">
                                        <?php echo $property['beschrijving']; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>

                            <hr>

                            <div class="price-holder">
                                <?php echo $subscription['prijs']; ?>
                            </div>

                            <hr>

                            <a href="<?php echo $subscription['aanmeld_pagina']; ?>" class="btn btn-primary">
                                <?php _e('Aanmelden', 'newheap'); ?>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif;
wp_reset_query();
