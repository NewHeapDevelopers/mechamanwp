( function( blocks, editor, i18n, element, components, _ ) {
    var __ = i18n.__;
    var el = element.createElement;
    var RichText = editor.RichText;

    blocks.registerBlockType( 'newheap-blocks/quote', {
        title: __('Streamer', 'newheap'),
        icon: 'index-card',
        category: 'layout',
        attributes: {
            title: {
                type: 'array',
                source: 'children',
                selector: '.quote-holder',
            },
        },

        example: {
            title: __( 'Title', 'newheap' ),
        },

        edit: function( props ) {
            var attributes = props.attributes;

            var onSelectImageFirst = function( media ) {
                return props.setAttributes( {
                    first_column_mediaURL: media.url,
                    first_column_mediaID: media.id,
                } );
            };

            var onSelectImageSecond = function( media ) {
                return props.setAttributes( {
                    second_column_mediaURL: media.url,
                    second_column_mediaID: media.id,
                } );
            };

            return el(
                'section',
                {
                    className: 'quote my-5'
                },
                el(
                    RichText,
                    {
                        tagName: 'span',
                        className: 'quote-holder',
                        placeholder: __('Titel', 'newheap'),
                        value: attributes.title,
                        onChange: function(value) {
                            props.setAttributes(
                                {
                                    title: value
                                }
                            );
                        },
                    }
                )
            );
        },
        save: function( props ) {
            var attributes = props.attributes;

            return el(
                'section',
                {
                    className: 'quote my-5'
                },
                el(
                    RichText.Content,
                    {
                        tagName: 'span',
                        className: 'quote-holder',
                        value: attributes.title,
                    },
                )
            );
        },
    } );
} )(
    window.wp.blocks,
    window.wp.editor,
    window.wp.i18n,
    window.wp.element,
    window.wp.components,
    window._
);
