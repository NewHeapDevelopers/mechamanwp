// TODO: Fix images which are shown double (background and img tag)

( function( blocks, editor, i18n, element, components, _ ) {
    var __ = i18n.__;
    var el = element.createElement;
    var RichText = editor.RichText;
    var MediaUpload = editor.MediaUpload;

    blocks.registerBlockType( 'newheap-blocks/one-col-image', {
        title: __('Enkele foto', 'newheap'),
        icon: 'index-card',
        category: 'layout',
        attributes: {
            first_column_mediaID: {
                type: 'number',
            },
            first_column_mediaURL: {
                type: 'string',
                source: 'attribute',
                selector: 'img',
                attribute: 'src',
            },
            first_column_title: {
                type: 'array',
                source: 'children',
                selector: '.image-title',
            },
            first_column_description: {
                type: 'array',
                source: 'children',
                selector: 'p',
            },
        },

        example: {
            attributes: {
                first_column_title: __( 'Title', 'newheap' ),
                first_column_mediaURL: 'https://via.placeholder.com/150',
                first_column_description: __('Content', 'newheap'),
            },
        },

        edit: function( props ) {
            var attributes = props.attributes;

            var onSelectImage = function( media ) {
                if (media.sizes.hasOwnProperty('large')) {
                    var mediaUrl = media.sizes.large.url
                } else if (media.sizes.hasOwnProperty('medium')) {
                    var mediaUrl = media.sizes.medium.url
                } else {
                    var mediaUrl = media.url
                }

                return props.setAttributes( {
                    first_column_mediaURL: mediaUrl,
                    first_column_mediaID: media.id,
                } );
            };

            return el(
                'section',
                {
                    className: 'image-holder'
                },
                el(
                    'div',
                    {
                        className: 'row',
                    },

                    // First column
                    el(
                        'div',
                        {
                            className: 'col-md-12',

                        },

                        el(
                            'div',
                            {
                                className: 'image-holder',
                                style: {
                                    backgroundImage: 'url(' + attributes.first_column_mediaURL + ')'
                                }
                            },
                            el( MediaUpload, {
                                onSelect: onSelectImage,
                                allowedTypes: 'image',
                                value: attributes.first_column_mediaID,
                                render: function( obj ) {
                                    return el(
                                        components.Button,
                                        {
                                            className: attributes.first_column_mediaID
                                                ? 'button button-large'
                                                : 'button button-large',
                                            onClick: obj.open,
                                            text: __('Change image', 'newheap'),
                                        },
                                        ! attributes.first_column_mediaID
                                            ? __( 'Upload Image', 'gutenberg-examples' )
                                            : el( 'img', { src: attributes.first_column_mediaURL } )
                                    );
                                },
                            } )
                        ),
                        el(
                            RichText,
                            {
                                tagName: 'div',
                                className: 'image-title',
                                // inline: true,
                                placeholder: __('Titel', 'newheap'),
                                value: attributes.first_column_title,
                                onChange: function(value) {
                                    props.setAttributes(
                                        {
                                            first_column_title: value
                                        }
                                    );
                                },

                            }
                        ),
                        el(
                            RichText,
                            {
                                tagName: 'p',
                                // inline: true,
                                placeholder: i18n.__('Content', 'newheap'),
                                value: attributes.first_column_description,
                                onChange: function(value) {
                                    props.setAttributes(
                                        {
                                            first_column_description: value
                                        }
                                    );
                                },
                            }
                        ),
                    ),
                ),
            );
        },
        save: function( props ) {
            var attributes = props.attributes;

            return el(
                'section',
                {
                    className: 'image-holder'
                },
                el(
                    'div',
                    {
                        className: 'row',
                    },

                    // First column
                    el(
                        'div',
                        {
                            className: 'col-md-12',

                        },
                        attributes.first_column_mediaURL &&
                        el(
                            'div',
                            {
                                className: 'image-holder',
                                style: {
                                    backgroundImage: 'url(' + attributes.first_column_mediaURL + ')'
                                }
                            },
                            el(
                                'img',
                                {
                                    src: attributes.first_column_mediaURL,
                                }
                            )
                        ),
                        el(
                            RichText.Content,
                            {
                                tagName: 'div',
                                className: 'image-title',
                                value: attributes.first_column_title,
                            }
                        ),
                        el(
                            RichText.Content,
                            {
                                tagName: 'p',
                                value: attributes.first_column_description,
                            }
                        )
                    ),
                ),
            );
        },
    } );
} )(
    window.wp.blocks,
    window.wp.editor,
    window.wp.i18n,
    window.wp.element,
    window.wp.components,
    window._
);
