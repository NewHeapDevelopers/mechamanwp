( function( blocks, editor, i18n, element, components, _ ) {
    var __ = i18n.__;
    var el = element.createElement;
    var RichText = editor.RichText;
    var MediaUpload = editor.MediaUpload;

    blocks.registerBlockType( 'newheap-blocks/article-highlight', {
        title: __('Kadertekst gebruiker', 'newheap'),
        icon: 'index-card',
        category: 'layout',
        attributes: {
            title: {
                type: 'array',
                source: 'children',
                selector: 'h3',
            },
            mediaID: {
                type: 'number',
            },
            mediaURL: {
                type: 'string',
                source: 'attribute',
                selector: 'img',
                attribute: 'src',
            },
            description: {
                type: 'array',
                source: 'children',
                selector: 'p',
            },
        },

        example: {
            attributes: {
                title: __( 'Title', 'newheap' ),
                mediaURL: 'https://placehold.it/400x200',
                description: __('Content', 'newheap'),
            },
        },

        edit: function( props ) {
            var attributes = props.attributes;

            var onSelectImage = function( media ) {
                return props.setAttributes( {
                    mediaURL: media.url,
                    mediaID: media.id,
                } );
            };

            return el(
                'section',
                {
                    className: 'article-highlight'
                },
                el(
                    'div',
                    {
                        className: 'row',
                    },
                    el(
                        'div',
                        {
                            className: 'col-md-3',

                        },

                        el(
                            'div',
                            {
                                className: 'image-holder',
                                style: {
                                    backgroundImage: 'url(' + attributes.mediaURL + ')'
                                }
                            },
                            el( MediaUpload, {
                                onSelect: onSelectImage,
                                allowedTypes: 'image',
                                value: attributes.mediaID,
                                render: function( obj ) {
                                    return el(
                                        components.Button,
                                        {
                                            className: attributes.mediaID
                                                ? 'image-button'
                                                : 'button button-large',
                                            onClick: obj.open,
                                        },
                                        ! attributes.mediaID
                                            ? __( 'Upload Image', 'gutenberg-examples' )
                                            : el( 'img', { src: attributes.mediaURL } )
                                    );
                                },
                            } )
                        ),
                    ),
                    el(
                        'div',
                        {
                            className: 'col-md-9',
                        },
                        el(
                            RichText,
                            {
                                tagName: 'h3',
                                // inline: true,
                                placeholder: __('Titel', 'newheap'),
                                value: attributes.title,
                                onChange: function(value) {
                                    props.setAttributes(
                                        {
                                            title: value
                                        }
                                    );
                                },

                            }
                        ),
                        el(
                            RichText,
                            {
                                tagName: 'p',
                                // inline: true,
                                placeholder: i18n.__('Content', 'newheap'),
                                value: attributes.description,
                                onChange: function(value) {
                                    props.setAttributes(
                                        {
                                            description: value
                                        }
                                    );
                                },
                            }
                        ),
                    ),
                ),
            );
        },
        save: function( props ) {
            var attributes = props.attributes;

            return el(
                'section',
                {
                    className: 'article-highlight'
                },
                el(
                    'div',
                    {
                        className: 'row',
                    },
                    el(
                        'div',
                        {
                            className: 'col-md-3',

                        },
                        attributes.mediaURL &&
                        el(
                            'div',
                            {
                                className: 'image-holder',
                                style: {
                                    backgroundImage: 'url(' + attributes.mediaURL + ')'
                                }
                            },
                            el(
                                'img',
                                {
                                    src: attributes.mediaURL,
                                }
                            )
                        ),
                    ),
                    el(
                        'div',
                        {
                            className: 'col-md-9',

                        },
                        el(
                            RichText.Content,
                            {
                                tagName: 'h3',
                                value: attributes.title,
                            }
                        ),
                        el(
                            RichText.Content,
                            {
                                tagName: 'p',
                                value: attributes.description,
                            }
                        )
                    ),
                ),
            );
        },
    } );
} )(
    window.wp.blocks,
    window.wp.editor,
    window.wp.i18n,
    window.wp.element,
    window.wp.components,
    window._
);
