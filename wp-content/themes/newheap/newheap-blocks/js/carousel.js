// ( function( blocks, editor, i18n, element, components, _ ) {
//     var __ = i18n.__;
//     var el = element.createElement;
//     var RichText = editor.RichText;
//     var MediaUpload = editor.MediaUpload;
//
//     blocks.registerBlockType( 'newheap-blocks/quote', {
//         title: __( 'Gutenberg Repeater Field' ),
//         icon: 'shield',
//         category: 'common',
//         attributes: {
//             locations: {
//                 type: 'array',
//                 default: [],
//             },
//         },
//         keywords: [
//             __( 'Gutenberg Repeater Field' ),
//             __( 'Repeatable' ),
//             __( 'ACF' ),
//         ],
//         edit: ( props ) => {
//             const handleAddLocation = () => {
//                 const locations = [ ...props.attributes.locations ];
//                 locations.push( {
//                     address: '',
//                 } );
//                 props.setAttributes( { locations } );
//             };
//
//             const handleRemoveLocation = ( index ) => {
//                 const locations = [ ...props.attributes.locations ];
//                 locations.splice( index, 1 );
//                 props.setAttributes( { locations } );
//             };
//
//             const handleLocationChange = ( address, index ) => {
//                 const locations = [ ...props.attributes.locations ];
//                 locations[ index ].address = address;
//                 props.setAttributes( { locations } );
//             };
//
//             let locationFields,
//                 locationDisplay;
//
//             if ( props.attributes.locations.length ) {
//                 locationFields = props.attributes.locations.map( ( location, index ) => {
//                     return <Fragment key={ index }>
//                         <TextControl
//                             className="grf__location-address"
//                             placeholder="350 Fifth Avenue New York NY"
//                             value={ props.attributes.locations[ index ].address }
//                             onChange={ ( address ) => handleLocationChange( address, index ) }
//                         />
//                         <IconButton
//                             className="grf__remove-location-address"
//                             icon="no-alt"
//                             label="Delete location"
//                             onClick={ () => handleRemoveLocation( index ) }
//                         />
//                     </Fragment>;
//                 } );
//
//                 locationDisplay = props.attributes.locations.map( ( location, index ) => {
//                     return <p key={ index }>{ location.address }</p>;
//                 } );
//             }
//
//             return [
//                 <InspectorControls key="1">
//                     <PanelBody title={ __( 'Locations' ) }>
//                         { locationFields }
//                         <Button
//                             isDefault
//                             onClick={ handleAddLocation.bind( this ) }
//                         >
//                             { __( 'Add Location' ) }
//                         </Button>
//                     </PanelBody>
//                 </InspectorControls>,
//                 <div key="2" className={ props.className }>
//                     <h2>Block</h2>
//                     { locationDisplay }
//                 </div>,
//             ];
//         },
//         save: ( props ) => {
//             const locationFields = props.attributes.locations.map( ( location, index ) => {
//                 return <p key={ index }>{ location.address }</p>;
//             } );
//
//             return (
//                 <div className={ props.className }>
//                     <h2>Block</h2>
//                     { locationFields }
//                 </div>
//             );
//         },
//     } );
// } )(
//     window.wp.blocks,
//     window.wp.editor,
//     window.wp.i18n,
//     window.wp.element,
//     window.wp.components,
//     window._
// );
//
//
//
//
