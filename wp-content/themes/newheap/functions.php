<?php
if (!function_exists('dd')) {
    /**
     * Function to easily dump data
     *
     * @param $dump_data
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    function dd($dump_data)
    {
        if (is_admin()) {
            echo '<pre style="padding-left: 200px">';
        } else {
            echo '<pre style="text-align: left;">';
        }

        var_dump($dump_data);

        echo '</pre>';
    }
}

require_once 'NewHeap/NewHeap.php';


add_filter('use_block_editor_for_post_type', 'prefix_disable_gutenberg', 10, 2);
function prefix_disable_gutenberg($current_status, $post_type)
{
	if ($post_type === 'video') {
		return false;
	}
    if ($post_type === 'magazine') {
        return false;
    }
    if ($post_type === 'agenda') {
        return false;
    }

	return $current_status;
}

// Log woodwing calls
if (strpos($_SERVER['REQUEST_URI'], 'publiqare') !== false) {
	\NewHeap\Theme\Logger\Logger::write_woodwing_log($_POST, 'functions.php');
	\NewHeap\Theme\Logger\Logger::write_woodwing_log(getallheaders(), 'functions.php');
}

//require __DIR__ . '/../../plugins/newheap-account/vendor/mollie-api-php/vendor/autoload.php';
//$mollie = new \Mollie\Api\MollieApiClient();
//$mollie->setApiKey(get_field('mollie_api_key', 'option'));
//$payment = $mollie->payments->get('tr_DJP46esQQs');
//dd($payment->isPaid());
//exit;
