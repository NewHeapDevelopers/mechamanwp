<?php
/**
 * The template for displaying 404 pages (Not Found)
 */

status_header(404);
get_header();

$page = get_page_by_title('404');
if ($page) : ?>
    <div class="container">
        <?php echo do_shortcode($page->post_content);?>
    </div>
<?php else : ?>
    <div class="container">
        <div class="my-5 py-5 text-center">
            <h1 class="mb-5"><?php _e('404: Pagina niet gevonden','newheap'); ?></h1>
            <p><?php _e('De door u opgezochte pagina kon niet gevonden worden. Controleer de URL en probeer het nogmaals.', 'newheap'); ?></p>
        </div>
    </div>
<?php endif;

get_footer();
