<?php
/**
 * Template Name: Agenda travel
 */

get_header();
wp_reset_query();

?>

<div class="main">
    <div class="container">
        <?php
        if ( have_posts() ) {
            while (have_posts()) : the_post();
                the_content();
            endwhile;
        }

        switch_to_blog(1);
        $args = array(
            'post_type' => 'reizen',
            'posts_per_page' => -1
        );

        $query = new WP_Query($args);
        ?>
        <div class="row">
            <div class="col-md-6">
                <h2>Aankomende reizen</h2>
                <?php if ($query->have_posts()) {
                    while ($query->have_posts()) {
                        $query->the_post();
                        if(time() < get_field('start_date')) {
                            ?><a href="<?=get_the_permalink()?>"><?=get_the_title()?></a><br /><?php
                        }
                    }
                }?>
            </div>
            <div class="col-md-6">
                <h2>Eerdere reizen</h2>
                <?php if ($query->have_posts()) {
                    while ($query->have_posts()) {
                        $query->the_post();
                        if(time() > get_field('start_date')) {
                            ?><a href="<?=get_the_permalink()?>"><?=get_the_title()?></a><br /><?php
                        }
                    }
                }?>
            </div>
        </div>
        <br />
        <br />
    </div>
</div><!-- /.main -->


<?php
restore_current_blog();
get_footer();
