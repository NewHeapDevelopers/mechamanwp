<?php
/**
 * Template Name: Agenda archief
 */

get_header();

$article_type = get_field('archive_for_type');

$args = [
    'post_type' => 'agenda',
    'meta_key' => 'event_start_date',
    'orderby' => 'meta_value',
    'order' => 'ASC',
    'meta_query' => [
        [
            'key' => 'event_end_date',
            'value' => date('Y-m-d').' 00:00:00',
            'compare' => '>=',
            'type'=>'DATE'
        ]
    ]
];

remove_all_filters('posts_orderby');
$query = new WP_Query($args);

if (has_post_thumbnail()) {
	$columns_image = get_the_post_thumbnail_url(null, 'large');
} else {
	$columns_image = get_field('placeholder_afbeelding', 'option')['sizes']['medium_large'];
}
?>


    <section class="author-posts my-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-10 offset-md-1">
                    <h3 class="section-title">
						<?php _e('Agenda items', 'newheap'); ?>
                    </h3>
                </div>

				<?php if ($query->have_posts()) : ?>
					<?php while ($query->have_posts()) : $query->the_post(); ?>
                        <div class="col-sm-12 col-md-10 offset-md-1">
                            <div class="dotted-bottom-black pb-3 mb-3 d-block">
                                <div class="row">
                                    <div class="col-md-12 my-2">
                                        <h2 class="bold mb-3"><?php the_title(); ?></h2>
                                        <div class="bold">
                                            <span>
                                                <?php echo str_replace(" 00:00", "", get_field('event_start_date', get_the_ID())); ?> - <?=str_replace(" 00:00", "",get_field('event_end_date', get_the_ID()))?>
                                            </span>
                                        </div>

                                        <p class="mt-2">
											<?php the_content(); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
					<?php endwhile; ?>
				<?php endif; ?>
            </div>
        </div>
    </section>

<?php get_footer();

