<?php
/*
Template Name: Front page
*/

get_header();


include get_theme_file_path( 'partials/header.php' );
include get_theme_file_path( 'partials/recent_items2.php' );

if(get_current_blog_id() != \NewHeap\Theme\Multisite\Multisite::SITE_LANDBOUW_MECHANISATIE) {
	include get_theme_file_path( 'partials/advertisement.php' );
}

include get_theme_file_path( 'partials/partners.php' );

if(get_current_blog_id() == \NewHeap\Theme\Multisite\Multisite::SITE_LANDBOUW_MECHANISATIE) {
	include get_theme_file_path( 'partials/advertisement.php' );
}

include get_theme_file_path( 'partials/multimedia.php' );
include get_theme_file_path( 'partials/subscriptions-cta.php' );
include get_theme_file_path( 'partials/dossiers.php' );
include get_theme_file_path( 'partials/tests.php' );
include get_theme_file_path( 'partials/newsletter.php' );
include get_theme_file_path( 'partials/categories.php' );
include get_theme_file_path( 'partials/read_most2.php' );
include get_theme_file_path( 'partials/loop.php' );
include get_theme_file_path( 'partials/subscriptions-cta.php' );

get_footer();

?>


