<?php
$categories = get_categories(    array(
    'meta_query' => array(
        'relation' => 'OR',
        array(
            'key'     => 'do_not_show',
            'value'   => '0',
            'compare' => '=',
        ),
        array(
            'key' => 'do_not_show',
            'compare' => 'NOT EXISTS'
        ),
    )
));
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no">
    <meta name="HandheldFriendly" content="True">
    <?php wp_head(); ?>
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon.png">

    <script>
        var AppConfig = {
            siteUrl: '<?php echo site_url(); ?>',
        }
    </script>

<?php include_once("analyticstracking.php"); ?>

</head>

<body <?php body_class(); ?>>


<div class="main-wrapper">


<?php
if ( ! is_singular( 'partners' ) ) {
    echo get_field('advertisement_header', 'option');
}
?>


    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container relative">
            <div class="logo-holder">
                <a href="<?php echo home_url() ?>">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/logos/lm.png" alt="<?php bloginfo(); ?>">
                </a>
            </div>
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
                <span></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
				<?php wp_nav_menu([
					'menu_id' => 'main-menu',
					'menu_class' => 'navbar-nav mr-auto',
					'theme_location' => 'main-menu',
					'container' => false,
					'depth' => 2,
				]); ?>
            </div>
            <div class="weather bg-white text-black text-center">
                <?php $weather = \NewHeap\Theme\Weather\Weather::getWeather(); ?>
                <div class="weather-icon" style="background-image:url('//openweathermap.org/img/wn/<?=$weather->weather[0]->icon?>@2x.png');"></div>
                <div class="current-weather">
                    <div class="weather-text bold"><?=$weather->weather[0]->description?></div>
                    <div class="celsius bold"><?=round($weather->main->temp)?>&deg; C</div>
                </div>
            </div>
        </div>
    </nav>
    <!-- Logo & user actions -->
    <div class="logo-row">
        <div class="container">
            <div class="row">
                <div class="col-7 col-md-6">
                    <div class="logo-holder">
                        <a href="<?php echo home_url() ?>">
                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/LM_logo_diap.png" alt="<?php bloginfo(); ?>">
                        </a>
                    </div>
                </div>
                <div class="col-5 col-md-6 vertical-middle">
                    <div class="user-items">
                        <a href="<?php echo get_field('favorites_page', 'option'); ?>" class="user-item-favorites">
                            <i class="fas fa-star"></i>
                        </a>
                        <a href="<?php echo get_field('notifications_page', 'option'); ?>" class="user-item-notifications">
                            <i class="fas fa-bell"></i>
                        </a>
                        <a href="<?php echo get_field('profile_page', 'option'); ?>" class="user-item-profile">
                            <i class="fas fa-user-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Searchbar -->
    <div class="sub-header my-3">
        <div class="container">
            <div class="row">
                <div class="col-12 col-xl-10">
                    <form action="<?php echo get_field('archive_page', 'option'); ?>" method="get" class="search-holder">
                        <div class="row">
                            <div class="col-12 col-sm-4">
                                <div class="select-holder">
                                    <select name="category" class="category-dropdown">
                                        <option value="" selected><?php _e('Categorie&euml;n', 'newheap'); ?></option>
						                <?php foreach ($categories as $category) : ?>
                                            <option value="<?php echo $category->term_id; ?>" <?php echo (isset($_GET['category']) && $category->term_id == $_GET['category']) ? 'selected' : ''; ?>>
								                <?php echo $category->name; ?>
                                            </option>
						                <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 col-sm-8">
                                <div class="input-search-holder">
                                    <input class="form-control search-input" name="search" type="text" value="<?php echo (isset($_GET['search'])) ? esc_attr($_GET['search']) : ''; ?>" placeholder="<?php _e('Bijv. Trekkers, Isobus, Zaaien', 'newheap'); ?>">

                                    <button class="btn btn-outline-regular my-2 my-sm-0" type="submit">
                                        <i class="fas fa-search"></i>
                                        <span class="bold"><?php _e('Zoek', 'newheap'); ?></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

	<?php echo \NewHeap\Theme\Notification\Notification::show_notifications(); ?>
