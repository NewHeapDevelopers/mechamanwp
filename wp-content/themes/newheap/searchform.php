<div class="container">
    <div class="row">
        <form role="search" method="get" id="searchform" class="searchform" action="<?php echo site_url(); ?>">
            <div class="close-search-form">
                &times;
            </div>
            <div class="bostec-search searchform__holder">
                <div class="row">
                    <input class="col-md-12" type="text" value="" name="s" id="s" placeholder="Zoeken...">
                </div>
            </div>
        </form>
    </div>
</div>
