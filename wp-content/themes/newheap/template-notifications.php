<?php
/**
 * Template Name: Notificaties
 */

get_header(); ?>
    <div class="container">
        <h3 class="section-title"><?php _e('Instellingen', 'newheap'); ?></h3>

        <div class="row">
            <div class="col-md-6">
                <strong><?php _e('Stuur mij een notificatie voor', 'newheap'); ?></strong>
                <ul>
                    <li>
                        <label>
                            <input type="checkbox"> <?php _e('Mijn favoriete auteurs', 'newheap'); ?>
                        </label>
                    </li>
                    <li>
                        <label>
                            <input type="checkbox"> <?php _e('Mijn favoriete onderwerpen', 'newheap'); ?>
                        </label>
                    </li>
                    <li>
                        <label>
                            <input type="checkbox"> <?php _e('Mijn dossiers', 'newheap'); ?>
                        </label>
                    </li>
                </ul>

                <strong><?php _e('Stuur mij een notificatie voor', 'newheap'); ?></strong>
                <ul>
                    <li>
                        <label>
                            <input type="checkbox"> <?php _e('Mijn account', 'newheap'); ?>
                        </label>
                    </li>
                    <li>
                        <label>
                            <input type="checkbox"> <?php _e('E-mail', 'newheap'); ?>
                        </label>
                    </li>
                </ul>
            </div>

            <div class="col-md-6">
                <strong><?php _e('Over uw favorieten', 'newheap'); ?></strong>
                <p><?php  _e('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur assumenda atque commodi consectetur corporis dicta dolorum eligendi, inventore ipsa, itaque laboriosam magnam necessitatibus nesciunt nostrum pariatur placeat quidem sequi. In?', 'newheap'); ?></p>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-4">
                <strong><?php _e('Favoriete auteurs', 'newheap'); ?></strong>
                <div class="favorite-label">
                    <i class="fas fa-times-circle"></i>
                    Albert van der Horst
                </div>
                <div class="favorite-label">
                    <i class="fas fa-times-circle"></i>
                    Gertjan Zevenbergen
                </div>
                <div class="favorite-label">
                    <i class="fas fa-times-circle"></i>
                    Chris Vlaanderen
                </div>
            </div>

            <div class="col-md-4">
                <strong><?php _e('Favoriete onderwerpen', 'newheap'); ?></strong>
                <div class="favorite-label">
                    <i class="fas fa-times-circle"></i>
                    Albert van der Horst
                </div>
                <div class="favorite-label">
                    <i class="fas fa-times-circle"></i>
                    Gertjan Zevenbergen
                </div>
                <div class="favorite-label">
                    <i class="fas fa-times-circle"></i>
                    Chris Vlaanderen
                </div>
            </div>

            <div class="col-md-4">
                <strong><?php _e('Favoriete dossiers', 'newheap'); ?></strong>
                <div class="favorite-label">
                    <i class="fas fa-times-circle"></i>
                    Albert van der Horst
                </div>
                <div class="favorite-label">
                    <i class="fas fa-times-circle"></i>
                    Gertjan Zevenbergen
                </div>
                <div class="favorite-label">
                    <i class="fas fa-times-circle"></i>
                    Chris Vlaanderen
                </div>
            </div>
        </div>

        <h3 class="section-title">Artikelen uit uw favorieten</h3>
    </div>

<?php get_footer();
