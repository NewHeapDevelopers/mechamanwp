<?php
/**
 * Template Name: Registreren
 */

get_header(); ?>

<div class="container">
	<?php if ( have_posts() ) : ?>
        <h1><?php the_title(); ?></h1>

		<?php while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
			<?php echo do_shortcode('[newheap_registration_form]'); ?>
		<?php endwhile; ?>

	<?php endif; ?>
</div>

<?php get_footer();
