<?php
/**
 * Template Name: Poll archief
 */

get_header(); ?>

<div class="container py-4">
    <div class="row">
        <?php
        $i = 1;
        if (get_current_blog_id() === \NewHeap\Theme\Multisite\Multisite::SITE_MECHAMAN) {
	        $result = $wpdb->get_results( " SELECT * FROM `nhme_democracy_q` ORDER BY `id` DESC");
        } else {
	        $result = $wpdb->get_results( " SELECT * FROM `nhme_".get_current_blog_id()."_democracy_q` ORDER BY `id` DESC");
        }
        foreach($result as $poll) {
            $date =  date_i18n( 'j F Y', $poll->added );
            ?>
            <div class="col-md-3">
               <div class="poll-holder bg-gray p-3">
                <h4 class="bold text-black">Poll <small><?=$date?></small></h4>
                    <?php echo do_shortcode('[democracy id="'.$poll->id.'"]'); ?>
                </div>
            </div>
            <?php if(($i % 3) == 0) { ?></div><div class="row"><?php }
            $i++;
        }
        ?>
    </div>
</div>

<?php get_footer();
