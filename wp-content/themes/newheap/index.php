<?php
get_header();

if ( have_posts() ) : ?>

    <?php if (is_search()) : ?>
        <div class="row mb-5">
            <div class="col-sm-12">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <?php echo do_shortcode('[header text="' . sprintf(__('Je zocht naar: %s'), $_GET['s']) . '" button="" background_image="142"]Zoekresultaten[/header]'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="search-item">
                            <a href="<?php  the_permalink(); ?>">
                                <h2 class="h3"><?php the_title(); ?></h2>
                                <?php echo the_excerpt(); ?>

                                <span class="btn-circle btn-circle--red">
                                    <span class="btn__icon">
                                        <i class="ico-arrow-white">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ico-arrow-right.svg" width="12" height="16" alt="<?php the_title(); ?>">
                                        </i>
                                    </span>

                                    <strong><?php _e('Lees verder', 'newheap'); ?></strong>
                                </span>
                            </a>
                        </div>
                    <?php endwhile; ?>
                </div>

                <div class="col-md-4">

                </div>
            </div>
        </div>
    <?php else : ?>
		<?php
        $showHeader = false;

        $header_image = get_field('placeholder_afbeelding', 'option');
        global $wp_query;

        if (is_tax('partner')) {
	        $tax = $wp_query->get_queried_object();

            if(get_field('partner_afbeelding', $tax)) {
                $header_image = get_field('partner_afbeelding', $tax);
                $showHeader = true;
            }

        }

        if (isset($wp_query->query) && isset($wp_query->query['category_name'])) {
            $term_name = ucfirst($wp_query->query['category_name']);
        }

        if($showHeader) {
        ?>
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-10 offset-md-1">
                        <div class="header-image"
                             style="background-image: url('<?php echo $header_image['url']; ?>');">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-8 offset-md-2">
                        <div class="header-content text-white bg-black remove-top-space p-5 pt-1 pb-3">

                            <?php if (isset($term_name) && !empty($term_name)) : ?>
                                <div class="tax-holder">
                                    <h4 class="primary-color">
                                        <?php echo $term_name; ?>
                                    </h4>
                                </div>
	                        <?php endif; ?>

                            <h1 class="">
                                <?php switch (get_post_type()) {
                                    case 'partners' :
	                                    echo $tax->name;
                                        break;
                                    case 'agenda' :
	                                    _e('Agenda', 'newheap');
	                                    break;
                                    default :
	                                    _e('Artikelen', 'newheap');
	                                    break;
                                }
                                ?>
                            </h1>

	                        <?php if (term_description()) : ?>
                                <p>
			                        <?php echo term_description(); ?>
                                </p>
	                        <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>

        <section class="posts-overview">
            <div class="container">
                <div class="row">

                    <?php if (isset($term_name) && !empty($term_name)) : ?>
                        <div class="col-sm-12 col-md-10 offset-md-1">
                            <h3 class="section-title">
                                <?php printf(__('Artikelen uit de categorie "%s"', 'newheap'), $term_name); ?>
                            </h3>
                        </div>
                    <?php else : ?>

                        <div class="col-sm-12 col-md-10 offset-md-1">
                            <?php if(isset($wp_query->query['post_type']) and $wp_query->query['post_type'] == "partners") { ?>
                                <h3 class="section-title">Kennispartners</h3>
                            <?php } else { ?>
                                <div class="dotted-bottom-black d-block mb-3"></div>
                            <?php } ?>
                        </div>
                    <?php endif; ?>

                    <?php $counter = 0; ?>

                    <?php while (have_posts()) : the_post(); $counter++; ?>
                        <?php if ($counter == 11) : ?>
                            <div class="col-sm-12 col-md-10 offset-md-1 google-ad-holder ad-between-posts-holder text-center">
                                <div class="dotted-bottom-black pt-5 pb-5 mb-3">
                                    <div class="ad-between-posts">
                                        <?=get_field('advertisement_archive', 'option')?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="col-sm-12 col-md-10 offset-md-1  pb-3">
                            <?php include get_theme_file_path('elements/article-listing-item.php'); ?>
                        </div>
                    <?php endwhile; ?>

	                <?php
                    global $posts;
                    if (count($posts) < 11) : ?>
                        <div class="col-sm-12 col-md-10 offset-md-1 google-ad-holder ad-between-posts-holder text-center">
                            <div class="pt-5 pb-5 mb-3">
                                <div class="ad-between-posts">
                                    <?=get_field('advertisement_archive', 'option')?>
                                </div>
                            </div>
                        </div>
	                <?php endif; ?>

                    <div class="col-sm-12 col-md-10 offset-md-1 pb-3 mb-3">
	                    <?php the_posts_pagination([
                            'screen_reader_text' => ' ',
                            'mid_size' => 1,
                            'prev_text' => '<i class="fas fa-chevron-left"></i>',
                            'next_text' => '<i class="fas fa-chevron-right"></i>',
                        ]); ?>
                    </div>
                </div>
            </div>
        </section>

    <?php endif; ?>

<?php endif;

get_footer();
