<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package newheap
 */

get_header();

$premium_article = get_field('premium_article');
$categories = get_the_terms(get_the_ID(), 'category');
$partners = get_the_terms(get_the_ID(), 'partner');

if ($premium_article) {
    $header_content_class = 'text-white bg-black remove-top-space';
    $bg_class = 'bg-black';
} else {
	$header_content_class = 'bg-white';
	$bg_class = 'bg-white';
}
?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <?php if (has_post_thumbnail()) : $thumbnail_url = get_the_post_thumbnail_url(get_the_ID(), 'xlarge'); ?>
            <div class="header <?php echo ($premium_article) ? 'premium-article': ''; ?> <?php echo (!empty(get_post_meta(get_the_ID(),'_old_id'))) ? 'legacy-post': ''; ?> ">
                <div class="header-image" style="background-image: url('<?php echo $thumbnail_url; ?>'); background-size: cover; background-repeat: no-repeat; background-position: center;">
                    <?php if (!empty($partners)) : ?>
                    <div class="content-type text-white absolute">
                        <span class="primary-background px-3 py-1">
                            <i class="fas fa-handshake"></i>
                            <?php echo $partners[0]->name; ?>
                        </span>
                    </div>
                    <?php elseif (current_user_can('administrator')) : ?>
                        <h2 style="color: white; background-color: red">
	                        <?php _e('Beste beheerder, dit artikel heeft geen partner. Selecteer a.u.b. een partner.', 'newheap'); ?>
                        </h2>

                    <?php endif; ?>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-8 offset-md-2">
                            <div class="header-content <?php echo $header_content_class; ?> px-5 pt-1 pb-3">
                                <div class="article-top-bar">
                                    <div class="article-options">
                                        <?php if ($premium_article) : ?>
                                            <div class="text-start">
                                                <span class="<?php echo $bg_class; ?> py-2">
                                                    <i class="far fa-gem"></i>
                                                    <span><?php _e('Premium', 'newheap'); ?></span>
                                                </span>
                                            </div>
                                        <?php endif; ?>
                                        <div class="text-end">
                                            <?php /*
                                            <span class="<?php echo $bg_class; ?> py-2 mr-3">
                                                <i class="fas fa-volume-up"></i>
                                                <span><?php _e('Luisteren', 'newheap'); ?></span>
                                            </span>
                                            */ ?>

                                            <span data-favorite-click="<?=get_the_ID()?>;<?=get_current_blog_id()?>;post" class="<?php echo $bg_class; ?> py-2">
                                                <i class="fas fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <span><?php _e('Favoriet', 'newheap'); ?></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <hr class="mt-1">
                                <div class="article-content-bar">
                                    <?php if (!empty($categories)) : ?>
                                        <div class="tax-holder">
                                            <h2 class="primary-color">
                                                <?php echo current($categories)->name; ?>
                                            </h2>
                                        </div>
                                    <?php endif; ?>


                                    <h1>
                                        <?php the_title(); ?>
                                    </h1>

                                    <?php if (has_excerpt()) : ?>
                                        <p class="mt-2">
                                            <?php the_excerpt(); ?>
                                        </p>
                                    <?php endif; ?>



                                    <span class="post-meta-holder">
                                        <span class="date-holder">
                                            <?php echo get_the_date('d-m-Y', get_the_ID()); ?>
                                        </span>

                                        <span class="read-time">
                                            | <?php echo \NewHeap\Theme\Helpers::get_read_time(get_the_ID()); ?>
                                        </span>

                                        <span class="author">
                                            |

                                            <i class="fas fa-handshake"></i>
                                            <?php if (!empty($partners)) : ?>
                                                <a href="<?php echo get_term_link($partners[0], 'partner'); ?>">
                                                    <?php echo $partners[0]->name; ?>
                                                </a>
                                            <?php endif; ?>
                                        </span>

                                        <?php if ($photographer = get_field('article_photographer')) : ?>
                                            | <span class="photographer">
                                                <i class="fas fa-camera"></i>
                                                <?php echo $photographer; ?>
                                            </span>
                                        <?php endif; ?>
                                    </span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        <?php endif; ?>

        <article class="article <?php echo ($premium_article) ? 'premium-article': ''; ?> <?php echo (!empty(get_post_meta(get_the_ID(),'_old_id'))) ? 'legacy-post': ''; ?>">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-8 offset-md-2">
	                    <?php if ($premium_article) : ?>
		                    <?php if (\Newheap\Plugins\NewheapAccount\User\User::has_valid_subscription()) : ?>
			                    <?php echo apply_filters( 'the_content', get_the_content() ); ?>
		                    <?php else: ?>
                                <div class="restricted-content">
                                    <?php echo wp_trim_words(get_the_content(), '55', ''); ?>
                                </div>

                                <div class="premium-article-popup">
                                    <h2><?php echo _e('Uw gratis premium artikelen zijn op', 'newheap'); ?></h2>
                                    <p><?php echo _e('Wilt u 14 dagen gratis toegang tot premium artikelen?', 'newheap'); ?></p>

                                    <a href="<?php echo get_field('registration_page', 'option'); ?>" class="btn btn-primary">
					                    <?php _e('Maak een account aan', 'newheap'); ?>
                                    </a>

                                    <div class="premium-article-popup-footer">
					                    <?php printf(__('Heeft u al een account? <a href="%s">Log dan in</a>', 'newheap'), get_field('login_page', 'option')); ?>
                                    </div>
                                </div>
			                    <?php //_e('Dit is een premium artikel. Abonneer u op dit vakblad om toegang te krijgen. Heeft u al een abonnement? Vul dan uw abonnementsnummer in in uw profiel.', 'newheap'); ?>
		                    <?php endif; ?>
	                    <?php else: ?>
		                    <?php echo apply_filters( 'the_content', get_the_content() ); ?>

	                    <?php endif; ?>
                    </div>
                </div>
            </div>

            <section class="share-content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-8 offset-md-2 text-black">
                            <div class="social-share">
			                    <?php _e('Deel dit artikel'); ?>

			                    <?php include get_template_directory() . '/partials/social-share.php'; ?>
                            </div>

                            <div class="article-tags">
			                    <?php if ($categories) : ?>
                                    <span class="title">
                                <i class="fas fa-tag"></i>
                                <span class="bold"><?php _e('Lees meer over:', 'newheap'); ?></span>
                            </span>

                                    <span class="category-holder">
                                <?php foreach ($categories as $category) : ?>
                                    <a href="<?php echo get_category_link($category); ?>">
                                        <?php echo $category->name; ?>
                                    </a>
                                <?php endforeach; ?>
                            </span>
			                    <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

		    <?php if (!empty($partners)) : ?>
                <section class="author my-4">
                    <?php if (is_singular('partners')) {
                        $author_image = get_field('partner_afbeelding', $partners[0])['sizes']['medium'];
                        $author_url = get_term_link($partners[0], 'partner');
                        $author_display_name = $partners[0]->name;
                        $author_description = $partners[0]->description;
                    }
                    ?>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-8 offset-md-2">
                                <a href="<?php echo $author_url; ?>">
                                    <div class="row">
                                        <div class="col-md-4 mobile-up">
                                            <div class="author-image-holder">
                                                <div class="author-image" style="background-image: url('<?php echo $author_image; ?>'); background-size: cover; background-repeat: no-repeat; background-position: center; height: 100%"></div>
                                            </div>
                                        </div>

                                        <div class="col-md-8 bg-black text-white dotted-left p-5">
                                            <h3 class="bold mb-3">
                                                <i class="fas fa-pen-nib"></i>
                                                <?php echo $author_display_name; ?>
                                            </h3>

                                            <p class="mt-2">
                                                <?php echo wp_trim_words($author_description, '20'); ?>
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>


            <?php if (get_current_blog_id() === \NewHeap\Theme\Multisite\Multisite::SITE_TUIN_EN_PARK_TECHNIEK || get_current_blog_id() === \NewHeap\Theme\Multisite\Multisite::SITE_VEEHOUDERIJ_TECHNIEK) : ?>
                <?php include get_theme_file_path('partials/magazine_articles.php'); ?>
            <?php else : ?>
                <?php include get_theme_file_path('partials/newsletter.php'); ?>
	            <?php include get_theme_file_path('partials/related_articles.php'); ?>
                <?php if ( get_current_blog_id() !== \NewHeap\Theme\Multisite\Multisite::SITE_MECHAMAN ) { ?>
	                <?php include get_theme_file_path('partials/magazine_articles.php'); ?>
                <?php } ?>
	            <?php include get_theme_file_path('partials/partners.php'); ?>
            <?php endif; ?>
        </article>
    <?php endwhile; ?>
<?php endif; ?>

<?php
global $post;
\NewHeap\Theme\Helpers::update_post_views($post->ID);

get_footer();
