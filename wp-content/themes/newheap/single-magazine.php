<?php
get_header();

?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="header">
            <div class="container">
                <div class="row m-0">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-7 mr-md-0 px-sm-0 px-xs-0">
                                <img src="<?php echo get_the_post_thumbnail_url(null, 'large'); ?>" alt="<?php echo get_the_post_thumbnail_caption(); ?>" height="100%" width="100%"/>
                            </div>

                            <div class="col-md-5 p-4 dotted-left bg-black text-white">
                                <h2 class="primary-color"> <?php the_title(); ?></h2>
                                <p class="text-white">
                                    <?php echo get_field('magazine_nummer'); ?><br/>
                                    Nummer <?php echo explode("-", get_field('magazine_nummer'))[1]; ?><br/>
                                    Jaargang <?php echo (explode("-", get_field('magazine_nummer'))[0] - 1949); ?><br/>
                                </p>

                                <div class="p-3 primary-background uppercase dotted-left negative-margin">
                                    <p>
                                        <span class="bold">
                                            <a href="?magazine_id=<?php echo get_the_ID(); ?>" class="text-white" target="_blank">
                                                Bekijk het vaklad digitaal
                                            </a>
                                        </span>

	                                    <?php if (!is_user_logged_in()) : ?>
                                            <div class="normal">(inloggen vereist)</div>
                                        <?php elseif (!\Newheap\Plugins\NewheapAccount\User\User::has_valid_subscription()) : ?>
                                            <div class="normal">(betaald abonnement vereist)</div>
                                        <?php endif; ?>
                                    </p>
                                </div>

                                <div class="mt-3 no-margin-p">
                                    <?php if(!is_user_logged_in()) { ?>
                                        <p>Nog geen premium account?</p>
                                        <p class="bold">
                                            <a href="<?=site_url()?>/abonnementen/" class="text-white">
                                                Maak er een aan.
                                            </a>
                                        </p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        $args = array(
            'meta_key' => 'artikel_magazine',
            'meta_value' => get_the_ID()
        );

        $query = new WP_Query( $args );


        if ($query->have_posts()) {
        ?>

        <section class="author-posts my-5">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="section-title">Artikelen uit <?=get_the_title()?></h3>
                    </div>

                    <?php
                        while ($query->have_posts()) {
                            $query->the_post();
                    ?>

                    <a href="<?php echo get_the_permalink(); ?>" class="col-sm-12 dotted-bottom-black pb-3 mb-3">
                        <div class="row">
                            <div class="col-md-3 relative">
                                <img class="author-img" src="<?php echo get_the_post_thumbnail_url(null, 'large'); ?>" alt="alt img hier"
                                     width="100%" height="100%"/>
                            </div>

                            <div class="col-md-9 my-2">
                                <h2 class="bold mb-3"><?php the_title(); ?></h2>
                                <div class="bold">
                                            <span>
                                                <?php echo get_the_date('d-m-Y'); ?>
                                                |
                                                <i class="far fa-clock"></i>
                                                <?php echo \NewHeap\Theme\Helpers::get_read_time(get_the_ID()); ?>
                                                <i class="fas fa-pen-nib"></i>
                                                <?php echo get_the_author_meta('display_name'); ?>
                                            </span>
                                </div>

                                <p class="mt-2">
                                    <?php the_excerpt(); ?>
                                </p>
                            </div>
                        </div>
                    </a>
                   <?php } ?>
                </div>
            </div>
        </section>

        <?php } ?>

        <section class="other-editions">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 my-5">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 mb-4">
                                <h2 class="primary-color uppercase">Laatste 4 edities</h2>
                            </div>

                            <?php

                            $args = array(
                                'post_type' => 'magazine',
                                'posts_per_page' => 4
                            );

                            $query = new WP_Query($args);

                            if ($query->have_posts()) {
                                $prevYear = "";
                                while ($query->have_posts()) {
                                    $query->the_post();

                                    $date = get_the_date('Y');
                                    $currentYear = $date;
                                 ?>
                                    <a href="<?php echo get_the_permalink(); ?>" class="col-sm-6 col-md-3 mb-2">
                                        <div class="postloop-image-holder relative">
                                            <img src="<?php echo get_the_post_thumbnail_url(null, 'large'); ?>" height="auto" width="100%"/>
                                        </div>

                                        <div class="bg-black text-white dotted-top p-3 pt-2">
                                            <h3 class="uppercase"><?php echo get_the_title() ?></h3>
                                            <div class="date-holder"><?php echo get_field('magazine_nummer') ?></div>
                                            <div class="number">Nummer <?php echo explode("-", get_field('magazine_nummer'))[1] ?></div>
                                            <div class="year">Jaargang <?php echo (explode("-", get_field('magazine_nummer'))[0] - 1949) ?></div>
                                        </div>
                                    </a>
                                <?php }
                            }
                            wp_reset_query(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>

<?php
global $post;
\NewHeap\Theme\Helpers::update_post_views($post->ID);

get_footer();
