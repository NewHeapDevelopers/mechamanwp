<?php
get_header();
?>

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <?php if (has_post_thumbnail()) : $thumbnail_url = get_the_post_thumbnail_url(get_the_ID(), 'xlarge'); ?>
			<?php if (has_post_thumbnail()) : $thumbnail_url = get_the_post_thumbnail_url(get_the_ID(), 'xlarge'); ?>
                <div class="header">
                    <div class="header-image"
                         style="background-image: url('<?php echo $thumbnail_url; ?>'); background-size: cover; background-repeat: no-repeat; background-position: center;">
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-8 offset-md-2">
                                <div class="header-content text-white bg-black remove-top-space px-5 pt-1 pb-3">
                                    <h1>
										<?php the_title(); ?>
                                    </h1>

									<?php if (has_excerpt()) : ?>
                                        <p class="mt-2">
											<?php the_excerpt(); ?>
                                        </p>
									<?php endif; ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
			<?php endif; ?>
        <?php endif; ?>

        <article class="article my-5">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-8 offset-md-2">
                        <?php echo get_the_content(); ?>

	                    <?php if (\Newheap\Plugins\NewheapAccount\User\User::has_valid_subscription()) : ?>
                            <a href="?magazine_id=<?php echo get_the_ID(); ?>" class="btn btn-primary" target="_blank">
			                    <?php _e('Bekijk', 'newheap'); ?>
                            </a>
	                    <?php endif; ?>
                    </div>
                </div>
            </div>

            <section class="share-content">
                <div class="container">
                    <div class="col-sm-12 col-md-8 offset-md-2 text-black">
                        <div class="social-share">
							<?php _e('Deel dit artikel'); ?>

							<?php include get_template_directory() . '/partials/social-share.php'; ?>
                        </div>
                    </div>
                </div>
            </section>
        </article>
    <?php endwhile; ?>
<?php endif; ?>

<?php
get_footer();
