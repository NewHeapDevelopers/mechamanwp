<?php

use \NewHeap\Component\Vc\Application\Service\VisualComposerService\VisualComposerBase;
use \NewHeap\Component\Vc\Application\Service\VisualComposerService\Field;


class PageHeader extends VisualComposerBase
{
    public function getName()
    {
        return "Page header";
    }


    public function constructFields()
    {
        $this->addField()
            ->setTitle('Title')
            ->setType(Field::TYPE_TEXTFIELD);

        $this->addField()
            ->setTitle('Content')
            ->setType(Field::TYPE_TEXTAREA_HTML);

        $this->addField()
            ->setTitle('Mobile text')
            ->setType(Field::TYPE_TEXTAREA);

        $this->addField()
            ->setTitle('Button')
            ->setType(Field::TYPE_VC_LINK);

        $this->addField()
            ->setTitle('Background image')
            ->setType(Field::TYPE_ATTACH_IMAGE);

    }



    public function render($attributes, $content, $shortCode)
    {
        ?>
        <div class="section-page-header">
            <div class="container">
                <div class="header">
                    <div class="header-image" style="background-image: url('<?php echo wp_get_attachment_image_url($attributes['background_image'], 'large'); ?>')">

                    </div>

                    <div class="header-content">
                        <?php if ($attributes['title']) : ?>
                            <h1><?php  echo $attributes['title']; ?></h1>
                        <?php endif; ?>

                        <div class="content-holder"><?php echo $content; ?></div>
                        <div class="content-holder-mobile"><?php echo $attributes['mobile_text']; ?></div>

                        <?php if (!empty($attributes['button']['url'])) : ?>
                            <a href="<?php echo $attributes['button']['url']; ?>" class="btn btn-magenta" target="<?php echo $attributes['button']['target']; ?>">
                                <?php echo $attributes['button']['title']; ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
