<?php
/**
 * Template Name: Artikel type archief
 */

if(isset($_GET['search']) and !empty($_GET['search'])) {
    include('template-category-archive.php');
} else {
    get_header();

    $counter = 0;
    $article_type = get_field('archive_for_type');
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

    $args = [
        'post_type' => 'post',
        'paged' => $paged
    ];

    if (!empty($article_type) && $article_type !== 'artikelen') {
        $args['tax_query'] = [
            [
                'taxonomy' => 'artikel_type',
                'field' => 'slug',
                'terms' => $article_type,
            ]
        ];
    }


    if(isset($_GET['category']) and !empty($_GET['category'])) {
        $args['cat'] = $_GET['category'];
    }
    $query = new WP_Query($args);

    $term_column = get_term_by('slug', $article_type, 'artikel_type');
    $columns_image = get_field('term_image', $term_column);

    if (has_post_thumbnail()) {
        $columns_image = get_the_post_thumbnail_url(null, 'large');
    } else {
        $columns_image = get_field('placeholder_afbeelding', 'option')['sizes']['medium_large'];
    }

    $latest_update = \NewHeap\Theme\Helpers::get_taxonomy_latest_date($term_column);
    ?>

        <section class="author-posts my-5">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-10 offset-md-1">
                        <h3 class="section-title">
                            <?php if ($term_column->slug === 'artikelen') : ?>
                                <?php
                                if(isset($_GET['search']) and !empty($_GET['search'])) {
                                    _e('Zoeken naar: ', 'newheap'); echo htmlentities($_GET['search']) . " ";
                                } else {
                                    _e('Alle artikelen ', 'newheap');
                                }
                                if(isset($_GET['category']) and !empty($_GET['category'])) {
                                    _e('In: ', 'newheap'); echo htmlentities(get_term($_GET['category'])->name);
                                }
                                ?>
                            <?php else: ?>
                                <?php printf(__('Artikelen van het type "%s"', 'newheap'), $term_column->name); ?>
                            <?php endif; ?>
                        </h3>
                    </div>

                    <?php if ($query->have_posts()) { ?>
                        <?php while ($query->have_posts()) {
                            $query->the_post(); $counter++;
                            if ($counter == 11) : ?>
                                <div class="col-sm-12 col-md-10 offset-md-1 google-ad-holder ad-between-posts-holder text-center">
                                    <div class="dotted-bottom-black pt-5 pb-5 mb-3">
                                        <div class="ad-between-posts">
                                            <?=get_field('advertisement_archive', 'option')?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="col-sm-12 col-md-10 offset-md-1">
                                <?php include get_theme_file_path('elements/article-listing-item.php'); ?>
                            </div>
                        <?php } ?>

                        <?php
                        global $posts;
                        if (count($posts) < 11) : ?>
                            <div class="col-sm-12 col-md-10 offset-md-1 google-ad-holder ad-between-posts-holder text-center">
                                <div class="pt-5 pb-5 mb-3">
                                    <div class="ad-between-posts">
                                        <?=get_field('advertisement_archive', 'option')?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="col-sm-12 col-md-10 offset-md-1 pb-3 mb-3">
                            <?php
                            $GLOBALS['wp_query']->max_num_pages = $query->max_num_pages;

                            echo the_posts_pagination([
                                'screen_reader_text' => ' ',
                                'mid_size' => 1,
                                'prev_text' => '<i class="fas fa-chevron-left"></i>',
                                'next_text' => '<i class="fas fa-chevron-right"></i>',
                            ]); ?>

                        </div>
                    <?php } else { ?>
                        <div class="col-sm-12 col-md-10 offset-md-1">
                            <?php _e('Geen resultaten gevonden.', 'newheap'); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>

    <?php get_footer();
}
