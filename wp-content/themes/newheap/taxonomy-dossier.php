<?php
get_header();

$term = get_queried_object();
$dossier_image = get_field('term_image', $term);

if (!empty($dossier_image)) {
	$dossier_image = get_field('term_image', $term)['url'];
} else {
	$dossier_image = get_field('placeholder_afbeelding', 'option');
}

if (is_array($dossier_image)) {
	$dossier_image = $dossier_image['url'];
}

$latest_update = \NewHeap\Theme\Helpers::get_taxonomy_latest_date($term);
?>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-10 offset-md-1">
                    <div class="header-image"
                         style="background-image: url('<?php echo $dossier_image; ?>');">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-8 offset-md-2">
                    <div class="header-content text-white bg-black remove-top-space p-5 pt-1 pb-3">
                        <div class="article-options">
                            <div class="text-start">
                                <span class="bg-black py-2">
                                    <i class="fas fa-folder-open"></i>
                                    <?php _e('Dossier', 'newheap'); ?>
                                </span>
                            </div>
                            <!--<div class="text-end">
                                <span class="bg-black py-2">
                                    <i class="fas fa-star"></i>
                                    <?php _e('Favoriet', 'newheap'); ?>
                                </span>
                            </div>-->
                        </div>
                        <hr class="mt-0">

                        <div class="tax-holder">
                            <h4 class="primary-color"><?php _e('Nieuwste bericht', 'newheap'); ?>: <?php echo $latest_update; ?></h4>
                        </div>

                        <h1 class="">
                            <?php echo $term->name; ?>
                        </h1>

                        <p class="mt-2">
                            <?php echo $term->description; ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="author-posts my-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-10 offset-md-1">
                    <h3 class="section-title">
	                    <?php printf(__('Artikelen uit het dossier "%s"', 'newheap'), $term->name); ?>
                    </h3>
                </div>

                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="col-sm-12 col-md-10 offset-md-1 mb-3">
                            <?php include get_theme_file_path('elements/article-listing-item.php'); ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </section>

<?php get_footer();

