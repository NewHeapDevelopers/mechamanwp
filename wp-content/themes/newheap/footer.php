</div>
<?php if (is_front_page()) { ?>
    <section class="prefooter my-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-6">
                    <h3 class="section-title"><?php _e('Agenda', 'newheap'); ?></h3>
					<?php
                    $args = [
                        'post_type' => 'agenda',
                        'posts_per_page' => 3,
                        'meta_key' => 'event_start_date',
                        'orderby' => 'meta_value',
                        'order' => 'ASC',
                        'meta_query' => [
                            [
                                'key' => 'event_end_date',
                                'value' => date('Y-m-d').' 00:00:00',
                                'compare' => '>=',
                                'type'=>'DATE'
                            ]
                        ]
                    ];
                    remove_all_filters('posts_orderby');
					$query = new \WP_Query($args);

					if ($query->have_posts()) {
						while ($query->have_posts()) {
							$query->the_post();

							$date = strtotime(get_field('event_start_date', get_the_ID()));
							$convertedDate = date('d-m-Y', $date);
							?>
                            <div class="bg-gray p-3 mb-1">
                                <a href="<?php echo get_field('agenda_archive_page', 'option');?>">
                                    <h4 class="bold text-black">
										<?php echo $convertedDate; ?> | <?php echo get_the_title(); ?>
                                    </h4>

                                    <p style="padding-bottom: 0;font-size:1em;">
										<?php echo get_the_excerpt(); ?>
                                    </p>
                                </a>
                            </div>
						<?php }
					} ?>

                    <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
                        <a href="<?php echo get_field('agenda_archive_page', 'option'); ?>">
							<?php _e('Volledige agenda', 'newheap') ?>
                            <div class="arrow-right pr-2 ml-20"></div>
                        </a>
                    </div>
                </div>

                <div class="col-sm-12 col-lg-6">
                    <h3 class="section-title"><?php _e('Uw mening', 'newheap'); ?></h3>

                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <?php include get_template_directory() . '/partials/latest-poll.php'; ?>

                            <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">
                                <a href="<?=site_url()?>/polls/">
                                    <?php _e('Uitslagen vorige polls', 'newheap') ?>
                                    <div class="arrow-right pr-2 ml-20"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <!--
                            <div class="poll-holder bg-gray p-3">
                                <h4 class="bold text-black">Enquête: wat mis je in de cabine?</h4>
                                <p class="">Uitleg over enquête Sed ut perspiciatis unde Omnis iste natus Error sit
                                    voluptatem accusantium Doloremque Laudantium,
                                    totam rem aperiam</p>

                                <p class="bold mt-2">Doe mee met de enquête</p>
                            </div>

                            <div class="see-more my-2 w-100 text-black bold py-2 text-right pr-2">Vorige enquêtes
                                <div class="arrow-right pr-2 ml-20"></div>
                            </div>-->
                            <?=get_field('advertisement_survey_footer', 'option')?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
<?php } ?>

<?php
$first_column = get_field('1e_kolom', 'option');
$second_column = get_field('2e_kolom', 'option');
$third_column = get_field('3e_kolom', 'option');
?>
<!-- Footer -->
<footer class="primary-background py-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4 text-white">
                <h4><?php echo $first_column['titel']; ?></h4>
                <p><?php echo $first_column['content']; ?></p>
            </div>

            <div class="col-sm-12 col-md-4 text-white">
                <h4><?php echo $second_column['titel']; ?></h4>
                <ul>
                    <?php foreach ($second_column['link'] as $link) : ?>
                        <li>
                            <a href="<?php echo $link['pagina']['url']; ?>" <?php echo (trim($link['pagina']['target']) === '_blank') ? 'target="_blank"' : ''; ?>>
	                            <?php echo $link['pagina']['title']; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <div class="col-sm-12 col-md-4 text-white">
                <h4><?php echo $third_column['titel']; ?></h4>
                <ul>
		            <?php foreach ($third_column['link'] as $link) : ?>
                        <li>
                            <a href="<?php echo $link['pagina']['url']; ?>" <?php echo (trim($link['pagina']['target']) === '_blank') ? 'target="_blank"' : ''; ?>>
					            <?php echo $link['pagina']['title']; ?>
                            </a>
                        </li>
		            <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

<style>
#cookieChoiceInfo {position: fixed; width: 100%; background-color: #fafafa; margin: 0px; left: 0px; color: #666; font-size: 12px; bottom: 0px; padding: 12px 10px; z-index: 1000; text-align: center; border-top: 2px solid #fff; font-family: arial, sans-serif; box-shadow: 0 0 8px rgba(0,0,0, 0.4); box-sizing: border-box; border-bottom: 10px solid #ccc;}
#cookieChoiceInfoLink {color: #2e8000}
#cookieChoiceDismiss {margin: 10px auto; padding: 7px 12px; border-radius: 8px; background-color: #2e8000; color: white; font-weight: bold; display: block;	width: 100%; max-width: 200px; font-size: 14px; font-family: arial, sans-serif;}
#cookieChoiceInfo::before {content: "Deze website maakt gebruik van cookies"; display:block; margin-bottom:5px; font-size:14px; font-family: arial, sans-serif; font-weight:bold;}
</style>
<script src="https://www.mechaman.nl/cookiechoices.js"></script>
<script>
  document.addEventListener('DOMContentLoaded', function(event) {
    cookieChoices.showCookieConsentBar('Deze website maakt gebruik van functionele en analytische cookies. Deze cookies maken het gebruik van deze site zo prettig mogelijk in het gebruik. U hoeft bijvoorbeeld niet steeds opnieuw gegevens in te voeren en kunnen wij u optimaal bedienen met goede artikelen.',
      'Ik ga akkoord', 'Meer info', 'https://www.mechaman.nl/privacybeleid/');
  });
</script>

</body>
</html>
