let NhHelpers = {
    init: function () {
        this.smoothScrollToId();
        this.searchToggleListener();
        this.toggleArchiveFilters();
        this.socialShareListener();
        this.adWatcher();
    },

    smoothScrollToId: function () {
        jQuery('a').on('click', function () {
            if (jQuery(this).attr('href').charAt(0) === '#' && jQuery(this).attr('href').length > 1) {
                jQuery('html, body').animate({
                    scrollTop: jQuery('a[name="' + jQuery(this).attr('href').replace('#', '') + '"]').offset().top - 50
                }, 300);
            }
        });
    },

    searchToggleListener: function () {
        jQuery('.logo-row .user-items .search-button-toggle').on('click', function (e) {
            e.preventDefault();
            jQuery('.logo-row .sub-header').toggleClass('active');
        });
    },

    toggleArchiveFilters: function () {
        if (jQuery('body.page-template-template-category-archive .btn-open-mobile-filters').length) {
            jQuery('body.page-template-template-category-archive .btn-open-mobile-filters').on('click', function () {
                jQuery('.filters-holder').addClass('mobile-active')
                jQuery(this).addClass('d-none')
            });
            jQuery('body.page-template-template-category-archive .btn-close-mobile-filters').on('click', function () {
                jQuery('.filters-holder').removeClass('mobile-active')
                jQuery('body.page-template-template-category-archive .btn-open-mobile-filters').removeClass('d-none')
            });
        }
    },

    socialShareListener: function () {
        if (jQuery('.urlToClipboard').length) {
            var $temp = $("<input>");
            var $url = $(location).attr('href');

            $('.urlToClipboard').on('click', function (e) {
                e.preventDefault();
                $("body").append($temp);
                $temp.val($url).select();
                document.execCommand("copy");
                $temp.remove();
                $(".urlToClipboard span").show();
                $(".urlToClipboard span").html("URL gekopieerd!");
                setTimeout(function () {
                    $(".urlToClipboard span").fadeOut();
                }, 1500);
            })
        }
    },

    adWatcher: function() {
        jQuery('.ad-holder').each(function() {
            if (jQuery(this).find('iframe').length) {
                var adHolder = this;

                setTimeout(function() {
                    if (jQuery(adHolder).prev().hasClass('ad-title')) {
                        jQuery(jQuery(adHolder).prev()).hide();
                    }
                }, 500)
            }
        });
    }
};

let NhRest = {
    currentPage: null,
    maxNumPages: null,
    currentFilters: null,
    oldFilters: null,
    paginationItems: null,

    init: function () {
        if (jQuery('.nh-rest-required').length) {
            this.currentPage = 1;
            this.paginationItems = 5;

            if (AppConfig.hasOwnProperty('pagination') && AppConfig.pagination.hasOwnProperty('maxNumPages')) {
                this.maxNumPages = AppConfig.pagination.maxNumPages;
            } else {
                this.maxNumPages = 1;
            }

            jQuery('[data-filter-name="clear"]').click(function () {
                $('.btn-filter.active').trigger('click');
            });

            this.resetFilters()
            this.pageListener();
            this.headerSearchListener();
            this.filterChangeListener();
            this.updatePagination();
            this.getDossiers();
            this.getPosts();
        }
    },

    headerSearchListener: function () {
        let self = this;
        let params = (new URL(document.location)).searchParams;
        let keyword = params.get('search'); // is the string "Jonathan Smith".
        let category = params.get('category'); // is the string "Jonathan Smith".

        if (keyword) {
            self.currentFilters.keyword = keyword;
        }


        if (category) {
            if (!Array.isArray(self.currentFilters.taxonomy.category)) {
                self.currentFilters.taxonomy.category = [];
            }

            self.currentFilters.taxonomy.category.push(category);

            jQuery('.filter-holder[data-taxonomy-name=category]').find('[data-term-id=' + category + ']').addClass('filter-active active');
        }
    },

    /**
     * Reset the filters to its original state
     */
    resetFilters: function () {
        this.currentFilters = {
            orderby: [],
            order: [],
            premium: [],
            taxonomy: {},
            currentPage: 1,
            keyword: null
        };

        let params = (new URL(document.location)).searchParams;
        let keyword = params.get('search'); // is the string "Jonathan Smith".
        let category = params.get('category'); // is the string "Jonathan Smith".

        if (keyword) {
            this.currentFilters.keyword = keyword;
        }
    },

    /**
     * List for filter changes
     */
    filterChangeListener: function () {
        let self = this;

        jQuery('.filters-holder .filter-holder button.btn-filter').on('click', function () {
            let filterHolder = jQuery(this).parentsUntil('.filter-holder').parent();

            if (filterHolder.data('filter-name') === 'sort') {
                jQuery(filterHolder).find('.filter-active').removeClass('filter-active active');
            }

            jQuery(this).toggleClass('filter-active active');

            self.setFilters();
        });
    },

    /**
     * Update the currently selected filters
     */
    setFilters: function () {
        let self = this;

        self.resetFilters();

        //Add remove filters if there is a filter actie
        if (jQuery('.btn-filter.filter-active').length > 0) {
            jQuery('[data-filter-name="clear"]').show();
        } else {
            jQuery('[data-filter-name="clear"]').hide();
        }

        jQuery('.btn-filter.filter-active').each(function () {
            let filterHolder = jQuery(this).parentsUntil('.filter-holder').parent()
            let filterName = filterHolder.data('filter-name');

            if (filterName === 'taxonomy') {
                let taxonomy = filterHolder.data('taxonomy-name');

                if (!self.currentFilters.taxonomy.hasOwnProperty(taxonomy)) {
                    self.currentFilters.taxonomy[taxonomy] = [];
                }
                self.currentFilters.taxonomy[taxonomy].push(jQuery(this).data('term-id'));
                // self.currentFilters.taxonomy.[taxonomy].push(jQuery(this).data('term-id'));
            } else if (filterName === 'sort') {
                self.currentFilters.order = jQuery(this).data('value');
            } else {
                self.currentFilters[filterName].push(jQuery(this).data('value'));
            }
        });

        self.getPosts();
    },

    /**
     * Get posts for requested page
     */
    pageListener: function () {
        let self = this;

        if (jQuery('.posts-pagination').length) {
            jQuery('body').on('click', '.posts-pagination li button', function () {
                // Set the current page
                self.currentPage = parseInt(jQuery(this).val());

                self.getPosts();
            });
        }
    },

    /**
     * Get posts by filters
     */
    getPosts: function () {
        let self = this;

        let postData = {
            orderby: this.currentFilters.orderby,
            order: this.currentFilters.order,
            premium: this.currentFilters.premium,
            taxonomies: this.currentFilters.taxonomy,
            paged: this.currentPage,
            keyword: this.currentFilters.keyword,
        };

        //Clear posts so its clear we are loading new ones
        jQuery('.posts-holder .posts-loop > .row').html('<div class="col-12"><strong>Laden...</strong></div>');

        jQuery.ajax({
            method: 'POST',
            url: AppConfig.siteUrl + '/wp-json/newheap/v1/get-posts',
            data: postData
        }).done(function (response) {
            if (response.status == 200) {
                self.apiResponse = response
                self.maxNumPages = response.data.max_num_pages;

                self.updatePosts();
                self.updatePagination();
            } else {
                alert('Er is iets misgegaan. Probeer het nogmaals')
            }
        });
    },

    updatePagination: function () {
        let self = this;

        let output = '';

        // Previous buttons
        output += '<li><button class="btn-prev" value="' + (self.currentPage - 1) + '"><i class="fas fa-chevron-left"></i></button></li>'
        output += '<li><button class="btn-prev btn-prev-first" value="1">1</button></li>'

        if (self.currentPage >= Math.ceil(self.paginationItems / 2)) {
            output += '<li><span>...</span></li>'
        }

        if (self.currentPage > 1) {
            output += '<li><button value="' + (self.currentPage - 1) + '">' + (self.currentPage - 1) + '</button></li>'
        }

        output += '<li><button class="btn-current-page" value="' + (self.currentPage) + '">' + (self.currentPage) + '</button></li>'

        if (self.currentPage < self.maxNumPages) {
            output += '<li><button value="' + (self.currentPage + 1) + '">' + (self.currentPage + 1) + '</button></li>'
        }

        if (self.currentPage < (self.maxNumPages - 1)) {
            output += '<li><span>...</span></li>'
        }

        // Next buttons
        output += '<li><button class="btn-next btn-next-last" value="' + self.maxNumPages + '">' + self.maxNumPages + '</button></li>'
        output += '<li><button class="btn-next" value="' + (self.currentPage + 1) + '"><i class="fas fa-chevron-right"></i></button></li>'

        jQuery('.posts-pagination ul').html(output)

        // Check if we should enable/show prev buttons
        if (self.currentPage >= Math.ceil(self.paginationItems / 2)) {
            jQuery('.posts-pagination .btn-prev.btn-prev-first').show();
            jQuery('.posts-pagination .btn-prev').removeClass('disabled');
        } else {
            jQuery('.posts-pagination .btn-prev.btn-prev-first').hide();

            if (self.currentPage === 1) {
                jQuery('.posts-pagination .btn-prev').addClass('disabled');
            }
        }

        // Check if we should enable/show next buttons
        if (self.currentPage < (self.maxNumPages - 1)) {
            jQuery('.posts-pagination .btn-next.btn-next-last').show();
            jQuery('.posts-pagination .btn-next').removeClass('disabled');
        } else {
            jQuery('.posts-pagination .btn-next.btn-next-last').hide();

            if (self.currentPage === self.maxNumPages) {
                jQuery('.posts-pagination .btn-next').addClass('disabled');
            }
        }
    },

    updatePosts: function () {
        let self = this

        if (self.apiResponse.data) {
            // Clear the posts
            jQuery('.posts-holder .posts-loop > a, .posts-holder .posts-loop > div').remove();

            for (let key in self.apiResponse.data) {
                // Show the ad after the first 10 posts
                if (key == 10) {
                    let ad = jQuery('.template-holder .ad-between-posts-holder').clone()
                    jQuery('.posts-holder .posts-loop').append(ad);
                }

                jQuery('.posts-holder .posts-loop').append(self.apiResponse.data[key].nh_meta.html);

                NhRest.maxNumPages = self.apiResponse.data[key].nh_meta.max_num_pages;
            }

            if (self.apiResponse.data.length < 10) {
                let ad = jQuery('.template-holder .ad-between-posts-holder').clone()

                jQuery(ad).find('.dotted-bottom-black').removeClass('dotted-bottom-black')

                jQuery('.posts-holder .posts-loop').append(ad);
            }
        } else {
            jQuery('.posts-holder .posts-loop > .row').html('<div class="col-md-12">Geen resultaten gevonden.</div>');
        }
    },

    getDossiers: function () {
        let self = this;

        let postData = {
            number: this.currentFilters.number
        };

        jQuery.ajax({
            method: 'POST',
            url: AppConfig.siteUrl + '/wp-json/newheap/v1/get-dossiers',
            data: postData
        }).done(function (response) {
            if (response.status == 200) {
                self.apiDossierResponse = response

                self.updatePagination()
                self.updateDossiers();
            } else {
                alert('Er is iets misgegaan. Probeer het nogmaals')
            }
        });
    },

    updateDossiers: function () {
        let self = this

        if (Object.keys(self.apiDossierResponse.data).length) {
            // Clear the posts
            jQuery('.dossiers-holder .dossiers-loop > .row').html('');
            let i = 0;
            for (let key in self.apiDossierResponse.data) {
                if (i < 4) {
                    // Clone element to populate
                    let template = jQuery('.template-holder .dossier-item').clone();

                    // Populate element
                    template.find('a').attr('href', self.apiDossierResponse.data[key].nh_meta.url);
                    template.find('.dossiers-image-holder').attr('style', 'background-image: url("' + self.apiDossierResponse.data[key].nh_meta.image + '");');
                    // template.find('img').attr('alt', self.apiDossierResponse.data[key].name);
                    template.find('h3').html(self.apiDossierResponse.data[key].name);
                    template.find('.date-holder').html(self.apiDossierResponse.data[key].nh_meta.last_modified_date);

                    jQuery('.dossiers-holder .dossiers-loop > .row').append(template);
                    i++;
                }
            }
        } else {

        }
    }
};

let ShowMore = {
    init: function () {
        if (jQuery('.load-more-btn').length) {
            this.loadMoreListener();
        }
    },

    loadMoreListener: function () {
        let self = this;

        jQuery('.load-more-btn').on('click', function () {
            jQuery(this).parentsUntil('section').parent().addClass('toggled').find('.nh-tile.d-none, .col-sm-12.relative.d-none').removeClass('d-none');
        })
    }
}

let NhMagazineArchive = {
    init: function() {
        this.filterListener();
        this.toggleArchiveFilters();
    },

    filterListener: function() {
        let self = this;

        jQuery('.filters-holder .filter-holder button.btn-filter').on('click', function () {
            let filterHolder = jQuery(this).parentsUntil('.filter-holder').parent();

            if (filterHolder.data('filter-name') === 'sort') {
                jQuery(filterHolder).find('.filter-active').removeClass('filter-active active');
            }

            jQuery(this).toggleClass('filter-active active');

            self.filterMagazines();
        });
    },

    filterMagazines: function() {
        let filteredYears = [];
        jQuery('#filter-page .filter-holder button.active').each(function() {
            filteredYears.push(jQuery(this).data('value'));
        });

        if (filteredYears.length) {
            jQuery('.magazines-archive .magazine-item, .magazines-archive .magazine-year-title').each(function() {
                if (filteredYears.includes(jQuery(this).data('date'))) {
                    jQuery(this).show();
                } else {
                    jQuery(this).hide();
                }
            });
        } else {
            jQuery('.magazines-archive .magazine-item, .magazines-archive .magazine-year-title').show();
        }
    },

    toggleArchiveFilters: function () {
        if (jQuery('body.page-template-template-dossier-archive .btn-open-mobile-filters').length) {
            jQuery('body.page-template-template-dossier-archive .btn-open-mobile-filters').on('click', function () {
                jQuery('.filters-holder').addClass('mobile-active')
                jQuery(this).addClass('d-none')
            });
            jQuery('body.page-template-template-dossier-archive .btn-close-mobile-filters').on('click', function () {
                jQuery('.filters-holder').removeClass('mobile-active')
                jQuery('body.page-template-template-dossier-archive .btn-open-mobile-filters').removeClass('d-none')
            });
        }
    },
}

jQuery(function () {
    NhHelpers.init();

    // Toggle registration fields
    if (jQuery('#registration-form').length) {
        jQuery('#registration-form .show-subscriptions').on('click', function () {
            jQuery('#registration-form .subscription-fields').toggleClass('d-none');
        });

        jQuery('#registration-form .payment-options input[type=radio]').on('change', function () {
            if (jQuery(this).val() === '1') {
                jQuery('#registration-form .automatische-incasso').removeClass('d-none');
            } else {
                jQuery('#registration-form .automatische-incasso').addClass('d-none');
            }
        });

        jQuery('#registration-form').on('submit', function (e) {
            if (!jQuery('input[name=mobile_phone]').length) {
                return;
            }

            // Required at least one phone number
            if (!jQuery('input[name=mobile_phone]').val() && !jQuery('input[name=phone]').val()) {
                // Remove old messages
                jQuery('.required-phone').remove();

                jQuery('input[name=mobile_phone]').after('<p class="required-phone">Vul a.u.b. een mobiel nummer of telefoonnummer in. </p>')
                jQuery('input[name=phone]').after('<p class="required-phone">Vul a.u.b. een mobiel nummer of telefoonnummer in. </p>')

                return false;
            }
        });
    }

    if (jQuery('.nh-rest-required').length && !jQuery('.nh-rest-required-false').length) {
        NhRest.init();
    }

    if (jQuery('.nh-rest-required-false').length) {
        NhMagazineArchive.init();
    }

    ShowMore.init();
});

/*SLICK sliders*/
jQuery(function () {
    $('section.single-slider .slider').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 3.5,
        slidesToScroll: 1
    });
    $("section.single-slider .slick-next").text("");
    $("section.single-slider .slick-prev").text("");
    $("section.single-slider .slick-dots li button").text("");
    $('.category-dropdown').change(function () {
        if ($(this).val() != "") {
            $('.search-holder').submit();
        }
    });
});
