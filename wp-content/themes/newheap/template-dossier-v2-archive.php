<?php
/**
 * Template Name: Dossier archief
 */

$category_id = \NewHeap\Theme\Taxonomy\Taxonomy::get_archive_page_category_id( get_the_ID() );


$terms = get_terms( [
	'taxonomy' => 'dossier',
] );

get_header();
if ( ! empty( $terms ) ) : ?>
    <div class="content-holder pt-4">
        <div class="dossiers-grid">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-10 offset-md-1">
                        <h1 class="section-title">
                            <?php _e( 'Dossiers', 'newheap' ); ?>
                        </h1>

                        <div class="row">
                            <?php foreach ( $terms as $term ) : ?>
	                            <?php
	                            $dossier_image = get_field('term_image', $term);
	                            if (!empty($dossier_image)) {
		                            $dossier_image = $dossier_image['sizes']['medium'];
	                            } else {
		                            $dossier_image = get_field('placeholder_afbeelding', 'option')['sizes']['medium'];
	                            }
	                            ?>

                                <div class="col-sm-12 col-md-4">
                                    <a href="<?php echo get_term_link($term, 'dossier'); ?>" class="dossier-item ">
                                        <img src="<?php echo $dossier_image; ?>" width="100%" height="auto"/>
                                        <div class="meta-holder dotted-top">
                                            <h3 class="uppercase" title="<?php echo $term->name; ?>"><?php echo $term->name; ?></h3>
                                        </div>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;

get_footer();
