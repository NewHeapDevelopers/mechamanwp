<?php
/**
 * Template Name: Multimedia type archief
 */

get_header();


$counter = 0;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$args = [
    'post_type' => 'post',
    'paged' => $paged
];


$args['tax_query'] = [
    [
        'taxonomy' => 'artikel_type',
        'field' => 'slug',
        'terms' => ['fotoreportages', 'videos', 'infographics', 'podcasts']
    ]
];

if(isset($_GET['category']) and !empty($_GET['category'])) {
    $args['cat'] = $_GET['category'];
}
$query = new WP_Query($args);

$term_column = get_term_by('slug', 'videos', 'artikel_type');
$columns_image = get_field('term_image', $term_column);

if (has_post_thumbnail()) {
    $columns_image = get_the_post_thumbnail_url(null, 'large');
} else {
    $columns_image = get_field('placeholder_afbeelding', 'option')['sizes']['medium_large'];
}

$latest_update = \NewHeap\Theme\Helpers::get_taxonomy_latest_date($term_column);
?>

<section class="author-posts my-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 offset-md-1">
                <h3 class="section-title">
                    <?php printf(__('Multimedia artikelen', 'newheap'), $term_column->name); ?>
                </h3>
            </div>

            <?php if ($query->have_posts()) { ?>
                <?php while ($query->have_posts()) {
                    $query->the_post(); $counter++;

                    $broadcast_data = ThreeWP_Broadcast()->get_post_broadcast_data( get_current_blog_id(), get_the_ID());
                    $parent = $broadcast_data->get_linked_parent();

                    if ($counter == 11) : ?>
                        <div class="col-sm-12 col-md-10 offset-md-1 google-ad-holder ad-between-posts-holder text-center">
                            <div class="dotted-bottom-black pt-5 pb-5 mb-3">
                                <div class="ad-between-posts">
                                    <?=get_field('advertisement_archive', 'option')?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="col-sm-12 col-md-10 offset-md-1 theme-<?php if(empty($parent['blog_id'])) { echo get_current_blog_id(); } else { ?><?php echo $parent['blog_id']; ?><?php } ?>">
                        <a href="<?php the_permalink(); ?>" class="dotted-bottom-black pb-3 mb-3 d-block">
                            <div class="row">
                                <div class="col-md-3 relative">
                                    <div class="image-holder">
                                        <div class="image" style="background-image: url('<?php echo get_the_post_thumbnail_url(null, 'large'); ?>');"></div>
                                    </div>

                                    <?php if (get_field('premium_article')) : ?>
                                        <div class="content-type text-white absolute">
                                            <span class="primary-background px-4 pl-3 py-1"><i class="far fa-gem"></i> Premium</span>
                                        </div>
                                    <?php endif; ?>
                                </div>

                                <div class="col-md-9 my-2">
                                    <h5 class="p-2 mb-0 pt-3 pb-0 dot-before">
                                        <?php if(!empty($parent['blog_id'])) { ?><?php echo get_blog_details($parent['blog_id'])->blogname; ?><?php } else { ?><?php echo bloginfo('name'); ?><?php } ?> <br/>
                                    </h5>
                                    <h2 class="bold mb-3"><?php the_title(); ?></h2>
                                    <div class="bold">
                                            <span>
                                                <?php echo get_the_date('d-m-Y'); ?>
                                                |
                                                <i class="far fa-clock"></i>
                                                <?php echo \NewHeap\Theme\Helpers::get_read_time(get_the_ID()); ?>
                                                |
                                                <i class="fas fa-pen-nib"></i>
                                                <?php echo get_the_author_meta('display_name'); ?>
                                            </span>
                                    </div>

                                    <p class="mt-2">
                                        <?php the_excerpt(); ?>
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php } ?>

                <?php
                global $posts;
                if (count($posts) < 11) : ?>
                    <div class="col-sm-12 col-md-10 offset-md-1 google-ad-holder ad-between-posts-holder text-center">
                        <div class="pt-5 pb-5 mb-3">
                            <div class="ad-between-posts">
                                <?=get_field('advertisement_archive', 'option')?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="col-sm-12 col-md-10 offset-md-1 pb-3 mb-3">
                    <?php
                    $GLOBALS['wp_query']->max_num_pages = $query->max_num_pages;

                    echo the_posts_pagination([
                        'screen_reader_text' => ' ',
                        'mid_size' => 1,
                        'prev_text' => '<i class="fas fa-chevron-left"></i>',
                        'next_text' => '<i class="fas fa-chevron-right"></i>',
                    ]); ?>

                </div>
            <?php } else { ?>
                <div class="col-sm-12 col-md-10 offset-md-1">
                    <?php _e('Geen resultaten gevonden.', 'newheap'); ?>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

<?php get_footer();
