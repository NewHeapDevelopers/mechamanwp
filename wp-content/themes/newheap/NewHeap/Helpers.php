<?php
namespace NewHeap\Theme;

// Prevent direct access
if (!defined('ABSPATH')) {
    exit;
}

class Helpers
{
	const WORDS_READ_PER_MINUTE = 225; // General 200 - 250 words a minute

    /**
     * Get the current URL
     *
     * @return string
     * @author Ferhat Ileri <ferhat@codio.nl>
     */
    public static function get_current_url()
    {
        $currentPageUrl  = is_ssl() ? 'https://' : 'http://';
        $currentPageUrl .= $_SERVER['HTTP_HOST'];
        $currentPageUrl .= $_SERVER['REQUEST_URI'];

        return $currentPageUrl;
    }

    /**
     * Redirect back to previous page
     *
     * @author Ferhat Ileri <ferhat@codio.nl>
     */
    public static function redirect_back()
    {
        if (!empty($_SERVER['HTTP_REFERER'])) {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
        } else {
            header('Location: ' . site_url());
        }
    }

    /**
     * Generate a random string
     *
     * @param int $length
     * @return string
     * @author Ferhat Ileri <ferhat@codio.nl>
     */
    public static function generate_random_string($length = 20) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

	public static function add_advertising_before_h2($string)
	{
		$ad = "<!-- /6275480/EL_Rectangle --><div id='div-gpt-ad-1568107531755-0' class=\"content-rectangle\"><script>googletag.cmd.push(function() { googletag.display('div-gpt-ad-1568107531755-0'); });</script></div>";

		return substr_replace($string, $ad, strpos($string, '<h2>'), 0);
	}

    public static function add_advertising_before($string, $tag = "<h2>")
    {
        if(strstr($string, $tag)) {
            $ad = "<center>".get_field('advertisement_blog_text', 'option')."</center>";
            return substr_replace($string, $ad, strpos($string, $tag), 0);
        } else {
        	return $string;
        }
    }

	/**
	 * Get the read time for a post
	 *
	 * @param $post_id
	 *
	 * @return string
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
    public static function get_read_time($post_id)
    {
        $read_minutes = ceil(str_word_count(strip_tags(get_the_content(null, false, $post_id))) / \NewHeap\Theme\Helpers::WORDS_READ_PER_MINUTE);

        if($read_minutes < 1) { $read_minutes = 1; }

        if ($read_minutes == 1) {
            return $read_minutes . ' ' . __('minuut', 'newheap');
        } else {
            return $read_minutes . ' ' . __('minuten', 'newheap');
        }
    }

	/**
	 * @param int $post_id
	 *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
    public static function update_post_views(int $post_id)
    {
    	$post_views = get_post_meta($post_id, 'post_view_count', true);

    	if (!empty($post_id)) {
    		$post_views = $post_views + 1;
	    } else {
		    $post_views = 1;
	    }

    	update_post_meta($post_id, 'post_view_count', $post_views);
    }

    public static function get_taxonomy_latest_date(\WP_Term $term)
    {
    	$posts = get_posts([
    		'post_type' => 'post',
		    'number_posts' => 15,
		    'orderby' => 'post_date',
		    'order' => 'DESC',
		    'suppress_filters' => true,
		    'tax_query' => [
		    	[
			        'taxonomy' => 'dossier',
			        'field' => 'term_id',
			        'terms' => $term->term_id,
				    'include_children' => false,
			    ]
		    ]
	    ]);

    	if (!empty($posts)) {
		    $latest_update = \DateTime::createFromFormat('Y-m-d H:i:s', $posts[0]->post_date);

		    return $latest_update->format('d-m-Y');
	    }
    }

    public static function get_type_icon($post_id)
    {
    	$article_type = get_the_terms($post_id, 'artikel_type');

    	if (empty($article_type)) {
    		return;
	    }
        // altijd maar 1 ding aanvinken, standaard artikel, altijd 1 soort aan hebben staan

    	switch ($article_type[0]->slug) {
		    case 'columns' :
		    	$icon = '<i class="fas fa-pen-nib"></i>';
		    	break;
		    case 'fotoreportages' :
		    	$icon = '<i class="fas fa-images"></i>';
		    	break;
		    case 'infographics' :
		    	$icon = '<i class="fas fa-chart-pie"></i>';
		    	break;
		    case 'podcasts' :
		    	$icon = '<i class="fas fa-microphone-alt"></i>';
		    	break;
		    case 'tests' :
		    	$icon = '<i class="fas fa-clipboard-list"></i>';
		    	break;
		    case 'videos' :
		    	$icon = '<i class="fas fa-film"></i>';
		    	break;
            default:
                $icon = '<i class="fas fa-question"></i>';
                break;
	    }

	    return $icon;
    }

    public static function get_post_article_type($post_id)
    {
	    $article_type = get_the_terms($post_id, 'artikel_type');

	    return $article_type[0];
    }
}
