<?php
namespace NewHeap\Theme\Notification;

class Notification {
    const LOCATION_ZENO = 'zeno';

    public function __construct()
    {
    }

    /**
     * Add success notifications
     *
     * @param string $message
     * @author Ferhat Ileri <ferhat@codio.nl>
     */
    public static function add_success(string $message)
    {
//        self::ajax_notification($message);

        $_SESSION['notifications']['success'][] = $message;
    }

    /**
     * Add notice notifications
     *
     * @param string $message
     * @author Ferhat Ileri <ferhat@codio.nl>
     */
    public static function add_notice(string $message)
    {
//        self::ajax_notification($message);

        $_SESSION['notifications']['notice'][] = $message;
    }

    /**
     * Add error notifications
     *
     * @param string $message
     * @author Ferhat Ileri <ferhat@codio.nl>
     */
    public static function add_error(string $message, $location = null)
    {
	    $message = self::translate_zeno_errors($message);

//        self::ajax_notification($message);
        if ($location) {
	        $_SESSION['notifications'][$location]['errors'][] = $message;
        } else {
	        $_SESSION['notifications']['errors'][] = $message;
        }
    }

    public static function ajax_notification($message)
    {
        if (wp_doing_ajax()) {
            echo json_encode([
                'error' => $message
            ]);

            exit;
        }
    }

    /**
     * Show the notifications once
     *
     * @author Ferhat Ileri <ferhat@codio.nl>
     */
    public static function show_notifications($location = null)
    {
        if (!wp_doing_ajax()) {
            if ($location) {
	            foreach ($_SESSION['notifications'][$location]['errors'] ?? [] as $error) : ?>
                    <div class="notification-error">
			            <?php echo $error; ?>
                    </div>
	            <?php endforeach;

	            foreach ($_SESSION['notifications'][$location]['success'] ?? [] as $success) : ?>
                    <div class="notification-success">
			            <?php echo $success; ?>
                    </div>
	            <?php endforeach;

	            foreach ($_SESSION['notifications'][$location]['notice'] ?? [] as $notice) : ?>
                    <div class="notification-notice">
			            <?php echo $notice; ?>
                    </div>
	            <?php endforeach;

	            self::clear_notifications($location);
            } else {
	            foreach ($_SESSION['notifications']['errors'] ?? [] as $error) : ?>
                    <div class="notification-error">
			            <?php echo $error; ?>
                    </div>
	            <?php endforeach;

	            foreach ($_SESSION['notifications']['success'] ?? [] as $success) : ?>
                    <div class="notification-success">
			            <?php echo $success; ?>
                    </div>
	            <?php endforeach;

	            foreach ($_SESSION['notifications']['notice'] ?? [] as $notice) : ?>
                    <div class="notification-notice">
			            <?php echo $notice; ?>
                    </div>
	            <?php endforeach;

	            self::clear_notifications();
            }
        }
    }

    /**
     * Set/clear notifications
     *
     * @author Ferhat Ileri <ferhat@codio.nl>
     */
    public static function clear_notifications($location = null)
    {
        if ($location) {
	        $_SESSION['notifications'][$location] = [
		        'success' => [],
		        'notice' => [],
		        'errors' => [],
	        ];
        } else {
	        $_SESSION['notifications']['success'] = [];
	        $_SESSION['notifications']['notice'] = [];
	        $_SESSION['notifications']['errors'] = [];
        }
    }

	/**
     * Translate zeno errors to understandable errors
     *
	 * @param $message
	 *
	 * @return mixed|string|void
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
	public static function translate_zeno_errors($message)
	{
		if (strpos($message, 'error100') !== false) {
			$message = __('Er is al een particulier account aangemaakt op dit adres. Vul een bedrijfsnaam in of neem contact op met ons. Wij kunnen dan een account voor u aanmaken.');
		}

		return $message;
	}
}
