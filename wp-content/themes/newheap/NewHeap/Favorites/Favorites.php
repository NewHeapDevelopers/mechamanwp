<?php
namespace NewHeap\Theme\Favorites;

/**
 * Class Favorites
 * @package NewHeap\Theme\Favorites
 *
 * Provide data-favorite-id="<POSTID>" class on HTML attribute
 * Class is-favorite will be added if is favorite for current user
 * Alert is shown when user is not logged in
 */
class Favorites
{
    public function __construct()
    {
        $this->addACFFields();

        /** Get favorites for current user */
        add_action('wp_ajax_get_favorites', [$this, 'getFavoritesAjax']);
        add_action('wp_ajax_nopriv_get_favorites', [$this, 'getFavoritesAjax']);

        /** Add favorite for current user */
        add_action('wp_ajax_add_favorite',  [$this, 'addFavoriteAjax']);
        add_action('wp_ajax_nopriv_add_favorite',  [$this, 'addFavoriteAjax']);

        /** Remove favorite for current user */
        add_action('wp_ajax_remove_favorite',  [$this, 'removeFavoriteAjax']);
        add_action('wp_ajax_nopriv_remove_favorite',  [$this, 'removeFavoriteAjax']);

        /** enqueue script */
	    add_action('wp_enqueue_scripts', [$this, 'enqueueScriptsStyles'], 11);

    }

    public function enqueueScriptsStyles()
    {
	    wp_enqueue_script('nh-favorites-script', get_template_directory_uri() . '/NewHeap/Favorites/assets/favorites.js', ['newheap-script', 'jquery'], false, true);
    }


    private function addACFFields()
    {
        if (function_exists('acf_add_local_field_group')) {
            acf_add_local_field_group(array(
                'key' => 'group_607820a82e7a8',
                'title' => 'Extra gebruikersinformatie',
                'fields' => array(
                    array(
                        'key' => 'field_60812b99a2475',
                        'label' => 'Favorieten',
                        'name' => 'favorites',
                        'type' => 'textarea',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'post_type' => '',
                        'taxonomy' => '',
                        'allow_null' => 1,
                        'multiple' => 1,
                        'return_format' => 'id',
                        'ui' => 1,
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'user_form',
                            'operator' => '==',
                            'value' => 'all',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            ));
        }
    }

    public function addFavoriteAjax()
    {
        $favoriteKey = $_POST['favoriteKey'];

        if(!get_current_user_id()) {
            echo json_encode((object)['success' => false, 'message' => 'User not logged in', 'code' => 'not_logged_in']);
            die();
        }

        if(!empty($favoriteKey)) {
            if($this->addFavorite($favoriteKey)) {
                echo json_encode((object)['success' => true]);
                die();
            } else {
                echo json_encode((object)['success' => false, 'message' => 'Could not update favorite field', 'code' => 'could_not_update']);
                die();
            }
        } else {
            echo json_encode((object)['success' => false, 'message' => 'Please provide key', 'code' => 'no_post_id']);
            die();
        }
    }

    public function removeFavoriteAjax()
    {
        $favoriteKey = $_POST['favoriteKey'];

        if(!get_current_user_id()) {
            echo json_encode((object)['success' => false, 'message' => 'User not logged in', 'code' => 'not_logged_in']);
            die();
        }

        if(!empty($favoriteKey)) {
            if($this->removeFavorite($favoriteKey)) {
                echo json_encode((object)['success' => true]);
                die();
            } else {
                echo json_encode((object)['success' => false, 'message' => 'Could not update favorite field', 'code' => 'could_not_update']);
                die();
            }
        } else {
            echo json_encode((object)['success' => false, 'message' => 'Please provide key', 'code' => 'no_post_id']);
            die();
        }
    }

    public function getFavoritesAjax()
    {
        if(!get_current_user_id()) {
            echo json_encode((object)['success' => false, 'message' => 'User not logged in', 'code' => 'not_logged_in']);
            die();
        }

        $favorites = $this->getFavorites();

        if(empty($favorites)) {
            echo "[]"; die();
        }

        echo json_encode($favorites);
        die();
    }

    public static function getFavorites($siteId = null, $type = null) {
        $data = json_decode(get_field('favorites', 'user_'.get_current_user_id()));

        //filter data not wished for
        foreach($data as $key => $value) {
            $explode = explode(';', $value);
            if(!empty($siteId)) {
                if($explode[1] != $siteId) {
                    unset($data[$key]);
                }
            }
        }

        return $data;
    }
    public function addFavorite($favoriteKey = null) {
        if(empty($postId)) { $postId = get_the_ID(); }

        $favorites = get_field('favorites', 'user_'.get_current_user_id());
        if(!empty($favorites)) {
            $favorites = json_decode($favorites);
            if(empty($favorites)) { $favorites = []; }
        } else {
            $favorites = [];
        }


        if(!in_array($favoriteKey, $favorites)) {
            $favorites[] = $favoriteKey;
        }

        return update_field('favorites', json_encode($favorites), 'user_'.get_current_user_id());
    }

    public function getFavoriteKey($postId = null, $siteId = null, $type = null) {
        return $postId . ";" . $siteId . ";" . $type;
    }

    public function removeFavorite($favoriteKey) {
        $favorites = json_decode(get_field('favorites', 'user_'.get_current_user_id()));
        foreach($favorites as $key => $favo) { if($favo == $favoriteKey) { unset($favorites[$key]); } }

        $favorites = array_values($favorites);

        return update_field('favorites', json_encode($favorites), 'user_'.get_current_user_id());
    }
}
