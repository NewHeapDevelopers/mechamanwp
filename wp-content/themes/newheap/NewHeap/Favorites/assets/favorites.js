var favorites = [];
var isLoggedIn = true;

/** Get all favorites and set add class **/
jQuery.ajax({
    url: NewHeap.ajax_url,
    type: 'post',
    data: {
        action: 'get_favorites'
    },
    success: (json) => {
        favorites = JSON.parse(json);

        if(favorites.code === "not_logged_in") {
            isLoggedIn = false;
        } else {
            favorites.forEach(function(favoriteKey) {
                jQuery('[data-favorite-key="'+favoriteKey+'"]').addClass('is-favorite');
                jQuery('[data-favorite-click="'+favoriteKey+'"]').addClass('is-favorite');
            });
        }
    },
    error: () => {
        console.log('Could not fetch favorites')
    },
}).done(function () {
    console.log('done');
});

jQuery(document).on('click', '[data-favorite-click]', function(e) {
    e.preventDefault();
    e.stopPropagation(); // TODO: Check if obsolete
    var favoriteKey = $(this).attr('data-favorite-click');
    if(isLoggedIn) {
        if($(this).hasClass('is-favorite')) {
            jQuery('[data-favorite-key="'+favoriteKey+'"]').removeClass('is-favorite');
            jQuery('[data-favorite-click="'+favoriteKey+'"]').removeClass('is-favorite');
            jQuery('[data-favorite-key="'+favoriteKey+'"].delete-on-click').remove();
            jQuery.ajax({
                url: NewHeap.ajax_url,
                type: 'post',
                data: {
                    action: 'remove_favorite',
                    favoriteKey: favoriteKey
                },
                success: (data) => {
                    var response = JSON.parse(data);

                    if (response.success) {
                        jQuery(this).removeClass('is-favorite');

                        // Remove item from list (favorites template pages)
                        if (typeof removeFavoriteElement !== "undefined") {
                            removeFavoriteElement(favoriteKey);
                        }
                    }
                },
                error: () => {
                    alert('Oeps, we konden het bericht niet opslaan aan favorieten');
                },
            });
        } else {
            jQuery('[data-favorite-key="'+favoriteKey+'"]').addClass('is-favorite');
            jQuery('[data-favorite-click="'+favoriteKey+'"]').addClass('is-favorite');
            jQuery.ajax({
                url: NewHeap.ajax_url,
                type: 'post',
                data: {
                    action: 'add_favorite',
                    favoriteKey: favoriteKey
                },
                success: (data) => {
                    var response = JSON.parse(data);

                    if (response.success) {
                        jQuery(this).addClass('is-favorite');
                    }
                },
                error: () => {
                    alert('Oeps, we konden het bericht niet opslaan aan favorieten');
                },
            });
        }
    } else {
        alert('U dient ingelogd te zijn om dit bericht op te slaan');
    }

});
