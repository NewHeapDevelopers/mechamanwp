<?php
namespace NewHeap\Theme\Yudu;

use Newheap\Plugins\NewheapAccount\User\User;
use NewHeap\Theme\Logger\Logger;
use Yudu\Publisher\Publisher;

class Yudu
{
	const YUDU_KEY = '4h30bG93Gvvk7w7aV9QyeafwtagioYutOvISRidgKHnSpNnaFDWKlydzLBptw1Pw';
	const YUDU_SECRET = 'NxYEAsBQ5YSSErUxQNW16TC3aDpsS0Y4t8NaJJvKYGqeT6pYfEpTK32ZJQsUeTmc';

	public function __construct()
	{
		$this->init();
	}

	public function init()
	{
		if (strpos($_SERVER['REQUEST_URI'], '/blog/magazine/test/') >= 0 && isset($_GET['magazine_id'])) {
			// TODO: Check if custom url for yudu and has parameters for this method. If so call get_yudu_magazine_url
			add_action('init', [$this, 'get_yudu_magazine_url']);

		}
	}

	/**
	 * @param $magazine_yudu_node_id
	 * @param $magazine_url
	 *
	 * @return string|void
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 * @throws \Yudu\Publisher\Exceptions\PublisherException
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
	public static function get_yudu_magazine_url()
	{
		$magazine_id = intval($_GET['magazine_id']);
		$magazine_yudu_node_id = intval(get_field('magazine_yudu_node_id', $magazine_id));
		$magazine_url = get_field('magazine_url', $magazine_id);

		if (!$magazine_yudu_node_id) {
			return;
		}

		require_once get_template_directory() . '/NewHeap/vendor/autoload.php';

		$options = [
			'debug' => false,
			'verify' => true,
		];

		// Create Publisher Client
		$publisher = new Publisher(self::YUDU_KEY, self::YUDU_SECRET, $options);

		if (User::has_valid_subscription()) {

			// Makes request for Publication Token
			$results = $publisher->createEditionToken(get_current_user_id(), $magazine_yudu_node_id); // EL 3 2020

			$xml_response = new \SimpleXMLElement($results);

			$token = $xml_response->tokenValue;

			$authenticated_magazine_url = $magazine_url . '?yuduAuthId=' . get_current_user_id() . '&yuduAuthToken=' . $token;

			wp_redirect($authenticated_magazine_url, 302);
			exit;
		}
	}
}
