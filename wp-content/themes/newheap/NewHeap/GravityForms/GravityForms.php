<?php
namespace NewHeap\Theme\GravityForms;

class GravityForms {

	public function __construct()
	{
		$this->init();
	}

	public function init()
	{
		/** Code to post form submissions to Sharpspring, see documentation: https://help.sharpspring.com/hc/en-us/articles/360029624032-Connecting-Gravity-Forms */
        add_action( 'gform_after_submission', [$this, 'post_to_sharpspring'], 10, 2 );
	}

	public function post_to_sharpspring($entry, $form)
	{
        if ( rgar( $entry, 'status' ) === 'spam' ) {
            return false;
        }
        $body = [];
        function dupeCheck($fieldName, $bodyData) {
            $cleanLabel = substr(preg_replace("/[^a-zA-Z0-9]+/", "", $fieldName), 0, 24);
            for ($x = 0; $x <= 20; $x++) {
                if(array_key_exists($cleanLabel, $bodyData)) {
                    $cleanLabel = $cleanLabel . $x;
                } else { break; }
            }
            return $cleanLabel;
        }
        $formFields = $form['fields'];
        foreach($formFields as $formField):
            if($formField['label'] == 'sharpspring_base_uri') {
                $base_uri = rgar( $entry, $formField['id']);
                $sendToSharpSpring = true;
            } elseif($formField['label'] == 'sharpspring_endpoint') {
                $post_endpoint = rgar( $entry, $formField['id']);
            } elseif($formField['type'] == 'multiselect') {
                $fieldNumber = $formField['id'];
                $fieldLabel = dupeCheck($formField['label'], $body);
                $tempValue = rgar ( $entry, strval($fieldNumber) );
                $trimmedValue = str_replace('[', '', $tempValue);
                $trimmedValue = str_replace(']', '', $trimmedValue);
                $trimmedValue = str_replace('"', '', $trimmedValue);
                $body[preg_replace("/[^a-zA-Z0-9]+/", "", $fieldLabel)] = $trimmedValue;
            } elseif($formField['inputs']) {
                if($formField['type'] == 'checkbox') {
                    $fieldNumber = $formField['id'];
                    $fieldLabel = dupeCheck($formField['label'], $body);
                    $checkBoxField = GFFormsModel::get_field( $form, strval($fieldNumber) );
                    $tempValue = is_object( $checkBoxField ) ? $checkBoxField->get_value_export( $entry ) : '';
                    $trimmedValue = str_replace(', ', ',', $tempValue);
                    $body[preg_replace("/[^a-zA-Z0-9]+/", "", $fieldLabel)] = $trimmedValue;
                } elseif($formField['type'] == 'time') {
                    $fieldNumber = $formField['id'];
                    $fieldLabel = dupeCheck($formField['label'], $body);
                    $body[preg_replace("/[^a-zA-Z0-9]+/", "", $fieldLabel)] = rgar( $entry, strval($fieldNumber) );
                } else {
                    foreach($formField['inputs'] as $subField):
                        $fieldLabel = dupeCheck($subField['label'], $body);
                        $fieldNumber = $subField['id'];
                        $body[preg_replace("/[^a-zA-Z0-9]+/", "", $fieldLabel)] = rgar( $entry, strval($fieldNumber) );
                    endforeach;
                } } else {
                $fieldNumber = $formField['id'];
                $fieldLabel = dupeCheck($formField['label'], $body);
                $body[preg_replace("/[^a-zA-Z0-9]+/", "", $fieldLabel)] = rgar( $entry, strval($fieldNumber) );
            };
        endforeach;
        $body['form_source_url'] = $entry['source_url'];
        $body['trackingid__sb'] = $_COOKIE['__ss_tk']; //DO NOT CHANGE THIS LINE... it collects the tracking cookie to establish tracking
        $post_url = $base_uri . $post_endpoint;
        if($sendToSharpSpring) {
            $request = new WP_Http();
            $response = $request->post( $post_url, array( 'body' => $body ) );
        }
	}


    public static function data_to_sharpspring($params, $endPoint = null) {
        $baseURL = "https://app-3QNJXQNLAC.marketingautomation.services/webforms/receivePostback/MzawMDE3NzEwAAA/";

        if(empty($endPoint)) {
            if (get_current_blog_id() === \NewHeap\Theme\Multisite\Multisite::SITE_MECHAMAN) {
                $endPoint = '7aed6831-736d-4486-a154-5fe97515308d';
            } elseif (get_current_blog_id() === \NewHeap\Theme\Multisite\Multisite::SITE_LANDBOUW_MECHANISATIE) {
                $endPoint = '2b00c010-ab8a-4198-b8da-0dfe3641a852';
            } elseif (get_current_blog_id() === \NewHeap\Theme\Multisite\Multisite::SITE_TUIN_EN_PARK_TECHNIEK) {
                $endPoint = '7e16f33f-c62c-43a1-b74a-daa167b14bd6';
            } elseif (get_current_blog_id() === \NewHeap\Theme\Multisite\Multisite::SITE_VEEHOUDERIJ_TECHNIEK) {
                $endPoint = 'd67929ad-c79b-43b4-b13e-4000dbaed44e';
            }
        }

        $urlQuery = [];
        foreach($params as $key => $value) {
            $urlQuery[] = $key . "=" . urlencode($value);
        }

        if (isset($_COOKIE['__ss_tk'])) {
            $trackingid__sb = $_COOKIE['__ss_tk'];
            $urlQuery[] = "trackingid__sb=" . urlencode($trackingid__sb);
        }

        // Prepare URL
        $ssRequest = $baseURL . $endPoint . "/jsonp/?" . implode("&", $urlQuery);

        // Send request
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $ssRequest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        //var_dump($result);
    }

}
