<?php
namespace NewHeap\Theme\Rest;

use NewHeap\Theme\Helpers;
use NewHeap\Theme\Multisite\Multisite;

class Posts {
	/**
	 * Get filtered Posts
	 *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
	public static function get_posts()
	{
		$filters = self::sanitize_filter_post_data();

		$args = [
			'post_type' => $filters['post_type'],
			'orderby' => $filters['orderby'],
			'order' => $filters['order'],
			'posts_per_page' => $filters['posts_per_page'],
			'paged' => $filters['paged'],
		];

		if (isset($filters['premium'])) {
			$args['meta_query'][] = $filters['premium'];
		}

		if (isset($filters['tax_query'])) {
			$args['tax_query'] = $filters['tax_query'];
		}

		if (isset($filters['keyword'])) {
			$args['s'] = $filters['keyword'];
		}

        $query = new \WP_Query($args);

        $posts = [];
        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
                $post = get_post();

                $post_categories = get_the_terms(get_the_ID(), 'category');
                $post_category_ids = [];

                if ($post_categories) {
                    foreach ($post_categories as $post_category) {
                        $post_category_ids[] = $post_category->term_id;
                    }
                }


                $post_dossiers = get_the_terms(get_the_ID(), 'dossier');
                $post_dossier_ids = [];

                if ($post_dossiers) {
                    foreach ($post_dossiers as $post_dossier) {
                        $post_dossier_ids[] = $post_dossier->term_id;
                    }
                }

                $post_author = get_the_author_meta('display_name', get_the_author());

                $post_date = get_post_datetime(get_the_ID())->format('d-m-Y');
                $is_premium = get_field('premium_article', get_the_ID());
                $read_time = Helpers::get_read_time(get_the_ID());

                ob_start();
                include get_theme_file_path('elements/article-listing-item.php');
                $html = ob_get_clean();

                $post->nh_meta = [
                    'category_ids' => $post_category_ids,
                    'dossier_ids' => $post_dossier_ids,
                    'post_date' => $post_date,
                    'post_excerpt' => get_the_excerpt(get_the_ID()),
                    'post_author_name' => $post_author,
                    'premium' => $is_premium,
                    'read_time' => $read_time,
                    'url' => get_the_permalink(get_the_ID()),
                    'image' => get_the_post_thumbnail_url(get_the_ID(), 'large'),
                    'html' => $html,
                    'max_num_pages' => $query->max_num_pages
                ];

                if (get_current_blog_id() === Multisite::SITE_MECHAMAN) {
                    $broadcast_data = ThreeWP_Broadcast()->get_post_broadcast_data(get_current_blog_id(), get_the_ID());
                    $parent = $broadcast_data->get_linked_parent();

                    if (!empty($parent)) {
                        $post->nh_meta['blog_id'] = $parent['blog_id'];
                        $post->nh_meta['blog_name'] = get_blog_details($parent['blog_id'])->blogname;

                        switch_to_blog($parent['blog_id']);
                        if (get_the_permalink($parent['post_id'])) {
                            $post->nh_meta['url'] = get_the_permalink($parent['post_id']);
                        }
                        restore_current_blog();
                    } else {
                        $post->nh_meta['blog_id'] = get_current_blog_id();
                        $post->nh_meta['blog_name'] = get_blog_details(get_current_blog_id())->blogname;
                    }
                }

                $posts[] = $post;
            }
		}

		JsonResponse::json_success($posts);
	}

	/**
	 * Sanitize the post data
	 *
	 * @return array
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
	public static function sanitize_filter_post_data()
	{
		// Set defaults
		$filters = [
			'orderby' => 'date',
			'order' => 'DESC',
			'premium' => null,
			'post_type' => 'post',
			'posts_per_page' => get_option('posts_per_page'),
			'paged' => 1,
			'keyword' => null,
		];

		if (isset($_POST['post_types']) && $_POST['post_types']) {
			$filters['post_types'] = $_POST['post_types'];
		}

		if (isset($_POST['orderby']) && $_POST['orderby']) {
			$filters['orderby'] = $_POST['orderby'];
		}

		if (isset($_POST['order']) && $_POST['order']) {
			$filters['order'] = $_POST['order'];
		}

		if (isset($_POST['posts_per_page']) && $_POST['posts_per_page']) {
			$filters['posts_per_page'] = intval($_POST['posts_per_page']);
		}

		if (isset($_POST['premium']) && !empty($_POST['premium'])) {
			if (count($_POST['premium']) === 1) {
				if ($_POST['premium'][0] == '1') {
					$filters['premium'] = [
						'key' => 'premium_article',
						'value' => 1,
						'compare' => '=',
					];
				} else {
					$filters['premium'] = [
						'key' => 'premium_article',
						'value' => 1,
						'compare' => '!=',
					];
				}
			}
		}

		if (isset($_POST['paged']) && $_POST['paged']) {
			$filters['paged'] = intval($_POST['paged']);
		}

		if (isset($_POST['taxonomies']) && !empty($_POST['taxonomies'])) {
			$filters['tax_query'] = [
				'relation' => 'OR'
			];

			foreach ($_POST['taxonomies'] as $key => $taxonomy) {
				$filters['tax_query'][] = [
					'taxonomy' => $key,
					'field' => 'term_id',
					'terms' => $taxonomy,
				];
			}
		}

		if (isset($_POST['keyword']) && !empty($_POST['keyword'])) {
			$filters['keyword'] = $_POST['keyword'];
		}

		return $filters;
	}
}
