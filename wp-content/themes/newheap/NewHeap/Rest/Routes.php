<?php
namespace NewHeap\Theme\Rest;

require_once 'JsonResponse.php';
require_once 'Posts.php';

class Routes {
	const REST_ENPOINT = '/wp-json/newheap/v1';

	public static function custom_routes() {
		// Get filtered posts
		register_rest_route('newheap/v1', '/get-posts', [
			'methods'  => 'POST',
			'callback' => ['NewHeap\Theme\Rest\Posts', 'get_posts'],
			'permission_callback' => false
		]);

		// Get dossiers
		register_rest_route('newheap/v1', '/get-dossiers', [
			'methods'  => 'POST',
			'callback' => ['NewHeap\Theme\Rest\Dossiers', 'get_dossiers'],
			'permission_callback' => false
		]);
	}
}
