<?php
namespace NewHeap\Theme\Rest;

class Dossiers {
	/**
	 * Get filtered Dossiers
	 *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
	public static function get_dossiers()
	{
		$dossiers = get_terms([
			'taxonomy' => 'dossier',
            'number' => 0,
            'meta_query' => array(
                'relation' => 'OR',
                array(
                    'key'     => 'do_not_show',
                    'value'   => '0',
                    'compare' => '=',
                ),
                array(
                    'key' => 'do_not_show',
                    'compare' => 'NOT EXISTS'
                ),
            )
		]);

		foreach ($dossiers as $dossier) {
			$dossier->nh_meta = [
				'url' => get_term_link($dossier),
				'image' => get_field('term_image', 'dossier_' . $dossier->term_id)['sizes']['medium_large'],
				'last_modified_date' => self::get_dossier_modified_date($dossier->term_id)
			];
		}

		JsonResponse::json_success($dossiers);
	}

	public static function get_dossier_modified_date($term_id)
	{
		// Get the latest item
		$latest_post = new \WP_Query([
			'post_type' => 'post',
			'posts_per_page' => 1,
			'tax_query' => [
				[
					'taxonomy' => 'dossier',
					'field' => 'term_id',
					'terms' => $term_id,
				]
			]
		]);

		$latest_date = \DateTime::createFromFormat('Y-m-d H:i:s', $latest_post->posts[0]->post_modified);
		return $latest_date->format('d-m-Y');
	}
}
