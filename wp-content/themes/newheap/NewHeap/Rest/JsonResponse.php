<?php
namespace NewHeap\Theme\Rest;

class JsonResponse {
	public static function json_success($data, $status_code = 200)
	{
		$response = new \stdClass();

		$response->status = $status_code;
		$response->data = $data;

		echo json_encode($response);
		exit;
	}

	public static function json_error($data, $status_code = 500)
	{
		$response = new \stdClass();

		$response->status = $status_code;
		$response->data = $data;

		echo json_encode($response);
		exit;
	}
}
