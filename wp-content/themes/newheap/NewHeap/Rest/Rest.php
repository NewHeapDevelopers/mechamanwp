<?php
namespace NewHeap\Theme\Rest;

require_once 'JsonResponse.php';
require_once 'Routes.php';

require_once 'Dossiers.php';
require_once 'Posts.php';

class Rest {
	public function __construct() {
		add_action('rest_api_init', ['NewHeap\Theme\Rest\Routes', 'custom_routes']);
	}

	public function init() {

	}
}
