<?php
namespace NewHeap\Theme\Blocks;

class Blocks
{
	public function __construct()
	{
		add_filter('block_categories', [$this, 'add_block_category'], 1, 2);
		add_action('enqueue_block_editor_assets', [$this, 'load_blocks']);

	}


	function add_block_category($categories, $post) {
		return array_merge(
			array(
				array(
					'slug' => 'newheap',
					'title' => 'NewHeap',
				),
			),
			$categories
		);
	}


	public function load_blocks()
	{
		wp_register_style(
			'blocks-styling',
			get_template_directory_uri() . '/newheap-blocks/css/admin-blocks.css',
			['wp-edit-blocks'],
			2
		);

		$this->include_blocks();
	}

	public function include_blocks()
	{
		$included_blocks = [];

		// Check child theme and load child theme blocks to override parent theme blocks
		if (is_dir(get_stylesheet_directory() . '/newheap-blocks/js')) {
			$files = scandir(get_stylesheet_directory() . '/newheap-blocks/js');

			foreach ($files as $file) {
				$file_pathinfo = pathinfo(get_stylesheet_directory() . '/newheap-blocks/js/' . $file);

				if (is_file(get_stylesheet_directory() . "/newheap-blocks/js/" . $file)) {

					// Check if it's a JS file and not already included
					if ($file_pathinfo['extension'] === 'js' && !in_array($file_pathinfo['filename'], $included_blocks)) {

						wp_register_script(
							$file_pathinfo['filename'] . '-script',
							get_stylesheet_directory_uri() . "/newheap-blocks/js/" . $file,
							['wp-blocks', 'wp-editor', 'wp-element'],
							1
						);

						register_block_type( 'newheap-blocks/' . $file_pathinfo['filename'], [
							'api_version' => 2,
							'style' => 'blocks-styling',
							'editor_style' => 'blocks-styling',
							'editor_script' => $file_pathinfo['filename'] . '-script',
						]);

						$included_blocks[] = $file_pathinfo['filename'];
					}
				}
			}
		}

        if (is_dir(get_template_directory() . '/newheap-blocks/js')) {
            $files = scandir(get_template_directory() . '/newheap-blocks/js');

            foreach ($files as $file) {
                $file_pathinfo = pathinfo(get_template_directory() . '/newheap-blocks/js/' . $file);

                if (is_file(get_template_directory() . "/newheap-blocks/js/" . $file)) {

                    // Check if it's a JS file and not already included
                    if ($file_pathinfo['extension'] === 'js' && !in_array($file_pathinfo['filename'], $included_blocks)) {
                        wp_register_script(
                            $file_pathinfo['filename'] . '-script',
                            get_template_directory_uri() . "/newheap-blocks/js/" . $file,
                            ['wp-blocks', 'wp-editor', 'wp-element'],
                            1
                        );

                        register_block_type('newheap-blocks/' . $file_pathinfo['filename'], [
                            'api_version' => 2,
                            'style' => 'blocks-styling',
                            'editor_style' => 'blocks-styling',
                            'editor_script' => $file_pathinfo['filename'] . '-script',
                        ]);

                        $included_blocks[] = $file_pathinfo['filename'];
                    }
                }
            }
        }
	}
}
