<?php
namespace NewHeap\Theme\Cpt;

use Codio\CodioAuth\Zeno\Zeno;

class Cron
{
    public function __construct()
    {
        // Create new cron schedules
        add_filter('cron_schedules', [$this, 'add_new_cron_schedules']);

        // Register events
        add_action('init' , [$this, 'register_events']);

	    add_action('reset_view_count', [$this, 'reset_view_count']);
	    add_action('get_countries', [$this, 'get_countries']);

    }

    /**
     * Addnew schedules for wp events
     *
     * @param $schedules
     * @return mixed
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public function add_new_cron_schedules($schedules){
        if(!isset($schedules["1min"])){
            $schedules["1min"] = array(
                'interval' => 60,
                'display' => __('Once every minute'));
        }
        if(!isset($schedules["5min"])){
            $schedules["5min"] = array(
                'interval' => 5*60,
                'display' => __('Once every 5 minutes'));
        }
        if(!isset($schedules["15min"])){
            $schedules["15min"] = array(
                'interval' => 15*60,
                'display' => __('Once every 15 minutes'));
        }
        if(!isset($schedules["30min"])){
            $schedules["30min"] = array(
                'interval' => 30*60,
                'display' => __('Once every 30 minutes'));
        }
        if(!isset($schedules["monthly"])){
            $schedules["monthly"] = array(
                'interval' => 60 * 60 * 24 * 30, // seconds * minutes * hours * days = 30 days
                'display' => __('Once every 30 days'));
        }
        return $schedules;
    }

    /**
     * @author Ferhat Ileri <ferhat@codio.nl>
     */
    public function register_events()
    {
        if (!wp_next_scheduled ('reset_view_count')) {
            wp_schedule_event(time(), 'monthly', 'reset_view_count');
        }
        if (!wp_next_scheduled ('get_countries')) {
            wp_schedule_event(time(), 'daily', 'get_countries');
        }
    }

    public function reset_view_count()
    {
        // Cronjob here
	    global $wpdb;

	    $query = $wpdb->query("UPDATE $wpdb->postmeta SET `meta_value` = 0 WHERE `meta_key` = 'post_view_count' ");
    }

    public function get_countries()
    {
        Zeno::get_country_codes();
    }
}
