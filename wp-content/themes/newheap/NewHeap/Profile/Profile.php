<?php
namespace NewHeap\Theme\Profile;

use NewHeap\Theme\Helpers;

class Profile
{
    public function __construct()
    {
        $this->init_hooks();
    }

    public function init_hooks()
    {
        add_action( 'show_user_profile', [$this, 'add_profile_picture_upload'] );
        add_action( 'edit_user_profile', [$this, 'add_profile_picture_upload'] );

        add_action( 'personal_options_update', [$this, 'save_profile_picture_upload'] );
        add_action( 'edit_user_profile_update', [$this, 'save_profile_picture_upload'] );

        add_action('user_edit_form_tag', [$this, 'make_form_accept_uploads']);

        add_filter( 'wp_mail_from', [$this, 'mail_from_address']);
        add_filter( 'wp_mail_from_name', [$this, 'mail_from_name']);
    }

    function mail_from_address( $email ) {
        return 'mail@agrimedia.nl';
    }

    function mail_from_name( $from_name ) {
        return 'Mechaman - Agrimedia';
    }

    public function add_profile_picture_upload($user)
    {
        $r = get_user_meta( $user->ID, 'picture', true );
        ?>

        <h3><?php _e("Profiel afbeelding", "blank"); ?></h3>

        <style>.user-profile-picture {display: none;}</style>
        <table class="form-table">

            <tr>
                <th scope="row">Afbeelding</th>
                <td>
                    <?php
                    if(!empty($r)) {
                        ?><img src="<?=$r?>" style="max-height: 100px;" /><?php
                    }
                    ?>
                    <input type="file" name="picture" value="" />
                    <?php
                    if (isset($r['error'])) {
                        $r = $r['error'];
                        echo $r;
                    }
                    ?>
                </td>
            </tr>

        </table>
        <?php
    }

    function save_profile_picture_upload( $user_id ) {
        if ( !current_user_can( 'edit_user', $user_id ) ) { return false; }

        $_POST['action'] = 'wp_handle_upload';

        if( $_FILES['picture']['error'] === UPLOAD_ERR_OK ) {
            $upload_overrides = array( 'test_form' => false );
            $currentBlog = get_current_blog_id();
            switch_to_blog(1);
            $picture = wp_handle_upload( $_FILES['picture'], $upload_overrides );
            switch_to_blog($currentBlog);
            update_user_meta( $user_id, 'picture', $picture['url'] );
        }

    }

    function make_form_accept_uploads() {
        echo ' enctype="multipart/form-data"';
    }
}
