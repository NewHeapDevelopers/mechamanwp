<?php
namespace NewHeap\Theme\Template;

class Template {

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        add_action('body_class', [$this, 'site_id_in_body_class']);
    }

    public function site_id_in_body_class($classes)
    {
        $this_id = get_current_blog_id();
        $classes[] = 'site-id-'.$this_id;
        return $classes;
    }

}
