<?php
namespace NewHeap\Theme\Gutenberg;

class Gutenberg
{
    public function __construct()
    {
        $this->init();
    }


    public function init()
    {
        add_filter( 'wp_kses_allowed_html', [$this, 'allow_iframe_in_editor'], 10, 2 );
    }

    function allow_iframe_in_editor( $tags, $context ) {
        if( 'post' === $context ) {
            $tags['iframe'] = array(
                'allowfullscreen' => TRUE,
                'frameborder' => TRUE,
                'height' => TRUE,
                'src' => TRUE,
                'style' => TRUE,
                'width' => TRUE,
            );
        }
        return $tags;
    }
}



