<?php
namespace NewHeap\Theme\Logger;

class Logger
{
    const LOG_FILE_PATH = '/assets/logs/logs.txt';
    const WW_LOG_FILE_PATH = '/assets/logs/ww_logs.txt';
    const ZENO_LOG_FILE_PATH = '/assets/logs/zeno_logs.txt';


    public function __construct()
    {

    }

    public static function write_log($log)
    {
        if (!is_string($log)) {
            $log = json_encode($log);
        }
        
        $log_file = get_template_directory() . self::LOG_FILE_PATH;

        $log_line = date('d-m-Y H:i:s') . ': ' . $log . PHP_EOL;

        $file = fopen($log_file, 'a');
        fwrite($file, $log_line);
        fclose($file);
    }

    public static function write_woodwing_log($log, $method = null)
    {
        if (!is_string($log)) {
            $log = json_encode($log);
        }

        $log_file = get_template_directory() . self::WW_LOG_FILE_PATH;

        $log_line = 'Method: ' . $method . ' ' . date('d-m-Y H:i:s') . ': ' . $log . PHP_EOL;

        $file = fopen($log_file, 'a');
        fwrite($file, $log_line);
        fclose($file);
    }

    public static function write_zeno_log($log)
    {
        if (!is_string($log)) {
            $log = json_encode($log);
        }

        $log_file = get_template_directory() . self::ZENO_LOG_FILE_PATH;

        $log_line = date('d-m-Y H:i:s') . ': ' . $log . PHP_EOL;

        $file = fopen($log_file, 'a');
        fwrite($file, $log_line);
        fclose($file);
    }

    public static function critical_error($log)
    {
        if (!is_string($log)) {
            $log = json_encode($log);
        }

        $to = 'ferhat@newheap.com';
        $subject = 'Critical error: ' . get_bloginfo('name');
        $message = 'Error: ' . $log . PHP_EOL;
        $message .= 'Website: ' . get_admin_url() . PHP_EOL;

        Logger::write_log($log);

//        wp_mail($to, $subject, $message);
    }
}
