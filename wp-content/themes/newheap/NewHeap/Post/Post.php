<?php
namespace NewHeap\Theme\Post;

use NewHeap\Theme\Helpers;

class Post
{
	public function __construct()
	{
		$this->init_hooks();
	}

	public function init_hooks()
	{
		add_action('save_post', [$this, 'on_post_save']);
	}

	public function on_post_save($post_id)
	{
		update_post_meta($post_id, 'post_read_time', Helpers::get_read_time($post_id));
	}
}
