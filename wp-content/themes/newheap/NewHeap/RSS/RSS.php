<?php
namespace NewHeap\Theme\RSS;

class RSS
{
    public function __construct()
    {
        /** Change title in RSS feed if article is premium */
        add_filter( 'the_title_rss', [$this, 'rss_title'], 20, 1 );

        /** Add extra fields to RSS */
        add_filter( 'rss2_item', [$this, 'add_fields'], 20, 1 );
    }

    public function rss_title($rssTitle)
    {
        global $wp_query;
        $isPremium = get_post_meta('premium_article', true);

        if($isPremium) {
            $rssTitle = "[Premium] " . $rssTitle;
        }

        return $rssTitle;
    }

	public function add_fields() {
        global $wp_query;

        /** Add premium tag to RSS feed */
        ?>
        <premium><?=get_post_meta(get_the_ID(), 'premium_article', true)?></premium>
        <?php
    }
}
