<?php
namespace NewHeap\Theme\Multisite;

class Multisite
{
	const SITE_MECHAMAN = 1;
	const SITE_LANDBOUW_MECHANISATIE = 2;
	const SITE_VEEHOUDERIJ_TECHNIEK = 7;
	const SITE_TUIN_EN_PARK_TECHNIEK = 5;

	const SITE_CODE_MECHAMAN = 'mm';
	const SITE_CODE_LANDBOUW_MECHANISATIE = 'lm';
	const SITE_CODE_VEEHOUDERIJ_TECHNIEK = 'vt';
	const SITE_CODE_TUIN_EN_PARK_TECHNIEK = 'tpt';
	public function __construct()
	{
		$this->init();
	}

	public function init()
	{

	}

	public static function get_site_code()
	{
		switch(get_current_blog_id()) {
			case self::SITE_LANDBOUW_MECHANISATIE :
				$site_code = self::SITE_CODE_LANDBOUW_MECHANISATIE;
				break;

			case self::SITE_VEEHOUDERIJ_TECHNIEK :
				$site_code = self::SITE_CODE_VEEHOUDERIJ_TECHNIEK;
				break;

			case self::SITE_TUIN_EN_PARK_TECHNIEK :
				$site_code = self::SITE_CODE_TUIN_EN_PARK_TECHNIEK;
				break;

			default :
				$site_code = self::SITE_CODE_MECHAMAN;
		}

		return $site_code;
    }

    public static function get_site_name($site_id = null)
    {
    	if (!$site_id) {
    		$site_id = get_current_blog_id();
	    }

	    switch($site_id) {
		    case self::SITE_LANDBOUW_MECHANISATIE :
			    $site_name = 'LandbouwMechanisatie';
			    break;

		    case self::SITE_VEEHOUDERIJ_TECHNIEK :
			    $site_name = 'Veehouderij Techniek';
			    break;

		    case self::SITE_TUIN_EN_PARK_TECHNIEK :
			    $site_name = 'Tuin en Park Techniek';
			    break;

		    default :
			    $site_name = 'Mechaman';
	    }

	    return $site_name;
    }
}
