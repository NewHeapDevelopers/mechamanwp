<?php
namespace NewHeap\Theme;

// Prevent direct access
if (!defined('ABSPATH')) {
    exit;
}

use NewHeap\Theme\Acf\Acf;
use NewHeap\Theme\Blocks\Blocks;
use NewHeap\Theme\Cache\Cache;
use NewHeap\Theme\Cache\CacheRss;
use NewHeap\Theme\Cpt\Cpt;
use NewHeap\Theme\Cpt\CptUi;
use NewHeap\Theme\Cpt\Cron;
use NewHeap\Theme\Favorites\Favorites;
use NewHeap\Theme\GravityForms\GravityForms;
use NewHeap\Theme\Gutenberg\Gutenberg;
use NewHeap\Theme\Newsletter\Newsletter;
use NewHeap\Theme\Profile\Profile;
use NewHeap\Theme\RSS\RSS;
use NewHeap\Theme\Post\Post;
use NewHeap\Theme\Rest\Rest;
use NewHeap\Theme\SharpSpring\SharpSpring;
use NewHeap\Theme\Taxonomy\Taxonomy;
use NewHeap\Theme\Template\Template;
use NewHeap\Theme\Uploads\Uploads;
use NewHeap\Theme\Weather\Weather;
use NewHeap\Theme\Yudu\Yudu;

require_once 'Helpers.php';

require_once 'Acf/Acf.php';
require_once 'Blocks/Blocks.php';
require_once 'Cache/Cache.php';
require_once 'Cache/CacheRss.php';
require_once 'Cpt/Cpt.php';
require_once 'Cpt/CptUi.php';
require_once 'Cron/Cron.php';
require_once 'Favorites/Favorites.php';
require_once 'GravityForms/GravityForms.php';
require_once 'Gutenberg/Gutenberg.php';
require_once 'Logger/Logger.php';
require_once 'Multisite/Multisite.php';
require_once 'Newsletter/Newsletter.php';
require_once 'Notification/Notification.php';
require_once 'Profile/Profile.php';
require_once 'Post/Post.php';
require_once 'Rest/Rest.php';
require_once 'RSS/RSS.php';
require_once 'Taxonomy/Taxonomy.php';
require_once 'Template/Template.php';
require_once 'Tracking/SharpSpring.php';
require_once 'Uploads/Uploads.php';
require_once 'Weather/Weather.php';
require_once 'Yudu/Yudu.php';

class NewHeap
{
    /** @var Acf */
    protected $acf;

    public function __construct()
    {
	    $this->acf = new Acf();

	    // Theme
	    new Blocks();
	    new Cache();
	    new CacheRss();
	    new Cpt();
	    new CptUi();
	    new Cron();
	    new Favorites();
	    new GravityForms();
	    new Gutenberg();
	    new Newsletter();
	    new Post();
	    new Profile();
	    new Rest();
	    new RSS();
	    new SharpSpring();
	    new Taxonomy();
	    new Template();
	    new Uploads();
	    new Weather();
	    new Yudu();
	    
        // Init hooks
        $this->init_hooks();
    }

    public function init_hooks()
    {
	    add_theme_support( 'title-tag' );
	    add_theme_support('align-wide');
	    add_theme_support( 'post-thumbnails' );
	    add_post_type_support( 'page', [
		    'excerpt',
	    ]);

        add_action('init', [$this, 'wp_init']);
        add_action('after_setup_theme', [$this, 'register_nav_menus'], 1);
        add_action('after_setup_theme', [$this, 'register_footer_nav_menus'], 11);
        add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts_styles'], 1);
        add_action('admin_enqueue_scripts', [$this, 'enqueue_admin_scripts_styles'] , 10);
        add_action('login_head', [$this, 'custom_login_styles']);
	    add_action('after_switch_theme', [$this, 'set_permalinks'], 10 ,  2);
	    add_action('after_setup_theme', [$this, 'add_custom_image_sizes']);


	    // Disable Gutenberg
//        add_filter('use_block_editor_for_post', '__return_false');
	    add_filter('gutenberg_can_edit_post_type', [$this, 'disable_gutenberg'], 10, 2);
	    add_filter('use_block_editor_for_post_type', [$this, 'disable_gutenberg'], 10, 2);

	    // Premium pages
	    add_filter('the_content', [$this, 'page_premium_content'], 1);
    }

    public function wp_init()
    {
        load_theme_textdomain('newheap', get_stylesheet_directory() . '/languages');
    }

    public function register_nav_menus()
    {
        register_nav_menus(array(
            'main-menu' => __('Main menu', 'newheap'),
        ));
    }

    public function register_footer_nav_menus()
    {
        register_nav_menus(array(
            'footer-first-menu' => __('Footer first menu', 'newheap'),
            'footer-second-menu' => __('Footer second menu', 'newheap'),
            'footer-third-menu' => __('Footer third menu', 'newheap'),
        ));
    }

    public function enqueue_scripts_styles()
    {
	    // Styles
        wp_enqueue_style('newheap', get_template_directory_uri() . '/assets/css/styles.css', false, 1);


        // Scripts
        wp_deregister_script('jquery');

        wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js', [], '3.6.0', true);
        wp_enqueue_script('bootstrap-script', get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js', ['jquery'], false, true);
        wp_enqueue_script('slick-script', get_template_directory_uri() . '/assets/js/slick.min.js', ['jquery'], false, true);
        wp_enqueue_script('newheap-script', get_template_directory_uri() . '/assets/js/scripts.js', [], false, true);

        /** Enqueue Sharpspring tracking code in header (Only if), possibilty this has to move to cookie plug-in */
        wp_add_inline_script( 'nh-sharpspring-tracking', "
            var _ss = _ss || [];
            _ss.push(['_setDomain', 'https://koi-3QNJXQNLAC.marketingautomation.services/net']);
            _ss.push(['_setAccount', 'KOI-45AAEDEXXK']);
            _ss.push(['_trackPageView']);
            window._pa = window._pa || {};
            (function() {
                var ss = document.createElement('script');
                ss.type = 'text/javascript'; ss.async = true;
                ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-3QNJXQNLAC.marketingautomation.services/client/ss.js?ver=2.4.0';
                var scr = document.getElementsByTagName('script')[0];
                scr.parentNode.insertBefore(ss, scr);
            })();
        ");


        echo get_field('advertisement_scripts', 'option');

        wp_localize_script( 'newheap-script', 'NewHeap', [
            'ajax_url' => admin_url( 'admin-ajax.php' ),
            'site_url' => site_url()
        ]);

    }

    public  function enqueue_admin_scripts_styles()
    {
        wp_enqueue_style('admin-styles', get_template_directory_uri() . '/assets/css/admin-styles.css', false, 1);
    }

    /**
     * Enqueue custom styling for login page
     */
    public function custom_login_styles()
    {
        wp_enqueue_style('custom-login', get_template_directory_uri() . '/assets/css/custom-login.css');
    }

	/**
	 * Set the permalinks
	 *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
	public function set_permalinks()
	{
		global $wp_rewrite;

		//Write the rule
		$wp_rewrite->set_permalink_structure('/artikel/%year%%monthnum%%day%/%postname%/');

		//Set the option
		update_option( "rewrite_rules", FALSE );

		//Flush the rules and tell it to write htaccess
		$wp_rewrite->flush_rules( true );
	}

	/**
	 * Add custom image sizes
	 *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
	public function add_custom_image_sizes() {
		add_image_size( 'small', 300, 300);
		add_image_size( 'normal', 500, 350);
	}

	/**
	 * Check if Gutenberg should be disabled
	 *
	 * @param $test
	 * @param $post_type
	 *
	 * @return bool
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
	public function disable_gutenberg($test, $post_type)
	{
		if ($post_type === 'post') {
			return true;
		}

		if (isset($_GET['post'])) {
			$template = get_page_template_slug(intval($_GET['post']));

			$excluded_templates = array(
				'front-page.php',
				'template-category-archive.php',
				'template-type-archive.php',
				'template-agenda-archive.php',
				'template-subscription.php',
				'template-subscription-overview.php',
			);

			if (in_array($template, $excluded_templates)) {
				return false;
			} else {
				return true;
			}
		}

		return true;
	}

	public function page_premium_content($content)
	{
		global $post;

		if (get_post_type($post->ID) === 'page') {
			$premium_article = get_field('premium_article', $post->ID);

			if ($premium_article) {
				if (!\Newheap\Plugins\NewheapAccount\User\User::has_valid_subscription()) {

					$content = '
                    <div class="restricted-content">
                    	<h1>' . get_the_title() . '</h1>
                    	' . $post->post_excerpt . '
                    </div>

                    <div class="premium-article-popup">
                        <h2>' . __( 'Uw gratis premium artikelen zijn op', 'newheap' ) . '</h2>
                        <p>' . __( 'Wilt u 14 dagen gratis toegang tot premium artikelen?', 'newheap' ) . '</p>

                        <a href="' . get_field( 'registration_page', 'option' ) . '" class="btn btn-primary">
					        ' . __( 'Maak een account aan', 'newheap' ) . '
                        </a>

                        <div class="premium-article-popup-footer">
					        ' . sprintf( __( 'Heeft u al een account? <a href="%s">Log dan in</a>', 'newheap' ), get_field( 'login_page', 'option' ) ) . '
                        </div>
                    </div>
                    ';
                }
			}

		}

		return $content;
	}
}

new NewHeap();

