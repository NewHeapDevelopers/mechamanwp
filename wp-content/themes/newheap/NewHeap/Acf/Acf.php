<?php
namespace NewHeap\Theme\Acf;

use NewHeap\Theme\Multisite\Multisite;

class Acf
{
    public function __construct()
    {
        // Check if ACF exists
        if (class_exists('acf')) {
            $this->init_hooks();
        }

        add_action('admin_menu', [$this, 'add_menu_item']);


    }

    function add_menu_item() {
        global $submenu;

        $menu_slug = "options-general.php?page=democracy-poll"; // used as "key" in menus
        $menu_pos = 12; // whatever position you want your menu to appear

        // create the top level menu
        add_menu_page( 'Polls', 'Polls', 'read', $menu_slug, '', 'dashicons-format-status', $menu_pos);
    }

    public function init_hooks()
    {
        add_action('acf/init', [$this, 'add_options_page'] );
        add_action('acf/init', [$this, 'mechaman_sell_data'] );

	    add_filter('acf/settings/load_json', [$this, 'update_acf_json_load_folder']);
	    add_filter('acf/settings/save_json', [$this, 'update_acf_json_save_folder']);

    }

    public function add_options_page()
    {
        // Activate theme options page
        acf_add_options_page( [
            'page_title' => 'Newheap settings',
            'menu_title' => 'NewHeap',
            'menu_slug' => 'newheap',
            'icon_url' => get_template_directory_uri() . '/assets/images/logo-newheap.png',
            'position' => 2
        ] );
    }

    public function update_acf_json_save_folder($paths)
    {
	    return get_template_directory() . '/acf-json';
    }

    public function update_acf_json_load_folder($paths)
    {
	    $paths = [get_template_directory() . '/acf-json'];

	    if (is_child_theme()) {
		    $paths[] = get_template_directory() . '/acf-json';
	    }

	    return $paths;
    }

    public function mechaman_sell_data()
    {
    	if (Multisite::SITE_MECHAMAN !== get_current_blog_id()) {
    		return;
	    }

	    if( function_exists('acf_add_local_field_group') ):

		    acf_add_local_field_group(array(
			    'key' => 'group_6101054a051ae',
			    'title' => 'Trekker verkoopcijfers',
			    'fields' => array(
				    array(
					    'key' => 'field_6101055f208c9',
					    'label' => 'Trekkers verkocht per maand',
					    'name' => '',
					    'type' => 'tab',
					    'instructions' => '',
					    'required' => 0,
					    'conditional_logic' => 0,
					    'wrapper' => array(
						    'width' => '',
						    'class' => '',
						    'id' => '',
					    ),
					    'placement' => 'left',
					    'endpoint' => 0,
				    ),
				    array(
					    'key' => 'field_61010582208ca',
					    'label' => 'Jaar',
					    'name' => 'verkocht_per_jaar',
					    'type' => 'repeater',
					    'instructions' => '',
					    'required' => 0,
					    'conditional_logic' => 0,
					    'wrapper' => array(
						    'width' => '',
						    'class' => '',
						    'id' => '',
					    ),
					    'collapsed' => '',
					    'min' => 0,
					    'max' => 0,
					    'layout' => 'row',
					    'button_label' => 'Nieuwe maand',
					    'sub_fields' => array(
						    array(
							    'key' => 'field_61010592208cb',
							    'label' => 'Datum',
							    'name' => 'datum',
							    'type' => 'text',
							    'instructions' => '',
							    'required' => 0,
							    'conditional_logic' => 0,
							    'wrapper' => array(
								    'width' => '',
								    'class' => '',
								    'id' => '',
							    ),
							    'default_value' => '',
							    'placeholder' => 'vk 2007',
							    'prepend' => '',
							    'append' => '',
							    'maxlength' => '',
						    ),
						    array(
							    'key' => 'field_610105d8208cc',
							    'label' => 'Aantal verkocht',
							    'name' => 'aantal_verkocht',
							    'type' => 'text',
							    'instructions' => '',
							    'required' => 0,
							    'conditional_logic' => 0,
							    'wrapper' => array(
								    'width' => '',
								    'class' => '',
								    'id' => '',
							    ),
							    'default_value' => '',
							    'placeholder' => '246,348,553,421,351,409,314,322,280,333,224,620',
							    'prepend' => '',
							    'append' => '',
							    'maxlength' => '',
						    ),
					    ),
				    ),
				    array(
					    'key' => 'field_610107738b647',
					    'label' => 'Trekkers verkocht per merk',
					    'name' => '',
					    'type' => 'tab',
					    'instructions' => '',
					    'required' => 0,
					    'conditional_logic' => 0,
					    'wrapper' => array(
						    'width' => '',
						    'class' => '',
						    'id' => '',
					    ),
					    'placement' => 'left',
					    'endpoint' => 0,
				    ),
				    array(
					    'key' => 'field_6101078b8b649',
					    'label' => 'Jaar',
					    'name' => 'verkocht_per_merk',
					    'type' => 'repeater',
					    'instructions' => '',
					    'required' => 0,
					    'conditional_logic' => 0,
					    'wrapper' => array(
						    'width' => '',
						    'class' => '',
						    'id' => '',
					    ),
					    'collapsed' => '',
					    'min' => 0,
					    'max' => 0,
					    'layout' => 'row',
					    'button_label' => 'Nieuwe maand',
					    'sub_fields' => array(
						    array(
							    'key' => 'field_6101078b8b64a',
							    'label' => 'Merk',
							    'name' => 'merk',
							    'type' => 'text',
							    'instructions' => '',
							    'required' => 0,
							    'conditional_logic' => 0,
							    'wrapper' => array(
								    'width' => '',
								    'class' => '',
								    'id' => '',
							    ),
							    'default_value' => '',
							    'placeholder' => 'vk Case-IH/Steyr',
							    'prepend' => '',
							    'append' => '',
							    'maxlength' => '',
						    ),
						    array(
							    'key' => 'field_6101078b8b64b',
							    'label' => 'Aantal verkocht',
							    'name' => 'aantal_verkocht',
							    'type' => 'text',
							    'instructions' => '',
							    'required' => 0,
							    'conditional_logic' => 0,
							    'wrapper' => array(
								    'width' => '',
								    'class' => '',
								    'id' => '',
							    ),
							    'default_value' => '',
							    'placeholder' => '',
							    'prepend' => '',
							    'append' => '',
							    'maxlength' => '',
						    ),
					    ),
				    ),
				    array(
					    'key' => 'field_61010cd3900ba',
					    'label' => 'Trekkers totaal',
					    'name' => '',
					    'type' => 'tab',
					    'instructions' => '',
					    'required' => 0,
					    'conditional_logic' => 0,
					    'wrapper' => array(
						    'width' => '',
						    'class' => '',
						    'id' => '',
					    ),
					    'placement' => 'top',
					    'endpoint' => 0,
				    ),
				    array(
					    'key' => 'field_61010d5d900bb',
					    'label' => 'Totaal trekkers verkocht per jaar',
					    'name' => 'totaal_trekkers_verkocht_per_jaar',
					    'type' => 'text',
					    'instructions' => '',
					    'required' => 0,
					    'conditional_logic' => 0,
					    'wrapper' => array(
						    'width' => '',
						    'class' => '',
						    'id' => '',
					    ),
					    'default_value' => '',
					    'placeholder' => '',
					    'prepend' => '',
					    'append' => '',
					    'maxlength' => '',
				    ),
			    ),
			    'location' => array(
				    array(
					    array(
						    'param' => 'options_page',
						    'operator' => '==',
						    'value' => 'newheap',
					    ),
				    ),
			    ),
			    'menu_order' => 0,
			    'position' => 'normal',
			    'style' => 'default',
			    'label_placement' => 'top',
			    'instruction_placement' => 'label',
			    'hide_on_screen' => '',
			    'active' => true,
			    'description' => '',
		    ));

	    endif;
    }
}
