<?php
namespace NewHeap\Theme\Cache;

use Codio\CodioAuth\Zeno\Zeno;
use NewHeap\Theme\Multisite\Multisite;

class Cache {
	const CACHE_FOLDER = '/assets/cache';
	const POSTS_FILENAME = '/posts/all_posts_page_';
	const COUNTRIES_FILENAME = '/countries/all_countries';
	const POSTS_META_FILENAME = '/posts/all_posts_page_meta_';

	public function __construct()
	{
		if (isset($_GET['clearcache'])) {
			add_action('init', function() {
				self::cache_posts_from_all_sites();
				Zeno::get_country_codes();
			});
		}
	}

	public static function cache_posts_from_all_sites()
	{
		$blogs = array(
			Multisite::SITE_MECHAMAN,
			Multisite::SITE_LANDBOUW_MECHANISATIE,
			Multisite::SITE_TUIN_EN_PARK_TECHNIEK,
			Multisite::SITE_VEEHOUDERIJ_TECHNIEK
		);

		$all_posts = [];

		foreach ($blogs as $blog) {
			switch_to_blog($blog);

			$args = array(
				'post_type' => 'post',
				'posts_per_page' => -1,
				'orderby' => 'date',
				'order' => 'DESC'
			);

			if (isset($_GET['category']) and !empty($_GET['category'])) {
				$args['cat'] = $_GET['category'];
			}

			$blog_posts = (new \WP_Query($args))->posts;


			foreach ($blog_posts as $blog_post) {
				$blog_post->nh_meta = [
					'thumbnail' => get_the_post_thumbnail_url($blog_post->ID, 'large'),
					'premium' => get_field('premium_article', $blog_post->ID),
					'read_time' => \NewHeap\Theme\Helpers::get_read_time($blog_post->ID),
					'author_display_name' => get_the_author_meta('display_name', $blog_post->post_author),
				];

				$blog_post->permalink = get_the_permalink($blog_post->ID);
				$all_posts[] = $blog_post;
			}

			restore_current_blog();
		}

		self::write_cache($all_posts);
	}

	public static function write_cache($all_posts)
	{
		self::clear_cache('posts');

		usort($all_posts, function($a, $b)
		{
			$a_date = strtotime($a->post_date);
			$b_date = strtotime($b->post_date);
			if ($a_date === $b_date) {
				return 0;
			}
			if ($a_date > $b_date) {
				return -1;
			}
			return 1;
		});

		$posts_per_page = get_option( 'posts_per_page' );
		$paged_posts = array_chunk($all_posts, $posts_per_page);

		foreach ($paged_posts as $key => $paged_post) {
			$page = $key + 1;
			$log_file = get_template_directory() . self::CACHE_FOLDER . self::POSTS_FILENAME . $page . '.json';
            if(!is_dir(get_template_directory() . self::CACHE_FOLDER)) { mkdir(get_template_directory() . self::CACHE_FOLDER); }
            if(!is_dir(get_template_directory() . self::CACHE_FOLDER . "/posts/")) { mkdir(get_template_directory() . self::CACHE_FOLDER . "/posts/"); }
			$cached_data = json_encode($paged_post);

			$file = fopen($log_file, 'w+');
			fwrite($file, $cached_data);
			fclose($file);
		}

		// Store cache meta
		self::store_cache_meta($all_posts, $posts_per_page);
	}

	public static function store_cache_meta($all_posts, $posts_per_page)
	{
		$posts_meta = [
			'posts_count' => count($all_posts),
			'pages' => ceil(count($all_posts) / $posts_per_page)
		];

		$log_file = get_template_directory() . self::CACHE_FOLDER . '/' . self::POSTS_META_FILENAME . '.json';
		$cached_data = json_encode($posts_meta);

		$file = fopen($log_file, 'w+');
		fwrite($file, $cached_data);
		fclose($file);
	}

	public static function get_posts_from_page($page)
	{
		return json_decode(file_get_contents(get_template_directory() . self::CACHE_FOLDER . '/' . self::POSTS_FILENAME . $page . '.json', FILE_USE_INCLUDE_PATH));
	}

	public static function get_cache_meta()
	{
		return json_decode(file_get_contents(get_template_directory() . self::CACHE_FOLDER . '/' . self::POSTS_META_FILENAME . '.json', FILE_USE_INCLUDE_PATH));
	}

	public static function clear_cache($type)
	{
		$files = glob(get_template_directory() . self::CACHE_FOLDER . '/' .$type . '/*');

		foreach ($files as $file) {
			if (is_file($file)) {
				unlink($file); // delete file
			}
		}
	}


}
