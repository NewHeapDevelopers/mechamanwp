<?php
namespace NewHeap\Theme\Cache;

use Codio\CodioAuth\Zeno\Zeno;
use NewHeap\Theme\Multisite\Multisite;

class CacheRss {
	const EXTERNAL_NEWS_FILENAME = '/external-news';

	public function __construct()
	{

	}

	public static function cache_external_news_mm()
	{
		$feedContent = file_get_contents("http://www.rssmix.com/u/12138875/rss.xml");
		$items = [];

		try {
			$feed = new \SimpleXmlElement($feedContent);

			foreach($feed->channel->item as $item) {
				$items[] = $item;
			}
		} catch (Exception $e) {
			\NewHeap\Theme\Logger\Logger::write_log('Couldn\'t get rss data in mechaman/footer.php');
		}

		self::write_cache($items);
	}

	public static function write_cache($items)
	{
		if (empty($items)) {
			return;
		}

		self::clear_cache('external-news');

		$log_file = get_template_directory() . Cache::CACHE_FOLDER . self::EXTERNAL_NEWS_FILENAME . '/' .self::EXTERNAL_NEWS_FILENAME . '.json';

		// Check if folder exists
        if (!is_dir(get_template_directory() . Cache::CACHE_FOLDER)) {
        	mkdir(get_template_directory() . Cache::CACHE_FOLDER);
        }

		// Check if folder exists
        if (!is_dir(get_template_directory() . Cache::CACHE_FOLDER . "/external-news/")) {
        	mkdir(get_template_directory() . Cache::CACHE_FOLDER . "/external-news/");
        }

		$cached_data = json_encode($items);

		$file = fopen($log_file, 'w+');
		fwrite($file, $cached_data);
		fclose($file);
	}

	public static function get_external_news()
	{
		return json_decode(file_get_contents(get_template_directory() . Cache::CACHE_FOLDER . self::EXTERNAL_NEWS_FILENAME . '/' . self::EXTERNAL_NEWS_FILENAME . '.json', FILE_USE_INCLUDE_PATH));
	}

	public static function clear_cache($type)
	{
		$files = glob(get_template_directory() . Cache::CACHE_FOLDER . '/' .$type . '/*');

		foreach ($files as $file) {
			if (is_file($file)) {
				unlink($file); // delete file
			}
		}
	}
}
