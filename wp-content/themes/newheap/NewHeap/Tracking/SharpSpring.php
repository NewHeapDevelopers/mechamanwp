<?php
namespace NewHeap\Theme\SharpSpring;

use NewHeap\Theme\Multisite\Multisite;

class SharpSpring
{
	public function __construct()
	{
	}

	/**
	 * Add SharpSpring tracking script on login page after registration
	 *
	 * @param array $mimes
	 *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
	public static function sharpspring_tracking_script($type)
	{
		$tracking_script = '';

		// Different scripts for each site
		if (get_current_blog_id() === Multisite::SITE_LANDBOUW_MECHANISATIE) {

			switch($type) {
				case 'gratis' :
					$tracking_script .= "<script type=\"text/javascript\">var __ss_noform = __ss_noform || [];__ss_noform.push(['baseURI', 'https://app-3QNJXQNLAC.marketingautomation.services/webforms/receivePostback/MzawMDE3NzEwAAA/']);__ss_noform.push(['endpoint', 'c66bc9ae-c360-4e7e-8921-5b8fd3a7fc14']);</script>";
					break;
				case 'plus' :
					$tracking_script .= "<script type=\"text/javascript\">var __ss_noform = __ss_noform || [];__ss_noform.push(['baseURI', 'https://app-3QNJXQNLAC.marketingautomation.services/webforms/receivePostback/MzawMDE3NzEwAAA/']);__ss_noform.push(['endpoint', 'c8683a86-1884-46bc-9797-9595adbe284e']);</script>";
					break;
				case 'totaal' :
					$tracking_script .= "<script type=\"text/javascript\">var __ss_noform = __ss_noform || [];__ss_noform.push(['baseURI', 'https://app-3QNJXQNLAC.marketingautomation.services/webforms/receivePostback/MzawMDE3NzEwAAA/']);__ss_noform.push(['endpoint', '14667295-09d8-42f8-937d-85e677bf8e4b']);</script>";
					break;
				case 'existing_user' :
					$tracking_script .= "<script type=\"text/javascript\">var ss_noform = ss_noform || [];ss_noform.push(['baseURI', 'https://app-3QNJXQNLAC.marketingautomation.services/webforms/receivePostback/MzawMDE3NzEwAAA/']); ss_noform.push(['endpoint', '47a9b43d-8b07-475f-84e6-8b6bf2580b76']);</script>";
					break;
			}

		} elseif (get_current_blog_id() === Multisite::SITE_TUIN_EN_PARK_TECHNIEK) {

			switch($type) {
				case 'gratis' :
					$tracking_script .= "<script type=\"text/javascript\">var __ss_noform = __ss_noform || [];__ss_noform.push(['baseURI', 'https://app-3QNJXQNLAC.marketingautomation.services/webforms/receivePostback/MzawMDE3NzEwAAA/']);__ss_noform.push(['endpoint', 'aeb599dc-217d-44ec-8cbc-8a0398e550c8']);</script>";
					break;
				case 'plus' :
					$tracking_script .= "<script type=\"text/javascript\">var __ss_noform = __ss_noform || [];__ss_noform.push(['baseURI', 'https://app-3QNJXQNLAC.marketingautomation.services/webforms/receivePostback/MzawMDE3NzEwAAA/']);__ss_noform.push(['endpoint', '2f8fd740-1dff-484c-8247-5a7bc18e245b']);</script>";
					break;
				case 'totaal' :
					$tracking_script .= "<script type=\"text/javascript\">var __ss_noform = __ss_noform || [];__ss_noform.push(['baseURI', 'https://app-3QNJXQNLAC.marketingautomation.services/webforms/receivePostback/MzawMDE3NzEwAAA/']);__ss_noform.push(['endpoint', '40dee930-9758-479b-826c-4d348451f162']);</script>";
					break;
				case 'existing_user' :
					$tracking_script .= "<script type=\"text/javascript\">var ss_noform = ss_noform || [];ss_noform.push(['baseURI', 'https://app-3QNJXQNLAC.marketingautomation.services/webforms/receivePostback/MzawMDE3NzEwAAA/']); ss_noform.push(['endpoint', '47a9b43d-8b07-475f-84e6-8b6bf2580b76']);</script>";
					break;
			}

		} elseif (get_current_blog_id() === Multisite::SITE_VEEHOUDERIJ_TECHNIEK) {

			switch($type) {
				case 'gratis' :
					$tracking_script .= "<script type=\"text/javascript\">var __ss_noform = __ss_noform || [];__ss_noform.push(['baseURI', 'https://app-3QNJXQNLAC.marketingautomation.services/webforms/receivePostback/MzawMDE3NzEwAAA/']);__ss_noform.push(['endpoint', '2127e5c6-bda4-4c5e-a9a8-af7c079d265c']);</script>";
					break;
				case 'plus' :
					$tracking_script .= "<script type=\"text/javascript\">var __ss_noform = __ss_noform || [];__ss_noform.push(['baseURI', 'https://app-3QNJXQNLAC.marketingautomation.services/webforms/receivePostback/MzawMDE3NzEwAAA/']);__ss_noform.push(['endpoint', '60dce663-6e6b-4d3f-84c3-6488623622fa']);</script>";
					break;
				case 'totaal' :
					$tracking_script .= "<script type=\"text/javascript\">var __ss_noform = __ss_noform || [];__ss_noform.push(['baseURI', 'https://app-3QNJXQNLAC.marketingautomation.services/webforms/receivePostback/MzawMDE3NzEwAAA/']);__ss_noform.push(['endpoint', '008a993e-562a-4237-821b-712ca5a623d7']);</script>";
					break;
				case 'existing_user' :
					$tracking_script .= "<script type=\"text/javascript\">var ss_noform = ss_noform || [];ss_noform.push(['baseURI', 'https://app-3QNJXQNLAC.marketingautomation.services/webforms/receivePostback/MzawMDE3NzEwAAA/']); ss_noform.push(['endpoint', '47a9b43d-8b07-475f-84e6-8b6bf2580b76']);</script>";
					break;
			}

		}

		$tracking_script .= '<script type="text/javascript" src="https://koi-3QNJXQNLAC.marketingautomation.services/client/noform.js?ver=1.24" ></script>';

		return $tracking_script;
	}
}
