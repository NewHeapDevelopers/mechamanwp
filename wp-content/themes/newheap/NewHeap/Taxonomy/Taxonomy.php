<?php
namespace NewHeap\Theme\Taxonomy;

class Taxonomy {
	public function __construct()
	{
		$this->init();
	}

	public function init()
	{
		add_action('init', [$this, 'modify_tags']);
	}

	public function modify_tags()
	{
		global $wp_taxonomies;

		$wp_taxonomies['post_tag']->hierarchical = true;
	}

	public static function get_archive_page_category_id(int $post_id)
	{
		global $wpdb;

		$result = $wpdb->get_row("SELECT * FROM {$wpdb->termmeta} WHERE `meta_key` = 'categorie_pagina' AND `meta_value` = {$post_id}");

		if (!empty($result)) {
			return intval($result->term_id);
		}
	}

}
