<?php
namespace NewHeap\Theme\Uploads;

/**
 * Class Uploads
 * @package NewHeap\Theme\Uploads
 *
 * Media library and upload functions
 */
class Uploads
{
    public function __construct()
    {
        /** Allow other mime types */
        /** WARNING: not working on MULTISITE, go to multisite to change allowed mime types */
        //add_action('upload_mimes', [$this, 'allowMimeTypes']);

        add_action( 'after_setup_theme', [$this, 'addImageSizes'] );

    }

    private function allowMimeTypes($mimes = [])
    {
        //$mimes['csv'] = "text/csv";
        //return $mimes;
    }

    public function addImageSizes()
    {
        add_image_size( 'xlarge', 2000, 2000, true );
    }
}
