<?php
namespace NewHeap\Theme\Cpt;

class CptUi
{
	public function __construct()
	{
		add_action('init', [$this, 'cptui_register_my_cpts']);
		add_action('init', [$this, 'cptui_register_my_taxes']);
	}

	/**
	 * Register CPT
	 * - partners
	 * - agenda
	 * - magazine
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
	function cptui_register_my_cpts()
	{
		/**
		 * Post Type: Partners.
		 */
		$labels = [
			"name" => __( "Kennispartners", "custom-post-type-ui" ),
			"singular_name" => __( "Kennispartner", "custom-post-type-ui" ),
		];

		$args = [
			"label" => __( "Kennispartner", "custom-post-type-ui" ),
			"labels" => $labels,
			"description" => "",
			"public" => true,
			"publicly_queryable" => true,
			"show_ui" => true,
			"show_in_rest" => true,
			"rest_base" => "",
			"rest_controller_class" => "WP_REST_Posts_Controller",
			"has_archive" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"delete_with_user" => false,
			"exclude_from_search" => false,
			"capability_type" => "post",
			"map_meta_cap" => true,
			"hierarchical" => false,
			"rewrite" => [ "slug" => "kennispartner", "with_front" => true ],
			"query_var" => true,
			"menu_icon" => "dashicons-groups",
			"supports" => [ "title", "editor", "thumbnail", "excerpt" ],
			"show_in_graphql" => false,
		];

		register_post_type( "partners", $args );


		/**
		 * Post Type: Agenda.
		 */
		$labels = [
			"name" => __( "Agenda", "custom-post-type-ui" ),
			"singular_name" => __( "Agenda", "custom-post-type-ui" ),
		];

		$args = [
			"label" => __( "Agenda", "custom-post-type-ui" ),
			"labels" => $labels,
			"description" => "",
			"public" => true,
			"publicly_queryable" => true,
			"show_ui" => true,
			"show_in_rest" => true,
			"rest_base" => "",
			"rest_controller_class" => "WP_REST_Posts_Controller",
			"has_archive" => false,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"delete_with_user" => false,
			"exclude_from_search" => false,
			"capability_type" => "post",
			"map_meta_cap" => true,
			"hierarchical" => false,
			"rewrite" => [ "slug" => "agenda", "with_front" => true ],
			"query_var" => true,
			"menu_icon" => "dashicons-calendar-alt",
			"supports" => [ "title", "editor", "thumbnail" ],
			"show_in_graphql" => false,
		];

		register_post_type( "agenda", $args );


		/**
		 * Post Type: Yudu magazines.
		 */
		$labels = [
			"name" => __( "Yudu magazines", "custom-post-type-ui" ),
			"singular_name" => __( "Magazine", "custom-post-type-ui" ),
		];

		$args = [
			"label" => __( "Yudu magazines", "custom-post-type-ui" ),
			"labels" => $labels,
			"description" => "",
			"public" => true,
			"publicly_queryable" => true,
			"show_ui" => true,
			"show_in_rest" => true,
			"rest_base" => "",
			"rest_controller_class" => "WP_REST_Posts_Controller",
			"has_archive" => false,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"delete_with_user" => false,
			"exclude_from_search" => false,
			"capability_type" => "post",
			"map_meta_cap" => true,
			"hierarchical" => false,
			"rewrite" => [ "slug" => "magazine", "with_front" => true ],
			"query_var" => true,
			"menu_icon" => "dashicons-book",
			"supports" => [ "title", "editor", "excerpt", "thumbnail" ],
			"show_in_graphql" => false,
		];

		register_post_type( "magazine", $args );


		/**
		 * Post Type: Reizen.
		 */
		$labels = [
			"name" => __( "Reizen", "custom-post-type-ui" ),
			"singular_name" => __( "Reis", "custom-post-type-ui" ),
		];

		$args = [
			"label" => __( "Reizen", "custom-post-type-ui" ),
			"labels" => $labels,
			"description" => "",
			"public" => true,
			"publicly_queryable" => true,
			"show_ui" => true,
			"show_in_rest" => true,
			"rest_base" => "",
			"rest_controller_class" => "WP_REST_Posts_Controller",
			"has_archive" => false,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"delete_with_user" => false,
			"exclude_from_search" => false,
			"capability_type" => "post",
			"map_meta_cap" => true,
			"hierarchical" => false,
			"rewrite" => [ "slug" => "reizen", "with_front" => true ],
			"query_var" => true,
			"menu_icon" => "dashicons-airplane",
			"supports" => [ "title", "editor", "thumbnail" ],
			"show_in_graphql" => false,
		];

		register_post_type( "reizen", $args );
	}

	/**
	 * Register Taxonomies
	 * - dossier
	 *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
	public function cptui_register_my_taxes()
	{

		/**
		 * Taxonomy: Dossiers.
		 */
		$labels = [
			"name" => __( "Dossiers", "custom-post-type-ui" ),
			"singular_name" => __( "Dossier", "custom-post-type-ui" ),
		];

		$args = [
			"label" => __( "Dossiers", "custom-post-type-ui" ),
			"labels" => $labels,
			"public" => true,
			"publicly_queryable" => true,
			"hierarchical" => true,
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"query_var" => true,
			"rewrite" => [ 'slug' => 'dossier', 'with_front' => true, ],
			"show_admin_column" => false,
			"show_in_rest" => true,
			"rest_base" => "dossier",
			"rest_controller_class" => "WP_REST_Terms_Controller",
			"show_in_quick_edit" => false,
			"show_in_graphql" => false,
		];
		register_taxonomy( "dossier", [ "post" ], $args );


		/**
		 * Taxonomy: Types.
		 */
		$labels = [
			"name" => __( "Soorten", "custom-post-type-ui" ),
			"singular_name" => __( "Soort   ", "custom-post-type-ui" ),
		];

		$args = [
			"label" => __( "Types", "custom-post-type-ui" ),
			"labels" => $labels,
			"public" => true,
			"publicly_queryable" => true,
			"hierarchical" => true,
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"query_var" => true,
			"rewrite" => [ 'slug' => 'artikel_type', 'with_front' => true, ],
			"show_admin_column" => false,
			"show_in_rest" => true,
			"rest_base" => "artikel_type",
			"rest_controller_class" => "WP_REST_Terms_Controller",
			"show_in_quick_edit" => false,
			"show_in_graphql" => false,
		];
		register_taxonomy( "artikel_type", [ "post" ], $args );


		/**
		 * Taxonomy: Partners.
		 */
		$labels = [
			"name" => __( "Partners", "custom-post-type-ui" ),
			"singular_name" => __( "Partner", "custom-post-type-ui" ),
		];


		$args = [
			"label" => __( "Partners", "custom-post-type-ui" ),
			"labels" => $labels,
			"public" => false,
			"publicly_queryable" => true,
			"hierarchical" => true,
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"query_var" => true,
			"rewrite" => [ 'slug' => 'partner', 'with_front' => true, ],
			"show_admin_column" => false,
			"show_in_rest" => true,
			"rest_base" => "partner",
			"rest_controller_class" => "WP_REST_Terms_Controller",
			"show_in_quick_edit" => false,
			"show_in_graphql" => false,
		];
		register_taxonomy( "partner", [ "partners" ], $args );
	}
}
