<?php
namespace NewHeap\Theme\Cpt;


class Cpt
{
    public function __construct()
    {
        // Disable default post
//        add_action('admin_menu', [$this, 'remove_default_post_type']);
//        add_action('admin_bar_menu', [$this, 'remove_default_post_type_menu_bar'], 999);
//        add_action('wp_dashboard_setup', [$this, 'remove_draft_widget'], 999);
    }

    /**
     * Remove default post from menu
     *
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public function remove_default_post_type() {
        remove_menu_page( 'edit.php' );
    }

    /**
     * Remove default post from top menu
     *
     * @param $wp_admin_bar
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public function remove_default_post_type_menu_bar( $wp_admin_bar ) {
        $wp_admin_bar->remove_node( 'new-post' );
    }

    /**
     * Remove default post quick draft
     *
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public function remove_draft_widget(){
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    }
}
