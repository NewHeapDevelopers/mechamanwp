<?php
namespace NewHeap\Theme\Weather;

/**
 * Class Weather
 * @package NewHeap\Theme\Taxonomy
 *
 * There is a ajax call to get the weather and a static function jQuery.get(ajaxurl, {action: 'get_weather', q: 'De Bilt,NL'}, function(data) { alert(data); });
 * And there are 3 static functions getWeather and getWeatherJson and clearCache
 * By default it caches the weather every 15 minutes
 */
class Weather {

    public static $apiKey = "936a20bc9da950646a3ebca98d19b901";
    public static  $url = "https://api.openweathermap.org/data/2.5/";

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        //Ajax calls to get weather
        add_action('wp_ajax_nopriv_get_weather', [$this, 'getWeatherJson']);
        add_action('wp_ajax_get_weather', [$this, 'getWeatherJson']);

        //Cronjob that clears cache every 15 minutes
        if ( ! get_transient( 'every_15_minutes' ) ) {
            set_transient('every_15_minutes', true, 15 * 60);
            $this->cronJob15Min();
        }
    }
	
	public static function iconToFontAwesomeIcon($icon) {
		switch($icon) {
			default:
				return 'fas fa-question-circle';
				break;
		}
	}

    public static function getWeather($q = "De Bilt,NL", $lang = "nl", $units = "metric", $apiKey = null)
    {
        $object = json_decode(self::getWeatherJson($q, $lang, $units, $apiKey));

        return $object;
    }

    public static function getWeatherJson($q = "De Bilt,NL", $lang = "nl", $units = "metric", $apiKey = null)
    {
        if(isset($_REQUEST['q']) and !empty($_REQUEST['q'])) { $q = $_REQUEST['q']; } // location query
        if(isset($_REQUEST['lang']) and !empty($_REQUEST['lang'])) { $lang = $_REQUEST['lang']; } //language
        if(isset($_REQUEST['units']) and !empty($_REQUEST['units'])) { $units = $_REQUEST['units']; } //units (metric = celcius)

        if(empty($apiKey)) { $apiKey = self::$apiKey; }
        $url = self::$url."weather?q=".urlencode($q)."&appid=".$apiKey."&lang=".$lang."&units=".$units;

        if(!self::getFromCache($url)) {
            $json = file_get_contents($url);
            self::updateCache($url, $json);
        } else {
			return self::getFromCache($url);
		}

        return $json;
    }

    public static function getFromCache($url) {
        $cacheFileName  = (__DIR__) . "/cache/" . md5($url) . ".cache";

        if(file_exists($cacheFileName)) {
            $json = file_get_contents($cacheFileName, true);
            if(!empty($json)) {
                return $json;
            }
        }

        return false;
    }

    private static function updateCache($url, $json) {

        $cachePath = (__DIR__) . "/cache/";
        if (!is_dir($cachePath)) {
        	mkdir($cachePath);
        }

        $cacheFileName  = $cachePath . md5($url) . ".cache";
        $file = fopen($cacheFileName, "w") or die("Unable to open file!");
        fwrite($file, $json);

        return true;
    }

    private function cronJob15Min() {
        self::clearCache();
    }

    public static function clearCache() {

        $cachePath  = (__DIR__) . "/cache/";
        $files = glob($cachePath . '/*');

        foreach($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }

        return true;
    }
}
