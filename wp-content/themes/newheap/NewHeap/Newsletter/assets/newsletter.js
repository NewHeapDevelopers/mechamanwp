jQuery(document).on('click', '[data-newsletter-message]', function(e) {
    e.preventDefault();
    var msgElement = $($(this).attr('data-newsletter-message'));
    var nameInput = $($(this).attr('data-newsletter-name'));
    var mailInput = $($(this).attr('data-newsletter-mail'));

    jQuery.ajax({
        url: NewHeap.ajax_url,
        type: 'post',
        data: {
            action: 'add_newsletter',
            email: mailInput.val(),
            name: nameInput.val()
        },
        success: (data) => {
            data = JSON.parse(data);
            msgElement.html(data.message);
        },
        error: () => {
            msgElement.html('Oeps er is wat fout gegaan, probeer het later nog eens.');
        },
    });

});
