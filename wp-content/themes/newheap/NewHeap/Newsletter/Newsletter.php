<?php
namespace NewHeap\Theme\Newsletter;

use NewHeap\Theme\GravityForms\GravityForms;
use NewHeap\Theme\SharpSpring\SharpSpring;

class Newsletter
{
    private $logins = [
        1 => [
            'sitename' => 'mechaman',
            'username' => 'mechaman',
            'password' => 'Uh$d15@2!',
            'apiKey' => 'B2RJK91BU479E1PAANAU',
            'groupId' => '1'
        ],
        2 => [
            'sitename' => 'landbouw mechanisatie',
            'username' => 'lmupdate',
            'password' => 'Uh$d15@2!',
            'apiKey' => 'YY9TVM8T46WGYWFU3324',
            'groupId' => '20'
        ],
        7 => [
            'sitename' => 'Veehouderrij Techniek',
            'username' => 'vtupdate',
            'password' => 'Uh$d15@2!',
            'apiKey' => 'FWHEEFMEYV1AWWGKNZ2K',
            'groupId' => '29'
        ],
        5 => [
            'sitename' => 'Tuin en Park Techniek',
            'username' => 'tptupdate',
            'password' => 'Uh$d15@2!',
            'apiKey' => 'F9GCZ2HP6QAWKCXMC0YK',
            'groupId' => '25'
        ]
    ];

	public function __construct()
	{
        add_action('wp_ajax_add_newsletter',  [$this, 'addNewsletterAjax']);
        add_action('wp_ajax_nopriv_add_newsletter',  [$this, 'addNewsletterAjax']);

        add_action('wp_enqueue_scripts', [$this, 'enqueueScriptsStyles'], 11);
	}

    public function enqueueScriptsStyles()
    {
        wp_enqueue_script('nh-newsletter-script', get_template_directory_uri() . '/NewHeap/Newsletter/assets/newsletter.js', ['newheap-script', 'jquery'], false, true);
    }

    public function addNewsletterAjax()
    {
        $blogId = $_POST['blogId'];
        $email = $_POST['email'];
        $name = $_POST['name'];

        $response = $this->addEmail($email, $blogId, $name);
        if(strstr($response, 'Invalid Email Address')) {
            echo json_encode((object)['success' => false, 'message' => 'Ongeldig e-mailadres.']);
            die();
        }
        if(strstr($response, 'Email address already')) {
            echo json_encode((object)['success' => false, 'message' => 'E-mailadres bestaat al voor deze website.']);
            die();
        }
        if(strstr($response, 'has been added')) {
            echo json_encode((object)['success' => true, 'message' => 'E-mailadres is succesvol toegevoegd.']);

            GravityForms::data_to_sharpspring([
                'email' => $_POST['email'],
                'name' => $_POST['name']
            ]);
            die();
        }

        echo json_encode((object)['success' => false, 'message' => 'Er is iets foutgegaan met de verwerking, wij willen vriendelijk verzoeken contact met ons op te nemen.', 'externalError' => $response]);
        die();
    }

	public function addEmail($email, $blogId = null, $name = null) {
	    if(empty($blogId)) { $blogId = get_current_blog_id(); }

	    $url = 'http://www.agrimedia.emailnewsletter-software.net/api/Contacts.Add?Key='.$this->logins[$blogId]['apiKey'].'&Username='.$this->logins[$blogId]['username'].'&Email='.$email.'&GroupID='.$this->logins[$blogId]['groupId'];
	    if(!empty($name)) {
	        $url .= "&Field2=".$name;
        }

	    $request = file_get_contents($url);
	    return $request;
    }

}
