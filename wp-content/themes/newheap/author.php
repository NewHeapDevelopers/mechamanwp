<?php
get_header();

$counter = 0;
$author = get_queried_object()->data;
$latest_post = get_posts( array(
	'author'      => $author->ID,
	'orderby'     => 'date',
	'numberposts' => 1
));
$latest_post_date = get_post_datetime($latest_post[0]->ID)->format('d-m-Y');

$currentBlogId = get_current_blog_id();
$author_image = get_user_meta( get_the_author_meta('ID'), 'picture', true );
?>

    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-10 offset-md-1">
                    <div class="header-image"
                         style="background-image: url('<?php echo $author_image; ?>');">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-8 offset-md-2">
                    <div class="header-content text-white bg-black remove-top-space px-5 pt-1 pb-3">
                        <div class="article-options">
                            <div class="text-start">
                                <span class="py-2">
                                    <i class="fas fa-pen-nib"></i>
                                    <span><?php _e('Auteur', 'newheap'); ?></span>
                                </span>
                            </div>

<!--                            <div class="text-end">-->
<!--                                <span data-favorite-click="--><?//=get_the_ID()?><!--" class="--><?php //echo $bg_class; ?><!-- py-2">-->
<!--                                    <i class="fas fa-star"></i>-->
<!--                                    <span>--><?php //_e('Favoriet', 'newheap'); ?><!--</span>-->
<!--                                </span>-->
<!--                            </div>-->
                        </div>

                        <hr class="mt-1">

                        <div class="tax-holder">
                            <h4 class="primary-color">
                                <?php _e('Nieuwste bericht', 'newheap'); ?>:
                                <?php echo $latest_post_date; ?>
                            </h4>
                        </div>

                        <h1 class="">
                            <?php echo $author->display_name; ?>
                        </h1>

                        <p class="mt-2">
	                        <?php echo get_the_author_meta('description', $author->ID); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="author-posts my-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-10 offset-md-1">
                    <h3 class="section-title">
	                    <?php printf(__('Artikelen van %s', 'newheap'), $author->display_name); ?>
                    </h3>
                </div>

                <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); $counter++; ?>
                        <?php if ($counter == 11) : ?>
                            <div class="col-sm-12 col-md-10 offset-md-1 google-ad-holder ad-between-posts-holder text-center">
                                <div class="dotted-bottom-black pt-5 pb-5 mb-3">
                                    <div class="ad-between-posts">
                                        <?=get_field('advertisement_author', 'option')?>
                                    </div>
                                </div>
                            </div>
		                <?php endif; ?>

                        <div class="col-sm-12 col-md-10 offset-md-1 mb-3">
                            <?php include get_theme_file_path('elements/article-listing-item.php'); ?>
                        </div>
                    <?php endwhile; ?>

	                <?php
	                global $posts;
	                if (count($posts) < 11) : ?>
                        <div class="col-sm-12 col-md-10 offset-md-1 google-ad-holder ad-between-posts-holder text-center">
                            <div class="pt-5 pb-5 mb-3">
                                <div class="ad-between-posts">
                                    <?=get_field('advertisement_author', 'option')?>
                                </div>
                            </div>
                        </div>
	                <?php endif; ?>

                    <div class="col-sm-12 col-md-10 offset-md-1 pb-3 mb-3">
		                <?php the_posts_pagination([
			                'screen_reader_text' => ' ',
			                'mid_size' => 1,
			                'prev_text' => '<i class="fas fa-chevron-left"></i>',
			                'next_text' => '<i class="fas fa-chevron-right"></i>',
		                ]); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </section>

<?php get_footer();

