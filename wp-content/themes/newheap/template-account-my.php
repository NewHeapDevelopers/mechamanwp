<?php
/**
 * Template Name: Mijn account
 */

get_header(); ?>

    <div class="container">
		<?php if ( have_posts() ) : ?>
            <h1><?php the_title(); ?></h1>

            <div class="row">
                <div class="col-md-6">
                    <h3 class="section-title">
                        <?php _e( 'Gegevens', 'newheap' ); ?>
                    </h3>

                    <div class="user-meta-holder">
                        <a href="<?php echo wp_logout_url(); ?>"><strong>Uitloggen</strong></a>
                        <label>
							<?php _e( 'Gegevens', 'newheap' ); ?>
                        </label>
                        <p>
							<?php echo get_user_meta( get_current_user_id(), 'first_name', true ); ?>
							<?php echo get_user_meta( get_current_user_id(), 'last_name', true ); ?>
                        </p>
                    </div>

                    <div class="user-meta-holder">
                        <label>
							<?php _e( 'Adres', 'newheap' ); ?>
                        </label>
                        <p>
							<?php echo get_user_meta( get_current_user_id(), 'address', true ); ?>
                            <br>
							<?php echo get_user_meta( get_current_user_id(), 'zip', true ); ?>
							<?php echo get_user_meta( get_current_user_id(), 'city', true ); ?>
                            <br>
							<?php echo get_user_meta( get_current_user_id(), 'country', true ); ?>
                            <br>
                        </p>
                    </div>

                    <div class="user-meta-holder">
                        <label>
							<?php _e( 'Telefoonnummer', 'newheap' ); ?>
                        </label>
                        <p>
							<?php echo get_user_meta( get_current_user_id(), 'phone', true ); ?>
                        </p>
                    </div>

                    <div class="user-meta-holder">
                        <label>
							<?php _e( 'E-mailadres', 'newheap' ); ?>
                        </label>
                        <p>
							<?php echo get_user_meta( get_current_user_id(), 'phone', true ); ?>
                        </p>
                    </div>

                    <div class="user-meta-holder">
                        <label>
							<?php _e( 'Rekeningnummer', 'newheap' ); ?>
                        </label>
                        <p>
							<?php echo get_user_meta( get_current_user_id(), 'account_number', true ); ?>
                        </p>
                    </div>
                </div>

                <div class="col-md-6">
                    <h3><?php _e( 'Contact', 'newheap' ); ?></h3>

                    <p>
						<?php _e( 'Wilt u uw persoonsgegevens wijzigen? Neem dan contact op met de abonnementen-administratie van AgriMedia via:',
							'newheap' ); ?>
                    </p>

                    <p>
						<?php _e( 'Telefoon:', 'newheap' ); ?>
						<?php echo get_field( 'general_phone', 'option' ); ?>
                    </p>
                    <p>
						<?php _e( 'E-mailadres:', 'newheap' ); ?>
						<?php echo get_field( 'general_mail', 'option' ); ?>
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <h3 class="section-title">
                        <?php _e( 'Abonnement', 'newheap' ); ?>
                    </h3>
                </div>
            </div>


			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
				<?php // echo do_shortcode( '[newheap_profile_form]' ); ?>
			<?php endwhile; ?>

		<?php endif; ?>
    </div>

<?php get_footer();
