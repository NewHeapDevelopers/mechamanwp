<?php
/**
 * Template Name: Abonnement
 */

get_header(); ?>
    <div class="subscription">
        <div class="container">
            <div class="row mt-5">
                <div class="col-sm-12 col-md-10 offset-md-1">
                    <h1>
                        <?php the_title(); ?>
                    </h1>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-sm-12 col-md-10 offset-md-1">
                    <?php the_content(); ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-10 offset-md-1">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <p><?php echo get_field('abonnement_text'); ?></p>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <img src="<?php echo get_field('abonnement_afbeelding'); ?>" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-10 offset-md-1">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h3 class="section-title mb-0">
                                <?php _e('Aanmeldformulier', 'newheap'); ?>
                            </h3>
                            <div class="form-holder p-5">
                                <div class="py-3">
                                    <p><?php echo get_field('form_intro'); ?></p>
                                </div>

                                <div class="form-notifications">
	                                <?php echo \NewHeap\Theme\Notification\Notification::show_notifications(\NewHeap\Theme\Notification\Notification::LOCATION_ZENO); ?>
                                </div>

                                <?php echo do_shortcode('[newheap_registration_form]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer();
