<?php
/**
 * Template Name: Vakblad archief
 *
 * FI 4-10-2021: This one is somehow used for magazines. Didn't rename this file because page templates will get unlinked
 */

$category_id = \NewHeap\Theme\Taxonomy\Taxonomy::get_archive_page_category_id(get_the_ID());

$args = array(
    'post_type' => 'magazine',
    'posts_per_page' => -1
);

$query = new WP_Query($args);


get_header();
if (!empty($query)) : ?>
    <div id="filter-page" class="content-holder  pt-4">
    <div class="container nh-rest-required nh-rest-required-false">
        <!-- Style tag can be deleted after sass is added  -->
        <style>
            button.btn-filter.filter-active {
                background-color: green;
            }

            /** Pagination */
            .posts-pagination button.disabled {
                opacity: .4;
                pointer-events: none;
            }

            .posts-pagination button.btn-current-page {
                background-color: #333;
            }

            .posts-pagination ul {
                margin: 0;
                padding: 0;
            }

            .posts-pagination ul li {
                display: inline-block;
                list-style-type: none;
            }
        </style>

        <div class="row">
            <div class="col-lg-3">
                <div class="toggle-mobile-filters">
                    <button class="btn-open-mobile-filters">
			            <?php _e('Filters', 'newheap'); ?>
                    </button>
                </div>
                <div class="filters-holder sidebar">
                    <button class="btn-close-mobile-filters">
                        &times;
                    </button>
                    <div class="overflow-holder">
<!--                        <div class="filter-holder sort" data-filter-name="sort">-->
<!--                            <h3 class="alt-title mt-0">--><?php //_e('Sorteer', 'newheap'); ?><!--</h3>-->
<!--                            <ul class="post-filters">-->
<!--                                <li>-->
<!--                                    <button class="btn-filter filter-active active" data-value="1">-->
<!--                                        <span class="checkbox"></span>-->
<!--                                        --><?php //_e('Nieuw naar oud', 'newheap') ?>
<!--                                    </button>-->
<!--                                </li>-->
<!--                                <li data-type="not_premium">-->
<!--                                    <button class="btn-filter" data-value="0">-->
<!--                                        <span class="checkbox"></span>-->
<!--                                        --><?php //_e('Oud naar nieuw', 'newheap') ?>
<!--                                    </button>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </div>-->

                        <h3 class="alt-title"><?php _e('Filters', 'newheap'); ?></h3>

                        <div class="filter-holder" data-filter-name="magazine_nummer">
                            <h3 class="sub-title"><?php _e('Jaartal', 'newheap'); ?></h3>
                            <ul class="post-filters">
                                <?php if ($query->have_posts()) {
                                    $prevYear = "";
                                    while ($query->have_posts()) {
                                        $query->the_post();
	                                    $date = substr(get_field('magazine_nummer'), 0, 4);
                                        $currentYear = $date;

                                        if ($currentYear != $prevYear) { ?>
                                            <li>
                                                <button class="btn-filter" data-value="<?php echo $date; ?>">
                                                    <span class="checkbox"></span>
                                                    <?php echo $date; ?>
                                                </button>
                                            </li>
                                        <?php }
                                        $prevYear = $currentYear;
                                    }
                                }
                                wp_reset_query(); ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-9">
                <div class="magazines-holder mb-5">
                    <h3 class="section-title mt-0"><?php _e('Vakbladarchief', 'newheap'); ?></h3>

                    <div class="magazines-archive">
                        <div class="row">
                            <?php
                            if ($query->have_posts()) {
                                $prevYear = "";
                                while ($query->have_posts()) {
                                    $query->the_post();

                                    $date = substr(get_field('magazine_nummer'), 0, 4);
                                    $currentYear = $date;

                                    if ($currentYear != $prevYear) { ?>
                                        <div class="magazine-year-title col-sm-12 col-md-12 mt-3" data-date="<?php echo $date; ?>">
                                            <h3 class="sub-title">
                                                <?php echo $date ?>
                                            </h3>
                                        </div>
                                        <?php
                                        $prevYear = $currentYear;
                                    } ?>
                                    <div class="magazine-item col-lg-3 col-md-4 col-6 mb-3" data-date="<?php echo $date; ?>">
                                        <a href="<?php echo get_the_permalink(); ?>">
                                            <img src="<?php echo get_the_post_thumbnail_url(null, 'medium'); ?>" width="100%"
                                                 height="auto"/>
                                            <div class="bg-black text-white dotted-top p-3">
                                                <h3 class="uppercase"><?php echo get_the_title() ?></h3>
                                                <div class="date-holder"><?php echo get_field('magazine_nummer') ?></div>
                                                <div class="number"><?php _e('Nummer', 'newheap'); ?> <?php echo explode("-", get_field('magazine_nummer'))[1] ?></div>
                                                <div class="year"><?php _e('Jaargang', 'newheap'); ?> <?php echo (explode("-", get_field('magazine_nummer'))[0] - 1949) ?></div>
                                            </div>
                                        </a>
                                    </div>
                                <?php }
                            }
                            wp_reset_query(); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php endif;

get_footer();
