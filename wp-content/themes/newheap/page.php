<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package newheap
 */

get_header();
wp_reset_query();

$premium_article = get_field('premium_article');
?>

<div class="main">
    <div class="container">
        <?php
        if ( have_posts() ) : ?>
            <?php while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div><!-- /.main -->



<?php

get_footer();
