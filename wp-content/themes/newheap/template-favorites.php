<?php
/**
 * Template Name: Favorieten
 */

if(get_current_blog_id() == \NewHeap\Theme\Multisite\Multisite::SITE_MECHAMAN) {
    $favoriteArray = \NewHeap\Theme\Favorites\Favorites::getFavorites();
} else {
    $favoriteArray = \NewHeap\Theme\Favorites\Favorites::getFavorites(get_current_blog_id());
}


get_header(); ?>
<section class="favorites">
    <div class="container">
        <div class="row mt-5">
            <?php if(is_user_logged_in()) { ?>
                <!-- design but nog so nice <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-10"><h2 style="font-size: 2.5em;margin-bottom: 30px;">Opgeslagen artikelen</h2></div>
                    </div>
                </div>-->
                <div class="col-md-3">
                    <div class="sidebar">
                        <h3 class="alt-title mt-0">
                            <?php _e('Sorteer', 'newheap'); ?>
                        </h3>

                        <a href="<?php echo explode('?', \NewHeap\Theme\Helpers::get_current_url())[0]; ?>" class="toggle-holder <?php echo (!isset($_GET['order'])) ? 'checked': ''; ?>">
                            <label for="order-desc">
                                <i class="fas fa-times-circle"></i>
                                <?php _e('Nieuw naar oud', 'newheap'); ?>
                            </label>
                        </a>

                        <a href="<?php echo explode('?', \NewHeap\Theme\Helpers::get_current_url())[0]; ?>?order=asc" class="toggle-holder <?php echo (isset($_GET['order'])) ? 'checked': ''; ?>">
                            <label for="order-asc">
                                <i class="fas fa-times-circle"></i>
                                <?php _e('Oud naar nieuw', 'newheap'); ?>
                            </label>
                        </a>
                    </div>
                </div>

                <div class="col-md-9">
                    <h3 class="section-title mt-0">
                        <?php _e('Opgeslagen artikelen', 'newheap'); ?>
                    </h3>
                    <div class="nh-posts-loop-holder">
                        <?php if(!empty($favoriteArray)) {
                            $originalBlogId = get_current_blog_id();
                            foreach($favoriteArray as $favoriteKey) {
                            $id = explode(";", $favoriteKey)[0];
                            $blogId = explode(";", $favoriteKey)[1];
                            $type = explode(";", $favoriteKey)[2];

                            switch_to_blog($blogId);
                            $post = get_post($id);
                             ?>
                            <div class="favorite-holder delete-on-click" data-favorite-key="<?php echo $post->ID; ?>;<?=get_current_blog_id()?>;post" style="position:relative;">
                                <button class="delete-favorite is-favorite" data-favorite-click="<?php echo $post->ID; ?>;<?=get_current_blog_id()?>;post">
                                    <i class="fas fa-times-circle"></i>
                                </button>


                                <a href="<?php echo get_the_permalink($post->ID); ?>" class="dotted-bottom-black pb-3 mb-3 d-block post-item">
                                    <div class="row">
                                        <div class="col-md-3 relative">
                                            <div class="overflow-hidden" style="max-width: 100%;height: 100%;">
                                                <div class="image-holder">
                                                    <div class="image" style="background-image: url('<?php echo get_the_post_thumbnail_url($post->ID, 'large'); ?>');"></div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-9 my-2 theme-container theme-<?=$blogId?>">
                                            <?php if($originalBlogId == 1) { ?>
                                                <h5 style="margin-left: 15px;" class="p-2 mb-0 pt-3 pb-0 dot-before">
                                                    <?php if(!empty($parent)) { ?>
                                                        <?php echo get_blog_details(['blog_id' => $blogId])->blogname; ?>
                                                    <?php } else { ?>
                                                        <?php echo get_bloginfo( 'name' ); ?>
                                                    <?php } ?>
                                                </h5>
                                            <?php } ?>
                                            <h2 class="bold mb-3"><?php echo get_the_title($post->ID); ?></h2>
                                            <div class="bold">
                                                <span>
                                                    <?php echo get_the_date('d-m-Y', $post->ID); ?> |
                                                    <i class="far fa-clock"></i>
                                                    <?php echo \NewHeap\Theme\Helpers::get_read_time($post->ID); ?> |
                                                    <i class="fas fa-pen-nib"></i>
                                                    <?php echo get_the_author_meta('display_name', $post->post_author); ?>
                                                </span>
                                            </div>

                                            <p class="mt-2">
                                                <?=get_the_excerpt();?>
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php }
                            switch_to_blog($originalBlogId);
                        } else { ?>
                            <span>Geen favorieten gevonden, voeg eerst favorieten toe.</span>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <div class="col-md-12 text-center">
                U dient ingelogd te zijn om favorieten te kunnen zien.
            </div>
        <?php } ?>
    </div>

    <script>
        function removeFavoriteElement(postId) {
            jQuery('.favorite-holder[data-favorite-id=' + postId + ']').remove();
        }
    </script>
</section>
<?php get_footer();
