<?php
/**
 * Template Name: Alle elementen (test doeleinden)
 */

get_header(); ?>

<style>
    h1.mt-4 {
        color:#fff;
        width: 100%;
        background-color:black;
        padding:20px;
        position: relative;
    }
    h1.mt-4:before {
        width: 200vw;
        left: -100vw;
        top: 0;
        display: block;
        content: " ";
        background-color: #000;
        position: absolute;
        z-index: -1;
        height: 100%;
    }

    .description {
        background-color: #dedede;
        padding:20px;
        margin-top:20px;
    }
</style>
<div class="main">

    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-4">
                <h1 class="mt-4">Basis html attributen</h1>
            </div>
            <div class="col-md-12">
                <Br />
                <a href="#">a href link</a>
                <br />&nbsp;
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mt-4">
                <h1 class="mt-4">Knoppen</h1>
            </div>
            <div class="col-md-6">
                <br/>
                <a class="link-block-1" href="#">Link blok arrow down (.link-block-1)</a>
                <br />&nbsp;
            </div>
            <div class="col-md-6">
                <br/>
                <a class="link-block-2" href="#">Link blok arrow down (.link-block-2)</a>
                <br />&nbsp;
            </div>

            <div class="col-md-6">
                <a class="cta-1" href="#">Call to action (.cta-1)</a>
            </div>

            <div class="col-md-6">
                <a class="cta-2" href="#">Call to action (.cta-2)</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mt-4">
                <h1 class="mt-4">Titels</h1>
            </div>
            <div class="col-md-12">
                <div class="section-title">
                    Sectie titel (volle breedte) .section-title (in col-12)
                </div>
            </div>
            <div class="col-md-11">
                <div class="section-title">
                    Sectie titel (minder groot) .section-title (in col-*:not 12)
                </div>
            </div>
            <div class="col-md-12">
                <div class="alt-title">
                    Alternatieve titel (volle breedte) .alt-title (in col-12)
                </div>
            </div>
            <div class="col-md-11">
                <div class="alt-title">
                    Alternatieve titel (minder groot) .alt-title (in col-*:not 12)
                </div>
            </div>
            <div class="col-md-12">
                <br />
                <div class="sub-title">
                    Subtitel .sub-title
                </div>
            </div>
            <div class="col-md-12">
                <div class="description">
                    <strong>Example url's:</strong><br />
                    <a href="<?=home_url()?>/archief/"><?=home_url()?>/archief/</a>
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <h1 class="mt-4">Afbeeldingen</h1>
            </div>
            <div class="col-md-5">
                <small>>> (.bg-image-holder)</small>
                <div class="bg-image-holder">
                    <div class="bg-image-div" style="background-image: url(https://mechaman.staging.nhp.cloud/wp-content/uploads/sites/2/2021/09/H2Trac_uitgelicht2-768x431-1.jpg);"></div>
                    <img src="https://mechaman.staging.nhp.cloud/wp-content/uploads/sites/2/2021/09/H2Trac_uitgelicht2-768x431-1.jpg" width="100%" height="auto" style="display: none;">
                </div>
            </div>
            <div class="col-md-5">
                <small>>> Image holder (.bg-image-holder.hoverable)</small>
                <div class="bg-image-holder hoverable">
                    <div class="bg-image-div" style="background-image: url(https://mechaman.staging.nhp.cloud/wp-content/uploads/sites/2/2021/09/H2Trac_uitgelicht2-768x431-1.jpg);"></div>
                    <img src="https://mechaman.staging.nhp.cloud/wp-content/uploads/sites/2/2021/09/H2Trac_uitgelicht2-768x431-1.jpg" width="100%" height="auto" style="display: none;">
                </div>
            </div>
            <div class="col-md-12">
                <div class="description">
                    <strong>Uitleg:</strong><br />
                    - Om eigen aspect ratios te bepalen, geef aan bg-image-div een padding-top: xx%; mee<br />
                    - Om hoogte 100% te hebben, geef bg-image-holder h-100 mee en bg-image-div ook h-100<br />
                    - Responsive: Worden alle images padding-top: 56.25% gezet<br />
                    - Het is mogelijk een .premium-label toe te voegen (zie artikel listing)<br />
                    <br />
                    <strong>Example url's:</strong><br />
                    <a href="<?=home_url()?>/archief/"><?=home_url()?>/archief/</a><br />
                    <a href="<?=home_url()?>/"><?=home_url()?>/archief/</a>
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <h1 class="mt-4">Artikel listing</h1>
            </div>
            <div class="col-md-12">
                <a class="article-item" href="#">
                    <div class="row">
                        <div class="col-xl-3 col-md-4 relative">
                            <div class="bg-image-holder h-100">
                                <div class="bg-image-div h-100" style="background-image: url(https://mechaman.staging.nhp.cloud/wp-content/uploads/sites/2/2021/09/H2Trac_uitgelicht2-768x431-1.jpg);"></div>
                                <img src="https://mechaman.staging.nhp.cloud/wp-content/uploads/sites/2/2021/09/H2Trac_uitgelicht2-768x431-1.jpg" width="100%" height="auto" style="display: none;">
                                <div class="premium-label">
                                    <i class="far fa-gem"></i> Premium
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-9 col-md-8 my-2 theme-container theme-1">
                            <?php if(get_current_blog_id() == \NewHeap\Theme\Multisite\Multisite::SITE_MECHAMAN) { ?><h5 class="p-2 mb-0 pt-3 pb-0 dot-before">Mechaman</h5><?php } ?>
                            <h3 class="sub-title">Test 2 titel</h3>
                            <div class="meta-holder">
                                <div class="meta-date">08-07-2021</div>
                                <div class="meta-read-time">
                                    <i class="far fa-clock"></i>
                                    <span class="text">1 minuut</span>
                                </div>
                                <div class="meta-author">evanderbroek</div>
                            </div>
                            <p class="excerpt">xccxbnmcbv bbhjhjghjgh kop 1 kop 2 dgfdj nbn</p>
                        </div>
                    </div>
                </a>
                <a class="article-item" href="#">
                    <div class="row">
                        <div class="col-xl-3 col-md-4 relative">
                            <div class="bg-image-holder h-100">
                                <div class="bg-image-div h-100" style="background-image: url(https://mechaman.staging.nhp.cloud/wp-content/uploads/sites/2/2021/09/H2Trac_uitgelicht2-768x431-1.jpg);"></div>
                                <img src="https://mechaman.staging.nhp.cloud/wp-content/uploads/sites/2/2021/09/H2Trac_uitgelicht2-768x431-1.jpg" width="100%" height="auto" style="display: none;">
                            </div>
                        </div>
                        <div class="col-xl-9 col-md-8 my-2 theme-container theme-undefined">
                            <h3 class="sub-title">Test 1 titel</h3>
                            <div class="meta-holder">
                                <div class="meta-date">08-07-2021</div>
                                <div class="meta-read-time">
                                    <i class="far fa-clock"></i>
                                    <span class="text">1 minuut</span>
                                </div>
                                <div class="meta-author">evanderbroek</div>
                            </div>
                            <p class="excerpt">xccxbnmcbv bbhjhjghjgh kop 1 kop 2 dgfdj nbn</p>
                        </div>
                    </div>
                </a>
                <div class="description">
                    <strong>Uitleg:</strong><br />
                    - begint met a[href] met .article-item klasse<br />
                    - Titel hierin is gelijk aan .sub-title (zie 'Titels')<br />
                    - voor image zie 'Afbeeldingen' element<br />
                    - Op mechaman komt er ook nog een site bolletje + naam boven<br />
                    <br />
                    <strong>Example url's:</strong><br />
                    <a href="<?=home_url()?>/archief/"><?=home_url()?>/archief/</a><br />
                    <a href="<?=home_url()?>/artikel/category/trekkers/"><?=home_url()?>/artikel/category/trekkers/</a>
                    <a href="<?=home_url()?>/artikel/artikelen/?category=31&search="><?=home_url()?>/artikelen/?category=31&search=</a>
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <h1 class="mt-4">Auteur block</h1>
            </div>
            <div class="col-md-12">
                <section class="author my-4">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-8 offset-md-2">
                                <a href="https://mechaman.staging.nhp.cloud/tuin-en-park-techniek/artikel/author/evanderbroek/">
                                    <div class="row">
                                        <div class="col-md-4 mobile-up pr-0">
                                            <div class="bg-image-holder h-100">
                                                <div class="bg-image-div h-100" style="background-image: url(https://secure.gravatar.com/avatar/220752756030e4efc6c698531a622712?s=96&amp;d=mm&amp;r=g);"></div>
                                                <img src="https://secure.gravatar.com/avatar/220752756030e4efc6c698531a622712?s=96&amp;d=mm&amp;r=g" width="100%" height="auto" style="display: none;">
                                            </div>
                                        </div>

                                        <div class="col-md-8 bg-black text-white dotted-left p-5">
                                            <h3 class="bold mb-3">
                                                <i class="fas fa-pen-nib"></i>
                                                evanderbroek                                        </h3>

                                            <p class="mt-2">
                                                Ebo is geen journalist en kan alleen artikelen kopieren en plaatsen.                                        </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-md-12">
                <div class="description">
                    <strong>Example url's:</strong><br />
                    Op alle artikel pagina's
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <h1 class="mt-4">Info block (premium & author) - </h1>
            </div>
            <div class="col-md-12">
                <div class="info-block-holder">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-8 offset-md-2">
                                <div class="info-block">
                                    <div class="block-header">
                                        <div class="text-start">
                                            <span class="block-premium">
                                                <i class="far fa-gem"></i>
                                                <span>Premium</span>
                                            </span>
                                        </div>
                                        <div class="text-end">
                                            <span data-favorite-click="298;5;post" class="block-favorite">
                                                <i class="fas fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <span>Favoriet</span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="block-content">
                                        <div class="tax-holder">
                                            <h2>Algemeen </h2>
                                        </div>
                                        <h1>TEST 123</h1>
                                        <span class="post-meta-holder">
                                            <span class="date-holder">08-07-2021</span>
                                            <span class="read-time"> | 1 minuut </span>
                                            <span class="author"> | <i class="fas fa-pen-nib"></i>evanderbroek </span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="description">
                    <strong>Example url's:</strong><br />
                    Op premium artikel pagina's
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <h1 class="mt-4">Qoute (streamer)</h1>
            </div>
            <div class="col-md-12">
                <section class="wp-block-newheap-blocks-quote quote my-5"><span class="quote-holder">De Claas 920 Axion is op 22 juni APK-gekeurd</span></section>
            </div>
            <div class="col-md-12 mt-4">
                <h1 class="mt-4">Afbeelding + tekst</h1>
            </div>
            <div class="col-md-12">
                <section class="wp-block-newheap-blocks-one-col-image image-holder"><div class="row"><div class="col-md-12"><div class="image-holder" style="background-image:url(https://mechaman.staging.nhp.cloud/tuin-en-park-techniek/wp-content/uploads/sites/5/2021/06/video.png)"><img src="https://mechaman.staging.nhp.cloud/tuin-park-techniek/wp-content/uploads/sites/5/2021/06/video.png"></div><div class="image-title">Claas 920 Axion</div><p>De Claas 920 Axion die op 22 juni APK-gekeurd is, doet dienst als demo. Hoewel Broeder geen problemen verwachtte, worden toch wat kleine puntjes ontdekt.</p></div></div></section>
            </div>
            <div class="col-md-12 mt-4">
                <h1 class="mt-4">Verticale artikelen</h1>
            </div>
            <div class="col-md-12 single">
                <section class="postloop pt-5 mb-5">
                        <div class="nh-tile-holder">
                            <div class="nh-tile">
                                <a href="https://mechaman.staging.nhp.cloud/tuin-en-park-techniek/artikel/20210708/lemken-titan-18-met-geintegreerde-vorenpakker-2/" class="post-item block-link">
                                    <div class="bg-image-holder">
                                        <div class="bg-image-div" style="padding-top:60%;background-image: url(https://mechaman.staging.nhp.cloud/tuin-en-park-techniek/wp-content/uploads/sites/5/2021/07/Lemken-Titan-18-560x373-1.jpg);"></div>
                                        <img src="https://mechaman.staging.nhp.cloud/tuin-en-park-techniek/wp-content/uploads/sites/5/2021/07/Lemken-Titan-18-560x373-1.jpg" width="100%" height="auto" style="display: none;">
                                    </div>

                                    <div class="bg-black text-white dotted-top p-3">
                                        <h2 class="">Lemken Titan 18 met geïntegreerde vorenpakker</h2>

                                        <span class="bold">
                                        08-07-2021                                        |
                                        <i class="far fa-clock"></i>
                                        1 minuut                                    </span>

                                        <p>
                                            Lemken introduceerde in 2019 de halfgedragen Titan 18 met geïntegreerde vorenpakker. Deze 15,2 meter lange ploeg met 12 scharen was een aantal weken in Nederland. De capaciteit bedroeg zo’n 3 hectare per uur.                                </p>
                                    </div>
                                </a>
                            </div>
                            <div class="nh-tile">
                                <a href="https://mechaman.staging.nhp.cloud/tuin-en-park-techniek/artikel/20210708/eerste-trekker-apk-gekeurd-2/" class="post-item block-link">
                                    <div class="bg-image-holder">
                                        <div class="bg-image-div" style="padding-top:60%;background-image: url(https://mechaman.staging.nhp.cloud/tuin-en-park-techniek/wp-content/uploads/sites/5/2021/07/Claas920Axion-300x200-1.jpg);"></div>
                                        <img src="https://mechaman.staging.nhp.cloud/tuin-en-park-techniek/wp-content/uploads/sites/5/2021/07/Claas920Axion-300x200-1.jpg" width="100%" height="auto" style="display: none;">
                                    </div>

                                    <div class="bg-black text-white dotted-top p-3">
                                        <h2 class="">Eerste trekker APK gekeurd</h2>

                                        <span class="bold">
                                        08-07-2021                                        |
                                        <i class="far fa-clock"></i>
                                        4 minuten                                    </span>

                                        <p>
                                            Een Claas 920 Axion is de eerste trekker in Nederland die op een APK-rapport staat. Deze week vond de eerste keuring plaats bij Jabotech in Nijbroek (Gld.). De RDW spreekt van een mijlpaal.                                </p>
                                    </div>
                                </a>
                            </div>
                            <div class="nh-tile">
                                <a href="https://mechaman.staging.nhp.cloud/tuin-en-park-techniek/artikel/20210708/topcon-introduceert-nieuwe-ags-2-ontvanger-2/" class="post-item block-link">
                                    <div class="bg-image-holder">
                                        <div class="bg-image-div" style="padding-top:60%;background-image: url(https://mechaman.staging.nhp.cloud/tuin-en-park-techniek/wp-content/uploads/sites/5/2021/07/AGS-2_Topcon_4-560x400-1.jpg);"></div>
                                        <img src="https://mechaman.staging.nhp.cloud/tuin-en-park-techniek/wp-content/uploads/sites/5/2021/07/AGS-2_Topcon_4-560x400-1.jpg" width="100%" height="auto" style="display: none;">
                                        <div class="premium-label">
                                            <i class="far fa-gem"></i> Premium
                                        </div>
                                    </div>

                                    <div class="bg-black text-white dotted-top p-3">
                                        <h2 class="">Topcon introduceert nieuwe AGS-2 ontvanger</h2>

                                        <span class="bold">
                                        08-07-2021                                        |
                                        <i class="far fa-clock"></i>
                                        1 minuut                                    </span>

                                        <p>
                                            Topcon Agriculture presenteert twee nieuwe ontvangers. De AGM-1 en de AGS-2. Eerstgenoemde is ontwikkeld voor handmatige besturing, de AGS-2 voorziet in automatische besturing.                                </p>
                                    </div>
                                </a>
                            </div>
                            <div class="nh-tile">
                                <a href="https://mechaman.staging.nhp.cloud/tuin-en-park-techniek/artikel/20210708/linde-komt-met-vier-2-tot-35-tons-elektrische-heftrucks-2/" class="post-item block-link">
                                    <div class="bg-image-holder">
                                        <div class="bg-image-div" style="padding-top:60%;background-image: url(https://mechaman.staging.nhp.cloud/tuin-en-park-techniek/wp-content/uploads/sites/5/2021/07/Linde-X30_1-560x383-1.jpg);"></div>
                                        <img src="https://mechaman.staging.nhp.cloud/tuin-en-park-techniek/wp-content/uploads/sites/5/2021/07/Linde-X30_1-560x383-1.jpg" width="100%" height="auto" style="display: none;">
                                    </div>

                                    <div class="bg-black text-white dotted-top p-3">
                                        <h2 class="">Linde komt met vier 2 tot 3,5 tons elektrische heftrucks</h2>

                                        <span class="bold">
                                        08-07-2021                                        |
                                        <i class="far fa-clock"></i>
                                        2 minuten                                    </span>

                                        <p>
                                            Heftruckfabrikant Linde introduceert de X20, X25, X30 en X35 elektrische heftrucks met een hefvermogen van 2,0 tot 3,5 ton. In de basis zijn de elektrische heftrucks gelijk aan de H20-H35 heftrucks met verbrandingsmotor.                                </p>
                                    </div>
                                </a>
                            </div>
                    </div>
                </section>
            </div>
            <div class="col-md-12">
                <div class="description">
                    <strong>Example url's:</strong><br />
                    <a href="<?=home_url()?>/a"><?=home_url()?></a><br />
                    Op alle artikel pagina's (onder aan)
                </div>
            </div>

        </div>
        <div class="col-md-12 mt-4">
            <h1 class="mt-4">Vakbladarchief</h1>
        </div>
        <div class="col-md-12 single">
            <div class="magazines-archive">
                <div class="row">
                    <div class="magazine-year-title col-sm-12 col-md-12 mt-3" data-date="2020">
                        <h3 class="sub-title">
                            2020                                            </h3>
                    </div>
                    <div class="magazine-item col-lg-3 col-md-4 col-6 mb-3" data-date="2020">
                        <a href="https://mechaman.staging.nhp.cloud/artikel/magazine/test/">
                            <img src="https://mechaman.staging.nhp.cloud/wp-content/uploads/laverda-web-eima-1021-1024x685.jpg" width="100%" height="auto">
                            <div class="bg-black text-white dotted-top p-3">
                                <h3 class="uppercase">Handige software</h3>
                                <div class="date-holder">2020-1</div>
                                <div class="number">Nummer 1</div>
                                <div class="year">Jaargang 71</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="description">
                <strong>Example url's:</strong><br />
                <a href="<?=home_url()?>/dossiers/"><?=home_url()?>/dossiers/</a><br />
            </div>
        </div>

    </div>


    <br />&nbsp;
    <br />&nbsp;
    <br />&nbsp;

    </div>
</div>

<?php get_footer();
