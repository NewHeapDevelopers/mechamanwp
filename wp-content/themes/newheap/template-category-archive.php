<?php
/**
 * Template Name: Categorie archief
 */

// Filters
$categories = get_terms([
    'taxonomy' => 'category',
    'meta_query' => array(
        'relation' => 'OR',
        array(
            'key'     => 'do_not_show',
            'value'   => '0',
            'compare' => '=',
        ),
        array(
            'key' => 'do_not_show',
            'compare' => 'NOT EXISTS'
        ),
    )
]);
$dossiers = get_terms([
    'taxonomy' => 'dossier',
    'number' => 0,
    'meta_query' => array(
        'relation' => 'OR',
        array(
            'key'     => 'do_not_show',
            'value'   => '0',
            'compare' => '=',
        ),
        array(
            'key' => 'do_not_show',
            'compare' => 'NOT EXISTS'
        ),
    )
]);

$category_id = \NewHeap\Theme\Taxonomy\Taxonomy::get_archive_page_category_id(get_the_ID());

$args = [
    'post_type' => 'post',

];

if (!empty($category_id)) {
    $args['tax_query'] = [
        'relation' => 'OR',
        [
            'taxonomy' => 'category',
            'field' => 'term_id',
            'terms' => $category_id,
        ],
        [
            'taxonomy' => 'category',
            'field' => 'term_id',
            'terms' => $category_id,
        ]
    ];
}

$query = new WP_Query($args);

get_header(); ?>
    <div id="filter-page" class="content-holder  pt-4">
        <div class="container nh-rest-required">
            <!-- Style tag can be deleted after sass is added  -->
            <style>
                /*button.btn-filter.filter-active {*/
                /*    background-color: green;*/
                /*}*/

                /*!** Pagination *!*/
                /*.posts-pagination button.disabled {*/
                /*    opacity: .4;*/
                /*    pointer-events: none;*/
                /*}*/
                /*.posts-pagination button.btn-current-page {*/
                /*    background-color: #333;*/
                /*}*/
                /*.posts-pagination ul {*/
                /*    margin: 0;*/
                /*    padding: 0;*/
                /*}*/
                /*.posts-pagination ul li {*/
                /*    display: inline-block;*/
                /*    list-style-type: none;*/
                /*}*/
            </style>

            <div class="row">
                <div class="col-lg-3">
                    <div class="toggle-mobile-filters">
                        <button class="btn-open-mobile-filters">
	                        <?php _e('Filters', 'newheap'); ?>
                        </button>
                    </div>
                    <div class="filters-holder sidebar">
                        <button class="btn-close-mobile-filters">
		                    &times;
                        </button>
                        <div class="overflow-holder">
                            <!--todo-->
                            <div class="filter-holder sort" data-filter-name="sort">
                                <h3 class="alt-title mt-0"><?php _e('Sorteer', 'newheap'); ?></h3>
                                <ul class="post-filters">
                                    <li>
                                        <button class="btn-filter filter-active active" data-value="DESC">
                                            <span class="checkbox"></span>
                                            <?php _e ('Nieuw naar oud', 'newheap'); ?>
                                        </button>
                                    </li>
                                    <li data-type="not_premium">
                                        <button class="btn-filter" data-value="ASC">
                                            <span class="checkbox"></span>
                                            <?php _e('Oud naar nieuw', 'newheap'); ?>
                                        </button>
                                    </li>
                                </ul>
                            </div>

                            <h3 class="alt-title"><?php _e('Filters', 'newheap'); ?></h3>

                            <div class="filter-holder" data-filter-name="clear" style="display: none">
                                <ul class="post-filters">
                                    <li>
                                        <button>
                                            <span class="checkbox"></span>
                                            <?php _e('Wis alle filters', 'newheap'); ?>
                                        </button>
                                    </li>
                                </ul>
                            </div>


                            <div class="filter-holder" data-filter-name="premium">
                                <h3 class="sub-title"><?php _e('Premium', 'newheap'); ?></h3>
                                <ul class="post-filters">
                                    <li>
                                        <button class="btn-filter" data-value="1">
                                            <span class="checkbox"></span>
                                            <?php _e('Wel premium', 'newheap'); ?>
                                        </button>
                                    </li>
                                    <li data-type="not_premium">
                                        <button class="btn-filter" data-value="0">
                                            <span class="checkbox"></span>
                                            <?php _e('Niet premium', 'newheap'); ?>
                                        </button>
                                    </li>
                                </ul>
                            </div>

                            <div class="filter-holder" data-filter-name="taxonomy" data-taxonomy-name="category">
                                <h3 class="sub-title"><?php _e('Categorie', 'newheap'); ?></h3>
                                <ul class="post-filters">
                                    <?php foreach ($categories as $category) : ?>
                                        <li>
                                            <button class="btn-filter" data-term-id="<?php echo $category->term_id; ?>">
                                                <span class="checkbox"></span>
                                                <?php echo $category->name; ?>
                                            </button>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>

                            <div class="filter-holder" data-filter-name="taxonomy" data-taxonomy-name="dossier">
                                <h3 class="sub-title"><?php _e('Dossier', 'newheap'); ?></h3>
                                <ul class="post-filters">
                                    <?php foreach ($dossiers as $dossier) : ?>
                                        <li>
                                            <button class="btn-filter" data-term-id="<?php echo $dossier->term_id; ?>">
                                                <span class="checkbox"></span>
                                                <?php echo $dossier->name; ?>
                                            </button>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-9">
                    <?php if(!isset($_GET['search']) or empty($_GET['search'])) { ?>
                        <div class="dossiers-holder mb-5">
                            <h3 class="section-title mt-0"><?php _e('Dossiers', 'newheap'); ?></h3>

                            <div class="dossiers-loop">
                                <div class="row"></div>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="posts-holder">
                        <h3 class="section-title"><?php _e('Artikelen', 'newheap'); ?></h3>

                        <div class="posts-loop">
                        </div>

                        <?php if ($query->max_num_pages > 1) : ?>
                            <div class="posts-pagination">
                                <script>
                                    AppConfig.pagination = {
                                        maxNumPages: <?php echo $query->max_num_pages; ?>
                                    };
                                </script>

                                <ul>
                                    <li></li>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>

                    <div class="template-holder d-none">
                        <div class="dossier-item col-md-3">
                            <a href="#">
                                <div class="dossiers-image-holder"></div>

                                <div class="bg-black text-white dotted-top p-3">
                                    <h3 class="uppercase"></h3>
                                    <div class="date-holder"></div>
                                    <div class="number"></div>
                                    <div class="year"></div>
                                </div>
                            </a>
                        </div>

                        <div class="col-md-12 google-ad-holder ad-between-posts-holder text-center">
                            <div class="dotted-bottom-black pt-5 pb-5 mb-3">
                                <div class="ad-between-posts">
                                    <?=get_field('advertisement_archive', 'option')?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer();

