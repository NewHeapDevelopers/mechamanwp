<?php
/**
 * Template Name: Categorieen archief
 */

$category_id = \NewHeap\Theme\Taxonomy\Taxonomy::get_archive_page_category_id( get_the_ID() );


$terms = get_terms( [
	'taxonomy' => 'category',
	'meta_query' => array(
		'relation' => 'OR',
		array(
			'key'     => 'do_not_show',
			'value'   => '0',
			'compare' => '=',
		),
		array(
			'key' => 'do_not_show',
			'compare' => 'NOT EXISTS'
		),
	)
] );

get_header();
if ( ! empty( $terms ) ) : ?>
    <div class="content-holder pt-4">
        <div class="dossiers-grid">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-10 offset-md-1">
                        <h1 class="section-title">
                            <?php _e( 'Categorie&eumln', 'newheap' ); ?>
                        </h1>

                        <div class="row">
                            <?php foreach ( $terms as $term ) : ?>
	                            <?php
	                            $category_image = get_field('term_image', $term);

	                            if (!empty($category_image)) {
		                            $category_image = get_field('term_image', $term)['sizes']['medium'];
	                            } else {
		                            $category_image = get_field('placeholder_afbeelding', 'option')['sizes']['medium'];
	                            }
	                            ?>

                                <div class="col-sm-12 col-md-4">
                                    <a href="<?php echo get_term_link($term, 'dossier'); ?>" class="dossier-item ">
                                        <img src="<?php echo $category_image; ?>" width="100%" height="auto"/>
                                        <div class="meta-holder dotted-top">
                                            <h3 class="uppercase" title="<?php echo $term->name; ?>"><?php echo $term->name; ?></h3>
                                        </div>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;

get_footer();
