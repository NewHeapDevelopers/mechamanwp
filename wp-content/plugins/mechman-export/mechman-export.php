<?php
/**
 * Plugin Name: Mechman export
 * Plugin URI: https://www.newheap.com
 * Description: Exports data from mechman in usable format
 * Version: 1.0.0
 * Author: NewHeap
 * Author URI: https://www.newheap.com
 * Developer: Marcel van Nuil
 * Developer URI: https://www.newheap.com
 * Text Domain: newheap
 * Domain Path: /languages
 */

// Prevent direct access
if (!defined('ABSPATH')) {
    exit;
}

require_once (__DIR__).'/MechamanExport.php';
require_once (__DIR__).'/MechamanImport.php';

// Instantiate the plugin
new \Newheap\Plugins\MechamanExport\MechamanExport();
new \Newheap\Plugins\MechamanExport\MechamanImport();
