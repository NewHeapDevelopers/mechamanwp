<?php
namespace Newheap\Plugins\MechamanExport;

use threewp_broadcast\meta_box\data;

class MechamanImport
{
    public function __construct()
    {
        add_action( 'wp_loaded', [$this, 'export'] );
    }

    public function export()
    {
        if(isset($_GET['import_media'])) {
            $this->media($_GET['startFrom']);
        }

        if(isset($_GET['correct_media'])) {
            $this->correctMedia();
        }

        if(isset($_GET['import_categories'])) {
            $this->categories($_GET['startFrom']);
        }

        if(isset($_GET['import_tags'])) {
            $this->tags($_GET['startFrom']);
        }

        if(isset($_GET['import_users'])) {
            $this->users($_GET['startFrom']);
        }

        if(isset($_GET['import_posts'])) {
            $this->posts($_GET['startFrom']);
        }
        if(isset($_GET['correct_posts'])) {
            $this->correctPosts();
        }
        if(isset($_GET['correct_categories'])) {
            $this->correctCategories();
        }
        if(isset($_GET['correct_tags'])) {
            $this->correctTags();
        }
        if(isset($_GET['get_redirects'])) {
            $this->getRedirects();
        }
        if(isset($_GET['correct_posts_images'])) {
            $this->correctPostImages();
        }
    }

    /** Import media from the export file */
    private function media($startFrom = 1) {
        /**
         * Warning: For optimalization reasons,
         * we not inserting the files the normal way,
         * make sure you upload the files over FTP,
         * we only create the post objects in the database here, with a reference to the image
         */

        if(!file_exists((__DIR__).'/export/'.$this->NewBlogIdToblogId(get_current_blog_id()).'/media.json')) {
            return;
        }

        $lineNumber = 1;

        $handle = fopen((__DIR__).'/export/'.$this->NewBlogIdToblogId(get_current_blog_id()).'/media.json', "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                if($lineNumber < $startFrom) { $lineNumber++; continue; }

                $object = json_decode($line);

                //find post on _old_id skip if already exists
                $args = array(
                    'post_type'   => 'attachment',
                    'post_status' => 'inherit',
                    'meta_query'  => array(
                        array(
                            'key'     => '_old_id',
                            'value'   => $object->object->ID
                        )
                    )
                );

                $imageQuery = new \WP_Query($args);
                $results = $imageQuery->get_posts();


                if($lineNumber >= 250) { die('max 250 imported'); }

                if(!empty($results)) {
                    $lineNumber++;
                    continue; //rest of image already exists, skip (todo: maybe update in future?)
                }

                $newImage = array(
                    'post_type'    => 'attachment',
                    'post_mime_type'    => $object->object->post_mime_type,
                    'guid'    => $object->object->guid,
                    'post_title'    => $object->object->post_title,
                    'post_status'   => $object->object->post_status,
                    'post_date'   => $object->object->post_date,
                    'post_name'   => $object->object->post_name,
                    'post_modified'   => $object->object->post_modified,
                    'meta_input' => [
                        '_wp_attached_file' => $object->meta->_wp_attached_file[0], //file name + path
                        '_wp_attachment_metadata' => $object->meta->_wp_attached_file[0], //serialized array with defined sizes (a:5:{s:5:"width";i:709;s:6:"height";i:473;s:4:"file";s:30:"LEMKEN-Soltair23Optidisc25.jpg";s:5:"sizes";a:9:{s:6:"medium";a:4:{s:4:"file";s:38:"LEMKEN-Soltair23Optidisc25-300x200.jpg)
                        '_old_id' => $object->object->ID,
                        '_old_author_id' => $object->object->post_author,
                        '_old_parent_id' => $object->object->post_parent,
                    ]
                );



                $post = wp_insert_post($newImage);

                if(empty($post) or !is_int($post)) {
                    if($post === 0) {
                        die('something broke, could not make media post on line number: ' . $lineNumber . ", name: ".$object->object->post_title."<br /> some values incorrect, wp returns 0");
                    }else {
                        die('something broke, could not make media post on line number: ' . $lineNumber . ", name: ".$object->object->post_title."<br />WP_ERROR.: " . $post->get_error_message());
                    }
                }

                $lineNumber++;
            }


            fclose($handle);
        }

        echo "$lineNumber media items imported";

        //todo: in the end we have to correct parent relations (to posts) post_parent
    }



    private function correctMedia() {

        /** here we correct the authors */
        $allImagesArgs = array(
            'post_type'      => 'attachment',
            'post_mime_type' => 'image',
            'post_status'    => 'inherit',
            'posts_per_page' => -1,
        );

        $allImages = new \WP_Query( $allImagesArgs );

        while ( $allImages->have_posts() ) {
            $allImages->the_post();
            $object = get_post();

            $oldAuthor = get_post_meta($object->ID, '_old_author_id', true);

            if(!empty($oldAuthor) and $oldAuthor !== false) {
                $users = get_users(array(
                    'meta_key' => '_old_id',
                    'meta_value' => $oldAuthor,
                    'blog_id' => 0
                ));

                if(!empty($users)) {
                    wp_update_post(array(
                        'ID' => $object->ID,
                        'post_author' => $users[0]->ID,
                    ));
                }
            }
        }


    }

    /** Import categories from the export file */
    private function categories($startFrom = 1) {

        $lineNumber = 1;

        if(!file_exists((__DIR__).'/export/'.$this->NewBlogIdToblogId(get_current_blog_id()).'/categories.json')) {
            return;
        }

        $handle = fopen((__DIR__).'/export/'.$this->NewBlogIdToblogId(get_current_blog_id()).'/categories.json', "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                if($lineNumber < $startFrom) { $lineNumber++; continue; }

                $object = json_decode($line);

                $args = [
                    'description' => $object->object->description,
                    'slug'        => $object->object->slug
                ];

               $newObject = wp_insert_term($object->object->name, 'category', $args);

               if(is_object($newObject)) {
                   die('something broke, could not make term on line number: ' . $lineNumber . ", name: ".$object->object->name."<br />WP_ERROR.: " . $newObject->get_error_message());
               }

               add_term_meta($newObject['term_id'], '_old_id', $object->object->term_id);
               if(!empty($object->object->parent)) { add_term_meta($newObject['term_id'], '_old_parent', $object->object->parent); }

               $lineNumber++;
            }

            fclose($handle);
            echo($lineNumber.' categories imported, starting corrections');
        }


        /**
         * Correct categorie parent-child relations based on the old parent and child id's.
         */
        $parentCategoriesCorrections = get_categories(
            array(
                'hide_empty' => '0',
                'meta_query' => array(
                    array(
                        'key'     => '_old_parent',
                        'compare' => 'EXISTS'
                    )
                )
            )
        );

        foreach($parentCategoriesCorrections as $category) {

            $meta = get_term_meta($category->term_id);

            $args = array(
                'hide_empty' => false, // also retrieve terms which are not used yet
                'meta_query' => array(
                    array(
                        'key'       => '_old_id',
                        'value'     => $meta['_old_parent'][0],
                        'compare'   => '=='
                    )
                ),
                'taxonomy'  => 'category',
            );

            $terms = get_terms( $args );

            if (!empty($terms)) {
                wp_update_term($category->term_id, 'category', ['parent' => $terms[0]->term_id]);
            } else {
                die('Could not find _old_id ' . $meta['_old_parent'][0]. " while correcting child-parents.");
            }

        }

        die();
    }

    /** Import tags from the export file */
    private function tags($startFrom = 1) {

        $lineNumber = 1;

        if(!file_exists((__DIR__).'/export/'.$this->NewBlogIdToblogId(get_current_blog_id()).'/tags.json')) {
            return;
        }

        $handle = fopen((__DIR__).'/export/'.$this->NewBlogIdToblogId(get_current_blog_id()).'/tags.json', "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                if($lineNumber < $startFrom) { $lineNumber++; continue; }

                $object = json_decode($line);

                $args = [
                    'description' => $object->object->description,
                    'slug'        => $object->object->slug
                ];

                $newObject = wp_insert_term($object->object->name, 'post_tag', $args);

                if(is_object($newObject)) {
                    die('something broke, could not make term on line number: ' . $lineNumber . ", name: ".$object->object->name."<br />WP_ERROR.: " . $newObject->get_error_message());
                }

                add_term_meta($newObject['term_id'], '_old_id', $object->object->term_id);
                if(!empty($object->object->parent)) { add_term_meta($newObject['term_id'], '_old_parent', $object->object->parent); }

                $lineNumber++;
            }

            fclose($handle);
            echo($lineNumber.' tags imported');
        }


    }


    /** Import users from the export file */
    private function users($startFrom = 1) {

        $lineNumber = 1;

        if(!file_exists((__DIR__).'/export/'.$this->NewBlogIdToblogId(get_current_blog_id()).'/users.json')) {
            return;
        }

        $handle = fopen((__DIR__).'/export/'.$this->NewBlogIdToblogId(get_current_blog_id()).'/users.json', "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                if($lineNumber < $startFrom) { $lineNumber++; continue; }


                $object = json_decode($line);

                $user = get_users(array(
                    'meta_key' => '_old_id',
                    'meta_value' => $object->object->data->ID,
                    'blog_id' => 0
                ));


                $user_data = array(
                    'user_login' => $object->object->data->user_login,
                    'user_pass' => 'M#_amkkwdawd!@Kef3',
                    'user_email' => $object->object->data->user_email,
                    'user_registered' => $object->object->data->user_registered,
                    'first_name' => $object->meta->first_name[0],
                    'last_name' => $object->meta->last_name[0],
                    'display_name' => $object->object->data->display_name,
                    'role' => 'subscriber'
                );
                global $wpdb;

                if(!empty($user)) {
                    $user_data['ID'] = $user[0]->ID;

                    add_user_meta($user[0]->ID, 'subscription_lm', $object->meta->wp_2_abonneenummer[0]);
                    add_user_meta($user[0]->ID, 'subscription_vt', $object->meta->wp_3_abonneenummer[0]);
                    add_user_meta($user[0]->ID, 'subscription_tpt', $object->meta->wp_4_abonneenummer[0]);
                    add_user_meta($user[0]->ID, 'zipcode', $object->meta->wp_0_postcode[0]);

                    $wpdb->update(
                        $wpdb->users,
                        array( 'user_pass' => $object->object->data->user_pass ),
                        array( 'ID' => $user[0]->ID )
                    );
                } else {
                    $user_id = wp_insert_user($user_data);

                    if(!is_int($user_id)) {
                        die('something broke, could not add user on line item: ' . $lineNumber . ", name: ".$object->object->data->user_login."<br />WP_ERROR.: " . $user_id->get_error_message());
                    }

                    add_user_meta($user_id, '_old_id', $object->object->data->ID);
                    add_user_meta($user_id, 'subscription_lm', $object->meta->wp_2_abonneenummer[0]);
                    add_user_meta($user_id, 'subscription_vt', $object->meta->wp_3_abonneenummer[0]);
                    add_user_meta($user_id, 'subscription_tpt', $object->meta->wp_4_abonneenummer[0]);
                    add_user_meta($user_id, 'zipcode', $object->meta->wp_0_postcode[0]);


                    $wpdb->update(
                        $wpdb->users,
                        array( 'user_pass' => $object->object->data->user_pass ),
                        array( 'ID' => $user_id )
                    );
                }

                $lineNumber++;
            }

            fclose($handle);
            echo($lineNumber.' users imported');
        }


    }


    /** Import posts from the export file */
    private function posts($startFrom = 1) {
        if(!file_exists((__DIR__).'/export/'.$this->NewBlogIdToblogId(get_current_blog_id()).'/posts.json')) {
            return;
        }

        $lineNumber = 1;

        $handle = fopen((__DIR__).'/export/'.$this->NewBlogIdToblogId(get_current_blog_id()).'/posts.json', "r");
        if ($handle) {
            /**
             * here we create an array of old category id's to new ones and old user id's to new ones
             */
            $oldUserNewId = [];
            $users = get_users(['blog_id' => 0 ]);

            foreach($users as $user) {
                $meta = get_user_meta($user->ID);
                if(isset($meta['_old_id']) and !empty($meta['_old_id'])) {
                    $oldUserNewId[$meta['_old_id'][0]] = $user->ID;
                }
            }

            /**
            $oldCategoriesNewId = [];
            $categories = get_categories(['hide_empty' => false]);

            foreach($categories as $category) {
                $oldId = get_term_meta($category->term_id, '_old_id', true);
                if(!empty($oldId)) {
                    $oldCategoriesNewId[$oldId] = $category->term_id;
                }
            }**/

            $oldMediaNewId = [];

            $allImagesArgs = array(
                'post_type'      => 'attachment',
                'post_status'    => 'inherit',
                'posts_per_page' => -1,
            );

            $allImages = new \WP_Query( $allImagesArgs );

            while ( $allImages->have_posts() ) {
                $allImages->the_post();
                $object = get_post();
                $oldId = get_post_meta($object->ID, '_old_id', true);
                if(!empty($oldId)) {
                    $oldMediaNewId[$oldId] = $object->ID;
                }
            }

            while (($line = fgets($handle)) !== false) {
                if($lineNumber < $startFrom) { $lineNumber++; continue; }

                $object = json_decode($line);

                if(!empty($object->parents)) {
                    $lineNumber++;
                    //this one has a parent, so we are gonna create it in another blog id
                    continue;
                }

                //find post on _old_id skip if already exists
                $args = array(
                    'blog_id' => get_current_blog_id(),
                    'post_type'   => $object->object->post_type,
                    'meta_query'  => array(
                        array(
                            'key'     => '_old_id',
                            'value'   => $object->object->ID
                        )
                    )
                );

                $postQuery = new \WP_Query($args);
                $results = $postQuery->get_posts();


                if(!empty($results)) {
                    $lineNumber++;
                    continue;
                }

                $newObject = array(
                    'post_type'    => $object->object->post_type,
                    'post_mime_type'    => $object->object->post_mime_type,
                    'guid'    => $object->object->guid,
                    'post_title'    => $object->object->post_title,
                    'post_content'  => $object->object->post_content,
                    'post_status'   => $object->object->post_status,
                    'post_excerpt'   => $object->object->post_excerpt,
                    'post_author'   => $oldUserNewId[$object->object->post_author],
                    'post_date'   => $object->object->post_date,
                    'comment_status' =>  $object->object->comment_status,
                    'post_name'   => $object->object->post_name,
                    'post_modified'   => $object->object->post_modified,
                    'menu_order' => $object->object->menu_order,
                    'meta_input' => [
                        '_old_id' => $object->object->ID,
                        '_old_author_id' => $object->object->post_author,
                        '_old_parent_id' => $object->object->post_parent,
                        '_old_thumbnail_id' => $object->meta->_thumbnail_id[0],
                        '_thumbnail_id' => $oldMediaNewId[$object->meta->_thumbnail_id[0]],
                        '_old_url' => $object->permalink
                    ]
                );


                $newPost = wp_insert_post($newObject);

                if(!is_int($newPost)) {
                    die('something broke, could not add post on line item: ' . $lineNumber . ", title: ".$object->object->post_title."<br />WP_ERROR.: " . $newPost->get_error_message());
                }

                if(is_object($object->childs)) {
                    $blogId = array_keys(get_object_vars($object->childs))[0];
                    \ThreeWP_Broadcast()->api()->broadcast_children($newPost, [$this->blogIdToNewBlogId($blogId)]);
                }

                /**
                 * Changed this to correct_tags and correct_categories
                $addCategories = [];
                foreach($object->categories as $category) {
                    $addCategories[] = $oldCategoriesNewId[$category->term_id];
                }

                 wp_set_post_categories($newPost, $addCategories);
                 */

                $lineNumber++;
            }


            fclose($handle);
        }

        echo "$lineNumber media items imported";

        //todo: correct thumbnail (both ways)
        //todo: correct parent id? Not necessary because not used in posts, but as asurance in next versions would be nice
    }

    private function correctTags() {
        $lineNumber = 1;

        if(!file_exists((__DIR__).'/export/'.$this->NewBlogIdToblogId(get_current_blog_id()).'/posts.json')) {
            return;
        }

        $handle = fopen((__DIR__).'/export/'.$this->NewBlogIdToblogId(get_current_blog_id()).'/posts.json', "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {

                $object = json_decode($line);

                //find post on _old_id skip if already exists
                $args = array(
                    'blog_id' => get_current_blog_id(),
                    'post_type'  => $object->object->post_type,
                    'name' => $object->object->post_name
                );

                $postQuery = new \WP_Query($args);
                $results = $postQuery->get_posts();

                if(!empty($results)) {
                    if(!empty($object->tags)) {
                        $tagsList = [];
                        foreach($object->tags as $tag) {
                            $tagsList[] = $tag->name;
                        }

                        wp_set_object_terms( $results[0]->ID, $tagsList, 'post_tag', false );
                    }
                }

                $lineNumber++;
            }

            fclose($handle);
            echo($lineNumber.' amount of posts, tags were added');
        }

    }

    private function correctCategories() {
        $lineNumber = 1;

        if(!file_exists((__DIR__).'/export/'.$this->NewBlogIdToblogId(get_current_blog_id()).'/posts.json')) {
            return;
        }

        $handle = fopen((__DIR__).'/export/'.$this->NewBlogIdToblogId(get_current_blog_id()).'/posts.json', "r");
        $count = 1;
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $count++;
                if($count > 75) { die('max 75 set'); }

                $object = json_decode($line);

                //find post on _old_id skip if already exists
                $args = array(
                    'blog_id' => get_current_blog_id(),
                    'post_type'  => $object->object->post_type,
                    'name' => $object->object->post_name
                );

                $postQuery = new \WP_Query($args);
                $results = $postQuery->get_posts();


                if(!empty($results)) {

                    if(!empty($object->categories)) {
                        $categoryList = [];
                        foreach($object->categories as $category) {
                            $categoryList[] = $category->name;
                        }

                        wp_set_object_terms( $results[0]->ID, $categoryList, 'category', false );
                    }

                    //If in category Foto add to soort Fotoreportages
                    //If in category Podcast add to Podcasts
                    //If in category Video add to Video’s
                    //Otherwise add to artikelen
                    if(in_array('Foto', $categoryList)) {
                        wp_set_object_terms( $results[0]->ID, 'Fotoreportages', 'artikel_type', false );
                    } elseif(in_array('Podcast', $categoryList)) {
                        wp_set_object_terms( $results[0]->ID, 'Podcasts', 'artikel_type', false );
                    } elseif(in_array('Video', $categoryList)) {
                        wp_set_object_terms( $results[0]->ID, 'Video’s', 'artikel_type', false );
                    } elseif(strstr(strtolower($results[0]->post_title), 'video')) {
                        wp_set_object_terms($results[0]->ID, 'Video’s', 'artikel_type', false);
                    } elseif(strstr(strtolower($results[0]->post_content), 'https://w.soundcloud.com')) {
                        wp_set_object_terms($results[0]->ID, 'Podcasts', 'artikel_type', false);
                    } elseif(strstr(strtolower($results[0]->post_content), 'https://www.youtube.com/embed')) {
                        wp_set_object_terms($results[0]->ID, 'Video’s', 'artikel_type', false);
                    } else {
                        //if not in other soort then add to artikelen
                        $soort = get_the_terms($results[0]->ID, 'artikel_type');
                        if(empty($soort)) {
                            wp_set_object_terms( $results[0]->ID, 'Artikelen', 'artikel_type', false );
                        }
                    }


                }

                $lineNumber++;
            }

            fclose($handle);
            echo($lineNumber.' amount of posts, tags were added');
        }

    }

    private function correctPostImages() {

        //Only correct posts with _old_id tag
        $allPostsArgs = array(
            'post_type' => 'post',
            'posts_per_page' => -1,
            'meta_query'  => array(
                array(
                    'key'     => '_old_id',
                    'compare'   => 'EXISTS'
                )
            )
        );

        $query = new \WP_Query( $allPostsArgs );

        while ( $query->have_posts() ) {
            $query->the_post();
            $object = get_post();

            /** Here we change all old image url's to new ones */
            preg_match_all(
                '/<img (.*?)>/',
                $object->post_content,
                $matches,
                PREG_SET_ORDER
            );



            foreach($matches as $key => $image) {
                $image = $image[0];

                if(strstr($image, '/files/'))
                {
                    $oldId = null;
                    $oldUrl = null;
                    if (preg_match('/wp-image-(.*?) /', $image, $match) == 1) {
                        $oldId = str_replace("\"", "", $match[1]);
                    }
                    if (preg_match('/src="(.*?)"/', $image, $match) == 1) {
                        $oldUrl = $match[1];
                    }

                    if(!empty($oldId) and !empty($oldUrl)) {

                        $args = array(
                            'post_type'   => 'attachment',
                            'post_status' => 'inherit',
                            'meta_query'  => array(
                                array(
                                    'key'     => '_old_id',
                                    'value'   => $oldId
                                )
                            )
                        );

                        $imageQuery = new \WP_Query($args);
                        $results = $imageQuery->get_posts();

                        if(!empty($results)) {
                            $newImageId = $results[0]->ID;
                            $newImageUrl = wp_get_attachment_image_url($results[0]->ID, 'large');

                            $newContentForShortcode = str_replace("wp-image-".$oldId, "wp-image-".$newImageId, $image);
                            $newContentForShortcode = str_replace($oldUrl, $newImageUrl, $newContentForShortcode);

                            $object->post_content = str_replace($image, $newContentForShortcode, $object->post_content);

                            wp_update_post(array(
                                'post_content' => $object->post_content,
                            ));
                        }
                    }
                }

/**
                $oldId = null;
                $oldUrl = null;
                if (preg_match('/wp-image-(.*?) /', $image, $match) == 1) {
                    $oldId = str_replace("\"", "", $match[1]);
                }
                if (preg_match('/src="(.*?)"/', $image, $match) == 1) {
                    $oldUrl = $match[1];
                }

                if(!empty($oldId) and !empty($oldUrl)) {

                    $args = array(
                        'post_type'   => 'attachment',
                        'post_status' => 'inherit',
                        'meta_query'  => array(
                            array(
                                'key'     => '_old_id',
                                'value'   => $oldId
                            )
                        )
                    );

                    $imageQuery = new \WP_Query($args);
                    $results = $imageQuery->get_posts();

                    if(!empty($results)) {
                        $newImageId = $results[0]->ID;
                        $newImageUrl = wp_get_attachment_image_url($results[0]->ID, 'large');

                        //if post thumbnail id is the same as the last image in the post, remove image

                        if(get_post_thumbnail_id() == $newImageId) {
                            if(count($matches) === ($key+1)) {
                                $object->post_content = str_replace($image, "", $object->post_content);
                            }
                        } else {
                            $newContentForShortcode = str_replace("wp-image-".$oldId, "wp-image-".$newImageId, $image);
                            $newContentForShortcode = str_replace($oldUrl, $newImageUrl, $newContentForShortcode);

                            $object->post_content = str_replace($image, $newContentForShortcode, $object->post_content);
                        }


                    } else {
                        //Still remove image if thumbnail id is the same as new image id (test for second time running of this command)
                        if(get_post_thumbnail_id() == $oldId) {
                            if(count($matches) === ($key+1)) {
                                $object->post_content = str_replace($image, "", $object->post_content);
                            }
                        }
                        echo('could not convert image for post ' . $object->ID);
                    }
                }**/
            }
        }
    }

    private function correctPosts() {

        /** Here we fix categories podcast and video, those 2 must become the artikel_type podcasts and videos */
        /**
        $allPostsArgs = array(
            'post_type'      => 'post',
            'posts_per_page' => -1,
            'cat' => get_category_by_slug('podcast')->term_id
        );

        $query = new \WP_Query( $allPostsArgs );

        while ( $query->have_posts() ) {
            $query->the_post();
            wp_set_object_terms(get_post()->ID, 'podcasts', 'artikel_type');
        }

        //Only correct posts with _old_id tag
        $allPostsArgs = array(
            'post_type'      => 'post',
            'posts_per_page' => -1,
            'cat' => get_category_by_slug('video')->term_id,

        );

        $query = new \WP_Query( $allPostsArgs );

        while ( $query->have_posts() ) {
            $query->the_post();
            wp_set_object_terms(get_post()->ID, 'videos', 'artikel_type');
        }**/


        /* Here we loop through all posts */

        //Only correct posts with _old_id tag
        $allPostsArgs = array(
            'post_type'      => 'post',
            'posts_per_page' => -1,
            'meta_query'  => array(
                array(
                    'key'     => '_old_id',
                    'compare'   => 'EXISTS'
                )
            )
        );

        $query = new \WP_Query( $allPostsArgs );

        while ( $query->have_posts() ) {
            $query->the_post();
            $object = get_post();

            /** Here we change all old image url's to new ones */
            preg_match_all(
                '/<img (.*?)>/',
                $object->post_content,
                $matches,
                PREG_SET_ORDER
            );

            foreach($matches as $key => $image) {
                $image = $image[0];

                $oldId = null;
                $oldUrl = null;
                if (preg_match('/wp-image-(.*?) /', $image, $match) == 1) {
                    $oldId = str_replace("\"", "", $match[1]);
                }
                if (preg_match('/src="(.*?)"/', $image, $match) == 1) {
                    $oldUrl = $match[1];
                }

                if(!empty($oldId) and !empty($oldUrl)) {

                    $args = array(
                        'post_type'   => 'attachment',
                        'post_status' => 'inherit',
                        'meta_query'  => array(
                            array(
                                'key'     => '_old_id',
                                'value'   => $oldId
                            )
                        )
                    );

                    $imageQuery = new \WP_Query($args);
                    $results = $imageQuery->get_posts();

                    if(!empty($results)) {
                        $newImageId = $results[0]->ID;
                        $newImageUrl = wp_get_attachment_image_url($results[0]->ID, 'large');

                        //if post thumbnail id is the same as the last image in the post, remove image

                        if(get_post_thumbnail_id() == $newImageId) {
                            if(count($matches) === ($key+1)) {
                                $object->post_content = str_replace($image, "", $object->post_content);
                            }
                        } else {
                            $newContentForShortcode = str_replace("wp-image-".$oldId, "wp-image-".$newImageId, $image);
                            $newContentForShortcode = str_replace($oldUrl, $newImageUrl, $newContentForShortcode);

                            $object->post_content = str_replace($image, $newContentForShortcode, $object->post_content);
                        }


                    } else {
                        //Still remove image if thumbnail id is the same as new image id (test for second time running of this command)
                        if(get_post_thumbnail_id() == $oldId) {
                            if(count($matches) === ($key+1)) {
                                $object->post_content = str_replace($image, "", $object->post_content);
                            }
                        }
                        echo('could not convert image for post ' . $object->ID);
                    }
                }
            }

            /** here we correct the authors */
            $oldAuthor = get_post_meta($object->ID, '_old_author_id', true);

            if(!empty($oldAuthor) and $oldAuthor !== false) {
                $users = get_users(array(
                    'meta_key' => '_old_id',
                    'meta_value' => $oldAuthor,
                    'blog_id' => 0
                ));

                if(!empty($users)) {
                    wp_update_post(array(
                        'ID' => $object->ID,
                        'post_author' => $users[0]->ID,
                    ));
                }
            }

            /** Here we transform the shortcodes to gutenberg items */
            preg_match_all(
                '/\[(.*?)\]/',
                $object->post_content,
                $matches,
                PREG_SET_ORDER
            );

            foreach($matches as $shortcode) {
                $shortcode = $shortcode[0];
                if(strstr($shortcode, "[caption")) {
                    preg_match("/src=\"(.*?)\"/", $shortcode, $imageMatch);
                    $oldImageUrl = $imageMatch[1];

                    preg_match("/attachment_(.*?)\"/", $shortcode, $idMatch);
                    $oldId = $idMatch[1];

                    preg_match("/>(.*?)\[/", $shortcode, $captionMatch);
                    $caption = trim($captionMatch[1]);

                    $args = array(
                            'post_type'   => 'attachment',
                            'post_status' => 'inherit',
                            'meta_query'  => array(
                                array(
                                    'key'     => '_old_id',
                                    'value'   => $oldId
                                )
                            )
                    );
                    $imageQuery = new \WP_Query($args);
                    $results = $imageQuery->get_posts();
                    $newImageId = $results[0]->ID;
                    $newImageUrl = wp_get_attachment_image_url($results[0]->ID, 'large');

                    $newContentForShortcode =
                        '<!-- wp:newheap-blocks/one-col-image {"first_column_mediaID":'.$newImageId.'} -->' .
                        '<section class="wp-block-newheap-blocks-one-col-image image-holder"><div class="row"><div class="col-md-12"><div class="image-holder" style="background-image:url('.$newImageUrl.')"><img src="'.$newImageUrl.'"/></div><div class="image-title"></div> <p> '.$caption.'</p></div></div></section>' .
                        '<!-- /wp:newheap-blocks/one-col-image -->';

                    $object->post_content = str_replace($shortcode, $newContentForShortcode, $object->post_content);
                    //this goes wrong, but we correct it later on
                }


                if(strstr($shortcode, "[g-slider")) {
                    preg_match("/gid=\"(.*?)\"/", $shortcode, $imageMatch);
                    $sliderId = $imageMatch[1];

                    preg_match("/width=\"(.*?)\"/", $shortcode, $imageMatch);
                    $width = $imageMatch[1];

                    preg_match("/height=\"(.*?)\"/", $shortcode, $imageMatch);
                    $height = $imageMatch[1];

                    //todo read which images are in json of this slider
                    $handle = fopen((__DIR__) . '/export/'.$this->NewBlogIdToblogId(get_current_blog_id()).'/gsliders.json', "r");
                    $oldImageIds = [];
                    $newImageIds = [];
                    if ($handle) {
                        while (($line = fgets($handle)) !== false) {
                            $slideObj = json_decode($line);
                            if($slideObj->object->ID == $sliderId) {
                                foreach($slideObj->data as $item) {
                                    $args = array(
                                        'post_type'   => 'attachment',
                                        'post_status' => 'inherit',
                                        'meta_query'  => array(
                                            array(
                                                'key'     => '_old_id',
                                                'value'   => $item->img_src
                                            )
                                        )
                                    );
                                    $imageQuery = new \WP_Query($args);
                                    $results = $imageQuery->get_posts();
                                    if(!empty($results)) {
                                        $newImageIds[] = $results[0]->ID;
                                    }
                                }
                            }
                        }
                    }

                    $newContentForShortcode = '
                        <!-- wp:cb/carousel -->
                        <div class="wp-block-cb-carousel cb-padding" data-slick="{&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1,&quot;speed&quot;:300,&quot;arrows&quot;:true,&quot;dots&quot;:true,&quot;infinite&quot;:false,&quot;responsive&quot;:[{&quot;breakpoint&quot;:769,&quot;settings&quot;:{&quot;slidesToShow&quot;:1}}]}">';

                    foreach($newImageIds as $id) {
                        $newContentForShortcode .= '  
                            <!-- wp:cb/slide -->
                                <div class="wp-block-cb-slide">
                                <!-- wp:image {"id":'.$id.',"sizeSlug":"full","linkDestination":"none"} -->
                                    <figure class="wp-block-image size-full"><img src="'.wp_get_attachment_image_url($id, 'large').'" alt="" class="wp-image-'.$id.'"/></figure>
                                <!-- /wp:image -->
                                </div>
                            <!-- /wp:cb/slide -->
                        ';
                    }

                    $newContentForShortcode .= '<!-- /wp:cb/slide --></div><!-- /wp:cb/carousel -->';


                    $object->post_content = str_replace($shortcode, $newContentForShortcode, $object->post_content);
                }
            }


            /* correct broken captcha tags */
            preg_match_all(
                '/\<\!\-\- wp:newheap-blocks(.*?)\/caption\]/',
                $object->post_content,
                $captions,
                PREG_SET_ORDER
            );

            foreach($captions as $caption) {
                $captionContent = $caption[0];

                preg_match("/\/> (.*?)\[/", $captionContent, $textMatch);

                preg_match("/\<\!\-\- \/(.*?)\[\/caption\]/", $captionContent, $test);

                $newData = str_replace($test[0], '', str_replace('<div class="image-title"></div>', '<div class="image-title">'.$textMatch[1].'</div>', $captionContent)). "<!-- /wp:newheap-blocks/one-col-image -->";

                $object->post_content = str_replace($captionContent, $newData, $object->post_content);
            }

            //Replace http:// links with https:// links
            $object->post_content = str_replace("http://", "https://", $object->post_content);

            //Remove double excerpts
            $object->post_content = str_replace("<p>".$object->post_excerpt."</p>", "", $object->post_content);
            $object->post_content = str_replace($object->post_excerpt."\r\n\r\n", "", $object->post_content);
            $object->post_content = str_replace($object->post_excerpt."\r\n", "", $object->post_content);
            $object->post_content = str_replace($object->post_excerpt, "", $object->post_content);

            wp_update_post(array(
                'post_content' => $object->post_content,
            ));
        }

    }


    private function getRedirects() {

        if(!file_exists((__DIR__).'/export/'.$this->NewBlogIdToblogId(get_current_blog_id()).'/posts.json')) {
            return;
        }

        $lineNumber = 1;

        ?><table style="width: 100%">
        <tr><td>Oude URL</td><td>Nieuwe URL</td></tr><?php

        $count = 0;
        $handle = fopen((__DIR__).'/export/'.$this->NewBlogIdToblogId(get_current_blog_id()).'/posts.json', "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {


                $object = json_decode($line);

                /*
                if(!strstr($object->permalink, "https://www.mechaman.nl/blog/")) {
                    continue;
                }*/

                //find post on _old_id skip if already exists
                $args = array(
                    'blog_id' => get_current_blog_id(),
                    'post_type'  => $object->object->post_type,
                    'name' => $object->object->post_name
                );

                $postQuery = new \WP_Query($args);
                $results = $postQuery->get_posts();

                if(!empty($results)) {
                    ?><tr><td><?=$object->permalink?></td><td><?=get_the_permalink($results[0]->ID)?></td></tr><?php
                }

                $count++;
            }
        }

        ?></table><?php
        die();
    }

    public function blogIdToNewBlogId($blogId) {
        switch($blogId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 7;
            case 4:
                return 5;
        }
    }

    public function NewBlogIdToblogId($blogId) {
        switch($blogId) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 7:
                return 3;
            case 5:
                return 4;
        }
    }

}
