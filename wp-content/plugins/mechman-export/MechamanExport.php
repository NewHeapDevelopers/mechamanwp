<?php
namespace Newheap\Plugins\MechamanExport;


class MechamanExport
{
    private $export = [];

    public function __construct()
    {
        add_action( 'wp_loaded', [$this, 'export'] );
    }

    public function export()
    {
        if(!is_dir((__DIR__)."/export")) {
            mkdir((__DIR__).'/export');
        }

        if(isset($_GET['export_all'])) {
            $this->media();
            $this->categories();
            $this->tags();
            $this->users();
            $this->posts();
            $this->gSliderData();
        }

        if(isset($_GET['export_media'])) {
            $this->media();
        }

        if(isset($_GET['export_categories'])) {
            $this->categories();
        }

        if(isset($_GET['export_tags'])) {
            $this->tags();
        }

        if(isset($_GET['export_users'])) {
            $this->users();
        }

        if(isset($_GET['export_posts'])) {
            $this->posts();
        }

        if(isset($_GET['export_gsliders'])) {
            $this->gSliderData();
        }

        /** custom post types mechman */
        //$this->questions();
        //$this->questions();


        //todo: vakbladarchief
        //todo: forms en submissions


    }

    /** Export all media */
    private function media() {
        if(!is_dir((__DIR__).'/export/'.get_current_blog_id())) { mkdir((__DIR__).'/export/'.get_current_blog_id()); }
        $file = fopen((__DIR__).'/export/'.get_current_blog_id().'/media.json', "w") or die("Unable to open file!");

        $allImagesArgs = array(
            'post_type'      => 'attachment',
            'post_status'    => 'inherit',
            'posts_per_page' => -1,
        );

        $allImages = new \WP_Query( $allImagesArgs );

        while ( $allImages->have_posts() ) {
            $allImages->the_post();
            $object = get_post();
            $meta = get_post_meta($object->ID);

            fwrite($file, json_encode(['object' => $object, 'meta' => $meta])."\n");
        }

        fclose($file);
    }

    /** Export all categories */
    private function categories() {
        if(!is_dir((__DIR__).'/export/'.get_current_blog_id())) { mkdir((__DIR__).'/export/'.get_current_blog_id()); }
        $file = fopen((__DIR__).'/export/'.get_current_blog_id().'/categories.json', "w") or die("Unable to open file!");

        $allCategories = get_categories();

        foreach ( $allCategories as $object ) {
            fwrite($file, json_encode(['object' => $object])."\n");
        }
    }

    /** Export all categories */
    private function tags() {
        if(!is_dir((__DIR__).'/export/'.get_current_blog_id())) { mkdir((__DIR__).'/export/'.get_current_blog_id()); }
        $file = fopen((__DIR__).'/export/'.get_current_blog_id().'/tags.json', "w") or die("Unable to open file!");

        $tags = get_tags();

        foreach ( $tags as $object ) {
            fwrite($file, json_encode(['object' => $object])."\n");
        }
    }

    /** Export all categories */
    private function users() {
        if(!is_dir((__DIR__).'/export/'.get_current_blog_id())) { mkdir((__DIR__).'/export/'.get_current_blog_id()); }
        $file = fopen((__DIR__).'/export/'.get_current_blog_id().'/users.json', "w") or die("Unable to open file!");

        $users = get_users();

        foreach ( $users as $object ) {
            $object = get_userdata($object->ID);
            $meta = get_user_meta($object->ID);
            fwrite($file, json_encode(['object' => $object, 'meta' => $meta])."\n");
        }
    }

    /** Export all posts */
    private function posts() {
        if(!is_dir((__DIR__).'/export/'.get_current_blog_id())) { mkdir((__DIR__).'/export/'.get_current_blog_id()); }
        $file = fopen((__DIR__).'/export/'.get_current_blog_id().'/posts.json', "w") or die("Unable to open file!");

        $allPostsArgs = [
            'post_type'      => 'post',
            'posts_per_page' => -1,
        ];

        $allPosts = new \WP_Query( $allPostsArgs );

        while ( $allPosts->have_posts() ) {
            $allPosts->the_post();

            $object = get_post();
            $meta = get_post_meta($object->ID);
            $categories = get_the_category();
            $tags = get_the_tags();
            $permalink = get_the_permalink();
            $linking = \ThreeWP_Broadcast()->api()->linking( get_current_blog_id(), $object->ID );

            fwrite($file, json_encode(['object' => $object, 'meta' => $meta, 'categories' => $categories, 'permalink' => $permalink, 'tags' => $tags, 'childs' => $linking->children(), 'parents' => $linking->parent()])."\n");
        }

        fclose($file);
    }

    /** g-slider data */
    private function gSliderData() {
        if(!is_dir((__DIR__).'/export/'.get_current_blog_id())) { mkdir((__DIR__).'/export/'.get_current_blog_id()); }
        $file = fopen((__DIR__).'/export/'.get_current_blog_id().'/gsliders.json', "w") or die("Unable to open file!");

        $allPostsArgs = [
            'post_type'      => 'gg_galleries',
            'posts_per_page' => -1,
        ];

        $allPosts = new \WP_Query( $allPostsArgs );

        while ( $allPosts->have_posts() ) {
            $allPosts->the_post();

            $object = get_post();

            $data = get_post_meta($object->ID, 'gg_gallery', true);

            if(!is_array($data) && !empty($data)) {
                $string = base64_decode($data);
                if(function_exists('gzcompress') && function_exists('gzuncompress') && !empty($string)) {
                    $string = gzuncompress($string);
                }
                $data = (array)unserialize($string);
            }

            if(!is_array($data) || (count($data) == 1 && !$data[0])) {$data = false;}

            if($data) {
                fwrite($file, json_encode(['object' => $object, 'data' => $data])."\n");
            }

        }

        fclose($file);
    }

}
