<?php

/* ************  Mapping ******************/

$Xml    = new DOMDocument('1.0', 'UTF-8');
$Xml->loadXML($str);
$Xpath  = new DOMXPath($Xml);

$config = $configs['aws'];

//Gets the correct configuration
foreach ($configs as $value) {
    $temp = getXpathPropertyValue($Xpath, $value, 'post-title',true);
    if (!empty($temp)) {
        $config = $value;
        break;
    }
}

$post = [];
// get post properties
$postTitle          = getXpathPropertyValue($Xpath, $config, 'post-title');
$postStatus         = getXpathPropertyValue($Xpath, $config, 'post-status');
$post_date          = getXpathPropertyValue($Xpath, $config, 'post_date');
$post_creator       = getXpathPropertyValue($Xpath, $config, 'post_creator');
$post_modifier      = getXpathPropertyValue($Xpath, $config, 'post_modifier');
$post_modified      = getXpathPropertyValue($Xpath, $config, 'post_modified');
$postCategory       = getXpathPropertyValue($Xpath, $config, 'category');
$postTags           = getXpathPropertyValue($Xpath, $config, 'tags');
$featuredImageId    = getXpathPropertyValue($Xpath, $config, 'featured-img-id');
$featuredImageId    = is_numeric($featuredImageId) ? $featuredImageId : preg_replace("/{{url:([^}]*)}}/", "$1", $featuredImageId);
$img_size_arr       = array('630', '370');
$postStatus         = ($postStatus == 'Publish' || $postStatus == 'Published') ? 'published' : $postStatus;
$post_date          = strtotime($post_date);
$post_date          = date('Y-m-d H:i:s', $post_date);
$post_modified      = strtotime($post_modified);
$post_modified      = date('Y-m-d H:i:s', $post_modified);

// get post content
$postIntro  = getXpathPropertyValue($Xpath, $config, 'post-intro');
$postBody   = getPostContent($Xpath, $config, 'post-content');


$postBody   = '<p class="intro">' . $postIntro . '</p>' . $postBody;

if(!empty($config['wcml-images-ids'])){
    $imgNodes = $Xpath->query($config['wcml-images-ids']);
    foreach($imgNodes as $node){
        if( ($node->nodeValue != getXpathPropertyValue($Xpath, $config, 'featured-img-id'))
        ||  ($node->nodeValue == getXpathPropertyValue($Xpath, $config, 'featured-img-id') && $config['show-featured-image'])
        ){
            $imgId = preg_replace("/{{url:([^}]*)}}/", "$1", $node->nodeValue);
            $postBody .= '<img src="'. wp_get_attachment_image_src($imgId, $img_size_arr)[0] .'" />';
        }
    }
}

$gettermID      = wp_insert_term($postCategory, 'category');

if (!is_wp_error($gettermID)) {
    $termID     = $gettermID['term_id'];
} else {
    $termID     = get_cat_ID($postCategory);
}

$postTags       = explode(',', $postTags);

/* Check Author */
$sqlforcreator      = "
SELECT ID
FROM {$wpdb->users} 
WHERE ({$wpdb->users}.user_login = '" . $post_creator . "')";

$creator_data       = $wpdb->get_results($sqlforcreator);
if (!empty($creator_data)) {
    $post_creator   = $creator_data[0]->ID;
} else {
    $userdata = array(
        'user_login'        =>  $post_creator,
        'user_pass'         => 'user_' . $post_creator,
        'display_name'      => $post_creator,
        'first_name'        => $post_creator,
        'user_registered'   => date('Y-m-d H:i:s'),
        'role'              => 'author'
    );

    $post_creator = wp_insert_user($userdata);
}
/* Update author */
$sqlformodifier      = "
SELECT ID
FROM {$wpdb->users} 
WHERE ({$wpdb->users}.user_login = '" . $post_modifier . "')";
$modifier_data       = $wpdb->get_results($sqlformodifier);
if (!empty($modifier_data)) {
    $post_modifier        = $modifier_data[0]->ID;
} else {
    $userdata = array(
        'user_login'        =>  $post_modifier,
        'user_pass'         => 'user_' . $post_modifier,
        'display_name'      => $post_modifier,
        'first_name'        => $post_modifier,
        'user_registered'   => date('Y-m-d H:i:s'),
        'role'              => 'author'
    );

    $post_modifier = wp_insert_user($userdata);
}


/**
 * @param DOMXPath  $Xpath
 * @param array     $config
 * @param string    $property
 * @param boolean   $canFail sometimes this method is used to check if some xpath exists and
 * so it can fail, if it cannot fail report the error on the log file
 * @return string
 * @throws Exception
 */
function getXpathPropertyValue($Xpath, $config, $property, $canFail = false )
{

    $property = strtolower($property);

    if (!array_key_exists($property, $config)) {
        throw new Exception('Property not in config.');
    }

    $result = [];

    try {
        $nodes = $Xpath->query($config[$property]);

        if(!empty($nodes) && count($nodes) > 0 ){
            foreach ($nodes as $node) {
                $result[] = $node->nodeValue;
            }
        }
    }catch (Exception $e){
        if(!$canFail){
            error_log("Problem with XPATH ' ". $config[$property] ." '. Error Message: " . $e->getMessage());
        }
    }


    return implode(",", $result);
}

/**
 * @param DOMXPath  $Xpath
 * @param array     $config
 * @param string    $property
 * @param boolean   $canFail
 * @return DOMElement
 * @throws Exception
 */
function getXpathProperty($Xpath, $config, $property, $canFail = false )
{

    $property = strtolower($property);

    if (!array_key_exists($property, $config)) {
        throw new Exception('Property not in config.');
    }

    try {
        $nodes = $Xpath->query($config[$property]);

        foreach ($nodes as $node) {
            return $node;
        }
    } catch (Exception $e){
        if(!$canFail){
            error_log("Problem with XPATH ' ". $config[$property] ." '. Error Message: " . $e->getMessage());
        }
    }

    return null;
}

/**
 * @param DOMXPath  $Xpath
 * @return string
 */
function getPostContent($Xpath, $config, $property)
{

    $result = "";

    $nodes = $Xpath->query($config[$property]);

    $figureCount = 0;

    /** @var DOMElement $node */
    foreach ($nodes as $node) {
        /** @var DOMElement $searchNode */

        // skip the title element
        $searchNode = getXpathProperty($Xpath, $config, 'post-title');
        if ($node->getNodePath() == ($searchNode ? $searchNode->getNodePath() : '')) {
            continue;
        }

        // skip the intro element
        $searchNode = getXpathProperty($Xpath, $config, 'post-intro');
        if ($node->getNodePath() == ($searchNode ? $searchNode->getNodePath() : '')) {
            continue;
        }

        // skip the image that it was used as featured image
        $searchNode = getXpathProperty($Xpath, $config, 'featured-img');
        if (   ( $node->getNodePath() == ($searchNode ? $searchNode->getNodePath() : '') )
            && ( !$config['show-featured-image'] )
        ) {
            $figureCount++;
            continue;
        }

        $result .= _getNode($node);
    }

    return $result;
}

/**
 * @param DOMElement $node
 * @return string
 */
function _processNodes($node)
{

    $result = '';

    /** @var DOMNode $childNode */
    foreach ($node->childNodes as $childNode) {

        if ($childNode->nodeType == XML_TEXT_NODE) {
            $result .= trim($childNode->nodeValue, "\n\r\t");
            //            $result .= $childNode->nodeValue;
        } else {
            $result .= _getNode($childNode);
        }
    }

    return $result;
}

/**
 * @param DOMElement $node
 * @return string
 */
function _getNode($node)
{
    $result = '';
    switch (strtolower($node->localName)) {
        case 'figure':

            if (strpos($node->getAttribute('class'), 'inception:author-image') === false) {
                // Get image width (required by the caption shortcode)
                $imageWidth = null;
                $imgNode = $node->getElementsByTagName('img')->item(0);
                $img_src = preg_replace(
                    "/{{url:([^}]*)}}/",
                    "$1",
                    !empty($imgNode->getAttribute('src')) ?  $imgNode->getAttribute('src') : $imgNode->getAttribute('href')
                );

                if ($img_src) {
                    $imageWidth = wp_get_attachment_image_src($img_src, $img_size_arr)[0];
                }

                $caption = $node->getElementsByTagName('figcaption')->item(0)->nodeValue;

                if ($caption) {
                    $result .= '[caption align="alignleft" width="' . ($imageWidth ? $imageWidth : '') . '"]';
                }

                $result .= _processNodes($node);

                if ($caption) {
                    $result .= $caption . '[/caption]';
                }
            } elseif (strpos($node->getAttribute('class'), 'inception:author-image') !== false) {

                $figcaptionNode = $node->getElementsByTagName('figcaption')->item(0);

                if ($figcaptionNode) {
                    $result .= _processNodes($figcaptionNode);
                }
            } else {
                $result .= _processNodes($node);
            }

            break;
        case 'figcaption':
            //nop
            break;
        case 'img':
            $img_style = $node->getAttribute('style');
            $src = !empty($node->getAttribute('src')) ?  $node->getAttribute('src') : $node->getAttribute('href');
            $img_src = preg_replace("/{{url:([^}]*)}}/", "$1", $src);
            $result .= '<img class="" src="' . wp_get_attachment_image_src($img_src, $img_size_arr)[0] . '" style="' . $img_style . '" />';

            break;
        case 'a':
            $result .= sprintf(
                '<a class="%s" href="%s">',
                $node->getAttribute('class'),
                $node->getAttribute('href')
            );
            $result .= _processNodes($node);
            $result .= sprintf("</%s>", $node->localName);
            break;
        case 'br':
            $result .= '<br>';
            break;
        case 'iframe':
            // When iframe is youtube movie strip the src parameters from the url,
            // the [embed] shortcode doesn't work with parameters in the src.
            $parentClass = $node->parentNode->getAttribute('class');
            $iframeSrc = $node->getAttribute('src');

            if ($parentClass == 'youtube' && strpos($iframeSrc, '?') !== false) {
                $iframeSrc = substr($iframeSrc, 0, strpos($iframeSrc, '?'));
            }

            // embed the iframe src
            $result .= '[embed]' . $iframeSrc . '[/embed]';
            break;

        case 'title':
            $result .= sprintf('<h1 class="%s">', $node->localName);
            $result .= _processNodes($node);
            $result .= sprintf("</h1>");
            break;

        default:
            $result .= sprintf('<%s class="%s">', $node->localName, $node->getAttribute('class'));
            $result .= _processNodes($node);
            $result .= sprintf("</%s>", $node->localName);
    }

    return $result;
}


/*************  Mapping ends ******************/
