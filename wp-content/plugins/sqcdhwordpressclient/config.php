<?php

define('SQWORDPRESSPOSTPLUGIN_PUBLICATION_CONFIGS', serialize([
    'digital' => [
        // The title of the post
        'post_title' => "//psv:content//div[@class='articleContent']/h1[contains(@class,'inception:title')][1]",
        // The post status, the property should either return 'Publish' or 'Draft'
        'post_status' => "//ExtraMetaData/value/Property[text() = 'C_CDH_WP_PUBLISH_STATE']/../Values/value[1]",
        // The date / time this article should be published, should return a valid date time stamp.
        // see http://us1.php.net/manual/en/datetime.formats.php
        'post_date' => "//WorkflowMetaData/Created[1]",
        // The name of the post creator
        'post_author' => "//WorkflowMetaData/Creator[1]",
        'post_modifier' => "//WorkflowMetaData/Modifier[1]",
        'post_modified' => "//WorkflowMetaData/Modified[1]",
        // Wordpress category name, if it doesn't exist it will be created
        'post_category' => "//BasicMetaData/Category/Name[1]",
        // Wordpress tags
        'post_tags' => "//ExtraMetaData/value/Property[text() = 'C_CDH_WP_TAGS']/../Values/value",
        // Intro, will be prepended to the post content separated by a <!--more--> tag
        'post_intro' => "//psv:content//div[@class='articleContent']/h2[contains(@class, 'inception:intro')]",
        // Content
        'post_content' => "//psv:content[1]//div[@class='articleContent']/*",
        
        // ######### AgriMedia 15-03-2021 Start
        'post_auteuranders' => "//ExtraMetaData/value/Property[text() = 'C_AUTEUR_ANDERS']/../Values/value",
		// ######### AgriMedia 15-03-2021 Einde

        // Featured image
        'featured_img' => "//psv:content[1]//div[@class='articleContent']/figure[1]",
        //XPaths that shouldn't end up in the article
        'ignore_paths' => [
            '',
        ]
    ],

    'wcml' => [
        // The title of the post
        'post_title' => "/Root/content/div[contains(@class,'Kop')]",
        // The post status, the property should either return 'Publish' or 'Draft'
        'post_status' => "/Root/MetaData//ExtraMetaData/value/Property[text() = 'C_CDH_WP_PUBLISH_STATE']/../Values/value[1]",
        // The date / time this article should be published, should return a valid date time stamp.
        // see http://us1.php.net/manual/en/datetime.formats.php
        'post_date' => "/Root/MetaData//MetaData/MetaData/WorkflowMetaData/Created[1]",
        // The name of the post creator
        //'post_author' => "/Root/MetaData//WorkflowMetaData/Creator[1]",
        'post_author' => "/Root/MetaData//ExtraMetaData/value/Property[text() = 'C_AUTEUR']/../Values/value[1]",
        'post_modifier' => "/Root/MetaData//WorkflowMetaData/Modifier[1]",
        'post_modified' => "/Root/MetaData//WorkflowMetaData/Modified[1]",
        // Wordpress category name, if it doesn't exist it will be created
        'post_category' => "/Root/MetaData//BasicMetaData/Category/Name[1]",
        // Wordpress tags
        'post_tags' => "/Root/MetaData//ExtraMetaData/value/Property[text() = 'C_CDH_WP_TAGS']/../Values/value",

        // Intro, will be prepended to the post content separated by a <!--more--> tag
        'post_intro' => "/Root/content/div[contains(@class,'Intro')]",
        // Content
        //'post_content' => "/Root/content/div[contains(@class,'plattetekst')]",
        'post_content' => "/Root/content/div[not(@class='Kop' or @class='Intro')]/*",

// ######### AgriMedia 10-03-2021 Start
        'post_uitgavenaam' => "/Root/Related/Dossiers/Object/Targets/Value/Issue/Name",
// ######### AgriMedia 10-03-2021 Einde

// ######### AgriMedia 15-03-2021 Start
        'post_auteuranders' => "/Root/MetaData/MetaData/ExtraMetaData/value/Property[text() = 'C_AUTEUR_ANDERS']/../Values/value",
// ######### AgriMedia 15-03-2021 Einde



        // Featured image Element
        'featured_img' => "(/Root/Related/Images/Object[MetaData/ExtraMetaData/Value/Property[text() = 'C_CDH_WP_FEATUREDIMAGE']/../Values/Value[number() =  1]])[1]",
        // WCML Images to add to the content
        'wcml_images' => "/Root/Related/Images/Object[MetaData/BasicMetaData/ExternalId]",
        //XPaths that shouldn't end up in the article
        'ignore_paths' => [
            "/Root/content/div[contains(@class,'Kaderkop_01')]"

        ]
    ],

    'pone' => [
        // The title of the post
        'post_title' => "//p[contains(@class, 'Title')][1]",
        // The post status, the property should either return 'Publish' or 'Draft'
        'post_status' => "//ExtraMetaData/value/Property[text() = 'C_CDH_WP_PUBLISH_STATE']/../Values/value[1]",
        // The date / time this article should be published, should return a valid date time stamp.
        // see http://us1.php.net/manual/en/datetime.formats.php
        'post_date' => "//WorkflowMetaData/Created[1]",
        // The name of the post creator
        'post_author' => "//WorkflowMetaData/Creator[1]",
        'post_modifier' => "//WorkflowMetaData/Modifier[1]",
        'post_modified' => "//WorkflowMetaData/Modified[1]",
        // Wordpress category name, if it doesn't exist it will be created
        'post_category' => "//BasicMetaData/Category/Name[1]",
        // Wordpress tags
        'post_tags' => "//ExtraMetaData/value/Property[text() = 'C_CDH_WP_TAGS']/../Values/value",
        // Intro, will be prepended to the post content separated by a <!--more--> tag
        'post_intro' => "",
        // Content
        'post_content' => "/Root/document/section/*",
        // Featured image Element
        'featured_img' => "/Root/document//img[1]",
        //XPaths that shouldn't end up in the article
        'ignore_paths' => [
        ]
    ],

]));


