jQuery(document).on( 'click', '.sQWordPressApi-notice .notice-dismiss', function(event) {
    event.preventDefault();
    error = jQuery(this).parent().data("error");
    nonce = jQuery(this).parent().data("nonce");
    jQuery.ajax({
        type : "post",
        contentType: "application/x-www-form-urlencoded",
        dataType : "json",
        url: sQWordPressAjax.ajaxurl,
        data: {
            nonce:nonce,
            error:error,
            action: 'sQWordPressApi_Remove_Notification'
        }
    });
});

jQuery(document).on( 'click', '.sQWordPressApi-notice a', function() {
    var notice = jQuery(this).parent().parent();
    setTimeout(function(){
        jQuery(notice).children('.notice-dismiss').click();
    },500);
});


jQuery(document).on( 'click', '.delete-notifications', function() {
    event.preventDefault();
    nonce = jQuery(this).data("nonce");
    jQuery.ajax({
        type : "post",
        contentType: "application/x-www-form-urlencoded",
        dataType : "json",
        url: sQWordPressAjax.ajaxurl,
        data: {
            nonce: nonce,
            action: 'sQWordPressApi_Remove_All_Notifications'
        }
    }).success(function(){
        location.reload();
    });
});