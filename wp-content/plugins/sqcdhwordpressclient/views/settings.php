
<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<style>
    
    input[name="submit"]{
        background-color: #e47c23!important;
        border-color:#e47c23!important;

    }
    .submit{
        display: inline-flex;
    }
    .delete{
        display: inline-flex;
    }

    .title_container{
        position: relative;
    }

    .image{
        width: 50px;
        position: absolute;
        left: 0px;
    }

    .title_container h1{
        margin-left:55px;
        margin-bottom: 10px;
    }

</style>

<div class="wrap" >
   
    <div id="icon-themes" class="icon32"></div>
    <div class="title_container">
        <img class="image" src="<?php echo plugins_url( '/../images/integration.png' , __FILE__);?>">
        <h1 class="wp-heading-inline">sQWordPressApi Plugin Settings</h1>
    </div>
    

    <div class="row">
        <div class="col card">
            <div class="card-body">
                <form method="POST" action="options.php">
                    <?php
                    settings_fields( 'plugin_name_general_settings' );
                    do_settings_sections( 'plugin_name_general_settings' );
                    ?>
                    
                    <?php submit_button(); ?>
                    <p class="delete">
                        <button data-nonce="<?php echo wp_create_nonce("sQWordPressApi_Remove_All_Notifications_Nonce");?>" 
                        class="delete-notifications button button-primary" onclick="event.preventDefault();">Delete All Notifications</button>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>