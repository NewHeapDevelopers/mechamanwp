<?php
/**
 * @package sQWordPressApi
 */
/*
  Plugin Name: sQWordPressApi
  Plugin URI: https://bitbucket.org/publiqare/sqcdhwordpressclient
  Description: The plugin is used for posting content provided by an external source to a WordPress website. The plugin can be connected with 3rd party tools which offer content posting solutions to WordPress websites. <br> The plugin provides an API that accepts REST (JSON/XML) requests from the 3rd party website and adds WordPress posts automatically after a successful authentication. <br> Using the XML the plugin can add or update the post of the WordPress website. The user can edit, update or remove the content of any post using this plugin. This is not limited to text only but it can update images, categories, tags etc. In short, the user will be able to add any information to the post as required.
  Version: 2.0
  Author: PubliQare
  Author URI: https://publiqare.com/
  License: GPL2
 */

 /*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright PubliQare
*/

if ( !function_exists( 'add_action' ) ) {
	echo 'sQWordpressApi Plugin';
	exit;
}

define( 'SQWORDPRESSPOSTPLUGIN_NAME', 'sQWordPressAPI' );
define( 'SQWORDPRESSPOSTPLUGIN_VERSION', '2.0' );
define( 'SQWORDPRESSPOSTPLUGIN_MINIMUM_WP_VERSION', '4.4' );
define( 'SQWORDPRESSPOSTPLUGIN_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'SQWORDPRESSPOSTPLUGIN_PLUGIN_BASENAME', plugin_basename(__FILE__) );
define( 'SQWORDPRESSPOSTPLUGIN_DEBUG', true );
define( 'SQWORDPRESSPOSTPLUGIN_DELETE_LIMIT', 100000 );
define( 'SQWORDPRESSPOSTPLUGIN_EXPIRING_TOKEN', 10 );

define( 'SQWORDPRESSPOSTPLUGIN_META_ACCESS_TOKEN', 'access_token' );
define( 'SQWORDPRESSPOSTPLUGIN_META_EXPIRING_TOKEN', 'access_token_expiring' );

define( 'WCML_EXTERNAL_ID_PATH', '/MetaData/BasicMetaData/ExternalId' );

require_once( SQWORDPRESSPOSTPLUGIN_PLUGIN_DIR . 'config.php' );
require_once( SQWORDPRESSPOSTPLUGIN_PLUGIN_DIR . 'class.sqwordpressapi.php' );
require_once( SQWORDPRESSPOSTPLUGIN_PLUGIN_DIR . 'class.sqwordpressapi.post.php' );
require_once( SQWORDPRESSPOSTPLUGIN_PLUGIN_DIR . 'class.sqwordpressapi.rest-api.php' );

register_activation_hook( __FILE__, array( 'sQWordpressApi', 'plugin_activation' ));
register_deactivation_hook( __FILE__, array( 'sQWordpressApi', 'plugin_deactivation' ));

add_action( 'init', array( 'sQWordpressApi', 'init' ));
add_action( 'rest_api_init', array( 'sQWordpressApi_REST_API', 'init' ));

add_action( 'wp_ajax_sQWordPressApi_Remove_Notification', array('sQWordpressApi','removeUserMessageCallback'));
add_action( 'wp_ajax_sQWordPressApi_Remove_All_Notifications', array('sQWordpressApi','removeAllMessages'));




