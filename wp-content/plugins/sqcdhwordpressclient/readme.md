### Description
Sqare CDH endpoint for WordPress posts.

###Information
Image Width and height: Directly sets the width and height of the frame for the image, then the aspect depends on the type of image fit that you want, cover or contain, when it’s set to cover you loose some of the image, and when it’s set to contain, the image will be placed inside the img frame

### Changelog

| Build                  				| Feature                                            						|
|------------------------				|----------------------------------------------------	                    |
| 2.0.0-build-27202101               	| Plugin Rework            					                                |
| 						             	| Error Handling Improved	             					                |
|                        				| Settings Panel Added                                                      |
|                        				| Ignore XPaths Array in config.php Added                                   |
|                        				|                                                                           |
| 1.2.1-build-0601202100             	| WCML Images are added to bottom of article.             					|
| 						             	| Backwards Compatibily check with php 5.	             					|
|                        				|                                                    						| 
| 1.1.1-build-0401202101             	| Added Featured Images Hide or Show Feature.              					|
|                                     	| Skipping elements mechanism working with xpaths.              			|
|                        				|                                                    						| 
| 1.1.0-build-3012202001 				| Added Support for Wordpress version 5.3.            						|
|                        				| Added Support for WCML and PONE to Wordpress Post.  						| 
|                        				|                                                    						| 
| 1.0.0-build-1912201800 				| Support for Wordpress version 4.            	       						|
|                        				| Support for Digital Articles to Wordpress Post.    						| 

### Known Issues
##### If the raw response from the plugin has text before the json structure:

To prevent Wordpress from writing php errors/warnings on the json response, that CDH is expecting,
the wp-config of the Wordpress environment (so not of the  plugin) should have the following lines:
```php
define('WP_DEBUG', true);
define( 'WP_DEBUG_LOG', true );
define('WP_DEBUG_DISPLAY', false);
```
