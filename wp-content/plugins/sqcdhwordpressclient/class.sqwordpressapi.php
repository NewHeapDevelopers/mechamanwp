<?php

class sQWordpressApi
{

    private static $initiated = false;
    private static $errorMessages = [];
    /***
     * @var bool|array
     */
    public static $publicationConfigs = false;

    public static function init()
    {
        self::$initiated = true;
        self::$publicationConfigs = unserialize(SQWORDPRESSPOSTPLUGIN_PUBLICATION_CONFIGS);
        sQWordpressApi::$errorMessages = json_decode(get_option('sQWordPressAPI_user_messages', json_encode(array())), true);

        add_action('admin_menu', array('sQWordpressApi', 'register_settings'));
        add_action('admin_init', array('sQWordpressApi', 'registerAndBuildFields'));
        add_action('admin_init', array('sQWordpressApi', 'register_scripts'));

        add_filter('plugin_action_links_' . SQWORDPRESSPOSTPLUGIN_PLUGIN_BASENAME, array('sQWordpressApi', 'add_plugin_page_settings_link'), 10, 2);

        add_action('admin_notices', array('sQWordpressApi', 'sqwpapi_notice'));

        add_action('wp_head', array('sQWordpressApi', 'custom_styles'), 100);
        add_action('admin_head', array('sQWordpressApi', 'custom_styles'), 100);
    }

    public  static function custom_styles()
    {
        echo "<style>
                .toplevel_page_sQWordPressSettings a img {
                    width:20px;
                    height:20px;
                    opacity:1!important;
                }
               
                .img-article{object-position: left;}
                .img-article-contain{object-fit: contain;}
                .img-article-cover{object-fit: cover;}
                .img-article-fill{object-fit: fill;}
                .img-article-none{object-fit: none;}
            </style>";
    }

    public static function plugin_activation()
    {
        if (version_compare($GLOBALS['wp_version'], SQWORDPRESSPOSTPLUGIN_MINIMUM_WP_VERSION, '<')) {
            sQWordpressApi::addErrorMessage(sprintf('sQWordPressApiPlugin %s requires WordPress %s or higher.', SQWORDPRESSPOSTPLUGIN_VERSION, SQWORDPRESSPOSTPLUGIN_MINIMUM_WP_VERSION));
        } elseif (!empty($_SERVER['SCRIPT_NAME']) && false !== strpos($_SERVER['SCRIPT_NAME'], '/wp-admin/plugins.php')) {
            add_option('Activated_sQWordpressApi', true);
        }
    }

    public static function plugin_deactivation()
    {
        $plugins = get_option('active_plugins');
        $sqwordpressapi = plugin_basename(SQWORDPRESSPOSTPLUGIN_PLUGIN_DIR . 'sqwordpressapi.php');
        $update = false;
        foreach ($plugins as $i => $plugin) {
            if ($plugin === $sqwordpressapi) {
                $plugins[$i] = false;
                $update = true;
            }
        }
        if ($update) {
            update_option('active_plugins', array_filter($plugins));
        }
    }

    public static function getParsedParams($request)
    {
        $params = [];
        try {
            $params = $request->get_body_params();

            foreach ($params as &$param) {
                $param = json_decode($param);
            }

            return $params;
        } catch (Exception $exception) {
            sQWordpressApi::Log('Cound\'t Parse Parameters.');
            sQWordpressApi::Log($exception->getMessage());
        }
        return null;
    }

    public static function getOrCreateUser($name)
    {
        $result = username_exists($name);

        if (!$result || $result == 0) {
            $user = array(
                'user_login' => $name,
                'user_pass' => md5(uniqid(rand(), true)),
                'display_name' => $name,
                'first_name' => $name,
                'user_registered' => date('Y-m-d H:i:s', strtotime('now')),
                'role' => 'author'
            );
            $result = wp_insert_user($user);
        }

        if (empty($result) || is_wp_error($result))
            throw new Exception(sprintf(
                'Could\'t get or create User %s. Wp_error: %s',
                $name,
                print_r($result, true)
            ));

        return $result;
    }

    public static function getOrCreateCategory($name)
    {
        $result = null;
        $termID = get_cat_ID($name);

        if (empty($termID)) {
            $gettermID = wp_insert_term($name, 'category');
            $termID = $gettermID['term_id'];
        }

        if (empty($termID))
            throw new Exception(sprintf('Could\'t get or create Category: %s', $name));

        return $termID;
    }

    public static function add_plugin_page_settings_link($plugin_actions)
    {
        $actions['Settings'] = sprintf(__('<a href="%s">Settings</a>', 'Settings'), esc_url(admin_url('options-general.php?page=sQWordPressSettings')));
        return array_merge($plugin_actions, $actions);
    }

    public static function register_settings()
    {
        $notification_count = is_array(sQWordpressApi::$errorMessages) ? count(sQWordpressApi::$errorMessages) : false;
        $title =  $notification_count ? sprintf('%s <span class="awaiting-mod">%d</span>', 'sQWP API', $notification_count) : 'sQWP API';
        add_menu_page(SQWORDPRESSPOSTPLUGIN_NAME, $title, 'administrator', 'sQWordPressSettings', array('sQWordpressApi', 'render_sqwpapi_settings_page'), plugins_url('/images/integration.png', __FILE__), 3);
    }

    public static function register_scripts()
    {
        wp_register_script("remove-notice-script", plugins_url('/js/notice-update.js', __FILE__), array('jquery'));
        wp_localize_script('remove-notice-script', 'sQWordPressAjax', array('ajaxurl' => admin_url('admin-ajax.php')));

        wp_enqueue_script('jquery');
        wp_enqueue_script('remove-notice-script');
    }


    public static function registerAndBuildFields()
    {
        add_settings_section(
            'sQWordPressApi_publishing_section',
            'Publishing',
            array('sQWordpressApi', 'sQWordPressApi_display_separator'),
            'plugin_name_general_settings'
        );

        add_settings_field(
            'sQWordPressAPI_post_default_title',
            'Default title when empty',
            array('sQWordpressApi', 'sQWordPressApi_render_settings_field'),
            'plugin_name_general_settings',
            'sQWordPressApi_publishing_section',
            array(
                'type' => 'input',
                'subtype' => 'text',
                'id' => 'sQWordPressAPI_post_default_title',
                'name' => 'sQWordPressAPI_post_default_title',
                'required' => 'false',
                'get_options_list' => '',
                'value_type' => 'normal',
                'wp_data' => 'option'
            )
        );
        register_setting(
            'plugin_name_general_settings',
            'sQWordPressAPI_post_default_title',
            array(
                'type' => 'string',
                'description' => 'If the featured image is shown in the content body or not.',
                'show_in_rest' => true,
                'default' => 'New Post'
            )
        );

        add_settings_section(
            'sQWordPressApi_img_section',
            'Images',
            array('sQWordpressApi', 'sQWordPressApi_display_separator'),
            'plugin_name_general_settings'
        );

        add_settings_field(
            'sQWordPressAPI_show_featured_img',
            'Show featured image in the article body.',
            array('sQWordpressApi', 'sQWordPressApi_render_settings_field'),
            'plugin_name_general_settings',
            'sQWordPressApi_img_section',
            array(
                'type' => 'input',
                'subtype' => 'checkbox',
                'id' => 'sQWordPressAPI_show_featured_img',
                'name' => 'sQWordPressAPI_show_featured_img',
                'required' => 'true',
                'get_options_list' => '',
                'value_type' => 'normal',
                'wp_data' => 'option'
            )
        );
        register_setting(
            'plugin_name_general_settings',
            'sQWordPressAPI_show_featured_img',
            array(
                'type' => 'boolean',
                'description' => 'If the featured image is shown in the content body or not.',
                'show_in_rest' => true,
                'default' => false
            )
        );
        add_settings_field(
            'sQWordPressAPI_img_width',
            'Max Width (Pixels)',
            array('sQWordpressApi', 'sQWordPressApi_render_settings_field'),
            'plugin_name_general_settings',
            'sQWordPressApi_img_section',
            array(
                'type' => 'input',
                'subtype' => 'number',
                'id' => 'sQWordPressAPI_img_width',
                'name' => 'sQWordPressAPI_img_width',
                'required' => 'true',
                'get_options_list' => '',
                'value_type' => 'normal',
                'wp_data' => 'option'
            )
        );
        register_setting(
            'plugin_name_general_settings',
            'sQWordPressAPI_img_width',
            array(
                'type' => 'number',
                'description' => 'Images widths.',
                'show_in_rest' => true,
                'default' => '630'
            )
        );
        add_settings_field(
            'sQWordPressAPI_img_height',
            'Max Height (Pixels)',
            array('sQWordpressApi', 'sQWordPressApi_render_settings_field'),
            'plugin_name_general_settings',
            'sQWordPressApi_img_section',
            array(
                'type' => 'input',
                'subtype' => 'number',
                'id' => 'sQWordPressAPI_img_height',
                'name' => 'sQWordPressAPI_img_height',
                'required' => 'true',
                'get_options_list' => '',
                'value_type' => 'normal',
                'wp_data' => 'option'
            )
        );
        register_setting(
            'plugin_name_general_settings',
            'sQWordPressAPI_img_height',
            array(
                'type' => 'number',
                'description' => 'Images Heights.',
                'show_in_rest' => true,
                'default' => '370'
            )
        );
        add_settings_field(
            'sQWordPressAPI_img_fit',
            'Image Fit',
            array('sQWordpressApi', 'sQWordPressApi_render_settings_field'),
            'plugin_name_general_settings',
            'sQWordPressApi_img_section',
            array(
                'type' => 'select',
                'subtype' => 'text',
                'id' => 'sQWordPressAPI_img_fit',
                'name' => 'sQWordPressAPI_img_fit',
                'required' => 'false',
                'get_options_list' => 'contain,cover,fill,none',
                'value_type' => 'normal',
                'wp_data' => 'option'
            )
        );

        register_setting(
            'plugin_name_general_settings',
            'sQWordPressAPI_img_fit',
            array(
                'type' => 'string',
                'description' => 'Image Fit',
                'show_in_rest' => true,
                'default' => 'contain'
            )
        );
    }

    public static function render_sqwpapi_settings_page()
    {
        sQWordpressApi::view('settings');
    }

    public static function view($name, array $args = array())
    {
        $args = apply_filters('sqwordpressapi_view_arguments', $args, $name);
        foreach ($args as $key => $val) {
            $key = $val;
        }
        load_plugin_textdomain('sQWordPressApi');
        $file = SQWORDPRESSPOSTPLUGIN_PLUGIN_DIR . 'views/' . $name . '.php';

        if (file_exists($file))
            include($file);
        else
            show_message(sprintf("Page %s Not found.", $name));
    }

    public static function sQWordPressApi_display_separator()
    {
        echo '<hr>';
    }

    public static function sQWordPressApi_render_settings_field($args)
    {
        if ($args['wp_data'] == 'option') {
            $wp_data_value = get_option($args['name']);
        } elseif ($args['wp_data'] == 'post_meta') {
            $wp_data_value = get_post_meta($args['post_id'], $args['name'], true);
        }

        switch ($args['type']) {
            case 'input':
                $value = ($args['value_type'] == 'serialized') ? serialize($wp_data_value) : $wp_data_value;
                if ($args['subtype'] != 'checkbox') {
                    $prependStart = (isset($args['prepend_value'])) ? '<div class="input-prepend"> <span class="add-on">' . $args['prepend_value'] . '</span>' : '';
                    $prependEnd = (isset($args['prepend_value'])) ? '</div>' : '';
                    $args['required'] = (isset($args['required'])) ? 'required="' . $args['required'] . '"' : '';
                    $step = (isset($args['step'])) ? 'step="' . $args['step'] . '"' : '';
                    $min = (isset($args['min'])) ? 'min="' . $args['min'] . '"' : '';
                    $max = (isset($args['max'])) ? 'max="' . $args['max'] . '"' : '';
                    if (isset($args['disabled'])) {
                        // hide the actual input bc if it was just a disabled input the informaiton saved in the database would be wrong - bc it would pass empty values and wipe the actual information
                        echo $prependStart . '<input type="' . $args['subtype'] . '" id="' . $args['id'] . '_disabled" ' . $step . ' ' . $max . ' ' . $min . ' name="' . $args['name'] . '_disabled" size="40" disabled value="' . esc_attr($value) . '" /><input type="hidden" id="' . $args['id'] . '" ' . $step . ' ' . $max . ' ' . $min . ' name="' . $args['name'] . '" size="40" value="' . esc_attr($value) . '" />' . $prependEnd;
                    } else {
                        echo $prependStart . '<input type="' . $args['subtype'] . '" id="' . $args['id'] . '" ' . $args['required'] . ' ' . $step . ' ' . $max . ' ' . $min . ' name="' . $args['name'] . '" size="40" value="' . esc_attr($value) . '" />' . $prependEnd;
                    }
                } else {
                    $checked = ($value) ? 'checked' : '';
                    echo '<input type="' . $args['subtype'] . '" id="' . $args['id'] . '" "' . $args['required'] . '" name="' . $args['name'] . '" size="40" value="1" ' . $checked . ' />';
                }
                break;
            case 'select':
                $value = ($args['value_type'] == 'serialized') ? serialize($wp_data_value) : $wp_data_value;
                $select = '<select name="' . $args['name'] . '">';
                $options = explode(',', $args['get_options_list']);
                foreach ($options as $option) {
                    if ($option == $value) {
                        $select .= "<option selected='selected' value='{$option}'>{$option}</option>";
                    } else {
                        $select .= "<option value='{$option}'>{$option}</option>";
                    }
                }
                $select .= '</select>';
                echo $select;
                break;
            default:
                # code...
                break;
        }
    }


    /***
     * @param $response
     * @return string
     */
    public static function FailResponse($response)
    {
        if (!is_array($response)) {
            $response = [$response];
        }

        $response['status'] = $response['status'] != 0 ? $response['status'] : 500;
        $response['success'] = $response['success'] ? true : false;
        sQWordpressApi::Log('Bad Request, sending fail response. ' . print_r($response, true));
        return rest_ensure_response($response);
    }

    public static function addErrorMessage($message)
    {
        sQWordpressApi::$errorMessages[] = array(
            "message" => $message,
            "type" => "error",
            "id" => count(sQWordpressApi::$errorMessages) + 1
        );

        update_option('sQWordPressAPI_user_messages', json_encode(sQWordpressApi::$errorMessages));
    }

    public static function addPostMessage($message, $postTitle)
    {
        foreach (sQWordpressApi::$errorMessages as $messageObj) {
            if (strpos($messageObj['message'], $postTitle) !== false) {
                self::removeUserMessage($messageObj['id']);
            }
        }
        self::addUserMessage($message);
    }

    public static function addUserMessage($message)
    {
        sQWordpressApi::$errorMessages[] = array(
            "message" => $message,
            "type" => "success",
            "id" => count(sQWordpressApi::$errorMessages) + 1
        );

        update_option('sQWordPressAPI_user_messages', json_encode(sQWordpressApi::$errorMessages));
    }

    public static function removeUserMessage($messageId)
    {
        $newArray = [];
        foreach (sQWordpressApi::$errorMessages as $message) {
            if (strval($message["id"]) != strval($messageId)) {
                $newArray[] = $message;
            }
        }
        update_option('sQWordPressAPI_user_messages', json_encode($newArray));
    }

    public static function removeUserMessageCallback()
    {
        if (!wp_verify_nonce($_REQUEST['nonce'], "sQWordPressApi_Remove_Notification_Nonce")) {
            exit("Denied");
        }
        $newArray = [];
        $messageId = $_REQUEST["error"];
        sQWordpressApi::removeUserMessage($messageId);
        exit("true");
    }

    public static function removeAllMessages()
    {
        if (!wp_verify_nonce($_REQUEST['nonce'], "sQWordPressApi_Remove_All_Notifications_Nonce")) {
            exit("Denied");
        }

        update_option('sQWordPressAPI_user_messages', json_encode([]));
        exit("true");
    }

    public static function sqwpapi_notice()
    {
        if (!empty(sQWordpressApi::$errorMessages) && is_array(sQWordpressApi::$errorMessages) && count(sQWordpressApi::$errorMessages) > 0) {
            foreach (sQWordpressApi::$errorMessages as $messageObj) {
                $nonce = wp_create_nonce("sQWordPressApi_Remove_Notification_Nonce");
                $class = sprintf('notice sQWordPressApi-notice notice-%s is-dismissible', $messageObj["type"]);
                echo sprintf('<div class="%s" data-nonce="%s" data-error="%s"><p>%s</p></div>', esc_attr($class), $nonce, $messageObj["id"], $messageObj["message"]);
            }
        }
    }

    /***
     * @param $message
     * @throws Exception
     */
    public static function Fail($message, $code = 500)
    {
        sQWordpressApi::Log('Error:');
        sQWordpressApi::Log($message);
        throw new Exception($message, $code);
    }

    /***
     * @param $message
     */
    public static function Log($message)
    {
        if (apply_filters('sqwordpressapi_debug_log', defined('WP_DEBUG') && WP_DEBUG && defined('WP_DEBUG_LOG') && WP_DEBUG_LOG && defined('SQWORDPRESSPOSTPLUGIN_DEBUG') && SQWORDPRESSPOSTPLUGIN_DEBUG)) {
            error_log(sprintf('%s: %s', SQWORDPRESSPOSTPLUGIN_NAME, print_r($message, true)));
        }
    }
}
