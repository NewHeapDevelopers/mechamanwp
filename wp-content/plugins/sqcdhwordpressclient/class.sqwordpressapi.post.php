<?php


class Post
{

    /***
     * @var array $config
     */
    private $config;

    /***
     * @var string $type
     */
    private $type;

    /***
     * @var array $img_size
     */
    private $img_size;

    /***
     * @var DOMDocument $xml
     */
    private $xml;

    /***
     * @var array $post
     */
    private $post;

    /***
     * @var bool $processed
     */
    private $processed;

    /***
     * @var array $ignorePaths
     */
    private $ignorePaths;

    /***
     * @var int $storyId
     */
    private $storyId;

    public function __construct()
    {
        $this->processed = false;
        $this->img_size = array(
            get_option('sQWordPressAPI_img_width', '630'),
            get_option('sQWordPressAPI_img_height', '370')
        );
    }

    /***
     * @return Post
     */
    public static function Create()
    {
        return new Post();
    }

    /***
     * Returns an post object ready to be published or updated to WordPress
     * @return array
     * @throws Exception
     */
    public function Process()
    {
        if (!$this->processed) {
            $this->post = [];

            if (!empty($this->storyId) && $this->storyId > 0)
                $this->post['ID'] = $this->storyId;

            $this->post['post_type'] = 'post';
            $this->post['post_title'] = $this->_getXpathPropertyValue('post_title', array('separator' => ' ', 'multiple' => true));
            $this->post['post_title'] = $this->post['post_title'] ? $this->post['post_title'] : get_option('sQWordPressAPI_post_default_title', 'New Post');
            $this->post['post_name'] = $this->post['post_title'];

            $this->post['post_status'] = $this->_getXpathPropertyValue('post_status');
            $this->post['post_status'] = ($this->post['post_status'] == 'Publish' || $this->post['post_status'] == 'Published') ?
                'published' : ($this->post['post_status'] ? $this->post['post_status'] : 'draft');

            if (empty($this->post['ID'])) {
                $this->post['post_date'] = $this->_getXpathPropertyValue('post_date');
                $this->post['post_date'] = $this->post['post_date'] ? $this->post['post_date'] : current_time('mysql');
                $this->post['post_date'] = strtotime($this->post['post_date']);
                $this->post['post_date'] = date('Y-m-d H:i:s', $this->post['post_date']);
            } else {
                $this->post['post_date'] = current_time('mysql');
            }

            $this->post['post_modified'] = current_time('mysql');
            $this->post['edit_date'] = $this->post['post_modified'];

            //Get Featured Img Id
            $this->post['post_featured_img'] = $this->_getExternalId($this->_getXpathProperty('featured_img'));

            $this->post['post_tags'] = $this->_getXpathPropertyValue('post_tags', array('multiple' => true));
            $this->post['post_tags'] = explode(',', $this->post['post_tags']); 
            

            // ######### AgriMedia 10-03-2021 Start
            
            if ($this->type == 'wcml') { // Als wcml (= Print) dan Categorie "Magazine" (26 op vakbladelite.nl) toevoegen

                $uitgavenaam = $this->_getXpathPropertyValue('post_uitgavenaam');

                $uitgavenaam = str_replace('_0', '-', $uitgavenaam); // uitgavenummer _06 = _6 

                $this->post['post_uitgavenaam'] = $uitgavenaam;

            } else {
                //
            }

            // ######### AgriMedia 10-03-2021 Einde

            // ######### voor 10-03-2021 Origineel:
            // $this->post['post_tags'] = explode(',', $this->post['post_tags']);


            //Gets or creates category
            $this->post['post_category'] = $this->_getXpathPropertyValue('post_category');
            if (!empty($this->post['post_category'])) {

                // ######### 10-03-2021 Start
                if ($this->type == 'wcml') { // Als wcml (= Print) dan Categorie "Magazine" (26 op vakbladelite.nl) toevoegen

                    $this->post['post_category'] = array(26, sQWordpressApi::getOrCreateCategory($this->post['post_category']));   

                } else {

                    $this->post['post_category'] = array(sQWordpressApi::getOrCreateCategory($this->post['post_category']));

                }
                // ######### 10-03-2021 Einde
                
                // ######### voor 10-03-2021 Origineel:
                // $this->post['post_category'] = array(sQWordpressApi::getOrCreateCategory($this->post['post_category']));


            }

            // Gets or Creates Author
            $this->post['post_author'] = $this->_getXpathPropertyValue('post_author');
            if (!empty($this->post['post_author'])) {
                $this->post['post_author'] = sQWordpressApi::getOrCreateUser($this->post['post_author']);
            }

            // Gets or Creates Modifier
            $this->post['post_modifier'] = $this->_getXpathPropertyValue('post_modifier');
            if (!empty($this->post['post_modifier'])) {
                $this->post['post_modifier'] = sQWordpressApi::getOrCreateUser($this->post['post_modifier']);
            }

            //Preparing for processing content
            $this->_addWcmlImagesToPostContent();
            $this->_setIgnorePathsByProperties();




            $intro = $this->_getXpathPropertyValue('post_intro', array('multiple' => true));
            $this->post['post_content'] = $this->_getPostContent('post_content');


            // ######### AgriMedia 10-03-2021 Start

            $intro = preg_replace('/\s\s+/', ' ', $intro);

            $this->post['post_excerpt'] = $intro;

            // ######### AgriMedia 10-03-2021 Einde
            





// ######### AgriMedia 10-03-2021 Start
function htmlopruimenSpan ($postBody) {

    $tekst = $postBody;
    
    $zoekwaarde = '<span class="[No character style]">'; // Aantal instanties van zoekterm opslaan
    $aantal = substr_count($tekst, $zoekwaarde);
    
    $zoekwaardestart = '<span class="[No character style]">';
    $zoekwaardeeind = '</span>';
        
    $lengtezoekwaardestart = strlen($zoekwaardestart);
    $lengtezoekwaardeeind = strlen($zoekwaardeeind);
    
    $aantalverwerkt = 0;

          while( $aantalverwerkt < $aantal ) {
    
          $posstart = strpos($tekst, $zoekwaardestart); // Open tag - Startpositie van zoekwaarde
    
          $poseind = strpos($tekst, $zoekwaardeeind, $posstart); // Closing tag - positie van zoekwaarde
   
          $tekst = substr_replace($tekst, "", $posstart, 35); // Open tag vervangen door ""
    
          $tekst = substr_replace($tekst, "", $poseind - 35, 7); // Closing tag vervangen door ""
    
          $aantalverwerkt++;
   
          } // Einde while( $aantalverwerkt <= $aantal ) 
    
    return $tekst;
    
    }

    
    $postinhoud = $this->post['post_content'];
    
    $postBody = htmlopruimenSpan($postinhoud);



    
function htmlopruimenTussenkop ($postBody) {

    $tekst = $postBody;
    
    $zoekwaarde = '<p class="PTK1_Tussenkop">'; // Aantal instanties van zoekterm opslaan
    $aantal = substr_count($tekst, $zoekwaarde);
    
    $zoekwaardestart = '<p class="PTK1_Tussenkop">';
    $zoekwaardeeind = '</p>';
        
    $lengtezoekwaardestart = strlen($zoekwaardestart);
    $lengtezoekwaardeeind = strlen($zoekwaardeeind);
    
    $aantalverwerkt = 0;

          while( $aantalverwerkt < $aantal ) {
    
          $posstart = strpos($tekst, $zoekwaardestart); // Open tag - Startpositie van zoekwaarde
    
          $poseind = strpos($tekst, $zoekwaardeeind, $posstart); // Closing tag - positie van zoekwaarde
   
          $tekst = substr_replace($tekst, "<h2>", $posstart, 27); // Open tag vervangen door ""
    
          $tekst = substr_replace($tekst, "</h2>", $poseind - 27, 4); // Closing tag vervangen door ""
    
          $aantalverwerkt++;
   
          } // Einde while( $aantalverwerkt <= $aantal ) 
    
    return $tekst;
    
    }

    $postBody = htmlopruimenTussenkop($postBody);

    

    $postBody = preg_replace('/\s\s+/', ' ', $postBody);

// ######### AgriMedia 10-03-2021 Einde



          //  $this->post['post_content'] = '<p class="intro">' . $intro . "</p>" . $postBody;
           


            $auteur_anders = $this->_getXpathPropertyValue('post_auteuranders');

            

            
            if (!empty($auteur_anders)) {
                $this->post['post_content'] = '<p class="intro">' . $intro . "</p>" . $postBody . '<p class="AuteurAnders">Door: ' . $auteur_anders . '</p>';    
            } else {
                $this->post['post_content'] = '<p class="intro">' . $intro . "</p>" . $postBody;
            }

            



            $this->processed = true;
            return $this->post;
        } else {
            sQWordpressApi::Log('Post has already been processed.');
        }
    }

    /**
     * @param array $args
     * @return mixed
     * @throws Exception
     */
    private function _getXpathPropertyValue($property, $args = array())
    {
        if (!array_key_exists($property, $this->config)) {
            sQWordpressApi::Fail(sprintf('Property <%s> not in config.', $property));
        }

        $result = [];
        if (!empty($this->config[$property])) {
            return $this->_getXpathValue($this->config[$property], $args);
        }

        return null;
    }

    /**
     * @param string $query
     * @param array $args
     * @throws Exception
     */
    private function _getXpathValue($query, $args = array())
    {
        $result = [];
        try {
            $nodes = $this->_getXpathElement($query, $args);
            if ($nodes) {
                if (isset($args['multiple']) && $args['multiple']) {

                    foreach ($nodes as $node) {
                        $result[] = $node->nodeValue;
                    }
                } else {
                    $result[] = $nodes->nodeValue;
                }
            }
        } catch (Exception $e) {
            $canFail = isset($args) && isset($args['canFail']) ? $args['canFail'] : false;
            if (!$canFail) {
                sQWordpressApi::Log("Problem with XPATH ' " . $query . " '. Error Message: " . $e->getMessage());
            }
        }

        $separator = isset($args['separator']) ? $args['separator'] : ',';
        return implode($separator, $result);
    }

    /**
     * @param string $query
     * @param array $args
     * @return DOMNode|DOMNodeList|null
     * @throws Exception
     */
    private function _getXpathElement($query, $args = array())
    {
        if (empty($query))
            return null;

        $xpath = new DOMXPath($this->xml);
        try {
            $nodes = $xpath->query($query);
            if ($nodes && $nodes->length > 0) {
                if (isset($args['multiple']) && $args['multiple']) {
                    return $nodes;
                } else {
                    return $nodes->item(0);
                }
            } else {
                sQWordpressApi::Log("xpath not found in document skipping. Xpath: ' " . $query . " '");
            }
        } catch (Exception $e) {
            $canFail = isset($args) && isset($args['canFail']) ? $args['canFail'] : false;
            if (!$canFail) {
                sQWordpressApi::Log("Problem with XPATH ' " . $query . " '. Error Message: " . $e->getMessage());
            }
        }
        return null;
    }

    /**
     * @param DOMNode $node
     * @return string|null
     */
    private function _getExternalId($node)
    {
        $result = null;
        $pattern = "/{{url:([^}]*)}}/";

        if (empty($node)) {
            return null;
        }

        if (empty($result)) {
            $result = self::_searchExternalId($node);
        }

        if (empty($result)) {
            $result = $this->_searchNodes($node, $pattern);
        }

        if (!empty($result)) {
            $externalId = is_numeric($result) ? $result :
                preg_replace($pattern, "$1", $result);

            return $externalId;
        }

        return null;
    }

    /***
     * @param DOMNode $node
     * @return string|null
     */
    private static function _searchExternalId(DOMNode $node)
    {
        if ($node->localName == 'ExternalId') {
            return $node->nodeValue;
        }

        if ($node->hasChildNodes()) {
            foreach ($node->childNodes as $childNode) {
                $temp = self::_searchExternalId($childNode);
                if (!empty($temp)) {
                    return $temp;
                }
            }
        }

        return null;
    }

    /**
     * @param DOMNode $node
     * @param $pattern
     * @return string|null
     */
    private function _searchNodes($node, $pattern)
    {
        if (empty($node) || !is_object($node))
            return null;

        if ($node->hasAttributes()) {
            foreach ($node->attributes as $attribute) {
                if (preg_match($pattern, $attribute->nodeValue)) {
                    return $attribute->nodeValue;
                }
            }
        }

        if ($node->nodeType == XML_TEXT_NODE) {
            if (preg_match($pattern, $node->nodeValue)) {
                return $node->nodeValue;
            }
        }

        if ($node->hasChildNodes()) {
            foreach ($node->childNodes as $childNode) {
                $value = $this->_searchNodes($childNode, $pattern);
                if (!empty($value))
                    return $value;
            }
        }

        return null;
    }

    /**
     * @param string $property
     * @param array $args
     * @return DOMElement|DOMNode|DOMNodeList
     * @throws Exception
     */
    private function _getXpathProperty($property, $args = array())
    {
        if (!array_key_exists($property, $this->config)) {
            sQWordpressApi::Fail('Property not in config.' . $property);
        }

        return $this->_getXpathElement($this->config[$property]);
    }

    private function _addWcmlImagesToPostContent()
    {
        //Place added wcml images inside the content
        if ($this->type == 'wcml' && !empty($this->config['wcml_images'])) {
            $xpath = new DOMXPath($this->xml);
            $imgNodes = $xpath->query($this->config['wcml_images']);
            if (is_object($imgNodes) && $imgNodes->length > 0) {
                foreach ($imgNodes as $imageNode) {
                    $externalId = $this->_getXpathValue($imageNode->getNodePath() . WCML_EXTERNAL_ID_PATH);
                    $elementId = $this->_getXpathValue($imageNode->getNodePath() . '/Relations/Value[Type="Placed"]/Placements/Value/ElementID');

                    if ($elementId && is_object($elementId)) {
                        //Place the image in the last div with the elementId
                        $query = sprintf('(%s)[last()]', sprintf('%s/div[@id="%s"]', $this->config['
                        '], $elementId));
                        $containerNode = $this->_getXpathElement($query);
                    }
                    if (empty($containerNode)) {
                        //Place the image in the last post_content element
                        $lastElement = $this->_getXpathElement(sprintf('(%s)[last()]', $this->config['post_content']));
                        try {
                            if( isset($this->_getXpathElement( $this->config['post_content'])->length)
                                && $this->_getXpathElement( $this->config['post_content'])->length > 0 ){
                                $containerNode = $this->xml->createElement($lastElement->localName);
                                $containerNode->setAttribute('class', 'image' );
                                $lastElement->parentNode->appendChild($containerNode);
                            }else{
                                $containerNode = $lastElement;
                            }

                        }catch (Exception $e){
                            $containerNode = $lastElement;
                        }
                    }

                    if (!empty($containerNode) && is_object($containerNode)) {

                        $img = $this->xml->createElement('img');
                        $img->setAttribute('src', $externalId);
                        $containerNode->appendChild($img);

                        $imgId = preg_replace("/{{url:([^}]*)}}/", "$1", $externalId);
                        if ($imgId == $this->post['post_featured_img'])
                            $this->config["featured_img"] = $img->getNodePath();
                    }
                }
            }
        }
    }

    public function _setIgnorePathsByProperties($properties = array())
    {
        $this->ignorePaths = [];
        //Removes By Default the title and intro and featuredImg if the setting is setup, from the post body.
        $properties = array_merge($properties, ['post_title', 'post_intro']);
        if (get_option('sQWordPressAPI_show_featured_img') != 1) { //Not Checked
            $properties = array_merge($properties, ['featured_img']);
        }

        foreach ($properties as $property) {
            $node = $this->_getXpathProperty($property);
            $this->addNodesToIgnorePaths([$node]);
        }

        $xpath = new DOMXPath($this->xml);
        foreach ($this->config['ignore_paths'] as $ignorePath) {
            if (!empty($ignorePath)) {
                $nodes = $xpath->query($ignorePath);
                $this->addNodesToIgnorePaths($nodes);

                if($ignorePath == "/Root/content/div[contains(@class,'fotobijschrift-1')]"){
                    sQWordpressApi::Log("/Root/content/div[contains(@class,'fotobijschrift-1')] Nodes:");
                    foreach($nodes as $node){
                        sQWordpressApi::Log(print_r($node,true));
                        sQWordpressApi::Log($node->getNodePath());
                    }
                    sQWordpressApi::Log('ignore paths : ' . print_r($this->ignorePaths,true));
                }
            }
        }
    }

    /***
     * @param $nodes
     */
    public function addNodesToIgnorePaths($nodes)
    {
        foreach ($nodes as $node) {
            /**@var DOMNode $node */
            if (!empty($node)) {
                $path = $node->getNodePath();
                if (!empty($path))
                    $this->ignorePaths[] = $path;

                if($node->hasChildNodes()){
                    foreach ($node->childNodes as $cNodes){
                        $path = $cNodes->getNodePath();
                        if (!empty($path))
                            $this->ignorePaths[] = $path;
                    }
                }
            }
        }
    }

    /**
     * @param string $property
     * @return string
     */
    private function _getPostContent($property)
    {
        $result = '';
        if (!array_key_exists($property, $this->config)) {
            sQWordpressApi::Fail(sprintf('Property <%s> not in config.', $property));
        }

        if (empty($this->config[$property])) {
            sQWordpressApi::Fail(sprintf('Property <%s> is empty.', $property));
        }

        $nodes = $this->_getXpathElement($this->config[$property], array('multiple' => true));
        /** @var DOMElement $node */
        foreach ($nodes as $node) {
            /** @var DOMElement $searchNode */

            if (!$this->filterNode($node))
                $result .= $this->_renderNode($node);
        }
        return $result;
    }

    /***
     * @param $node
     * @return bool
     */
    public function filterNode($node)
    {
        foreach ($this->ignorePaths as $ignorePath) {
            if ($node->getNodePath() == $ignorePath) {
                sQWordpressApi::Log('Being Ignored : ' . $ignorePath);
                return true;
            }
        }
        return false;
    }

    /**
     * @param DOMElement $node
     * @return string
     */
    private function _renderNode($node)
    {
        $result = '';
        switch (strtolower($node->localName)) {
            case 'figure':
                if (strpos($node->getAttribute('class'), 'inception:author-image') === false) {

                    $caption = $node->getElementsByTagName('figcaption')->item(0)->nodeValue;
                    if ($caption) {
                        $result .= '[caption align="alignleft" width="' . ($this->img_size[0] ? $this->img_size[0] : '') . '"]';
                    }

                    $result .= $this->_processNodes($node);
                    if ($caption) {
                        $result .= $caption . '[/caption]';
                    }
                } elseif (strpos($node->getAttribute('class'), 'inception:author-image') !== false) {

                    $figcaptionNode = $node->getElementsByTagName('figcaption')->item(0);
                    if ($figcaptionNode) {
                        $result .= $this->_processNodes($figcaptionNode);
                    }
                } else {
                    $result .= $this->_processNodes($node);
                }

                break;
            case 'figcaption':
                //nop
                break;
            case 'img':
                $img_style = $node->getAttribute('style');
                $src = !empty($node->getAttribute('src')) ? $node->getAttribute('src') : $node->getAttribute('href');
                $img_src = preg_replace("/{{url:([^}]*)}}/", "$1", $src);

                sQWordpressApi::Log($this->_getExternalId($node));
                $imgResult = wp_get_attachment_image_src($img_src, $this->img_size);
                sQWordpressApi::Log($imgResult);
                
                $result .= sprintf(
                    '<img class="img article img-article-%s" src="%s" style="width:%spx; height:%spx; %s" />',
                    get_option('sQWordPressAPI_img_fit', 'contain'),
                    $imgResult[0],
                    $this->img_size[0],
                    $this->img_size[1],
                    $img_style
                );
                break;
            case 'a':
                $result .= sprintf(
                    '<a class="%s" href="%s">',
                    $node->getAttribute('class'),
                    $node->getAttribute('href')
                );
                $result .= $this->_processNodes($node);
                $result .= sprintf("</%s>", $node->localName);
                break;
            case 'br':
                $result .= '<br>';
                break;
            case 'hr':
                $result .= '<hr>';
                break;
            case 'iframe':
                // When iframe is youtube movie strip the src parameters from the url,
                // the [embed] shortcode doesn't work with parameters in the src.
                $parentClass = $node->parentNode->getAttribute('class');
                $iframeSrc = $node->getAttribute('src');

                if ($parentClass == 'youtube' && strpos($iframeSrc, '?') !== false) {
                    $iframeSrc = substr($iframeSrc, 0, strpos($iframeSrc, '?'));
                }

                // embed the iframe src
                $result .= '[embed]' . $iframeSrc . '[/embed]';
                break;

            case 'title':
                $result .= sprintf('<h1 class="article-title %s">', $node->localName);
                $result .= $this->_processNodes($node);
                $result .= sprintf("</h1>");
                break;

            default:
                $result .= sprintf('<%s class="%s">', $node->localName, $node->getAttribute('class'));
                $result .= $this->_processNodes($node);
                $result .= sprintf("</%s>", $node->localName);
        }
        return $result;
    }

    /**
     * @param DOMElement $node
     * @return string
     */
    private function _processNodes($node)
    {
        $result = '';
        /** @var DOMElement $childNode */
        foreach ($node->childNodes as $childNode) {
            if ($childNode->nodeType == XML_TEXT_NODE) {
                $result .= trim($childNode->nodeValue, "\n\r\t");
            } else {
                if (!$this->filterNode($childNode))
                    $result .= $this->_renderNode($childNode);
            }
        }
        return $result;
    }

    /***
     * @param $xml
     * @return $this
     * @throws Exception
     */
    public function setXml($xml)
    {
        $this->xml = new DOMDocument('1.0', 'UTF-8');
        $this->xml->loadXML($xml);
        $xpath = new DOMXPath($this->xml);
        $this->type = "Undefined";

        if (!sQWordpressApi::$publicationConfigs)
            sQWordpressApi::Fail('Publication configurations not initialized.');

        //Getting the Publication Configuration for the current XML
        try {
            $nodes = $xpath->query('/Root/@type');
            if (empty($nodes) || count($nodes) <= 0 || empty($nodes[0]->value)) {
                sQWordpressApi::Fail('Document doesn\'t have an export document type defined.');
            }

            $this->type = $nodes[0]->value;
            switch (strtoupper($this->type)) {
                case 'AWS':
                    $this->config = sQWordpressApi::$publicationConfigs['digital'];
                    $this->type = 'digital';
                    break;
                case 'WCML':
                    $this->config = sQWordpressApi::$publicationConfigs['wcml'];
                    $this->type = 'wcml';
                    break;
                case 'PONE':
                    $this->config = sQWordpressApi::$publicationConfigs['pone'];
                    $this->type = 'pone';
                    break;
                default:
                    if (array_key_exists($this->type, sQWordpressApi::$publicationConfigs)) {
                        $this->config = sQWordpressApi::$publicationConfigs[$this->type];
                    }
            }
        } catch (Exception $exception) {
            sQWordpressApi::Fail(sprintf('Problem with the export type of the document. Type: %s. Error: %s', $this->type, $exception->getMessage()));
        }

        if (empty($this->config)) {
            sQWordpressApi::Fail(sprintf('Export type is not configured. Type:%s', $this->type));
        }

        return $this;
    }

    /***
     * @param $storyId
     * @return $this
     */
    public function setId($storyId)
    {
        $this->storyId = $storyId;
        return $this;
    }
}
