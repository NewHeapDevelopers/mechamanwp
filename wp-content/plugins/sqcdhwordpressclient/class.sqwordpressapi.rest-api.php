<?php

class sQWordpressApi_REST_API extends WP_REST_Controller
{
    public static function init()
    {
        $namespace = 'publiqare';
        $base_logon = 'auth/logon';
        $base_logoff = 'auth/logoff';
        $base_asset = 'asset';
        $base_asset_meta = 'asset/metadata';
        $base_story = 'story';


        if (!function_exists('register_rest_route')) {
            show_message('The REST API wasn\'t integrated into core until 4.4.');
            return false;
        }

        //User logon Authentication
        register_rest_route($namespace, '/' . $base_logon, array(
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array('sQWordpressApi_REST_API', 'logon_operation'),
                'permission_callback' => '__return_true',

            )
        ));
        //User logoff Authentication
        register_rest_route($namespace, '/' . $base_logoff, array(
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array('sQWordpressApi_REST_API', 'logoff_operation'),
                'permission_callback' => '__return_true'
            )
        ));

        //Add asset into wp media
        register_rest_route($namespace, '/' . $base_asset, array(
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array('sQWordpressApi_REST_API', 'add_asset_to_wpmedia'),
                'permission_callback' => array('sQWordpressApi_REST_API', 'privileged_permission_callback')

            )
        ));

        //Update and delete asset
        register_rest_route($namespace, '/' . $base_asset . '(?:/(?P<id>\d+))?', array(
            array(
                'methods' => WP_REST_Server::EDITABLE,
                'callback' => array('sQWordpressApi_REST_API', 'update_asset_to_wpmedia'),
                'permission_callback' => array('sQWordpressApi_REST_API', 'privileged_permission_callback'),
                'args' => [
                    'id'
                ]
            ),
            array(
                'methods' => WP_REST_Server::DELETABLE,
                'permission_callback' => array('sQWordpressApi_REST_API', 'privileged_permission_callback'),
                'callback' => array('sQWordpressApi_REST_API', 'delete_asset_to_wpmedia'),
                'args' => [
                    'id'
                ]
            )
        ));

        //Update Asset Metadata
        register_rest_route($namespace, '/' . $base_asset_meta . '(?:/(?P<id>\d+))?', array(
            array(
                'methods' => WP_REST_Server::EDITABLE,
                'permission_callback' => array('sQWordpressApi_REST_API', 'privileged_permission_callback'),
                'callback' => array('sQWordpressApi_REST_API', 'update_asset_metadata'),
                'args' => [
                    'id'
                ]
            )
        ));

        // Insert post
        register_rest_route($namespace, '/' . $base_story, array(
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'permission_callback' => array('sQWordpressApi_REST_API', 'privileged_permission_callback'),
                'callback' => array('sQWordpressApi_REST_API', 'add_new_post'),
            )
        ));

        //Update and Delete Post
        register_rest_route($namespace, '/' . $base_story . '(?:/(?P<id>\d+))?', array(
            array(
                'methods' => WP_REST_Server::EDITABLE,
                'permission_callback' => array('sQWordpressApi_REST_API', 'privileged_permission_callback'),
                'callback' => array('sQWordpressApi_REST_API', 'update_post_function'),
                'args' => [
                    'id'
                ]
            ),
            array(
                'methods' => WP_REST_Server::DELETABLE,
                'permission_callback' => array('sQWordpressApi_REST_API', 'privileged_permission_callback'),
                'callback' => array('sQWordpressApi_REST_API', 'delete_post_function'),
                'args' => [
                    'id'
                ]
            )
        ));
    }

    public static function logon_operation($request)
    {
        $username = $request->get_param('name');
        $password = $request->get_param('password');

        \NewHeap\Theme\Logger\Logger::write_woodwing_log('Login: ' . $username . ' ' . $password);

        try {
            $user = wp_signon([
                'user_login' => $username,
                'user_password' => $password,
                'remember' => false,
            ], false);

            if ($user && $user->ID != 0) {
                $new_access_token = md5(uniqid(rand(), true));
                $access_token_meta = get_user_meta($user->ID, SQWORDPRESSPOSTPLUGIN_META_ACCESS_TOKEN);
                $access_token_meta = $access_token_meta ? $access_token_meta[0] ? $access_token_meta[0] : null : null;

                $expiring_meta = get_user_meta($user->ID, SQWORDPRESSPOSTPLUGIN_META_EXPIRING_TOKEN);
                $expiring_meta = $access_token_meta ? $expiring_meta[0] ? $expiring_meta[0] : null : null;
                $expiring = strtotime(sprintf('now + %s minutes', SQWORDPRESSPOSTPLUGIN_EXPIRING_TOKEN));

                if (!empty($access_token_meta)) {
                    update_user_meta($user->ID, SQWORDPRESSPOSTPLUGIN_META_ACCESS_TOKEN, $new_access_token);
                    update_user_meta($user->ID, SQWORDPRESSPOSTPLUGIN_META_EXPIRING_TOKEN, $expiring);
                } else {
                    add_user_meta($user->ID, SQWORDPRESSPOSTPLUGIN_META_ACCESS_TOKEN, $new_access_token, true);
                    add_user_meta($user->ID, SQWORDPRESSPOSTPLUGIN_META_EXPIRING_TOKEN, $expiring, true);
                }

                return rest_ensure_response(array(
                    "status" => 200,
                    "success" => true,
                    "access_token" => $new_access_token
                ));
            } else {
                sQWordpressApi::Fail("Invalid Credentials.", 401);
            }
            
        } catch (Exception $exception) {
            return sQWordpressApi::FailResponse(array(
                "status" => $exception->getCode(),
                "success" => false,
                "message" => $exception->getMessage()
            ));
        }
    }

    public static function logoff_operation(WP_REST_Request $request)
    {
        try {
            global $wpdb;
            $authenticatedUser = self::_getAuthorization();
            $wpdb->last_error = '';

            delete_user_meta($authenticatedUser->ID, SQWORDPRESSPOSTPLUGIN_META_ACCESS_TOKEN);
            delete_user_meta($authenticatedUser->ID, SQWORDPRESSPOSTPLUGIN_META_EXPIRING_TOKEN);

            if (empty(get_user_meta($authenticatedUser->ID, SQWORDPRESSPOSTPLUGIN_META_ACCESS_TOKEN))) {
                return rest_ensure_response(array(
                    "status" => 200,
                    "success" => true,
                ));
            } else {
                sQWordpressApi::Fail("Couldn\'t Logoff. " .  $wpdb->last_error !== '' ? $wpdb->last_error: '', 401);
            }
        } catch (Exception $exception) {
            return sQWordpressApi::FailResponse(array(
                "status" => $exception->getCode(),
                "success" => false,
                "message" => $exception->getMessage()
            ));
        }

    }

    protected static function _getAuthorization()
    {
        global $wpdb;
        $access_token = self::_getAuthorizationHeader();

	    \NewHeap\Theme\Logger\Logger::write_woodwing_log('_getAuthorization1: ' . json_encode($access_token));
	    \NewHeap\Theme\Logger\Logger::write_woodwing_log('_getAuthorization2: ' . json_encode($_POST));

        if ($access_token) {
            $query = $wpdb->prepare("SELECT user_id FROM {$wpdb->usermeta} WHERE ({$wpdb->usermeta}.meta_key = %s AND {$wpdb->usermeta}.meta_value = '%s')", SQWORDPRESSPOSTPLUGIN_META_ACCESS_TOKEN, $access_token);
            $user_id = $wpdb->get_var($query);

            $user = get_user_by('id', $user_id);
            if ($user_id && $user_id != 0 && $user->ID != 0) {
                $query = $wpdb->prepare("SELECT meta_value FROM {$wpdb->usermeta} WHERE ({$wpdb->usermeta}.meta_key = %s and user_id = %s)", SQWORDPRESSPOSTPLUGIN_META_EXPIRING_TOKEN, $user_id);
                $expiring_meta = $wpdb->get_var($query);

                if ($expiring_meta >= strtotime('now')) {
                    $result = wp_signon([
                        'user_login' => $user->user_login,
                        'remember' => false,
                    ], false);

                    if (!empty($result) && !is_wp_error($result)) {
                        return $result;
                    }

                    return $user;
                } else {
                    sQWordpressApi::Fail('Session Expired', 401);
                }
            } else {
                sQWordpressApi::Fail('Invalid Authentication Token.', 401);
            }
        } else {
            sQWordpressApi::Fail('Authentication Token Required.', 401);
        }
    }

    protected static function _getAuthorizationHeader()
    {
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
	        \NewHeap\Theme\Logger\Logger::write_woodwing_log($headers, '_getAuthorizationHeader1');
        } else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
	        \NewHeap\Theme\Logger\Logger::write_woodwing_log($headers, '_getAuthorizationHeader2');
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
	        \NewHeap\Theme\Logger\Logger::write_woodwing_log($requestHeaders, '_getAuthorizationHeader3');
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
	        \NewHeap\Theme\Logger\Logger::write_woodwing_log($requestHeaders, '_getAuthorizationHeader4');
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }

        \NewHeap\Theme\Logger\Logger::write_woodwing_log($_SERVER, '_getAuthorizationHeader5');

        if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
            $access_token = $matches[1];
            return $access_token;
        }

        return null;
    }

    public static function delete_post_function(WP_REST_Request $request)
    {
        try {
            $authenticatedUser = self::_getAuthorization();
            $storyId = $request->get_param("id");
            $result = wp_delete_post($storyId);

	        // TODO: added for debugging
	        \NewHeap\Theme\Logger\Logger::write_woodwing_log($storyId, 'delete_post_function');

            if (!$result || is_wp_error($result))
                sQWordpressApi::Fail("Couldn\'t Update Post." . is_wp_error($result) ? $result->get_error_message() : '', 500);

            return rest_ensure_response(array(
                "status" => 200,
                "success" => true
            ));
        } catch (Exception $exception) {
            return sQWordpressApi::FailResponse(array(
                "status" => $exception->getCode(),
                "success" => false,
                "message" => $exception->getMessage()
            ));
        }
    }

    public static function update_post_function(WP_REST_Request $request)
    {
        try {
            $authenticatedUser = self::_getAuthorization();
            $storyId = $request->get_param("id");
            $xmlRaw = $request->get_body();

	        // TODO: added for debugging
	        \NewHeap\Theme\Logger\Logger::write_woodwing_log($xmlRaw, 'update_post_function');

            $postOutput = Post::Create()
                ->setXml($xmlRaw)
                ->setId($storyId)
                ->Process();

            sQWordpressApi::Log('Post Trying To Update:' . print_r($postOutput, true));

            $result = wp_update_post($postOutput);
            if (empty($result) || is_wp_error($result))
                sQWordpressApi::Fail("Couldn\'t Update Post." . is_wp_error($result) ? $result->get_error_message() : '', 500);

            if (!empty($postOutput['post_tags']))
                wp_set_post_tags($result, $postOutput['post_tags'], false);
            else
                wp_set_post_tags($result, '', false);

            if (!empty($postOutput['post_featured_img']))
                set_post_thumbnail($storyId, $postOutput['post_featured_img']);
            else
                delete_post_thumbnail($storyId);


                
            // ######### AgriMedia 10-03-2021 Start

            if (!empty($postOutput['post_uitgavenaam'])){
                wp_set_post_terms( $result, $postOutput['post_uitgavenaam'], 'uitgaven' );
            }
            
            // ######### AgriMedia 10-03-2021 Einde


            sQWordpressApi::addPostMessage(sprintf(
                'The article \'%s\' was updated at %s. See the changes <a href="%s" target="_blank">here</a>.',
                $postOutput['post_title'], 
                date('d-m-Y H:i:s', strtotime($postOutput['post_date'])),
                get_permalink($postOutput['ID'], true)
            ), $postOutput['post_title']);
            
            return rest_ensure_response(array(
                "status" => 200,
                "success" => true
            ));

        } catch (Exception $exception) {
            return sQWordpressApi::FailResponse(array(
                "status" => $exception->getCode(),
                "success" => false,
                "message" => $exception->getMessage()
            ));
        }
    }

    public static function add_new_post(WP_REST_Request $request)
    {
        try {
            $authenticatedUser = self::_getAuthorization();
            $xmlRaw = $request->get_body();

	        // TODO: added for debugging
	        \NewHeap\Theme\Logger\Logger::write_woodwing_log($xmlRaw, 'add_new_post');

            $postOutput = Post::Create()
                ->setXml($xmlRaw)
                ->Process();

            sQWordpressApi::Log('Post Create:' . print_r($postOutput, true));

            $result = wp_insert_post($postOutput, false);
            if (empty($result) || is_wp_error($result)) {
                sQWordpressApi::Fail("Couldn\'t add new post. " . is_wp_error($result) ? $result->get_error_message() : '', 500);
            }

            if (!empty($postOutput['post_tags']))
                wp_set_post_tags($result, $postOutput['post_tags'], false);
            if (!empty($postOutput['post_featured_img']))
                set_post_thumbnail($result, $postOutput['post_featured_img']);




            // ######### AgriMedia 10-03-2021 Start

            if (!empty($postOutput['post_uitgavenaam'])){
                wp_set_post_terms( $result, $postOutput['post_uitgavenaam'], 'uitgaven' );
            }            

            // ######### AgriMedia 10-03-2021 Einde
            



            sQWordpressApi::addPostMessage(sprintf(
                'The article \'%s\' was posted at %s. See the post <a href="%s" target="_blank">here</a>.',
                $postOutput['post_title'],
                date('d-m-Y H:i:s', strtotime($postOutput['post_date'])),
                get_permalink($postOutput['ID'], true)
            ), $postOutput['post_title']);

            return rest_ensure_response(array(
                "status" => 200,
                "success" => true,
                'postid' => $result
            ));
        } catch (Exception $exception) {
            return sQWordpressApi::FailResponse(array(
                "status" => $exception->getCode(),
                "success" => false,
                "message" => $exception->getMessage()
            ));
        }
    }

    public static function update_asset_metadata(WP_REST_Request $request)
    {
        try {
            $authenticatedUser = self::_getAuthorization();
            $asset_id = $request->get_param("id");
            $params = sQWordpressApi::getParsedParams($request);

	        // TODO: added for debugging
	        \NewHeap\Theme\Logger\Logger::write_woodwing_log($params, 'update_asset_metadata');

            if (!empty($params['metadata'])) {
                $metadata = $params['metadata'];
            } else {
                sQWordpressApi::Fail("Metadata not provided for asset metadata update.Update stopped.", 500);
            }

            $result = wp_update_post(array(
                'ID' => $asset_id,
                'post_type' => 'attachment',
                'post_name' => sanitize_title_with_dashes(str_replace("_", "-", basename($metadata->filename))),
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($metadata->filename)),
                'post_author' => $authenticatedUser->ID,
                'post_excerpt' => $metadata->caption,
                'post_content' => $metadata->filename,
                'post_modified' => current_time('mysql'),
                'post_modified_gmt' => current_time('mysql'),
            ));

            if (empty($result) || is_wp_error($result)) {
                sQWordpressApi::Fail("Coundn\'t update asset metadata.", 500);
            }

            return rest_ensure_response(array(
                "status" => 200,
                "success" => true,
            ));
        } catch (Exception $exception) {
            return sQWordpressApi::FailResponse(array(
                "status" => $exception->getCode(),
                "success" => false,
                "message" => $exception->getMessage()
            ));
        }
    }

    public static function add_asset_to_wpmedia(WP_REST_Request $request)
    {
        try {
            $authenticatedUser = self::_getAuthorization();
            $params = sQWordpressApi::getParsedParams($request);

	        // TODO: added for debugging
	        \NewHeap\Theme\Logger\Logger::write_woodwing_log($params, 'add_asset_to_wpmedia');

            if (!empty($params)) {
                $metadata = $params['metadata'];
            } else {
                sQWordpressApi::Fail("Metadata not provided when adding asset to WordPress.", 500);
            }

            if (!function_exists('wp_handle_upload')) {
                require_once(ABSPATH . 'wp-admin/includes/file.php');
            }
            if (!function_exists('wp_crop_image')) {
                include(ABSPATH . 'wp-admin/includes/image.php');
            }

            //require_once(ABSPATH . 'wp-admin/includes/media.php');

            $_FILES['file']['name'] = $metadata->filename ? $metadata->filename : $_FILES['file']['name'];
            $wp_upload_dir = wp_upload_dir();
            $upload = wp_handle_upload(
                $_FILES['file'],
                array(
                    'mimes' => array(
                        'bmp' => 'image/bmp',
                        'gif' => 'image/gif',
                        'jpe' => 'image/jpeg',
                        'jpeg' => 'image/jpeg',
                        'jpg' => 'image/jpeg',
                        'png' => 'image/png',
                        'tif' => 'image/tiff',
                        'tiff' => 'image/tiff'
                    ),
                    'test_form' => false
                ));
            remove_filter('upload_dir', array('sQWordpressApi_REST_API', $wp_upload_dir));

            if (empty($upload) || is_wp_error($upload))
                sQWordpressApi::Fail("Couldn\'t finish upload for new asset."
                . is_wp_error($upload) ? $upload->get_error_message() : '', 500);


            // File uploaded successfully.
            $uploadedFilepath = $upload['url'];
            $uploadedFileName = basename($upload['url']);
            $filename = $wp_upload_dir['path'] . '/' . $uploadedFileName;

            // Check the type of file. We'll use this as the 'post_mime_type'.
            $filetype = wp_check_filetype(basename($filename), null);

            // Prepare an array of post data for the attachment.
            $attachment = array(
                'guid' => $wp_upload_dir['url'] . '/' . basename($filename),
                'post_mime_type' => $filetype['type'],
                'post_name' => sanitize_title_with_dashes(str_replace("_", "-", basename($filename))),
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
                'post_content' => $metadata->caption,
                'post_excerpt' => $metadata->caption,
                'post_author' => $authenticatedUser->ID,
                'post_type' => 'attachment',
                'post_date' => current_time('mysql'),
                'post_date_gmt' => current_time('mysql'),
                'post_modified' => current_time('mysql'),
                'post_modified_gmt' => current_time('mysql'),
                'post_status' => 'inherit',
            );

            // Insert the attachment.
            $attach_id = wp_insert_attachment($attachment, $filename);

            if (is_wp_error($attach_id))
                sQWordpressApi::Fail("Error finishing upload for new asset. "
                    . $attach_id->get_error_message(), 500);

            $attachment_data = wp_generate_attachment_metadata($attach_id, $filename);
            wp_update_attachment_metadata($attach_id, $attachment_data);

            return rest_ensure_response(array(
                "status" => 200,
                "success" => true,
                "assetid" => $attach_id
            ));
        } catch (Exception $exception) {
            return sQWordpressApi::FailResponse(array(
                "status" => $exception->getCode(),
                "success" => false,
                "message" => $exception->getMessage()
            ));
        }
    }

    public static function update_asset_to_wpmedia(WP_REST_Request $request)
    {
        try {
            $authenticatedUser = self::_getAuthorization();
            $asset_id = $request->get_param("id");
            $params = sQWordpressApi::getParsedParams($request);

	        // TODO: added for debugging
	        \NewHeap\Theme\Logger\Logger::write_woodwing_log($params, 'update_asset_to_wpmedia');

            if (!empty($params)) {
                $metadata = $params['metadata'];
            } else {
                sQWordpressApi::Fail("Metadata not provided when updating asset {$asset_id}.", 500);
            }

            //require_once(ABSPATH . 'wp-admin/includes/media.php');
            if (!function_exists('wp_handle_upload')) {
                require_once(ABSPATH . 'wp-admin/includes/file.php');
            }

            if (!function_exists('wp_crop_image')) {
                include(ABSPATH . 'wp-admin/includes/image.php');
            }

            $wp_upload_dir = wp_upload_dir();
            $_FILES['file']['name'] = $metadata->filename ? $metadata->filename : $_FILES['file']['name'];
            $upload = wp_handle_upload($_FILES['file'], array(
                'mimes' => array(
                    'bmp' => 'image/bmp',
                    'gif' => 'image/gif',
                    'jpe' => 'image/jpeg',
                    'jpeg' => 'image/jpeg',
                    'jpg' => 'image/jpeg',
                    'png' => 'image/png',
                    'tif' => 'image/tiff',
                    'tiff' => 'image/tiff'
                ),
                'test_form' => false
            ));

            remove_filter('upload_dir', array('sQWordpressApi_REST_API', $wp_upload_dir));

            if (empty($upload || is_wp_error($upload)))
                sQWordpressApi::Fail("Couldn\'t start upload for update asset: {$asset_id}."
                . is_wp_error($upload) ? $upload->get_error_message() : '', 500);


            $uploadedFileName = basename($upload['url']);
            $filename = $wp_upload_dir['path'] . '/' . $uploadedFileName;
            $filetype = wp_check_filetype(basename($filename), null);

            $attachment = array(
                'ID' => $asset_id,
                'post_author' => $authenticatedUser->ID,
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($metadata->filename)),
                'post_name' => sanitize_title_with_dashes(str_replace("_", "-", basename($metadata->filename))),
                'post_modified' => current_time('mysql'),
                'post_modified_gmt' => current_time('mysql'),
                'guid' => $wp_upload_dir['url'] . '/' . basename($filename),
                'post_mime_type' => $filetype['type'],
                'post_status' => 'inherit',
                'post_excerpt' => $metadata->caption,
                'post_content' => $metadata->filename,
            );
            $result = wp_insert_attachment($attachment, $filename);

            if (is_wp_error($result))
                sQWordpressApi::Fail("Couldn\'t finish upload for update asset: {$asset_id}." .
                is_wp_error($result) ? $result->get_error_message() : '', 500);

            wp_update_attachment_metadata($result, wp_generate_attachment_metadata($result, $filename));

            return rest_ensure_response(array(
                "status" => 200,
                "success" => true
            ));
        } catch (Exception $exception) {
            return sQWordpressApi::FailResponse(array(
                "status" => $exception->getCode(),
                "success" => false,
                "message" => $exception->getMessage()
            ));
        }
    }

    public static function delete_asset_to_wpmedia(WP_REST_Request $request)
    {
        try {
            $authenticatedUser = self::_getAuthorization();
            $asset_id = $request->get_param("id");

	        // TODO: added for debugging
	        \NewHeap\Theme\Logger\Logger::write_woodwing_log($asset_id, 'delete_asset_to_wpmedia');

            $del_post = delete_post_meta($asset_id, '_wp_attachment_metadata');
            $result = wp_delete_attachment($asset_id);

            if (is_wp_error($result))
                sQWordpressApi::Fail("Couldn\'t delete asset." . $result->get_error_message(), 500);

            return rest_ensure_response(array(
                "status" => 200,
                "success" => true
            ));
        } catch (Exception $exception) {
            return sQWordpressApi::FailResponse(array(
                "status" => $exception->getCode(),
                "success" => false,
                "message" => $exception->getMessage()
            ));
        }
    }

    public static function privileged_permission_callback()
    {
        //return current_user_can('edit_posts');
        return true;
    }
}
