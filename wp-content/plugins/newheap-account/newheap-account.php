<?php
/**
 * Plugin Name: NewHeap account
 * Plugin URI: https://www.newheap.com
 * Description: Account functionalities
 * Version: 1.0.0
 * Author: NewHeap
 * Author URI: https://www.newheap.com
 * Developer: Ferhat Ileri
 * Developer URI: https://www.newheap.com
 * Text Domain: newheap
 * Domain Path: /languages
 */

// Prevent direct access
if (!defined('ABSPATH')) {
    exit;
}

require_once 'NewheapAccount.php';

if (!function_exists('dd')) {
    function dd($dump_data)
    {
        if (is_admin()) {
            echo '<pre style="padding-left: 200px">';
        } else {
            echo '<pre class="text-left">';
        }

        var_dump($dump_data);

	    echo debug_backtrace()[1]['function'];

        echo '</pre>';
    }
}

// Instantiate the plugin
new \Newheap\Plugins\NewheapAccount\NewheapAccount();
