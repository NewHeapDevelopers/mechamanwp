<?php
namespace Newheap\Plugins\NewheapAccount\User;

use Codio\CodioAuth\Zeno\Zeno;
use NewHeap\Theme\Logger\Logger;
use NewHeap\Theme\Multisite\Multisite;
use threewp_broadcast\ajax\json;

class UserRegistration
{
	private $error;

	private $company_name;
	private $gender;
	private $initials;
	private $prefix;
	private $street_name;
	private $house_number;
	private $house_number_addition;
	private $zip;
	private $city;
	private $country;
	private $mobile_phone;
	private $phone;
	private $newsletter;
	private $invoice_email;
	private $payment_method;
	private $account_number;
	private $account_holder_name;
	private $first_name;
	private $last_name;
	private $user_login;
	private $email;
	private $user_id;
	private $errors;

	private $payment_id;

    public function __construct()
    {
    	$this->init();
    }

    public function init()
    {
	    if (isset($_POST['submit_registration'])) {
		    add_action('init', [$this, 'registration_validation']);
	    }
    }

	/**
	 * Validate registration data
	 *
	 * @author Ferhat Ileri <ferhat@newheap.com>
	 */
	public function registration_validation()
	{
		$this->update_user_meta();
		$this->errors = new \WP_Error;

		if (empty(self::check_if_zeno_user())) {
			if ($_POST['user_type'] !== 'gratis') {
				$this->first_name = esc_attr(strtolower($_POST['first_name']));
				$this->last_name = esc_attr(strtolower($_POST['last_name']));
				$this->company_name = esc_attr(strtolower($_POST['company_name']));
				$this->gender = esc_attr(strtolower($_POST['gender']));
				$this->initials = esc_attr(strtolower($_POST['initials']));
				$this->prefix = esc_attr(strtolower($_POST['prefix']));
				$this->street_name = esc_attr(strtolower($_POST['street_name']));
				$this->house_number = esc_attr(strtolower($_POST['house_number']));
				$this->house_number_addition = esc_attr(strtolower($_POST['house_number_addition']));
				$this->zip = esc_attr(strtolower($_POST['zip']));
				$this->city = esc_attr(strtolower($_POST['city']));
				$this->country = esc_attr(strtolower($_POST['country']));
				$this->mobile_phone = esc_attr(strtolower($_POST['mobile_phone']));
				$this->phone = esc_attr(strtolower($_POST['phone']));
				$this->newsletter = esc_attr(strtolower($_POST['newsletter']));
				$this->invoice_email = esc_attr(strtolower($_POST['invoice_email']));
				$this->payment_method = esc_attr($_POST['payment']);
				$this->account_number = esc_attr(strtolower($_POST['account_number']));
				$this->account_holder_name = esc_attr(strtolower($_POST['account_holder_name']));
                $this->email = esc_attr($_POST['email']);
                $this->user_login = esc_attr($_POST['user_login']);

				// Username validations
				if (empty($this->first_name)) {
					$this->errors->add('first_name', __('Voornaam is een verplicht veld.', 'newheap'));
				}

				// Username validations
				if (empty($this->last_name)) {
					$this->errors->add('last_name', __('Achternaam is een verplicht veld.', 'newheap'));
				}

				// Email validations
				if (!is_email($this->email)) {
					$this->errors->add('email', __('U heeft een ongeldig e-mailadres ingevoerd', 'newheap'));
				}
				if (email_exists($this->email)) {
					$this->errors->add('email', __('Dit e-mailadres is al gebruik. <a href="'.get_field('profile_page', 'option').'" target="_blank">Log in</a> om je abonnement te bekijken en een nieuw abonnement aan te vragen.', 'newheap'));
				}
				if (username_exists($this->user_login)) {
					$this->errors->add('user_login', __('Deze gebruikersnaam is al gebruik. <a href="'.get_field('profile_page', 'option').'" target="_blank">Log in</a> om je abonnement te bekijken en een nieuw abonnement aan te vragen.', 'newheap'));
				}
			}

			$this->user_login = esc_attr($_POST['user_login']);
			$this->email = esc_attr($_POST['email']);
		}



		$GLOBALS['registration_errors'] = $this->errors;

		if (!count($this->errors->get_error_messages())) {
			if ($_POST['user_type'] !== 'gratis') {
				$zeno = new Zeno();

				// Create payment so we have the payment ID;
				if ($_POST['payment'] == 3) {
					$checkout_url = $this->start_ideal_payment();
				}

				// Check if user already has a Zeno user by checking if the user has a subscription for LM/TPT/VT
				if ($relation_number = self::check_if_zeno_user()) {
					$zeno_data = $zeno->create_subscription($relation_number, $this->payment_id);

					Logger::write_zeno_log('zeno data: ' .json_encode($zeno_data));

					$subscription_number = $zeno_data['subscription_number'];
					$subscription_code = $zeno_data['subscription_code'];
				} else {
					Logger::write_zeno_log('Before zeno user create');
					$zeno_data = $zeno->create_person($this->payment_id);
					Logger::write_zeno_log('After zeno user create: ' . json_encode($zeno_data));

					// Check if the user is created in Zeno
					if (empty($zeno_data)) {
						Logger::write_zeno_log('Kan gebruiker niet aanmaken');
						$this->errors->add('user_create_failed', __('Kon gebruiker niet aanmaken', 'newheap'));
					}

					// Stop if we have errors
					if ($this->errors->has_errors()) {
						return;
					}

					$relation_number = $zeno_data['relation_number'];
					$subscription_number = $zeno_data['subscription_number'];
					$subscription_code = $zeno_data['subscription_code'];

					// Create WP User with meta data
					$this->user_id = User::create_user();
					Logger::write_zeno_log('WP user create: ' . json_encode($this->user_id));
				}

				Logger::write_zeno_log('Before user meta update: ' . json_encode($relation_number) . json_encode($subscription_number) . json_encode($subscription_code));

				// Update user meta
				$this->update_user_meta($relation_number, $subscription_number, $subscription_code);

				Logger::write_zeno_log('updated user metae');
				User::get_zeno_data($this->user_id, true);

				Logger::write_zeno_log('Before ideal payment');

				// Start iDeal payment if chosen
				if ($_POST['payment'] == 3) {
					header("Location: " . $checkout_url, true, 303); // 303 as documented by Mollie api php
					exit;
				}
			} else {
				Logger::write_zeno_log('Before WP user create free');
				$this->user_id = User::create_user();
				Logger::write_zeno_log('After WP user create free: ' . json_encode($this->user_id));

			}
		} else {
			Logger::write_zeno_log($this->errors->get_error_messages());
		}

		if ($this->user_id) {

            $newLetterResponse = "";
            if ($_POST['newsletter']) {
                require_once get_template_directory() . '/NewHeap/Newsletter/Newsletter.php';
                $newsletter = new \NewHeap\Theme\Newsletter\Newsletter();
                $newLetterResponse = $newsletter->addEmail($this->email, get_current_blog_id(), $_POST['first_name'] . ' ' . $_POST['last_name']);

                Logger::write_zeno_log($newLetterResponse);
            }

            //Send mail to Ebo and Gerard
            $to = array(
                'gvanbraak@agrimedia.nl',
                'evanderbroek@agrimedia.nl',
                'marcel@newheap.com',
	            'mail@agrimedia.nl'
            );

            $subject = 'Nieuwe registratie op website';

            $message = 'Er is een nieuwe registratie op de website binnengekomen met de volgende gegevens:<br /><br />';
            $message .= 'Email: ' . esc_attr(($_POST['email'])) . '<br />';
            $message .= 'Login: ' . esc_attr(($_POST['user_login'])) . '<br />';
            $message .= 'Type: ' . esc_attr(($_POST['user_type'])) . '<br />';
            $message .= 'Initialen: ' . esc_attr(($_POST['initials'])) . '<br />';
            $message .= 'Voornaam: ' . esc_attr(($_POST['first_name'])) . '<br />';
            $message .= 'Tussenvoegsels: ' . esc_attr(($_POST['prefix'])) . '<br />';
            $message .= 'Achternaam: ' . esc_attr(($_POST['last_name'])) . '<br />';
            $message .= 'Bedrijfsnaam: ' . esc_attr(($_POST['company_name'])) . '<br />';
            $message .= 'Geslacht: ' . esc_attr(($_POST['gender'])) . '<br />';
            $message .= 'Straatnaam: ' . esc_attr(($_POST['street_name'])) . '<br />';
            $message .= 'Huisnummer: ' . esc_attr(($_POST['house_number'])) . '<br />';
            $message .= 'Huisnummer additie: ' . esc_attr(($_POST['house_number_addition'])) . '<br />';
            $message .= 'Postcode: ' . esc_attr(($_POST['zip'])) . '<br />';
            $message .= 'Stad: ' . esc_attr(($_POST['city'])) . '<br />';
            $message .= 'Land: ' . esc_attr(($_POST['country'])) . '<br />';
            $message .= 'Mobiel: ' . esc_attr(($_POST['mobile_phone'])) . '<br />';
            $message .= 'Telefoon: ' . esc_attr(($_POST['phone'])) . '<br />';
            $message .= 'Nieuwsbrief: ' . esc_attr($_POST['newsletter']) . '<br />';
            $message .= 'Factuuradres: ' . esc_attr($_POST['invoice_email']) . '<br />';
            $message .= 'Betaling: ' . esc_attr($_POST['payment']) . '<br />';
            $message .= 'Bankrekening: ' . esc_attr($_POST['account_number']) . '<br />';
            $message .= 'Account houder: ' . esc_attr($_POST['account_holder_name']) . '<br />';
            $message .= 'Newsletter response: ' . esc_attr($newLetterResponse) . '<br />';


            $headers = array( 'Content-Type: text/html; charset=UTF-8' );

            wp_mail( $to, $subject, $message, $headers);

            wp_clear_auth_cookie();
            wp_set_current_user ( $this->user_id );
            wp_set_auth_cookie  ( $this->user_id );

            Logger::write_zeno_log('User created so login and redirect');

			wp_redirect(get_field('registration_finished_page', 'option') ?? get_field('login_page', 'option') . '?registration=completed&type=' . esc_attr(($_POST['user_type'])));
			exit;
		}
	}

	public function update_user_meta($relation_number = null, $subscription_number = null, $subscription_code = null)
	{
		if (!self::check_if_zeno_user()) {
			$this->user_login = esc_attr($_POST['user_login']);
			$this->email = esc_attr($_POST['email']);

			if ($_POST['user_type'] !== 'gratis') {
				$this->company_name          = esc_attr( $_POST['company_name'] );
				$this->gender                = esc_attr( $_POST['gender'] );
				$this->initials              = esc_attr( $_POST['initials'] );
				$this->prefix                = esc_attr( $_POST['prefix'] );
				$this->street_name           = esc_attr( $_POST['street_name'] );
				$this->house_number          = esc_attr( $_POST['house_number'] );
				$this->house_number_addition = esc_attr( $_POST['house_number_addition'] );
				$this->zip                   = esc_attr( $_POST['zip'] );
				$this->city                  = esc_attr( $_POST['city'] );
				$this->country               = esc_attr( $_POST['country'] );
				$this->mobile_phone          = esc_attr( $_POST['mobile_phone'] );
				$this->phone                 = esc_attr( $_POST['phone'] );
				$this->newsletter            = esc_attr( $_POST['newsletter'] );

				$this->first_name = esc_attr( $_POST['first_name'] );
				$this->last_name  = esc_attr( $_POST['last_name'] );

				update_user_meta( $this->user_id, 'company_name', $this->company_name );
				update_user_meta( $this->user_id, 'gender', $this->gender );
				update_user_meta( $this->user_id, 'initials', $this->initials );
				update_user_meta( $this->user_id, 'prefix', $this->prefix );
				update_user_meta( $this->user_id, 'mobile_phone', $this->mobile_phone );
				update_user_meta( $this->user_id, 'phone', $this->phone );
				update_user_meta( $this->user_id, 'newsletter', $this->newsletter );

				update_user_meta( $this->user_id, 'street_name', $this->street_name );
				update_user_meta( $this->user_id, 'house_number', $this->house_number );
				update_user_meta( $this->user_id, 'house_number_addition', $this->house_number_addition );
				update_user_meta( $this->user_id, 'zip', $this->zip );
				update_user_meta( $this->user_id, 'city', $this->city );
				update_user_meta( $this->user_id, 'country', $this->country );
				update_user_meta( $this->user_id, 'relation_number', $this->account_holder_name );
			}
		} else {
			$this->user_id = get_current_user_id();
		}

		if ($_POST['user_type'] !== 'gratis') {
			$this->invoice_email = esc_attr($_POST['invoice_email']);
			$this->payment_method = esc_attr($_POST['payment']);
			$this->account_number = esc_attr($_POST['account_number']);
			$this->account_holder_name = esc_attr($_POST['account_holder_name']);

			update_user_meta($this->user_id, 'invoice_email' ,$this->invoice_email);
			update_user_meta($this->user_id, 'payment_method' ,$this->payment_method);
			update_user_meta($this->user_id, 'account_number' ,$this->account_number);
			update_user_meta($this->user_id, 'account_holder_name' ,$this->account_holder_name);

			if (!empty($relation_number)) {
				update_user_meta($this->user_id, 'relation_number', $relation_number);
			}

			if (!empty($subscription_number)) {
				$site_code = strtolower(Multisite::get_site_code());
				update_user_meta($this->user_id, 'subscription_' . $site_code, $subscription_number);
			}

			if (!empty($this->payment_id)) {
				update_user_meta($this->user_id, 'mollie_payment_id', $this->payment_id);
			}

			if (!empty($subscription_code)) {
				update_user_meta($this->user_id, $subscription_code['key'], $subscription_code['value']);
			}
		}
	}

	public function start_ideal_payment()
	{
		require_once __DIR__ . '/../vendor/mollie-api-php/vendor/autoload.php';

		$mollie_api_key = get_field('mollie_api_key', 'option');

		if ($_POST['user_type'] === 'plus') {
			$user_type = 'plus';
			$total_price = number_format(get_field('kosten_abonnement_plus', 'option'), 2, '.', '');
		} elseif ($_POST['user_type'] === 'totaal') {
			$user_type = 'totaal';
			$total_price = number_format(get_field('kosten_abonnement_totaal', 'option'), 2, '.', '');
		}


		if ($user_type === 'plus' || $user_type === 'totaal') {
			if ($mollie_api_key ) {
				$mollie = new \Mollie\Api\MollieApiClient();
				$mollie->setApiKey(get_field('mollie_api_key', 'option'));

				$payment_completed_page = get_field('registration_finished_page', 'option') ?? get_field('login_page', 'option');

				$payment = $mollie->payments->create([
					"amount" => [
						"currency" => "EUR",
						"value" => $total_price
					],
					"description" => 'Mechaman abonnement: ' . $user_type,
					"redirectUrl" => $payment_completed_page,
//					"webhookUrl"  => "https://webshop.example.org/mollie-webhook/",
				]);

				$this->payment_id = $payment->id;

				return $payment->getCheckoutUrl();
			}
		}
	}

	public static function check_if_zeno_user()
	{
		if (is_user_logged_in()) {
			$relation_number = get_user_meta(get_current_user_id(), 'relation_number', true);

			if ($relation_number) {
				return $relation_number;
			}
		}
	}
}
