<?php
namespace Newheap\Plugins\NewheapAccount\User;

use Codio\CodioAuth\Zeno\Zeno;
use Newheap\Plugin\NewheapAccount\Auth\Auth;
use Newheap\Plugins\NewheapAccount\NewheapAccount;
use NewHeap\Theme\Logger\Logger;
use NewHeap\Theme\Multisite\Multisite;
use Newheap\Theme\Notification\Notification;

class User
{
    const ZENO_EXPIRY_IN_SECONDS = '86400'; // 1 day
    const SUBSCRIPTION_LM = 'subscription_lm';
    const SUBSCRIPTION_VT = 'subscription_vt';
    const SUBSCRIPTION_TPT = 'subscription_tpt';
    const SUBSCRIPTION_LAST_CHECKED = 'subscription_last_checked';

    public function __construct()
    {
    	$this->init();
    }

    public function init()
    {
	    add_action('init', [$this, 'get_zeno_data']);
	    add_action('wp_login', [$this, 'has_valid_subscription']);
    }

    /**
     * Create user
     *
     * @param $username
     * @param $email
     * @param $password
     * @param $role
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public static function create_user()
    {
        $userdata = array(
            'user_login' => sanitize_user($_POST['user_login']),
            'user_email' => sanitize_email($_POST['email']),
            'user_pass' => $_POST['password'],
            'role' => 'subscriber',
        );

        $user_id = wp_insert_user($userdata);

        if (is_int($user_id)) {
        	self::update_user_meta($user_id);
        	self::get_zeno_data($user_id);

        	return $user_id;
        } elseif ($user_id instanceof \WP_Error) {
        	foreach ($user_id->errors as $error) {
		        Notification::add_error($error[0]);
	        }
        } else {
            Notification::add_error(__('Uw registratie is mislukt. Probeer het nogmaals of neem contact met ons op.', 'newheap'));
        }
    }

    /**
     * Update user
     *
     * @param $user_meta
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public static function update_user($user_meta)
    {
        $valid_nonce = wp_verify_nonce($_POST['update_profile'], 'update_profile_action');

        if (!isset($_POST['submit_profile_update']) || !$valid_nonce) {
            Logger::write_log('Invalid nonce for user ID: ' . get_current_user_id());
            Notification::add_error(__('Ongeldige data in formulier. Probeer het nogmaals of neem contact op met de beheerder', 'newheap'));
        } elseif (isset($_POST['submit_profile_update'])) {
            // Update profile
            foreach($user_meta as $meta_key => $meta_value) {
                update_user_meta(get_current_user_id(), $meta_key, $meta_value);
            }

            // Update password
            if ($user_meta['new_password']) {
                wp_set_password($user_meta['new_password'], get_current_user_id());
            }
            Notification::add_success(__('Uw profiel is bijgewerkt', 'newheap'));
            self::get_zeno_data(get_current_user_id(), true);
        }

	    Auth::redirect_to_profile_page();
    }

	/**
	 * 
	 *
	 * @param $user_id
	 * @param $user_meta
	 *
	 * @author Ferhat Ileri <ferhat@newheap.nl>
	 */
    public static function update_user_meta($user_id)
    {
	    update_user_meta($user_id, 'first_name', esc_attr($_POST['first_name']));
	    update_user_meta($user_id, 'last_name', esc_attr($_POST['last_name']));
	    update_user_meta($user_id, self::SUBSCRIPTION_LAST_CHECKED, 0);

	    $capabilities = [
	    	'subscriber' => true
	    ];

	    /** @var \WP_Site $site */
	    foreach (get_sites() as $site) {
	    	// Skip first blog as this is already set
	    	if (intval($site->blog_id) === 1) {
	    		continue;
		    }

		    global $wpdb;

		    update_user_meta($user_id, $wpdb->prefix . '_' . $site->blog_id . '_capabilities', $capabilities);
	    }

	    // Check if we need to add subscriptions and add them as user_meta
	    if (!empty($_POST['subscription_lm'])) {
		    update_user_meta($user_id, self::SUBSCRIPTION_LM, esc_attr($_POST['subscription_lm']));
	    }
	    if (!empty($_POST['subscription_vt'])) {
		    update_user_meta($user_id, self::SUBSCRIPTION_VT, esc_attr($_POST['subscription_vt']));
	    }
	    if (!empty($_POST['subscription_tpt'])) {
		    update_user_meta($user_id, self::SUBSCRIPTION_TPT, esc_attr($_POST['subscription_tpt']));
	    }
    }

	/**
	 * Check if we need to sync with Zeno, if so start sync
	 *
	 * @param null $user_id
	 *
	 * @author Ferhat Ileri <ferhat@newheap.nl>
	 */
    public static function get_zeno_data($user_id = null, $force = false)
    {
	    if (!$user_id) {
		    $user_id = get_current_user_id();
	    }

	    $user = new \WP_User($user_id);

	    if ($user instanceof \WP_User && !empty($user->ID)) {
		    $last_checked = intval(get_user_meta($user->ID, self::SUBSCRIPTION_LAST_CHECKED, true));

		    // Check if the Zeno data is expired in the WP DB
		    if ((time() - $last_checked) >= self::ZENO_EXPIRY_IN_SECONDS || $force) {
			    $subscription_lm_abo_number = get_user_meta($user->ID, self::SUBSCRIPTION_LM, true);
			    $subscription_vt_abo_number = get_user_meta($user->ID, self::SUBSCRIPTION_VT, true);
			    $subscription_tpt_abo_number = get_user_meta($user->ID, self::SUBSCRIPTION_TPT, true);

			    if (!empty($subscription_lm_abo_number)) {
				    $zeno = new Zeno($user, 'lm');
				    $zeno->validate_subscription_number('lm');
			    }

			    if (!empty($subscription_vt_abo_number)) {
				    $zeno = new Zeno($user, 'vt');
				    $zeno->validate_subscription_number('vt');
			    }

			    if (!empty($subscription_tpt_abo_number)) {
				    $zeno = new Zeno($user, 'tpt');
				    $zeno->validate_subscription_number('tpt');
			    }

			    if (empty($subscription_lm_abo_number) || empty($subscription_vt_abo_number) || empty($subscription_tpt_abo_number)) {
			    	$relation_number = get_user_meta($user_id, 'relation_number', true);

			    	$zeno = new Zeno($user, Multisite::get_site_code());
			    	$zeno->get_subscription_details($relation_number);
			    }

			    update_user_meta($user->ID, self::SUBSCRIPTION_LAST_CHECKED, time());
		    }

		    // Check iDeal payment
		    self::validate_ideal_payment();
	    }
    }

	/**
	 * @param int|null $user_id
	 *
	 * @return bool
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
    public static function has_valid_subscription($user_id = null)
    {
	    if (!$user_id) {
		    $user_id = get_current_user_id();
	    }

	    $user = new \WP_User($user_id);

	    if (current_user_can('administrator') || current_user_can('editor')) {
	    	return true;
	    }

	    if ($user->ID) {
		    self::get_zeno_data($user->ID);
		    $site_code = Multisite::get_site_code();

		    $trial_days = get_field('gratis_premium_dagen', 'option');

		    // Check if within trial period
		    if ($trial_days) {
			    $registered_date = \DateTime::createFromFormat('Y-m-d H:i:s', $user->user_registered);
			    $current_date = new \DateTime();
			    $expiry_date = $registered_date->modify('+' . $trial_days . ' day');

			    if ($expiry_date >= $current_date) {
				    return true;
			    }
		    }

		    if ($site_code === Multisite::SITE_CODE_MECHAMAN) {
			    return true;
		    } else {
			    $subscription_expires = intval(get_user_meta($user->ID, 'subscription_' . $site_code . '_expires', true));

			    if (empty($subscription_expires)) {
			    	self::get_zeno_data(get_current_user_id(), true);
				    $subscription_expires = intval(get_user_meta($user->ID, 'subscription_' . $site_code . '_expires', true));
			    }

			    // Check if we have an expiry date. If so check if expired
			    if ($subscription_expires === Zeno::SUBSCRIPTION_HAS_NO_EXPIRY) {
				    return true;
			    } elseif ($subscription_expires && $subscription_expires > time()) {
				    return true;
			    }
		    }
	    }
    }

	/**
	 * Get the user subscription number for the current blog_id
	 *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
    public static function get_user_subscription_number(\WP_User $user = null, $site_code = null)
    {
	    if (!$user) {
		    $user = new \WP_User(get_current_user_id());
	    }

	    if ($user instanceof \WP_User && !empty($user->ID)) {
	    	if (!$site_code) {
			    $site_code = Multisite::get_site_code();
		    }

		    return get_user_meta($user->ID, 'subscription_' . $site_code,true);
	    }
    }

    public static function validate_ideal_payment($user_id = null)
    {
	    if (!$user_id) {
		    $user = new \WP_User(get_current_user_id());
	    } else {
		    $user = new \WP_User($user_id);
	    }

	    if ($user instanceof \WP_User && !empty($user->ID)) {
		    require_once __DIR__ . '/../vendor/mollie-api-php/vendor/autoload.php';

		    $payment_id = get_user_meta($user->ID, 'mollie_payment_id', true);
		    $mollie_api_key = get_field('mollie_api_key', 'option');

		    if (!empty($payment_id) && $mollie_api_key) {
			    $mollie = new \Mollie\Api\MollieApiClient();
			    $mollie->setApiKey( get_field( 'mollie_api_key', 'option' ) );

			    $payment = $mollie->payments->get($payment_id);

			    if ($payment && $payment->status !== 'paid') {
			    	delete_user_meta($user->ID, 'subscription_lm');
			    	delete_user_meta($user->ID, 'subscription_vt');
			    	delete_user_meta($user->ID, 'subscription_tpt');
			    }
		    }
	    }
    }
}
