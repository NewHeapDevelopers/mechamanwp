<?php
namespace Codio\CodioAuth\Zeno;

use mysql_xdevapi\Exception;
use Newheap\Plugins\NewheapAccount\User\User;
use Newheap\Plugins\NewheapAccount\User\UserRegistration;
use NewHeap\Theme\Cache\Cache;
use NewHeap\Theme\Logger\Logger;
use NewHeap\Theme\Multisite\Multisite;
use NewHeap\Theme\Notification\Notification;

/**
 * Class Codio
 *
 * @package Codio
 */
class Zeno
{

	const ABO_ENDPOINT = 'https://app.socho.nl:9456/ZenoApiTest/AboApiV6';
//	const ABO_ENDPOINT = 'https://app.socho.nl:9456/ZenoApi/AboApiV6';
	const RELATION_ENDPOINT = 'https://app.socho.nl:9456/ZenoApiTest/RelatieApiV6';
//	const RELATION_ENDPOINT = 'https://app.socho.nl:9456/ZenoApi/RelatieApiV6';

	const API_TIMEOUT = 30;
	const SUBSCRIPTION_HAS_NO_EXPIRY = 1;

	const COUNTRIES_EUROPE = [
		'BG',
		'CY',
		'DK',
		'DE',
		'EE',
		'FI',
		'FR',
		'GR',
		'HU',
		'IE',
		'IT',
		'HR',
		'LV',
		'LT',
		'LU',
		'MT',
		'AT',
		'PL',
		'PT',
		'RO',
		'SI',
		'SK',
		'ES',
		'CZ',
		'SE',
	];

    /**
     * @var
     */
    /** @var \SoapClient */
    protected $client;

	private static $zeno_username = 'Website'; // TEST
	private static $zeno_password = 'e&4Sd%sG*q97%np3*s$4'; // TEST
//	private static $zeno_username = 'Website'; // LIVE
//	private static $zeno_password = '3.PE74Wzg<yd'; // LIVE

    protected $user;
    protected $username;
    protected $password;
    protected $relation_number;
    protected $relation_type;
    protected $subscription_details;
    protected $post_data;
    protected $start_date;
    protected $end_date;
    protected $abo_number;
    protected $subscription_number;
    protected $site_code;
    protected $subscription_type;

    /**
     * Zeno constructor.
     * @param $subscription_number
     */
    public function __construct(\WP_User $user = null, $site_code = null)
    {
	    $this->username = 'Website'; // TEST
	    $this->password = 'e&4Sd%sG*q97%np3*s$4'; // TEST
//	    $this->username = 'Website'; // LIVE
//	    $this->password = '3.PE74Wzg<yd'; // LIVE

    	if ($user && $site_code) {
		    $this->user = $user;
		    $this->site_code = $site_code;
		    $this->abo_number = User::get_user_subscription_number($user, $site_code);
	    }
    }

    /**
     * Validates the subscription number of the user
     *
     * @author Ferhat Ileri <ferhat@newheap.nl>
     */
    public function validate_subscription_number($site_code)
    {
    	$this->site_code = $site_code;

        if (!$this->abo_number || strlen($this->abo_number) > 15) {
            return false;
        }

        $this->prepare_api_call();
        $this->get_relation_number();

        if ($this->relation_number) {
	        $this->get_subscription_details();
	        $this->update_user($site_code);
        } else {
	        delete_user_meta(get_current_user_id(), 'subscription_' . $site_code . '_expires');
        }
    }

	/**
	 * Set the default settings for the relatie api request
	 *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
    public function prepare_api_call()
    {
    	$this->post_data = [
    		'timeout' => self::API_TIMEOUT,
		    'headers' => [
			    'Content-Type' => 'application/json',
			    'Authorization' => 'Basic ' . base64_encode($this->username . ':' . $this->password),
		    ],
	    ];
    }

	/**
	 * Get relNo by aboNo
	 *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
    public function get_relation_number()
    {
    	$body_data = new \stdClass();
	    $body_data->Body = new \stdClass();
	    $body_data->Body->searchRelNoByAboNoRequest = new \stdClass();
	    $body_data->Body->searchRelNoByAboNoRequest->aboNo = $this->abo_number;

    	$this->post_data['body'] = json_encode($body_data);

    	$response = wp_remote_post(self::ABO_ENDPOINT, $this->post_data);

	    if (self::zeno_response_has_errors($response)) {
		    Logger::critical_error(__('Kon geen abonnement data vinden. Controleer je abonnementsnummer en probeer het nogmaals. Of neem contact met ons op.') . ' AboNo: ' . json_encode($this->abo_number));
	    } else {
		    $response_body = json_decode($response['body']);

		    $this->relation_number = $response_body->Body->searchRelNoByAboNoResponse->relNo;

		    update_user_meta($this->user->ID, 'relation_number', $this->relation_number);
	    }
    }

	/**
	 * Get the subscription details by relNo
	 *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
    public function get_subscription_details($relation_number = null)
    {
    	if (!empty($relation_number)) {
    		$this->relation_number = intval($relation_number);
	    }

    	if (empty($this->post_data)) {
    		$this->prepare_api_call();
	    }

    	$body_data = new \stdClass();
	    $body_data->Body = new \stdClass();
	    $body_data->Body->getAbonnementInfoRequest = new \stdClass();
	    $body_data->Body->getAbonnementInfoRequest->relNo = $this->relation_number;

	    $this->post_data['body'] = json_encode($body_data);

    	$response = wp_remote_post(self::ABO_ENDPOINT, $this->post_data);

    	$body = json_decode(utf8_encode($response['body']));

    	if (self::zeno_response_has_errors($response)) {
		    Logger::critical_error(__('Kon geen abonnement data vinden. Controleer je abonnementsnummer en probeer het nogmaals. Of neem contact met ons op.') . ' RelNo: ' . json_encode($this->relation_number));
	    } else {

		    if (!empty($body->Body->getAbonnementInfoResponse->return1->aboRelatie[0]->aboAbonnementen->abonnement)) {

			    // Because the user can have multiple subscription we need to loop the subscripbtions and check if it corresponds with the $site_code
		    	foreach ($body->Body->getAbonnementInfoResponse->return1->aboRelatie[0]->aboAbonnementen->abonnement as $subscription) {
		    		if ($this->site_code === 'lm' && strpos($subscription->abonnementSoortCode, 'LM-') !== false) {
					    $this->start_date = $subscription->ingangsDatum;
					    $this->end_date = $subscription->eindDatum;

					    // Update subscript_lm_code
					    update_user_meta($this->user->ID, 'subscription_lm_code', $subscription->abonnementSoortCode);
					    $this->update_user($this->site_code);
				    } elseif ($this->site_code === 'vt' && strpos($subscription->abonnementSoortCode, 'VT-') !== false) {
					    $this->start_date = $subscription->ingangsDatum;
					    $this->end_date = $subscription->eindDatum;

					    // Update subscript_vt_code
					    update_user_meta($this->user->ID, 'subscription_vt_code', $subscription->abonnementSoortCode);
					    $this->update_user($this->site_code);
				    } elseif ($this->site_code === 'tpt' && strpos($subscription->abonnementSoortCode, 'TP-') !== false) {
					    $this->start_date = $subscription->ingangsDatum;
					    $this->end_date = $subscription->eindDatum;

					    // Update subscript_tpt_code
					    update_user_meta($this->user->ID, 'subscription_tpt_code', $subscription->abonnementSoortCode);
					    $this->update_user($this->site_code);
				    }
			    }
		    }
	    }
    }

	/**
	 * Update the user meta
	 *
	 * @param null $site_code
	 *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
    public function update_user($site_code = null)
    {
    	if ($this->end_date) {
		    if (empty($site_code)) {
			    $site_code = Multisite::get_site_code();
		    }

		    $expiry_date = \DateTime::createFromFormat('Y-m-d H:i:s', $this->end_date . ' 00:00:00');

		    update_user_meta($this->user->ID, 'subscription_' . $site_code . '_expires', intval($expiry_date->format('U')));
	    } elseif ($this->start_date) {
		    update_user_meta($this->user->ID, 'subscription_' . $site_code . '_expires', self::SUBSCRIPTION_HAS_NO_EXPIRY);
	    } else {
    		delete_user_meta($this->user->ID, 'subscription_' . $site_code . '_expires');
		    Logger::critical_error(__('Kon geen abonnement data vinden. Controleer je abonnementsnummer (' . $this->abo_number . ') en probeer het nogmaals. Of neem contact met ons op.'));
	    }
    }

	/**
	 * Check if we have a correct Zeno response
	 *
	 * @param $response
	 *
	 * @return bool|void
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
    public static function zeno_response_has_errors($response)
    {
	    if ($response instanceof \WP_Error) {
		    Logger::critical_error(__('Kon geen abonnement data vinden. Controleer je abonnementsnummer en probeer het nogmaals. Of neem contact met ons op.') . json_encode($response->errors));

		    return true;
	    } else {
	    	if (is_array($response) && isset($response['response']) && isset($response['response']['code']) && $response['response']['code']) {
	    		if ($response['response']['code'] === 200) {
	    			return;
			    } else {
				    Logger::critical_error([
					    'code' => $response['response']['code'],
					    'message' => $response['response']['message']
				    ]);

				    return true;
			    }
		    }
	    }
    }

    public function create_person($payment_id)
    {
	    if ($_POST['user_type'] === 'gratis') {
	    	return;
	    }

    	// Get all the needed POST data
	    $user_login = esc_attr($_POST['user_login']);
	    $email = esc_attr($_POST['email']);

	    $company_name = esc_attr($_POST['company_name']);
	    $gender = esc_attr($_POST['gender']);
	    $initials = esc_attr($_POST['initials']);
	    $prefix = esc_attr($_POST['prefix']);
	    $street_name = esc_attr($_POST['street_name']);
	    $house_number = esc_attr($_POST['house_number']);
	    $house_number_addition = esc_attr($_POST['house_number_addition']);
	    $zip = esc_attr($_POST['zip']);
	    $city = esc_attr($_POST['city']);
	    $country = esc_attr($_POST['country']);
	    $mobile_phone = esc_attr($_POST['mobile_phone']);
	    $phone = esc_attr($_POST['phone']);
	    $invoice_email = esc_attr($_POST['invoice_email']);
	    $payment_method = esc_attr($_POST['payment']);
	    $account_number = esc_attr($_POST['account_number']);
	    $account_holder_name = esc_attr($_POST['account_holder_name']);
	    $first_name = esc_attr($_POST['first_name']);
	    $last_name = esc_attr($_POST['last_name']);
	    $newsletter = esc_attr($_POST['newsletter']);

	    if (empty($invoice_email)) {
		    $invoice_email = $email;
	    }

	    $body_data = new \stdClass();
	    $body_data->Body = new \stdClass();

	    $person = new \stdClass();

	    $person->achternaam = $last_name;
	    $person->voornaam = $first_name;
	    $person->voorletters = $initials;
	    $person->voorvoegsels = $prefix;
	    $person->geslachtsCode = $gender;
	    $person->email = $invoice_email;
	    $person->email2 = $email;
	    $person->telefoonnummer = $phone;
	    $person->telefoonnummerMobiel = $mobile_phone;
	    $person->username = $user_login;
	    $person->landCode = $country;

	    if (!empty($newsletter)) {
		    $person->uitsluitenVanMailing = false;
	    }

	    $person->adresBezoek = new \stdClass();
	    $person->adresBezoek->straatnaam = $street_name;
	    $person->adresBezoek->huisnummer = $house_number;
	    $person->adresBezoek->huisnummerToevoeging = $house_number_addition;
	    $person->adresBezoek->postcode = $zip;
	    $person->adresBezoek->plaatsnaam = $city;

	    $person->adresPrive = new \stdClass();
	    $person->adresPrive->straatnaam = $street_name;
	    $person->adresPrive->huisnummer = $house_number;
	    $person->adresPrive->huisnummerToevoeging = $house_number_addition;
	    $person->adresPrive->postcode = $zip;
	    $person->adresPrive->plaatsnaam = $city;

	    Logger::write_zeno_log('paymentmethod: ' . $payment_method);

	    $person->betaling = new \stdClass();
	    $person->betaling->betaalwijze = $payment_method;
	    if ($payment_method == 1) {
		    $person->betaling->ibannummer = $account_number;
//		    $person->betaling->bankrekeningnummer = preg_replace("/[^\d]+/","", substr($account_number, -11)); // Obsolete
	    }

	    // Check if company or private person
	    if (!empty($company_name) && $country === 'NL') {
		    $body_data->Body->createContactpersoonRequest = new \stdClass();
		    $body_data->Body->createContactpersoonRequest->bedrijf = new \stdClass();

		    $body_data->Body->createContactpersoonRequest->bedrijf->naam = $company_name;
		    $body_data->Body->createContactpersoonRequest->bedrijf->email = $invoice_email;
		    $body_data->Body->createContactpersoonRequest->bedrijf->email2 = $email;

		    $body_data->Body->createContactpersoonRequest->bedrijf->adresBezoek = new \stdClass();
		    $body_data->Body->createContactpersoonRequest->bedrijf->adresBezoek->straatnaam = $street_name;
		    $body_data->Body->createContactpersoonRequest->bedrijf->adresBezoek->huisnummer = $house_number;
		    $body_data->Body->createContactpersoonRequest->bedrijf->adresBezoek->huisnummerToevoeging = $house_number_addition;
		    $body_data->Body->createContactpersoonRequest->bedrijf->adresBezoek->postcode = $zip;
		    $body_data->Body->createContactpersoonRequest->bedrijf->adresBezoek->plaatsnaam = $city;

		    $body_data->Body->createContactpersoonRequest->contactpersoon = new \stdClass();
		    $body_data->Body->createContactpersoonRequest->contactpersoon->persoon = $person;

	    } else {
		    $body_data->Body->createPersoonRequest = new \stdClass();
		    $body_data->Body->createPersoonRequest->persoon = $person;
//		    $body_data->Body->createPersoonRequest->forceerKandidaat = true;
	    }

	    Logger::write_zeno_log('Data sending to Zeno: ' . json_encode($body_data));

	    $this->prepare_api_call();
	    $this->post_data['body'] = json_encode($body_data);
	    $response = wp_remote_post(self::RELATION_ENDPOINT, $this->post_data);
	    $body = json_decode(utf8_encode($response['body']));
	    Logger::write_zeno_log('Response from Zeno: ' . json_encode($body));

	    // Validate response
	    Logger::write_zeno_log('Body must be object:' . gettype($body));
	    if (property_exists($body, 'Body')) {
	    	if (property_exists($body->Body, 'Fault')) {
	    		$fault_code = $body->Body->Fault->faultCode;
			    $fault_message = $body->Body->Fault->faultString;
			    $error_code = $body->Body->Fault->detail->applicationError->code;
			    $error_message = $body->Body->Fault->detail->applicationError->message;

			    $GLOBALS['registration_errors']->add('zeno_error', $error_message);
	    		Notification::add_error(sprintf(__('Foutcode %s: %s', 'newheap'), $error_code, $error_message), Notification::LOCATION_ZENO);
	    		Logger::write_zeno_log($fault_code . ': '. $fault_message);


		    } elseif (property_exists($body->Body, 'createContactpersoonResponse')) {
	    		$this->relation_number = $body->Body->createContactpersoonResponse->relatienummerContactpersoon;

	    		$subscription = $this->create_subscription($this->relation_number, $payment_id);

			    $this->subscription_number = $subscription['subscription_number'];
			    $this->subscription_code = $subscription['subscription_code'];

			    Logger::write_zeno_log('relationNo: ' . $this->relation_number);
			    Logger::write_zeno_log('subscriptionNo: ' . $this->subscription_number);
			    Logger::write_zeno_log('subscriptionCode: ' . json_encode($this->subscription_code));

	    		return [
	    			'relation_number' => $this->relation_number,
				    'subscription_number' => $this->subscription_number,
				    'subscription_code' => $this->subscription_code
			    ];

	    	} elseif (property_exists($body->Body, 'createPersoonResponse')) {
			    $this->relation_number = $body->Body->createPersoonResponse->relatienummer;
			    $this->subscription_number = $this->create_subscription($this->relation_number, $payment_id);

			    return [
				    'relation_number' => $this->relation_number,
				    'subscription_number' => $this->subscription_number['subscription_number'],
				    'subscription_code' => $this->subscription_code
			    ];

	    	}

	    } else {
	    	Notification::add_error('Uw account kan niet aangemaakt worden. Probeer het nogmaals of neem contact met ons op.');
	    	Logger::critical_error('Corrupt response from Zeno: ' . $body);
	    }
    }

    public function create_subscription($relation_number, $tx_code = null)
    {
    	$subscription_code = self::get_subscription_code();
    	$payment_method = intval($_POST['payment']);
    	Logger::write_zeno_log('paymentmethod2: ' . $payment_method);

	    $body_data = new \stdClass();
	    $body_data->Body = new \stdClass();

	    $body_data->Body->createAboAboNoRequest = new \stdClass();
	    $body_data->Body->createAboAboNoRequest->relNo = $relation_number;
	    Logger::write_zeno_log('subscription_code: ' . $subscription_code);

	    $body_data->Body->createAboAboNoRequest->aboObject = new \stdClass();
	    $body_data->Body->createAboAboNoRequest->aboObject->aboAboSoortCode = $subscription_code;
	    $body_data->Body->createAboAboNoRequest->aboObject->aboBetaalwijze = $payment_method;
	    $body_data->Body->createAboAboNoRequest->aboObject->aboIngangsDatum = date('Y-m-d');
	    $body_data->Body->createAboAboNoRequest->aboObject->aboVerzendingBestandType = '0';

	    if ($tx_code) {
		    $body_data->Body->createAboAboNoRequest->aboObject->aboBetaalReferentie = $tx_code;
	    }

	    Logger::write_zeno_log('Create subscription data: ' . json_encode($body_data));

	    $this->prepare_api_call();
	    $this->post_data['body'] = json_encode($body_data);
	    $response = wp_remote_post(self::ABO_ENDPOINT, $this->post_data);
	    $body = json_decode(utf8_encode($response['body']));
	    Logger::write_zeno_log('Create subscription data response: ' . json_encode($body));

	    // Validate response
	    if (property_exists($body, 'Body')) {
		    if (property_exists($body->Body, 'Fault')) {
			    $fault_code = $body->Body->Fault->faultCode;
			    $fault_message = $body->Body->Fault->faultString;
			    $error_code = $body->Body->Fault->detail->applicationError->code;
			    $error_message = $body->Body->Fault->detail->applicationError->message;

			    $GLOBALS['registration_errors']->add('zeno_error', $error_message);
			    Notification::add_error(sprintf(__('Foutcode %s: %s', 'newheap'), $error_code, $error_message), Notification::LOCATION_ZENO);
			    Logger::write_zeno_log($fault_code . ': '. $fault_message);

		    } elseif (property_exists($body->Body, 'createAboAboNoResponse')) {
			    $this->subscription_number = $body->Body->createAboAboNoResponse->aboNo;

			    // Update user meta
			    Logger::write_zeno_log('Subscription code: ' . $subscription_code . 'subscription_' . Multisite::get_site_code() . '_code');
			    
			    return [
			    	'subscription_number' => $this->subscription_number,
			    	'subscription_code' => [
			    		'key' => 'subscription_' . Multisite::get_site_code() . '_code',
			    		'value' => $subscription_code
				    ]
			    ];
		    }

	    } else {
		    Logger::critical_error('Corrupt response from Zeno: ' . json_encode($body));
	    }

	    Logger::critical_error('Create subscription failed: ' . json_encode($body));
    }

	/**
	 * Get the relation number of the current user by email
	 *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
    public function get_relation_number_by_email()
    {
	    $user = wp_get_current_user();

	    if (!is_user_logged_in() || empty($user)) {
		    return;
	    }

	    $body_data = new \stdClass();
	    $body_data->Body = new \stdClass();

	    $body_data->Body->searchEmailRequest = new \stdClass();
	    $body_data->Body->searchEmailRequest->email = 'evanderbroek@agrimedia.nl'; // $user->user_email;
	    $body_data->Body->searchEmailRequest->zoekEmailVeld = 0;

	    $this->prepare_api_call();
	    $this->post_data['body'] = json_encode($body_data);
	    $response = wp_remote_post(self::RELATION_ENDPOINT, $this->post_data);
	    $body = json_decode(utf8_encode($response['body']));
	    Logger::write_zeno_log('Get relation number by email data response: ' . json_encode($body));


	    if (!empty($body->Body->searchEmailResponse->relaties->relatienr)) {
	    	if (count($body->Body->searchEmailResponse->relaties->relatienr) > 1) {
	    		Logger::critical_error('Email has multiple relation numbers');
		    }

		    $this->relation_number = $body->Body->searchEmailResponse->relaties->relatienr[0]->relatienummer;
	    	$this->relation_type = $body->Body->searchEmailResponse->relaties->relatienr[0]->relatietype;
	    }
    }

	/**
	 * Get the relation number of the current user by email
	 *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
    public function get_relation_number_by_abo($aboNo)
    {
	    $body_data = new \stdClass();
	    $body_data->Body = new \stdClass();

	    $body_data->Body->searchRelNoByAboNoRequest = new \stdClass();
	    $body_data->Body->searchRelNoByAboNoRequest->aboNo = $aboNo;

	    $this->prepare_api_call();
	    $this->post_data['body'] = json_encode($body_data);
	    $response = wp_remote_post(self::ABO_ENDPOINT, $this->post_data);
	    $body = json_decode(utf8_encode($response['body']));
	    Logger::write_zeno_log('Get relation number by aboNo data response: ' . json_encode($body));

	    if (!empty($body->Body->searchRelNoByAboNoResponse->relNo)) {
		    $this->relation_number = $body->Body->searchRelNoByAboNoResponse->relNo;

		    return $this->relation_number;
	    }
    }

    public function get_relation_type()
    {
    	if (empty($this->relation_number)) {
    		return;
	    }

	    $body_data = new \stdClass();
	    $body_data->Body = new \stdClass();

	    $body_data->Body->getRelatietypeRequest = new \stdClass();
	    $body_data->Body->getRelatietypeRequest->relatienummer = $this->relation_number;

	    $this->prepare_api_call();
	    $this->post_data['body'] = json_encode($body_data);
	    $response = wp_remote_post(self::RELATION_ENDPOINT, $this->post_data);
	    $body = json_decode(utf8_encode($response['body']));
	    Logger::write_zeno_log('Get relation type by relNo data response: ' . json_encode($body));



	    if (!empty($body->Body->getRelatietypeResponse->relatietype)) {
		    $this->relation_type = $body->Body->getRelatietypeResponse->relatietype;

		    return $this->relation_type;
	    }
    }

	/**
	 * Get the user details for an old existing user
	 *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
    public function get_user_details($aboNo = null)
    {
    	if ($aboNo) {
    		$this->get_relation_number_by_abo($aboNo);
    		$this->get_relation_type();
	    } elseif (is_user_logged_in()) {
		    $this->get_relation_number_by_email();
	    }

	    if (empty($this->relation_number)) {
    		return 'no_subcription_found';
	    }

	    $body_data = new \stdClass();
	    $body_data->Body = new \stdClass();

    	switch($this->relation_type) {
		    case 'B' :
			    $body_data->Body->getBedrijfRequest = new \stdClass();
			    $body_data->Body->getBedrijfRequest->relatienummer = $this->relation_number;
			    $body_data->Body->getBedrijfRequest->includeContactpersonen = true;
			    break;
		    case 'P' :
			    $body_data->Body->getPersoonRequest = new \stdClass();
			    $body_data->Body->getPersoonRequest->relatienummer = $this->relation_number;
			    break;
		    case 'C' :
		    	Logger::critical_error('Register existing user (ContactPersoon) with aboNo: ' . $aboNo);
		    	return 'no_subcription_found';
	    }



	    $this->prepare_api_call();
	    $this->post_data['body'] = json_encode($body_data);
	    $response = wp_remote_post(self::RELATION_ENDPOINT, $this->post_data);
	    $body = json_decode(utf8_encode($response['body']));
	    Logger::write_zeno_log('Get person  data response: ' . json_encode($body));

	    // Validate response
	    if (property_exists($body, 'Body')) {
		    if (property_exists($body->Body, 'Fault')) {
			    $fault_code = $body->Body->Fault->faultCode;
			    $fault_message = $body->Body->Fault->faultString;
			    $error_code = $body->Body->Fault->detail->applicationError->code;
			    $error_message = $body->Body->Fault->detail->applicationError->message;

			    if (!$GLOBALS['registration_errors'] instanceof \WP_Error) {
				    $GLOBALS['registration_errors'] = new \WP_Error();
			    }

			    $GLOBALS['registration_errors']->add('zeno_error', $error_message);
			    Notification::add_error(sprintf(__('Foutcode %s: %s', 'newheap'), $error_code, $error_message), Notification::LOCATION_ZENO);
			    Logger::write_zeno_log($fault_code . ': '. $fault_message);

		    } elseif (property_exists($body->Body, 'getBedrijfResponse') ||
		              property_exists($body->Body, 'getPersoonResponse') ||
		              property_exists($body->Body, 'getContactpersoonResponse')) {
			    return $body->Body;
		    }

	    } else {
		    Logger::critical_error('Corrupt response from Zeno: ' . json_encode($body));
	    }

	    Logger::critical_error('Create subscription failed: ' . json_encode($body));
    }

    public static function get_subscription_code()
    {
	    $subscription_code = '';
	    $site_code = strtoupper(Multisite::get_site_code());

	    if (UserRegistration::check_if_zeno_user()) {
		    $country = get_user_meta(get_current_user_id(), 'country', true);
	    } else {
	    	$country = esc_attr($_POST['country']);
	    }

	    if (empty($country)) {
	    	Logger::critical_error('Zeno::get_subscription_code no country. Post data: ' .json_encode($_POST));
	    	$country = 'NL';
	    }

	    // Because the sitecode only has 2 characters at the external
	    if ($site_code === strtoupper(Multisite::SITE_CODE_TUIN_EN_PARK_TECHNIEK)) {
		    $site_code = 'TP';
	    }

	    Logger::write_zeno_log($site_code);
	    Logger::write_zeno_log($_POST['user_type']);

	    // Check which subscription should be created for this user
	    if ($_POST['user_type'] === 'plus') {
		    switch (strtoupper($country)) {
			    case 'NL' :
				    $subscription_code = $site_code . '-K05';
				    break;
			    case 'BE' :
				    $subscription_code = $site_code . '-K25';
				    break;
			    default :
				    $subscription_code = $site_code . '-K35';
		    }

		    if (in_array($country, self::COUNTRIES_EUROPE)) {
			    $subscription_code = $site_code . '-K15';
		    }
	    } elseif ($_POST['user_type'] === 'totaal') {
		    switch (strtoupper($country)) {
			    case 'NL' :
//				    $subscription_code = $site_code . '-J01'; // Without discount
				    $subscription_code = $site_code . '-K02'; // With discount
				    break;
			    case 'BE' :
//				    $subscription_code = $site_code . '-J21'; // Without discount
				    $subscription_code = $site_code . '-K22'; // With discount
				    break;
			    default :
//				    $subscription_code = $site_code . '-J31'; // Without discount
				    $subscription_code = $site_code . '-K32'; // With discount
		    }

		    if (in_array($country, self::COUNTRIES_EUROPE)) {
//			    $subscription_code = $site_code . '-J11'; // Without discount
			    $subscription_code = $site_code . '-K12'; // With discount
		    }
	    }

	    Logger::write_zeno_log('country: ' . $country);
	    Logger::write_zeno_log('subscription code: ' . $subscription_code);

	    return $subscription_code;
    }

    public static function get_country_codes()
    {
    	// Prepare API call
	    $post_data = [
		    'timeout' => self::API_TIMEOUT,
		    'headers' => [
			    'Content-Type' => 'application/json',
			    'Authorization' => 'Basic ' . base64_encode(self::$zeno_username . ':' . self::$zeno_password),
		    ],
	    ];

	    // Create the body data to send
	    $body_data = new \stdClass();
	    $body_data->Body = new \stdClass();
	    $body_data->Body->getLandcodesRequest = new \stdClass();

	    $post_data['body'] = json_encode($body_data);

	    $response = wp_remote_post(self::RELATION_ENDPOINT, $post_data);
	    $body = json_decode(utf8_encode($response['body']));

	    // Validate response
	    if (property_exists($body, 'Body')) {
		    if (property_exists($body->Body, 'Fault')) {
			    $fault_code = $body->Body->Fault->faultCode;
			    $fault_message = $body->Body->Fault->faultString;

			    Logger::write_zeno_log($fault_code . ': '. $fault_message);
		    } elseif (property_exists($body->Body, 'getLandcodesResponse')) {

			    $log_file = get_template_directory() . Cache::CACHE_FOLDER . Cache::COUNTRIES_FILENAME . '.json';
			    $cached_data = json_encode($body->Body->getLandcodesResponse->landCodes);

			    $file = fopen($log_file, 'w+');
			    fwrite($file, $cached_data);
			    fclose($file);
		    }

	    } else {
		    Logger::critical_error('Corrupt response from Zeno: ' . json_encode($body));
	    }
    }

    public static function get_countries_list()
    {
	    return json_decode(file_get_contents(get_template_directory() . Cache::CACHE_FOLDER . '/' . Cache::COUNTRIES_FILENAME . '.json', FILE_USE_INCLUDE_PATH));
    }
}
