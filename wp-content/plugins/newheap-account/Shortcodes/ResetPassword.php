<?php
namespace Newheap\Plugins\NewheapAccount\Shortcodes;

class ResetPassword
{
    public function __construct()
    {
	    add_action('login_form_lostpassword', [$this, 'redirect_to_custom_password_reset']);
	    add_action('login_form_lostpassword', [$this, 'process_reset_password'] );

        add_shortcode('newheap_reset_password_form', [$this, 'custom_reset_password']);
    }

	/**
     * Redirect to custom reset password page
     *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
	public function redirect_to_custom_password_reset()
	{
		if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
			if ( is_user_logged_in() ) {
				$this->redirect_logged_in_user();
				exit;
			}

			wp_redirect(get_field('reset_password_page', 'option'));
			exit;
		}
	}

    /**
     * Shortcode function
     *
     * @return false|string
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public function custom_reset_password()
    {

        ob_start(); ?>

        <div id="reset-password-form">
            <?php
            if (is_user_logged_in()) : ?>
                <h3><?php the_title(); ?></h3>

                <p><?php _e('U bent al ingelogd', 'newheap'); ?></p>

                <?php return; ?>
            <?php endif; ?>

            <form action="<?php echo wp_lostpassword_url(); ?>" method="post" class="my-5">

                <h3><?php the_title(); ?></h3>

                <p>
		            <?php _e('Vul uw e-mailadres in en we sturen u een e-mail met verdere instructies om uw wachtwoord opnieuw in te stellen', 'newheap' ); ?>
                </p>

                <p class="form-row">
                    <label>
                        <?php _e( 'E-mailadres', 'newheap' ); ?>
                        <input type="text" name="user_login" id="user_login">
                    </label>
                </p>

                <button type="submit" class="cta-2">
                    <?php _e( 'Reset Password', 'newheap' ); ?>
                </button>
            </form>
        </div>

        <?php ob_end_flush();
    }

    public function process_reset_password()
    {
	    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		    $errors = retrieve_password();

            // Email sent
            $redirect_url = get_field('reset_password_page', 'option') . '?request=success';
		    wp_redirect($redirect_url);
		    exit;
	    }
    }
}
