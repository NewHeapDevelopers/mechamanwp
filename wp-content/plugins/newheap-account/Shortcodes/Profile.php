<?php
namespace Newheap\Plugins\NewheapAccount\Shortcodes;

use Newheap\Plugin\NewheapAccount\Auth\Auth;
use Newheap\Plugins\NewheapAccount\User\User;
use NewHeap\Theme\Multisite\Multisite;

class Profile
{
    private $errors;

    private $user;
    private $first_name;
    private $insertion;
    private $last_name;
    private $subscription_lm;
    private $subscription_vt;
    private $subscription_tpt;
    private $street_name;
    private $house_number;
    private $house_number_addition;
    private $zip;
    private $city;
    private $delivery_options;
    private $old_password;
    private $new_password;
    private $password_confirmation;

    public function __construct()
    {
        $user = new \WP_User(get_current_user_id());

        if ($user->ID) {
            $this->user = $user;
        }

        add_shortcode('newheap_profile_form', [$this, 'custom_profile_function']);
        add_shortcode('newheap_profile_show', [$this, 'profile_show_function']);
    }

    /** Show profile info */
    public function profile_show_function()
    {
        // TODO: Check if logged in. Else redirect to login
        if (!is_user_logged_in()) {
            ob_start(); ?>

            <h1><?php _e('Inloggen', 'newheap') ;?></h1>
            <p><?php printf(__('U moet ingelogd zijn om uw profiel te bekijken. Heeft u nog geen account? Dan kun u <a href="%s">hier</a> registreren', 'newheap'), get_field('registration_page', 'option')) ;?></p>

            <?php ob_end_flush();
            echo do_shortcode('[newheap_login_form]');
        } else {
	        User::has_valid_subscription();

	        $this->user = new \WP_User(get_current_user_id());

	        $name = $this->user->display_name;
	        $address = get_user_meta($this->user->ID, 'street_name', true);
	        $house_number = get_user_meta($this->user->ID, 'house_number', true);
	        $house_number_addition = get_user_meta($this->user->ID, 'house_number_addition', true);
	        $zip = get_user_meta($this->user->ID, 'zip', true);
	        $city = get_user_meta($this->user->ID, 'city', true);
	        $country = get_user_meta($this->user->ID, 'country', true);
	        $phone = get_user_meta($this->user->ID, 'phone', true) ?? get_user_meta($this->user->ID, 'mobile_phone', true);
	        $email = $this->user->user_email;
	        $bank_account_number = get_user_meta($this->user->ID, 'account_number', true);

	        $subscription_code = get_user_meta(get_current_user_id(), 'subscription_' . Multisite::get_site_code() . '_code', true);

	        ob_start();
	        ?>
            <div class="row login-page">
                <div class="col-md-12">
                    <h1 class="mt-2 d-inline">Uw account</h1>

                    <a href="<?php echo wp_logout_url(); ?>" class="logout-btn">Uitloggen</a>

                    <h3 class="section-title">Gegevens</h3>

                    <div class="row">
                        <div class="col-md-6">
                            <p class="head-alt"><strong>Naam:</strong><br /><?php echo $name; ?></p>

                            <p class="head-alt"><strong>Adres:</strong><br /><?php echo $address; ?> <?php echo $house_number; ?> <?php echo $house_number_addition; ?><br /><?php echo $zip; ?> <?php echo $city; ?><br /><?php echo $country; ?></p>

                            <p class="head-alt"><strong>Telefoonnummer:</strong><br /><?php echo $phone; ?></p>

                            <p class="head-alt"><strong>E-mailadres:</strong><br /><?php echo $email; ?></p>

                            <?php if (!empty($bank_account_number)) : ?>
                                <p class="head-alt"><strong>Rekeningnummer:</strong><br /><?php echo $bank_account_number; ?></p>
                            <?php endif; ?>
                            <br />
                        </div>
                        <div class="col-md-6 mb-4">
                            <div class="card">
                                <div class="card-body">
                                    <p class="head-alt"><strong>Contact</strong><br />Wilt u uw persoonsgegevens wijzigen? Neem dan contact op met de abonnementen-administratie van AgriMedia via:<br /><br />Telefoon: <strong>0317-465 670</strong><br /><br/>E-mailadres: <strong>mail@agrimedia.nl</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php if (get_current_blog_id() !== Multisite::SITE_MECHAMAN) : ?>
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="section-title">Abonnement</h3>

                        <div class="row">
                            <div class="col-md-6">
                                <?php if ($subscription_code) : ?>
                                    <p class="heading">
                                        <?php
                                        $trial_subscription_codes = [
                                            'LM-K05',
                                            'LM-K15',
                                            'LM-K25',
                                            'LM-K35',
                                            'VT-K05',
                                            'VT-K15',
                                            'VT-K25',
                                            'VT-K35',
                                            'TP-K05',
                                            'TP-K15',
                                            'TP-K25',
                                            'TP-K35',
                                        ];

                                        if (in_array($subscription_code, $trial_subscription_codes)) : ?>
                                            <strong>
                                                <?php _e('Proefabonnement', 'newheap'); ?>
                                            </strong>
                                        <?php else : ?>
                                            <strong>
                                                <?php _e('Totaal abonnement', 'newheap'); ?>
                                            </strong>
                                        <?php endif; ?>
                                    </p>

                                    <p class="heading">
                                        <?php _e('U heeft een abonnement', 'newheap'); ?>
                                    </p>

                                    <p class="heading">
                                        <?php _e('Einddatum:', 'newheap'); ?>
                                        <?php
                                        $expiry_date = get_user_meta(get_current_user_id(), 'subscription_' . Multisite::get_site_code() . '_expires', true);

                                        if (!empty($expiry_date)) {
                                            $human_date = \DateTime::createFromFormat('U', $expiry_date)->format('d-m-Y');

                                            if ($expiry_date > 1) {
                                                echo $human_date;
                                            } else {
                                                _e( 'Doorlopend abonnement', 'newheap' );
                                            }
                                        }
                                        ?>


                                    </p>

                                    <?php if (in_array($subscription_code, $trial_subscription_codes)) : ?>
                                        <br />&nbsp;
                                        <p>
                                            Uw proefabonnement loopt binnenkort af. Bent u tevreden over <?php bloginfo(); ?>? Vraag dan een jaarabonnement aan! U ontvangt dan (naast de nieuwsbrief), toegang tot het digitale archief en het vakblad Tuin en Park Techniek. tevens het digitale magazine, korting op studiedagen en evenementen en het eerste jaar 25% korting.
                                        </p>
                                        <a href="<?=home_url()?>/abonnementen/" class="cta-1">
                                            <?php echo __("Jaarabonnement aanvragen","newheap"); ?>
                                        </a>
                                        <br />
                                        <br />
                                    <?php endif; ?>
                                <?php else: ?>
                                    <p class="heading">
                                        <?php _e('U heeft nog geen abonnement voor dit vakblad.'); ?>
                                    </p>
                                    <a href="<?=home_url()?>/abonnementen/" class="cta-1">
                                        <?php echo __("Abonnement aanvragen","newheap"); ?>
                                    </a>
                                    <a href="<?php echo get_field('registration_existing_user_page', 'option')?>" class="cta-1 mt-3">
                                        <?php echo __("Bestaande abonnement koppelen","newheap"); ?>
                                    </a>
                                <?php endif; ?>
                            </div>

                            <div class="col-md-6 mb-4">
                                <div class="card">
                                    <div class="card-body">
                                        <p class="head-alt"><strong>Contact</strong><br />Heeft u een vraag over uw abonnement of wilt u uw abonnement opzeggen? Neem dan contact op met de abonnementen-administratie van AgriMedia via:<br /><br />Telefoon: <strong>0317-465 670</strong><br /><br/>E-mailadres: <strong>mail@agrimedia.nl</strong><br /><br/>Of bekijk de <strong>veelgestelde vragen</strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php
            $qandas = get_field('questions_and_answeres', 'option');
            if(!empty($qandas)) {
            ?>
            <div class="row">
                <div class="col-md-12">
                    <h3 class="section-title">Veelgestelde vragen</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <?php foreach($qandas as $qanda) { ?>
                            <div class="question-answer-holder">
                                <div class="question"><strong><?=$qanda['question']?></strong></div>
                                <p class="answer"><?=$qanda['answere']?></p>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <br /><br />

	        <?php

	        ob_end_flush();
        }


    }

    /**
     * Shortcode function
     *
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public function custom_profile_function()
    {
        if (!is_user_logged_in()) {
            Auth::redirect_to_profile_page();
        }

        if (isset($_POST['submit_profile_update'])) {
            $this->first_name = esc_attr($_POST['first_name']);
            $this->last_name = esc_attr($_POST['last_name']);
            $this->subscription_lm = esc_attr($_POST['subscription_lm']);
            $this->subscription_vt = esc_attr($_POST['subscription_vt']);
            $this->subscription_tpt = esc_attr($_POST['subscription_tpt']);
            $this->old_password = esc_attr($_POST['old_password']);
            $this->new_password = esc_attr($_POST['new_password']);
            $this->password_confirmation = esc_attr($_POST['password_confirmation']);

            // Validate form data
            $this->profile_update_validation();

            // If no validation errors update user
            if (empty($this->errors->errors)) {
                User::update_user([
                    'first_name' => $this->first_name,
                    'last_name' => $this->last_name,
                    'new_password' => $this->new_password,
                    'subscription_lm' => $this->subscription_lm,
                    'subscription_vt' => $this->subscription_vt,
                    'subscription_tpt' => $this->subscription_tpt,
                ]);
            }

        }

        $this->profile_form();
    }

    /**
     * Render profile form
     *
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public function profile_form()
    {
        $current_user = wp_get_current_user();

        ob_start();
        ?>

        <h1><?php _e('Profiel bijwerken', 'newheap'); ?></h1>
        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" id="profile-update-form" class="auth-form">
            <?php wp_nonce_field( 'update_profile_action', 'update_profile' ); ?>

            <div class="form-group">
                <input type="text" name="first_name"
                       value="<?php echo $this->first_name ?? get_user_meta($current_user->ID, 'first_name')[0]; ?>"
                       placeholder="<?php _e('Voornaam', 'newheap'); ?>" class="form-control" required>
                <?php self::render_errors('first_name', $this->errors) ;?>
            </div>

            <div class="form-group">
                <input type="text" name="last_name"
                       value="<?php echo $this->last_name ?? get_user_meta($current_user->ID, 'last_name', true); ?>"
                       placeholder="<?php _e('Achternaam', 'newheap'); ?>" class="form-control" required>
                <?php self::render_errors('last_name', $this->errors) ;?>
            </div>

            <hr>

            <h3><?php _e('Abonnementen', 'newheap'); ?></h3>

            <div class="form-group">
                <div class="form-group">
                    <input type="text" name="subscription_lm" value="<?php echo $this->subscription_lm ?? get_user_meta($current_user->ID, 'subscription_lm', true); ?>"
                           placeholder="<?php _e('Abonnementnummer LandbouwMechanisatie', 'newheap'); ?>" class="form-control">
		            <?php self::render_errors('subscription_lm', $this->errors) ;?>
                </div>

                <div class="form-group">
                    <input type="text" name="subscription_vt" value="<?php echo $this->subscription_vt ?? get_user_meta($current_user->ID, 'subscription_vt', true); ?>"
                           placeholder="<?php _e('Abonnementnummer Veehouderij Techniek', 'newheap'); ?>" class="form-control">
		            <?php self::render_errors('subscription_vt', $this->errors) ;?>
                </div>

                <div class="form-group">
                    <input type="text" name="subscription_tpt" value="<?php echo $this->subscription_tpt ?? get_user_meta($current_user->ID, 'subscription_tpt', true); ?>"
                           placeholder="<?php _e('Abonnementnummer Tuin en Park Techniek', 'newheap'); ?>" class="form-control">
		            <?php self::render_errors('subscription_tpt', $this->errors) ;?>
                </div>
            </div>

            <hr>

            <h3><?php _e('Wachtwoord wijzigen', 'newheap'); ?></h3>

            <p><?php _e('Alleen invullen indien u uw wachtwoord wilt wijzigen. Indien u uw wachtwoord wijzigt moet u opnieuw inloggen!', 'newheap') ?></p>

            <div class="form-group">
                <input type="password" name="old_password"
                       value="<?php echo $old_password ?? null; ?>"
                       placeholder="<?php _e('Huidige wachtwoord', 'newheap'); ?>" class="form-control">
                <?php self::render_errors('old_password', $this->errors) ;?>
            </div>

            <div class="form-group">
                <input type="password" name="new_password"
                       value="<?php echo $new_password ?? null; ?>"
                       placeholder="<?php _e('Nieuw wachtwoord', 'newheap'); ?>" class="form-control">
                <?php self::render_errors('new_password', $this->errors) ;?>
            </div>

            <div class="form-group">
                <input type="password" name="password_confirmation"
                       value="<?php echo $password ?? null; ?>"
                       placeholder="<?php _e('Bevestig wachtwoord', 'newheap'); ?>" class="form-control">
                <?php self::render_errors('password_confirmation', $this->errors) ;?>
            </div>

            <input type="submit" name="submit_profile_update" value="<?php _e('Opslaan', 'newheap'); ?>"/>

            <?php if(isset($_GET['login']) && $_GET['login'] === 'failed') : ?>
                <p class="errors">
                    <?php _e('Inloggen mislukt. Controleer uw gebruikersnaam en wachtwoord en probeer het nogmaals.', 'newheap'); ?>
                </p>
            <?php endif; ?>

        </form>

        <?php

        ob_end_flush();
    }

    /**
     * Validate form data
     *
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public function profile_update_validation()
    {
        if (!$this->errors instanceof \WP_Error) {
            $this->errors = new \WP_Error;
        }

        if (empty($this->first_name)) {
            $this->errors->add('first_name', __('Voornaam is een verplicht veld.', 'newheap'));
        }

        if (empty($this->last_name)) {
            $this->errors->add('last_name', __('Achternaam is een verplicht veld.', 'newheap'));
        }

        // Password validations
        if (!empty($this->old_password) || !empty($this->new_password) || !empty($this->password_confirmation)) {
            // Check current password
            $current_user = wp_get_current_user();

            if (!wp_check_password($this->old_password, $current_user->user_pass, $current_user->ID)) {
                $this->errors->add('old_password', __('Uw huidige wachtwoord klopt niet.', 'newheap'));
            }

            // Check if new password is strong
            $contains_letter = preg_match('/[a-zA-Z]/', $this->new_password);
            $contains_digit = preg_match('/\d/', $this->new_password);
            $contains_special = preg_match('/[^a-zA-Z\d]/', $this->new_password);

            if (strlen($this->new_password) < 8 || !$contains_letter || !$contains_digit || !$contains_special) {
                $this->errors->add('new_password', __('Uw nieuwe wachtwoord moet minimaal 8 karakters lang zijn en bestaan uit minimaal &eacute;&eacute;n:<br>- Kleine letter<br>- Hoofdletter<br>- Cijfer<br>- Speciale teken', 'newheap'));
            }

            if ($this->new_password !== $this->password_confirmation) {
                $this->errors->add('password_confirmation', __('Uw wachtwoord bevestiging komt niet overeen met uw nieuwe wachtwoord. Probeer het nogmaals.', 'newheap'));
            }
        }
    }

    /**
     * Render the error for field
     *
     * @param $name
     * @param $errors
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public static function render_errors($name, $errors)
    {
        if (is_wp_error($errors) && isset($errors->errors[$name])) : ?>
            <p class="errors">
                <ul>
                    <?php foreach ($errors->errors[$name] as $error) : ?>
                        <li><?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            </p>
        <?php endif;
    }
}
