<?php
namespace Newheap\Plugins\NewheapAccount\Shortcodes;

use Codio\CodioAuth\Zeno\Zeno;
use Newheap\Logger\Logger;
use Newheap\Plugin\NewheapAccount\Auth\Auth;
use Newheap\Plugins\NewheapAccount\User\User;
use Newheap\Plugins\NewheapAccount\User\UserRegistration;
use NewHeap\Theme\Multisite\Multisite;
use NewHeap\Theme\SharpSpring\SharpSpring;

class Registration
{
    private $errors;

    private $company_name;
    private $gender;
    private $initials;
    private $prefix;
    private $street_name;
    private $house_number;
    private $house_number_addition;
    private $zip;
    private $city;
    private $country;
    private $mobile_phone;
    private $phone;
    private $policy;
    private $newsletter;
    private $invoice_email;
    private $account_number;
    private $account_holder_name;
    private $first_name;
    private $last_name;
    private $user_login;
    private $password;
    private $email;
    private $payment_method;
    private $subscription_lm;
    private $subscription_vt;
    private $subscription_tpt;

    public function __construct()
    {
        add_shortcode('newheap_registration_form', [$this, 'custom_registration_function']);
    }

    /**
     * Shortcode function
     *
     * @return false|string
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public function custom_registration_function()
    {
        ob_start();

	    global $registration_errors;

//	    if (is_user_logged_in()) {
//		    Auth::redirect_to_profile_page();
//	    }

	    if (isset($_POST['submit_registration']) && is_wp_error($registration_errors) && $registration_errors->has_errors() && !UserRegistration::check_if_zeno_user()) {
		    // Log post data
	        \NewHeap\Theme\Logger\Logger::write_zeno_log('### New registration POST data: ' . json_encode($_POST));

		    $this->company_name = esc_attr($_POST['company_name']);
		    $this->gender = esc_attr($_POST['gender']);
		    $this->initials = esc_attr($_POST['initials']);
		    $this->prefix = esc_attr($_POST['prefix']);
		    $this->street_name = esc_attr($_POST['street_name']);
		    $this->house_number = esc_attr($_POST['house_number']);
		    $this->house_number_addition = esc_attr($_POST['house_number_addition']);
		    $this->zip = esc_attr($_POST['zip']);
		    $this->city = esc_attr($_POST['city']);
		    $this->country = esc_attr($_POST['country']);
		    $this->mobile_phone = esc_attr($_POST['mobile_phone']);
		    $this->phone = esc_attr($_POST['phone']);
		    $this->policy = esc_attr($_POST['policy']);
		    $this->newsletter = esc_attr($_POST['newsletter']);
		    $this->invoice_email = esc_attr($_POST['invoice_email']);
		    $this->payment_method = esc_attr($_POST['payment']);
		    $this->account_number = esc_attr($_POST['account_number']);
		    $this->account_holder_name = esc_attr($_POST['account_holder_name']);

		    $this->first_name = esc_attr($_POST['first_name']);
		    $this->last_name = esc_attr($_POST['last_name']);
		    $this->user_login = esc_attr($_POST['user_login']);
		    $this->email = esc_attr($_POST['email']);
		    $this->password = esc_attr($_POST['password']);
		    $this->subscription_lm = esc_attr($_POST['subscription_lm']);
		    $this->subscription_vt = esc_attr($_POST['subscription_vt']);
		    $this->subscription_tpt = esc_attr($_POST['subscription_tpt']);
	    } elseif (is_user_logged_in()) {
		    $type = get_field('registratie_formulier_type');
		    $subscription_number = get_user_meta(get_current_user_id(), 'subscription_' . Multisite::get_site_code(), true);
		    $subscription_expiry_date = get_user_meta(get_current_user_id(), 'subscription_' . Multisite::get_site_code() . '_expires', true);

		    // We can see the subscription type by the expiry date. If the expiry date = 1, it means its never endig. So its not a trial subscription
		    if ($type === 'gratis') {
			    _e('U heeft al een gratis abonnement voor deze site.', 'newheap');
			    return;
		    } elseif (!empty($subscription_number) && $subscription_expiry_date === 1 && $type === 'totaal') {
			    _e('U heeft al een abonnement voor deze site.', 'newheap');
			    return;
		    } else {
		        $this->company_name = get_user_meta(get_current_user_id(), 'company_name', true);
		        $this->gender = get_user_meta(get_current_user_id(), 'gender', true);
		        $this->initials = get_user_meta(get_current_user_id(), 'initials', true);
		        $this->prefix = get_user_meta(get_current_user_id(), 'prefix', true);
		        $this->street_name = get_user_meta(get_current_user_id(), 'street_name', true);
		        $this->house_number = get_user_meta(get_current_user_id(), 'house_number', true);
		        $this->house_number_addition = get_user_meta(get_current_user_id(), 'house_number_addition', true);
		        $this->zip = get_user_meta(get_current_user_id(), 'zip', true);
		        $this->city = get_user_meta(get_current_user_id(), 'city', true);
		        $this->country = get_user_meta(get_current_user_id(), 'country', true);
		        $this->mobile_phone = get_user_meta(get_current_user_id(), 'mobile_phone', true);
		        $this->phone = get_user_meta(get_current_user_id(), 'phone', true);
		        $this->newsletter = get_user_meta(get_current_user_id(), 'newsletter', true);
		        $this->invoice_email = get_user_meta(get_current_user_id(), 'invoice_email', true);
		        $this->account_number = get_user_meta(get_current_user_id(), 'account_number', true);
		        $this->account_holder_name = get_user_meta(get_current_user_id(), 'account_holder_name', true);

		        $this->first_name = get_user_meta(get_current_user_id(), 'first_name', true);
		        $this->last_name = get_user_meta(get_current_user_id(), 'last_name', true);
		        $this->user_login = get_user_meta(get_current_user_id(), 'user_login', true);
		        $this->email = get_user_meta(get_current_user_id(), 'email', true);
            }

        }

        $this->registration_form();

        return ob_get_clean();
    }

    /**
     * Render Registration form
     *
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public function registration_form()
    {
        $privacyPage = get_privacy_policy_url();

        $type = get_field('registratie_formulier_type');
	    $subscription_number = get_user_meta(get_current_user_id(), 'subscription_' . Multisite::get_site_code(), true);
	    $subscription_expiry_date = get_user_meta(get_current_user_id(), 'subscription_' . Multisite::get_site_code() . '_expires', true);

        ob_start();
        ?>

        <?php if (!empty($subscription_number) && $type == 'plus') : ?>
	        <?php _e('U heeft al een proefabonnement voor deze website.', 'newheap'); ?>
        <?php elseif (UserRegistration::check_if_zeno_user()) : ?>
            <p class="mb-5">
                <?php _e('U heeft al een betaalde account voor een ander vakblad. Uw gegevens zijn daardoor reeds bekend.', 'newheap'); ?>
            </p>

            <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" id="registration-form" class="auth-form">
                <?php wp_nonce_field( 'registration_action', 'registration' ); ?>
                <input type="hidden" name="user_type" value="<?php echo $type; ?>">

                <?php if($type !== 'gratis') : ?>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="company_name"><?php _e('Bedrijfsnaam', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
	                            <?php echo $this->company_name ?? null; ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group gender-select">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label><?php _e('Geslacht', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <?php if ($this->gender == 1) : ?>
	                                <?php _e('Man', 'newheap'); ?>
                                <?php elseif ($this->gender == 2) : ?>
	                                <?php _e('Vrouw', 'newheap'); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="initials"><?php _e('Voorletters', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
	                            <?php echo $this->initials; ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="first_name"><?php _e('Voornaam', 'newheap'); ?></label>

                            </div>
                            <div class="col-sm-12 col-md-6">
	                            <?php echo $this->first_name ?? null; ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="prefix"><?php _e('Voorvoegsels', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
	                            <?php echo $this->prefix ?? null; ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="last_name"><?php _e('Achternaam', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
	                            <?php echo $this->last_name ?? null; ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label><?php _e('Adres', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6 mb-3">
	                            <?php echo $this->street_name ?? null; ?>
	                            <?php echo $this->house_number ?? null; ?>
	                            <?php echo $this->house_number_addition ?? null; ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="zip"><?php _e('Postcode', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
	                            <?php echo $this->zip ?? null; ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="city"><?php _e('Plaats', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
	                            <?php echo $this->city ?? null; ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="city"><?php _e('Land', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <?php foreach (Zeno::get_countries_list()->landcode as $country) {
	                                if ( $country->landCode === $this->country ) {
		                                echo $country->landNaam;
	                                }
                                }; ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="mobile_phone"><?php _e('Mobiel nummer', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
	                            <?php echo $this->mobile_phone ?? null; ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="phone"><?php _e('Telefoonnummer', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
	                            <?php echo $this->phone ?? null; ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label><?php _e('Factuur e-mailadres', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
	                            <?php echo $this->invoice_email ?? null; ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="user_login"><?php _e('Gebruikersnaam', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
	                            <?php echo get_currentuserinfo()->display_name ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="email"><?php _e('E-mailadres', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
	                            <?php echo get_currentuserinfo()->user_email; ?>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="form-group payment-options">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label> <?php _e('Betalingswijze', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div>
                                    <label style="font-weight: normal;">
                                        <input type="radio" name="payment" value="0" <?php echo ($this->payment_method === '0' || empty($this->payment_method)) ? 'checked' : ''; ?>>
                                        <span><?php _e('Overschrijven', 'newheap'); ?></span>
                                    </label>
                                </div>

                                <div>
                                    <label style="font-weight: normal;">
                                        <input type="radio" name="payment" value="1" <?php echo ($this->payment_method === '1') ? 'checked' : ''; ?>>
                                        <span><?php _e('Automatische incasso', 'newheap'); ?></span>
                                    </label>
                                </div>

                                <div>
                                    <label style="font-weight: normal;">
                                        <input type="radio" name="payment" value="3" <?php echo ($this->payment_method === '3') ? 'checked' : ''; ?>>
                                        <span><?php _e('iDeal', 'newheap'); ?></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group automatische-incasso d-none">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label><?php _e('Naam rekeningnummer', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" name="account_holder_name" value="<?php echo $this->account_holder_name ?? null; ?>"
                                       placeholder="<?php _e('Naam rekeningnummer', 'newheap'); ?>" class="form-control">
                                <?php self::render_errors('account_holder_name', $this->errors) ;?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group automatische-incasso d-none">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label><?php _e('Rekeningnummer', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" name="account_number" value="<?php echo $this->account_number ?? null; ?>"
                                       placeholder="<?php _e('Rekeningnummer', 'newheap'); ?>" class="form-control">
                                <?php self::render_errors('account_number', $this->errors) ;?>
                            </div>
                        </div>
                    </div>

                    <hr class="d-none">

                    <div class="form-group">
                        <label style="font-weight: normal;">
                            <input type="checkbox" name="policy" value="1" class="form-control" required <?php echo ($this->policy) ? 'checked' : ''; ?>>
                            <span><?php printf(__('Akkoord op <a href="%s" target="_blank">abonnementsvoorwaarden</a>', 'newheap'), get_field('subscription_policy', 'option')); ?></span>
                        </label>
		                <?php self::render_errors('password', $this->errors) ;?>
                    </div>

                    <button class="show-subscriptions d-none" type="button">
                        <?php _e('Registreer met uw vakblad-abonnementnummer(s)', 'newheap'); ?>
                    </button>
                <?php endif; ?>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="hidden" name="submit_registration" value="1">
                            <button class="btn btn-primary" type="submit" name="submit_registration" value="<?php _e('Registreren', 'newheap'); ?>"><?php _e('Registreren', 'newheap'); ?></button>
                        </div>
                    </div>
                </div>
            </form>
        <?php else : ?>
            <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" id="registration-form" class="auth-form">
                <?php wp_nonce_field( 'registration_action', 'registration' ); ?>
                <input type="hidden" name="user_type" value="<?php echo $type; ?>">

                <?php if($type !== 'gratis') : ?>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="company_name"><?php _e('Bedrijfsnaam', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" name="company_name" value="<?php echo $this->company_name ?? null; ?>"
                                       placeholder="<?php _e('Bedrijfsnaam', 'newheap'); ?>" class="form-control">
                                <?php self::render_errors('company_name', $this->errors) ;?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group gender-select">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label><?php _e('Geslacht', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <span>
                            <label style="font-weight: normal;">
                                <input type="radio" name="gender" value="1"
                                    <?php echo ($this->gender === '1' || empty($this->gender)) ? 'checked' : ''; ?>
                                       class="form-control">
                                <span><?php _e('Man', 'newheap'); ?></span>
                            </label>

                            <label style="font-weight: normal;">
                                <input type="radio" name="gender" value="2"
                                    <?php echo ($this->gender === '2') ? 'checked' : ''; ?>
                                       class="form-control">
                                <span><?php _e('Vrouw', 'newheap'); ?></span>
                            </label>
                        </span>
                                <?php self::render_errors('gender', $this->errors) ;?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="initials"><?php _e('Voorletters', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" name="initials" value="<?php echo $this->initials ?? null; ?>"
                                       placeholder="<?php _e('Voorletters', 'newheap'); ?>" class="form-control" required>
                                <?php self::render_errors('initials', $this->errors) ;?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="first_name"><?php _e('Voornaam', 'newheap'); ?></label>

                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" name="first_name" value="<?php echo $this->first_name ?? null; ?>"
                                       placeholder="<?php _e('Voornaam', 'newheap'); ?>" class="form-control" required>
                                <?php self::render_errors('first_name', $this->errors) ;?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="prefix"><?php _e('Voorvoegsels', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" name="prefix" value="<?php echo $this->prefix ?? null; ?>"
                                       placeholder="<?php _e('Voorvoegsels', 'newheap'); ?>" class="form-control">
                                <?php self::render_errors('prefix', $this->errors) ;?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="last_name"><?php _e('Achternaam', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" name="last_name" value="<?php echo $this->last_name ?? null; ?>"
                                       placeholder="<?php _e('Achternaam', 'newheap'); ?>" class="form-control" required>
                                <?php self::render_errors('last_name', $this->errors) ;?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="street_name"><?php _e('Straatnaam', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6 mb-3">
                                <input type="text" name="street_name" value="<?php echo $this->street_name ?? null; ?>"
                                       placeholder="<?php _e('Straatnaam', 'newheap'); ?>" class="form-control" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-6"></div>
                            <div class="col-sm-12 col-md-6">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" name="house_number" value="<?php echo $this->house_number ?? null; ?>"
                                               placeholder="<?php _e('Huisnummer', 'newheap'); ?>" class="form-control" required>
                                    </div>

                                    <div class="col-sm-6">
                                        <input type="text" name="house_number_addition" value="<?php echo $this->house_number_addition ?? null; ?>"
                                               placeholder="<?php _e('Toevoeging', 'newheap'); ?>" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <?php self::render_errors('street_name', $this->errors) ;?>
                            <?php self::render_errors('house_number', $this->errors) ;?>
                            <?php self::render_errors('house_number_addition', $this->errors) ;?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="zip"><?php _e('Postcode', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" name="zip" value="<?php echo $this->zip ?? null; ?>"
                                       placeholder="<?php _e('Postcode', 'newheap'); ?>" class="form-control" required>
                                <?php self::render_errors('zip', $this->errors) ;?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="city"><?php _e('Plaats', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" name="city" value="<?php echo $this->city ?? null; ?>"
                                       placeholder="<?php _e('Plaats', 'newheap'); ?>" class="form-control" required>
                                <?php self::render_errors('city', $this->errors) ;?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="city"><?php _e('Land', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <select name="country">
                                    <option value="NL"><?php _e('Nederland', 'newheap'); ?></option>
                                    <option value="BE"><?php _e('Belgi&euml;', 'newheap'); ?></option>
                                    <?php foreach (Zeno::get_countries_list()->landcode as $country) : ?>
                                        <?php if ($country->landCode === 'NL' || $country->landCode === 'BE') {
                                            continue;
                                        } ?>
                                        <option value="<?php echo $country->landCode; ?>" <?php echo ($this->country === $country->landCode) ? 'selected' : ''; ?>>
                                            <?php echo ucwords(strtolower($country->landNaam)); ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                                <?php self::render_errors('country', $this->errors) ;?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="mobile_phone"><?php _e('Mobiel nummer', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" name="mobile_phone" value="<?php echo $this->mobile_phone ?? null; ?>"
                                       placeholder="<?php _e('Mobiel nummer', 'newheap'); ?>" class="form-control">
                                <?php self::render_errors('mobile_phone', $this->errors) ;?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="phone"><?php _e('Telefoonnummer', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" name="phone" value="<?php echo $this->phone ?? null; ?>"
                                       placeholder="<?php _e('Telefoonnummer', 'newheap'); ?>" class="form-control">
                                <?php self::render_errors('phone', $this->errors) ;?>
                            </div>
                        </div>
                    </div>


                <?php endif; ?>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <label for="user_login"><?php _e('Gebruikersnaam', 'newheap'); ?></label>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <input type="text" name="user_login" value="<?php echo $this->user_login ?? null; ?>"
                                   placeholder="<?php _e('Gebruikersnaam', 'newheap'); ?>" class="form-control" required>
                            <?php self::render_errors('user_login', $this->errors) ;?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <label for="email"><?php _e('E-mailadres', 'newheap'); ?></label>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <input type="email" name="email" value="<?php echo $this->email ?? null; ?>"
                                   placeholder="<?php _e('E-mailadres', 'newheap'); ?>" class="form-control" required>
                            <?php self::render_errors('email', $this->errors) ;?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <label for="password"><?php _e('Wachtwoord', 'newheap'); ?></label>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <input type="password" name="password" value="<?php echo $this->password ?? null; ?>"
                                   placeholder="<?php _e('Wachtwoord', 'newheap'); ?>" class="form-control" required>
                            <?php self::render_errors('password', $this->errors) ;?>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label style="font-weight: normal;">
                        <input type="checkbox" name="policy" value="1" class="form-control" required <?php echo ($this->policy) ? 'checked' : ''; ?>>
                        <span><?php printf(__('Akkoord op <a href="%s" target="_blank">abonnementsvoorwaarden</a>', 'newheap'), get_field('subscription_policy', 'option')); ?></span>
                    </label>
                    <?php self::render_errors('password', $this->errors) ;?>
                </div>

                <div class="form-group">
                    <label style="font-weight: normal;">
                        <input type="checkbox" name="newsletter" value="1" class="form-control" <?php echo ($this->newsletter) ? 'checked' : ''; ?>>
                        <span><?php _e('Aanmelden voor de nieuwsbrief', 'newheap'); ?></span>
                    </label>
                    <?php self::render_errors('newsletter', $this->errors) ;?>
                </div>

                <?php if($type !== 'gratis') : ?>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="invoice_email"><?php _e('Factuur e-mailadres', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input type="email" name="invoice_email" value="<?php echo $this->invoice_email ?? null; ?>"
                                       placeholder="<?php _e('Factuur e-mailadres', 'newheap'); ?>" class="form-control">
                                <?php self::render_errors('invoice_email', $this->errors) ;?>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="form-group payment-options">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="invoice_email"> <?php _e('Betalingswijze', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div>
                                    <label style="font-weight: normal;">
                                        <input type="radio" name="payment" value="0" <?php echo ($this->payment_method === '0' || empty($this->payment_method)) ? 'checked' : ''; ?>>
                                        <span><?php _e('Overschrijven', 'newheap'); ?></span>
                                    </label>
                                </div>

                                <div>
                                    <label style="font-weight: normal;">
                                        <input type="radio" name="payment" value="1" <?php echo ($this->payment_method === '1') ? 'checked' : ''; ?>>
                                        <span><?php _e('Automatische incasso', 'newheap'); ?></span>
                                    </label>
                                </div>

                                <div>
                                    <label style="font-weight: normal;">
                                        <input type="radio" name="payment" value="3" <?php echo ($this->payment_method === '3') ? 'checked' : ''; ?>>
                                        <span><?php _e('iDeal', 'newheap'); ?></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group automatische-incasso d-none">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="account_holder_name"><?php _e('Naam rekeningnummer', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" name="account_holder_name" value="<?php echo $this->account_holder_name ?? null; ?>"
                                       placeholder="<?php _e('Naam rekeningnummer', 'newheap'); ?>" class="form-control">
                                <?php self::render_errors('account_holder_name', $this->errors) ;?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group automatische-incasso d-none">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <label for="account_number"><?php _e('Rekeningnummer', 'newheap'); ?></label>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" name="account_number" value="<?php echo $this->account_number ?? null; ?>"
                                       placeholder="<?php _e('Rekeningnummer', 'newheap'); ?>" class="form-control">
                                <?php self::render_errors('account_number', $this->errors) ;?>
                            </div>
                        </div>
                    </div>

                    <hr class="d-none">

                    <button class="show-subscriptions d-none" type="button">
                        <?php _e('Registreer met uw vakblad-abonnementnummer(s)', 'newheap'); ?>
                    </button>

                    <div class="subscription-fields d-none">
                        <div class="form-group">
                            <input type="text" name="subscription_lm" value="<?php echo $this->subscription_lm ?? null; ?>"
                                   placeholder="<?php _e('Abonnementnummer LandbouwMechanisatie', 'newheap'); ?>" class="form-control">
                            <?php self::render_errors('subscription_lm', $this->errors) ;?>
                        </div>

                        <div class="form-group">
                            <input type="text" name="subscription_vt" value="<?php echo $this->subscription_vt ?? null; ?>"
                                   placeholder="<?php _e('Abonnementnummer Veehouderij Techniek', 'newheap'); ?>" class="form-control">
                            <?php self::render_errors('subscription_vt', $this->errors) ;?>
                        </div>

                        <div class="form-group">
                            <input type="text" name="subscription_tpt" value="<?php echo $this->subscription_tpt ?? null; ?>"
                                   placeholder="<?php _e('Abonnementnummer Tuin en Park Techniek', 'newheap'); ?>" class="form-control">
                            <?php self::render_errors('subscription_tpt', $this->errors) ;?>
                        </div>

                        <p><?php _e('U kunt deze ook later invoeren.', 'newheap'); ?></p>
                    </div>

                    <hr>
                <?php endif; ?>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="hidden" name="submit_registration" value="1">
                            <button class="btn btn-primary" type="submit" name="submit_registration" value="<?php _e('Registreren', 'newheap'); ?>"><?php _e('Registreren', 'newheap'); ?></button>
                        </div>
                    </div>
                </div>
            </form>
        <?php endif; ?>

        <?php
	    echo SharpSpring::sharpspring_tracking_script($type);
	    
        ob_end_flush();
    }

    /**
     * Render the error for field
     *
     * @param $name
     * @param $errors
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public static function render_errors($name, $errors)
    {
        global $registration_errors;

        if (is_wp_error($registration_errors) && isset($registration_errors->errors[$name])) : ?>
            <ul class="errors">
                <?php foreach ($registration_errors->errors[$name] as $error) : ?>
                    <li><?php echo $error; ?></li>
                <?php endforeach; ?>
            </ul>
        <?php endif;
    }
}
