<?php
namespace Newheap\Plugins\NewheapAccount\Shortcodes;

use Newheap\Plugin\NewheapAccount\Auth\Auth;

class Login
{
    public function __construct()
    {
        add_shortcode('newheap_login_form', [$this, 'custom_login_function']);
    }

    /**
     * Shortcode function
     *
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public function custom_login_function()
    {
        if (!is_user_logged_in()) {
            $this->login_form();
        } else {
            Auth::redirect_to_profile_page();
        }
    }

    /**
     * Render Registration form
     *
     * @param null $username
     * @param null $password
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public function login_form($username = null, $password = null)
    {
        ob_start();
        ?>

        <form action="<?php echo site_url( '/wp-login.php' ); ?>" method="post" id="login-form" class="auth-form mb-5">
            <input type="hidden" value="<?php echo esc_attr( get_field('profile_page', 'option') ); ?>" name="redirect_to">

            <div class="form-group">
                <input type="text" name="log" value="<?php echo ($username) ? $username : null; ?>"
                       placeholder="<?php _e('Gebruikersnaam/E-mailadres', 'newheap'); ?>" class="form-control" required>
            </div>

            <div class="form-group">
                <input type="password" name="pwd"
                       value="<?php echo ($password) ? $password : null; ?>"
                       placeholder="<?php _e('Wachtwoord', 'newheap'); ?>" class="form-control" required>
            </div>

            <div class="form-group">
                <label>
                    <input name="rememberme" type="checkbox" id="rememberme" value="forever">
                    <?php _e('Onthoud mij', 'newheap'); ?>
                </label>
            </div>

	        <?php if(isset($_GET['login']) && $_GET['login'] === 'failed') : ?>
                <p class="errors">
			        <?php _e('Inloggen mislukt. Controleer uw gebruikersnaam en wachtwoord en probeer het nogmaals.', 'newheap'); ?>
                </p>
	        <?php endif; ?>

            <input type="submit" class="cta-2" name="submit_login" value="<?php _e('Inloggen', 'newheap'); ?>"/>

            <a href="<?php echo wp_lostpassword_url(); ?>">
                <?php _e('Wachtwoord vergeten?', 'newheap'); ?>
            </a>

        </form>

        <?php

        ob_end_flush();
    }
}
