<?php
namespace Newheap\Plugins\NewheapAccount\Shortcodes;

use Codio\CodioAuth\Zeno\Zeno;
use Newheap\Logger\Logger;
use Newheap\Plugins\NewheapAccount\User\User;
use Newheap\Plugins\NewheapAccount\User\UserRegistration;
use NewHeap\Theme\Multisite\Multisite;
use NewHeap\Theme\Notification\Notification;
use NewHeap\Theme\SharpSpring\SharpSpring;

class RegistrationExistingUser
{
    private $errors;

    private $policy;
    private $user_login;
    private $password;
    private $email;
    private $subscription_number;
    private $relation_number;
    private $user_id;
    private $user_details;

    public function __construct()
    {
        add_action('init', [$this, 'validate_registration'], 1);
        add_action('init', [$this, 'create_user'], 2);
        add_shortcode('newheap_registration_form_existing', [$this, 'zeno_user_registration_existing']);
    }

	/**
     * Shortcode to show the registration form
     *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
    public function zeno_user_registration_existing()
    {
	    ob_start();
        if (is_user_logged_in()) : $user = wp_get_current_user(); ?>
            <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" id="registration-form" class="auth-form">
		        <?php wp_nonce_field( 'registration_action', 'registration' ); ?>
                <input type="hidden" name="site_code" value="<?php echo Multisite::get_site_code(); ?>">

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <label for="user_login"><?php _e('Gebruikersnaam', 'newheap'); ?></label>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <span><?php echo $user->user_login; ?></span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <label for="email"><?php _e('E-mailadres', 'newheap'); ?></label>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <span><?php echo $user->user_email; ?></span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <label for="subscription_number"><?php _e('Abonneenummer', 'newheap'); ?></label>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <input type="text" name="subscription_number" value="<?php echo $this->subscription_number ?? null; ?>"
                                   placeholder="<?php _e('Abonneenummer', 'newheap'); ?>" class="form-control" required>
					        <?php $this->render_errors('subscription_number', $this->errors) ;?>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="form-group">
                    <label style="font-weight: normal;">
                        <input type="checkbox" name="policy" value="1" class="form-control" required <?php echo ($this->policy) ? 'checked' : ''; ?>>
                        <span><?php printf(__('Akkoord op <a href="%s" target="_blank">abonnementsvoorwaarden</a>', 'newheap'), get_field('subscription_policy', 'option')); ?></span>
                    </label>
			        <?php $this->render_errors('password', $this->errors) ;?>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="hidden" name="submit_registration_existing" value="1">
                            <button class="cta-1" type="submit" name="submit_registration" value="<?php _e('Registreren', 'newheap'); ?>"><?php _e('Registreren', 'newheap'); ?></button>
                        </div>
                    </div>
                </div>
            </form>
        <?php else : ?>
            <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" id="registration-form" class="auth-form">
		        <?php wp_nonce_field( 'registration_action', 'registration' ); ?>
                <input type="hidden" name="site_code" value="<?php echo Multisite::get_site_code(); ?>">

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <label for="user_login"><?php _e('Gebruikersnaam', 'newheap'); ?></label>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <input type="text" name="user_login" value="<?php echo $this->user_login ?? null; ?>"
                                   placeholder="<?php _e('Gebruikersnaam', 'newheap'); ?>" class="form-control" required>
					        <?php $this->render_errors('user_login', $this->errors) ;?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <label for="email"><?php _e('E-mailadres', 'newheap'); ?></label>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <input type="email" name="email" value="<?php echo $this->email ?? null; ?>"
                                   placeholder="<?php _e('E-mailadres', 'newheap'); ?>" class="form-control" required>
					        <?php $this->render_errors('email', $this->errors) ;?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <label for="password"><?php _e('Wachtwoord', 'newheap'); ?></label>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <input type="password" name="password" value="<?php echo $this->password ?? null; ?>"
                                   placeholder="<?php _e('Wachtwoord', 'newheap'); ?>" class="form-control" required>
					        <?php $this->render_errors('password', $this->errors) ;?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <label for="subscription_number"><?php _e('Abonneenummer', 'newheap'); ?></label>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <input type="text" name="subscription_number" value="<?php echo $this->subscription_number ?? null; ?>"
                                   placeholder="<?php _e('Abonneenummer', 'newheap'); ?>" class="form-control" required>
					        <?php $this->render_errors('subscription_number', $this->errors) ;?>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="form-group">
                    <label style="font-weight: normal;">
                        <input type="checkbox" name="policy" value="1" class="form-control" required <?php echo ($this->policy) ? 'checked' : ''; ?>>
                        <span><?php printf(__('Akkoord op <a href="%s" target="_blank">abonnementsvoorwaarden</a>', 'newheap'), get_field('subscription_policy', 'option')); ?></span>
                    </label>
			        <?php $this->render_errors('password', $this->errors) ;?>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="hidden" name="submit_registration_existing" value="1">
                            <button class="cta-1" type="submit" name="submit_registration" value="<?php _e('Registreren', 'newheap'); ?>"><?php _e('Registreren', 'newheap'); ?></button>
                        </div>
                    </div>
                </div>
            </form>
        <?php endif;

	    echo SharpSpring::sharpspring_tracking_script('existing_user');

	    $output = ob_get_contents();
	    ob_end_clean();
	    return $output;
	    

    }

	/**
     * Validate the post data
     *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
    public function validate_registration()
    {
        if (empty($_POST['submit_registration_existing'])) {
            return;
        }

        // Get the post data and sanitize it
	    $this->user_login = esc_attr($_POST['user_login']);
	    $this->subscription_number = esc_attr($_POST['subscription_number']);
	    $this->email = esc_attr($_POST['email']);
	    $this->password = esc_attr($_POST['password']);

        if (!$this->errors instanceof \WP_Error) {
            $this->errors = new \WP_Error();
        }

        // Validation for not logged in users
        if (!is_user_logged_in()) {
	        // Check user_login
	        if (empty($this->user_login)) {
		        $this->errors->add('user_login', __('Gebruikersnaam is verplicht.', 'newheap'));
	        } elseif (username_exists($this->user_login)) {
		        $this->errors->add('user_login', __('Gebruikersnaam bestaat al. <a href="' . get_field('profile_page', 'option') . '">Klik hier om in te loggen</a>.', 'newheap'));
	        }

	        // Check email
	        if (empty($this->email)) {
		        $this->errors->add('email', __('E-mailadres is verplicht.', 'newheap'));
	        } elseif (email_exists($this->email)) {
		        $this->errors->add('email', __('Met dit e-mailadres is al geregistreerd. <a href="' . get_field('profile_page', 'option') . '">Klik hier om in te loggen</a>.', 'newheap'));
	        }
        }

        // Check subscription_number
        if (empty($this->subscription_number)) {
            $this->errors->add('subscription_number', __('Abonneenummer is verplicht.', 'newheap'));
        } elseif (strlen($this->subscription_number) < 3) {
	        $this->errors->add('subscription_number', __('Vul een geldig abonneenummer in.', 'newheap'));
        }

        $zeno = new Zeno();
	    $this->user_details = $zeno->get_user_details($this->subscription_number);
    }

	/**
     * Create the user
     *
	 * @author Ferhat Ileri <ferhat@codio.nl>
	 */
    public function create_user()
    {
        if ($this->errors instanceof \WP_Error && $this->errors->has_errors()) {
            return;
        }

        $zeno = new Zeno();

        if (!empty($this->subscription_number)) {
            // Log post data
            if (!empty($_POST)) {
	            \NewHeap\Theme\Logger\Logger::write_zeno_log('### New registration existing user POST data: ' . json_encode($_POST));
            }

            if ($this->user_details === 'no_subcription_found') {
	            $this->errors->add('subscription_number', __('Vul een geldig abonneenummer in.', 'newheap'));
            } else {
                // Check for relation type (Bedrijf, ContactPersoon or Persoon)
                if (property_exists($this->user_details, 'getBedrijfResponse')) {
                    $user_details = $this->user_details->getBedrijfResponse->bedrijf->bedrijf;

                    $relation_number = $user_details->relatienummer;
	                $first_name = $user_details->naam;
	                $street_name = $user_details->adresCorrespondentie->straatnaam . ' ' .  $user_details->adresCorrespondentie->huisnummer . ' ' . $user_details->adresCorrespondentie->huisnummerToevoeging;
	                $zip = $user_details->adresCorrespondentie->postcode;
	                $city = $user_details->adresCorrespondentie->plaatsnaam;
	                $country = $user_details->landCode;
                } elseif (property_exists($this->user_details, 'getPersoonResponse')) {
	                $user_details = $this->user_details->getPersoonResponse->persoon->persoon;

	                $relation_number = $user_details->relatienummer;
	                $first_name = $user_details->voornaam;
	                $last_name = $user_details->achternaam;
	                $gender = $user_details->geslachtsCode;
	                $initials = $user_details->voorletters;
	                $prefix = $user_details->voorvoegsels;
	                $country = $user_details->landCode;

	                if (!empty($user_details->adresPrive->postcode)) {
		                $street_name = $user_details->adresPrive->straatnaam . ' ' .  $user_details->adresPrive->huisnummer . ' ' . $user_details->adresPrive->huisnummerToevoeging;
		                $zip = str_replace(' ', '', $user_details->adresPrive->postcode);
		                $city = $user_details->adresPrive->plaatsnaam;
	                } elseif (!empty($user_details->adresBezoek->postcode)) {
		                $street_name = $user_details->adresBezoek->straatnaam . ' ' .  $user_details->adresBezoek->huisnummer . ' ' . $user_details->adresBezoek->huisnummerToevoeging;
		                $zip = str_replace(' ', '', $user_details->adresBezoek->postcode);
		                $city = $user_details->adresBezoek->plaatsnaam;
	                }
                }

                if (empty($relation_number)) {
	                $this->errors->add('subscription_number', __('Kon geen abonnement data vinden. Probeer het nogmaals of neem contact met ons op.', 'newheap'));
                }


                // Check if user is already authenticated
                if (is_user_logged_in()) {
                    $user_id = get_current_user_id();
                } else {
	                $userdata = array(
		                'user_login' => sanitize_user($_POST['user_login']),
		                'user_email' => sanitize_email($_POST['email']),
		                'user_pass' => $_POST['password'],
		                'role' => 'subscriber',
	                );


	                $user_id = wp_insert_user($userdata);
                }


	            if ($user_id instanceof \WP_Error) {
		            if (!$this->errors instanceof \WP_Error) {
			            $this->errors = $user_id;
		            } else {
			            $this->errors->add('existing_user_login', __('Deze gebruikersnaam bestaat al!', 'newhap'));
		            }

		            return;
	            }

	            // Login user
	            wp_clear_auth_cookie();
	            wp_set_current_user($user_id);
	            wp_set_auth_cookie($user_id);

	            if (is_int($user_id)) {
		            update_user_meta($user_id, 'relation_number', $relation_number);
                    update_user_meta($user_id, 'first_name', $first_name);
                    update_user_meta($user_id, 'last_name', $last_name);
                    update_user_meta($user_id, 'gender', $gender);
                    update_user_meta($user_id, 'initials', $initials);
		            update_user_meta($user_id, 'prefix', $prefix);
		            update_user_meta($user_id, 'street_name', $street_name);
		            update_user_meta($user_id, 'zip', $zip);
		            update_user_meta($user_id, 'city', $city);
		            update_user_meta($user_id, 'country', $country);

		            wp_redirect(get_field('registration_finished_page', 'option'));
                    exit;

	            } elseif ($user_id instanceof \WP_Error) {
		            foreach ($user_id->errors as $error) {
		                \NewHeap\Theme\Logger\Logger::write_log('Cant create user with aboNo: ' . $this->subscription_number);
		                \NewHeap\Theme\Logger\Logger::write_log('Cant create user. Error: ' . json_encode($error));
		            }
	            } else {
		            \NewHeap\Theme\Logger\Logger::write_log('Cant create user with aboNo: ' . $this->subscription_number);
	            }
            }
        }

    }

    /**
     * Render the error for field
     *
     * @param $name
     * @param $errors
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public function render_errors($name, $errors)
    {
        if (is_wp_error($this->errors) && isset($this->errors->errors[$name])) : ?>
            <ul class="errors">
                <?php foreach ($this->errors->errors[$name] as $error) : ?>
                    <li><?php echo $error; ?></li>
                <?php endforeach; ?>
            </ul>
        <?php endif;
    }
}
