<?php
namespace Newheap\Plugins\NewheapAccount;

use Newheap\Plugin\NewheapAccount\Auth\Auth;
use Newheap\Plugins\NewheapAccount\Shortcodes\Login;
use Newheap\Plugins\NewheapAccount\Shortcodes\Profile;
use Newheap\Plugins\NewheapAccount\Shortcodes\Registration;
use Newheap\Plugins\NewheapAccount\Shortcodes\RegistrationExistingUser;
use Newheap\Plugins\NewheapAccount\Shortcodes\ResetPassword;
use Newheap\Plugins\NewheapAccount\User\User;
use Newheap\Plugins\NewheapAccount\User\UserRegistration;

require_once 'Auth/Auth.php';
require_once 'Shortcodes/Login.php';
require_once 'Shortcodes/Profile.php';
require_once 'Shortcodes/Registration.php';
require_once 'Shortcodes/RegistrationExistingUser.php';
require_once 'Shortcodes/ResetPassword.php';
require_once 'User/User.php';
require_once 'User/UserRegistration.php';
require_once 'Zeno/Zeno.php';

class NewheapAccount
{
    public function __construct()
    {
        // Start session if not started yet
	    if (session_status() === PHP_SESSION_NONE && !headers_sent()) {
		    session_start();
        }

        new Auth();
        new Login();
        new Profile();
        new Registration();
        new RegistrationExistingUser();
        new ResetPassword();
        new User();
        new UserRegistration();

        $this->init_hooks();
    }

    public function init_hooks()
    {
        add_action('wp_enqueue_scripts', [$this, 'enqueue_styles_scripts']);
        add_action('init', [$this, 'redirect_subscriber']);
	    add_action('set_current_user', [$this, 'hide_admin_bar']);
    }

    /**
     * @return string
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public static function get_plugin_path()
    {
        return plugin_dir_path(__FILE__);
    }

    /**
     * @return string
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public static function get_plugin_url()
    {
        return plugin_dir_url(__FILE__);
    }

    public function enqueue_styles_scripts()
    {
        wp_enqueue_script( 'newheap-account-script', self::get_plugin_url() . 'assets/js/scripts.js', ['jquery'], false, true);
    }

    public function redirect_subscriber()
    {
    	if (is_admin() && !current_user_can('edit_posts') && !wp_doing_ajax()) {
    		$profile_url = get_field('profile_page', 'option');
		    wp_redirect($profile_url);
	    }
    }

	public function hide_admin_bar() {
		if (!current_user_can('editor')) {
			show_admin_bar(false);
		}
	}

	/**
	 * Get the login page
	 *
	 * @return mixed
	 * @author Ferhat Ileri <ferhat@newheap.nl>
	 */
    public static function get_login_page()
    {
    	return get_field('login_page', 'option');
    }

	/**
	 * Get the profile page
	 *
	 * @return mixed
	 * @author Ferhat Ileri <ferhat@newheap.nl>
	 */
    public static function get_profile_page()
    {
    	return get_field('profile_page', 'option');
    }
}
