<?php
namespace Newheap\Plugin\NewheapAccount\Auth;

use Newheap\Helpers;

class Auth
{
    public function __construct()
    {
        add_action('wp_login_failed', [$this, 'failed_login_attempt']);
        add_action('wp_logout', [$this, 'redirect_after_logout']);
        add_filter( 'logout_redirect', function() {
            return esc_url(get_field('logout_page', 'option') );
        } );
    }

    /**
     * Redirect to back after a failed login attempt
     *
     * @param $username
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public function failed_login_attempt($username) {
    	if (!REST_REQUEST) {
		    $full_referrer = explode('?',$_SERVER['HTTP_REFERER']);
		    $referrer = $full_referrer[0];

		    // If there's a valid referrer, and it's not the default log-in screen
		    if (!empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
			    wp_redirect( $referrer . '?login=failed' );
			    exit;
		    } else {
			    wp_redirect(get_field('profile_page', 'option'));
			    exit;
		    }
	    }
    }

    /**
     * After logout redirect to profile page
     *
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public function redirect_after_logout(){
        self::redirect_to_profile_page();
    }

    /**
     * Redirect to profile page
     *
     * @author Ferhat Ileri <ferhat@newheap.com>
     */
    public static function redirect_to_profile_page()
    {
    	if (is_admin()) {
    		return;
	    }

        global $wp;
        $profile_page = get_field('profile_page', 'option');

	    if (headers_sent()) {
		    if (\NewHeap\Theme\Helpers::get_current_url() === $profile_page && !is_user_logged_in()) {
		    	$url = get_field('login_page', 'option') ?? site_url();
			    echo '<script type="text/javascript">window.location = "' . $url . '"</script>';
		    } else {
		    	$url = get_field('profile_page', 'option') ?? site_url();
			    echo '<script type="text/javascript">window.location = "' . $url . '"</script>';
		    }
	    } else {
		    if (\NewHeap\Theme\Helpers::get_current_url() === $profile_page && !is_user_logged_in()) {
			    wp_redirect(get_field('login_page', 'option') ?? site_url());
			    exit;
		    }

		    wp_redirect(get_field('profile_page', 'option'));
	    }
    }
}
